$(document).ready(function() {
    //Recuperamos la medida de la pantalla
    // var alto = $( window ).height();
    // var ancho = $( window ).width();
    var alto = screen.height;
    var ancho = screen.width;

    //Verificamos el tamaño de la pantalla
    if (ancho < 501) {
        //Calculamos las medidad de la pantalla
        var nvo_altura = alto * 0.20;
        var nvo_ancho = ancho * 0.80;
        console.log("Movil");
    }else if ((ancho > 499) && (ancho < 1030)) {
        //Calculamos las medidad de la pantalla
        var nvo_altura = alto * 0.22;
        var nvo_ancho = ancho * 0.50;
        console.log("Tablet");
    }else {
        //Calculamos las medidad de la pantalla
        var nvo_altura = alto * 0.22;
        var nvo_ancho = ancho * 0.33;
        console.log("PC");
    }

    // Asignamos los valores al recuadro de la firma
    $("#canvas").prop("width",nvo_ancho.toFixed(2));
    $("#canvas").prop("height","200");

    //Lienzo para firma general
    var signaturePad = new SignaturePad(document.getElementById('canvas'));

    $("#btnSign").on('click', function(){
        //Recuperamos la ruta de la imagen
        var data = signaturePad.toDataURL('image/png');
        //Comprobamos a donde se enviara
        var firma = $('#firma_actual').val();
        var ruta = "";
        var tipo = "";
        // console.log(destino);
        
        // Comprobamos el tipo de usuario que firmara
        if (firma == "firma_gerente") {
            $("#firma_gerente_img").attr("src",data);
            $('#ruta_firma_gerente').val(data);
            tipo = "1";

        }else if (firma == "firma_logistica_1") {
            $("#firma_logistica_1_img").attr("src",data);
            $('#ruta_firma_logistica_1').val(data);
            tipo = "2";

        } else if (firma == "firma_asesor") {
            $("#firma_asesor_img").attr("src",data);
            $('#ruta_firma_asesor').val(data);
            tipo = "3";

        } else if (firma == "firma_previas") {
            $("#firma_previas_img").attr("src",data);
            $('#ruta_firma_previas').val(data);
            tipo = "4";

        } else if (firma == "firma_gerente_general") {
            $("#firma_gerente_general_img").attr("src",data);
            $('#ruta_firma_gerente_general').val(data);
            tipo = "5";

        } else if (firma == "firma_logistica_2") {
            $("#firma_logistica_2_img").attr("src",data);
            $('#ruta_firma_logistica_2').val(data);
            tipo = "6";
        }

        guardar_firma(data,tipo);

        signaturePad.clear();
    });

    $('#limpiar').on('click', function(){
        signaturePad.clear();
    });

    $('#cerrarCuadroFirma').on('click', function(){
        signaturePad.clear();
    });
});

$(".cuadroFirma").on('click',function() {
    var firma = $(this).data("value");
    $('#firma_actual').val(firma);
    // Comprobamos el tipo de usuario que firmara
    if (firma == "firma_gerente") {
        //Cambiamos el encabezado del modal
        $("#firmaDigitalLabel").text("FIRMA DE GERENTE DE EXPERIENCIA");
    }else if (firma == "firma_logistica_1") {
        //Cambiamos el encabezado del modal
        $("#firmaDigitalLabel").text("FIRMA DE LOGÍSTICA");
    } else if (firma == "firma_asesor") {
        //Cambiamos el encabezado del modal
        $("#firmaDigitalLabel").text("FIRMA DE GERENTE DE EXPERIENCIA Y ASESOR");
    } else if (firma == "firma_previas") {
        //Cambiamos el encabezado del modal
        $("#firmaDigitalLabel").text("FIRMA DE ENCARGADO DE PREVIAS");
    } else if (firma == "firma_gerente_general") {
        //Cambiamos el encabezado del modal
        $("#firmaDigitalLabel").text("FIRMA DEL GERENTE GENERAL");
    } else if (firma == "firma_logistica_2") {
        //Cambiamos el encabezado del modal
        $("#firmaDigitalLabel").text("FIRMA DE LOGÍSTICA");
    } 
});


//Enviamos la imagen para generarla y guardarla
function guardar_firma(base_64,tipo) {
    // Usando jQuery AJAX
    $.ajax({
        url: base_url+"autos/Checklist/firmas",
        method: 'post',
        data: {
            base_64: base_64,
            destino: "firmas",
        },
        success:function(resp){
            console.log("url devuelto: "+resp);

            if (resp.indexOf("handler           </p>")<1) {
                if (tipo == "1") {
                    $("#url_firma_gerente").val(resp);
                }else if (tipo == "2") {
                    $("#url_firma_logistica_1").val(resp);
                }else if (tipo == "3") {
                    $("#url_firma_asesor").val(resp);
                }else if (tipo == "4") {
                    $("#url_firma_previas").val(resp);
                }else if (tipo == "4") {
                    $("#url_firma_previas").val(resp);
                }else if (tipo == "5") {
                    $("#url_firma_gerente_general").val(resp);
                }else if (tipo == "6") {
                    $("#url_firma_logistica_2").val(resp);
                }
            }

        //Cierre de success
        },
        error:function(error){
            console.log(error);
        //Cierre del error
        }
    //Cierre del ajax
    });
    
}
