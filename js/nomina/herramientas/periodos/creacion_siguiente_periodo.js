var _appsFunction = function () {
    this.init = function () {},

    this.cancelar = function(){
        window.location.href = PATH+'/nomina/herramientas/periodos';
    },
   
    this.guardar = function(){
        $.ajax({
            dataType: "json",
            type: 'post',
            url: PATH + 'nomina/herramientas/periodos/index_guardar',
            data: {
                'id_periodo': id_periodo,
                'id_periodicidad': id_periodicidad,
                'fecha_inicio': $('input[name=fecha_inicio]').val(),
                'fecha_fin': $('input[name=fecha_fin]').val(),
            },
            success: function (response, status, xhr) {
                Swal.fire({
                    icon: 'success',
                    title: response.message,
                    text: '¿Deseas cambiarte al siguiente periodo?',
                    allowOutsideClick: false,
                    

                    showDenyButton: true,
                    showCancelButton: true,
                    confirmButtonText: "Sí",
                    cancelButtonText: `No`,
                }).then((result) => {
                    if (result.value == true) {
                        Apps.seleccionar_periodo( response.data.id );
                    } else {
                        Apps.seleccionar_periodo( id_periodo );
                    }

                });
                
            }
        });
    },

    this.seleccionar_periodo = function(id_periodo_a){
        $.ajax({
            dataType: "json",
            type: 'post',
            url: PATH + 'nomina/herramientas/periodos/guarda_periodo_actual',
            data: {
                'periodo': id_periodo_a
            },
            success: function (response, status, xhr) {
                window.location.href = PATH+'/nomina/inicio/trabajador';
            }
        });
    }

}



var Apps;
$(function () {
	Apps = new _appsFunction();
    
});
