var _appsFunction = function () {
    this.init = function () {},
    this.tipo_periodicidad = 0,
    this.fecha_inicio = null,
    this.fecha_fin = null,

    this.addContenido = function(){
        var table = $('table#listado').DataTable();
        table.ajax.reload(null, false);
        return;
        $('div#exampleModal').modal('show');
    },

    
	this.deleteContenido = function ($this) {
		Swal.fire({
			icon: 'warning',
			title: '¿Esta seguro de eliminar el registro?',
			//text: response.info,
			showCancelButton: true,
			confirmButtonText: "Si",
			cancelButtonText: "No"
		}).then((result) => {
			if (result.value) {

				$.ajax({
					dataType: "json",
					type: 'post',
					url: PATH + '/nomina/herramientas/periodos/modal_nuevo_periodo_delete',
					data: {
						'id': $($this).attr('data-id')
					},
					success: function (response, status, xhr) {
						Swal.fire({
							icon: 'success',
							title: '',
							text: response.message,
							confirmButtonText: "Aceptar"
						}).then((result) => {
							var table = $('table#listado').DataTable();
							table.ajax.reload(null, false);
						});
					}

				});
			}
		});
	},

    this.recalcular = function(){
        $.ajax({
            dataType: "json",
            type: 'post',
            url: PATH + 'nomina/herramientas/periodos/index_recalcular_get',
            data: {
                'periodo': $('select#periodicidad option:selected').val()
            },
            success: function (response, status, xhr) {

                var fecha = moment(response.data.FechaInicio);
                if(fecha.isValid()){
                    $('input#fecha_inicio').val(fecha.format('DD-MM-YYYY'));
                }

                var fecha = moment(response.data.FechaFin);
                if(fecha.isValid()){
                    $('input#fecha_fin').val(fecha.format('DD-MM-YYYY'));
                }

            }
        });
    },  

    this.guardar = function(){
        $.ajax({
            dataType: "json",
            type: 'post',
            url: PATH + 'nomina/herramientas/periodos/index_guardar',
            data: {
                'periodo': $('select#periodicidad option:selected').val()
            },
            success: function (response, status, xhr) {
                $('div#exampleModal').modal('hide');
                Swal.fire({
                    icon: 'success',
                    title: '',
                    text: response.message,
                    confirmButtonText: "Aceptar"
                }).then((result) => {
                    var table = $('table#listado').DataTable();
                    table.ajax.reload(null, false);
                });

            }
        });
    },  

	this.get = function () {
		$('table#listado').dataTable({
			colReorder: true,
			fixedHeader: {
				header: true,
			},
			autoWidth: false,
			stateSave: false,
			order: [[0, 'desc']],
			ajax: {
				url: PATH + '/nomina/herramientas/periodos/index_get',
                type: "POST",
                // data: function ( d ) {
                //     return { 'id_Periodicidad': $('select[name=Periodicidad] option:selected').val() };
                // }
			},
			columns: [
				{
					title: 'Periodo',
                    data: 'FechaFin',
                    render: function ( data, type, row, meta ) {
                        var fecha = moment(data);
                        if(fecha.isValid()){
                            return ((type === 'sort')? data : fecha.format('DD/MM/YYYY'));
                        }
                        return data;

                    }
                },
                {
                    title: 'Estado',
                    data: 'Procesado',
                    render: function ( data, type, row, meta ) {
                        if(row.Procesado == '1'){
                            return 'Concluido'
                        // }else{
                        //     var actual = moment();
                        //     var fecha = moment(row.FechaInicio);
                        //     if(fecha.isValid()){
                        //         if(actual.diff(fecha) > 0){
                        //             return 'En curso';
                        //         }
                        //     }
                        }
                        return 'Abierto';
                    }
				},
                {
					title: 'Número de nómina',
                    data: 'Clave'
                },
                {
					title: 'Periodicidad',
                    data: 'Descripcion_Periodicidad'
                },
                {
					title: 'Tipo de nómina',
                    defaultContent: 'N'
                },
                {
					title: "CFDI's no generados",
                    defaultContent: '-'
                },
                {
					title: "CFDI's en proceso de timbrado",
                    defaultContent: '-'
                },
                
				// {
				// 	title: 'Inicio de inicio',
                //     data: 'FechaInicio',
                //     render: function ( data, type, row, meta ) {
                //         var fecha = moment(data);
                //         if(fecha.isValid()){
                //             return fecha. format('DD/MM/YYYY');
                //         }
                //         return data;
                //     }
                // },
                

                // {
				// 	title: 'Fecha de corte',
                //     data: 'FechaProceso',
                //     render: function ( data, type, row, meta ) {
                //         var fecha = moment(data);
                //         if(fecha.isValid()){
                //             return fecha. format('DD/MM/YYYY');
                //         }
                //         return data;
                //     }
                // },
               
                {
					title: '-',
					width: '100px',
					render: function ( data, type, row, meta ) {
						if(row.Procesado == 0){
							// var editar = "<a class='btn btn-info btn-sm' onclick='Apps.modal_editar_periodo("+ row.id +");' title='Editar' > <i class='fas fa-pencil-alt'></i> </a>";
                            // var eliminar = "<button type='button' onclick='Apps.deleteContenido(this);' class='btn-borrar btn btn-danger ml-2 btn-sm' data-id=" + row.id + "><i class='fas fa-trash'></i></button>";

                            // var actual = moment();
                            // var fecha = moment(row.FechaInicio);
                            // if(fecha.isValid()){
                            //     if(actual.diff(fecha) > 0){
                            //         editar = '';
                            //         eliminar = '';
                            //     }
                            // }
							
                            var crear = "<a class='btn btn-success btn-sm ml-1 mr-1' onclick='Apps.creacion_siguiente_periodo("+ row.id +");' title='Creación del siguiente periodo' > <i class='far fa-calendar-plus'></i> </a>";
                            var seleccionar = "<a class='btn btn-info btn-sm ml-1 mr-1' onclick='Apps.seleccionar_periodo("+ row.id +");' title='Seleccionar periodo' > <i class='fas fa-sign-out-alt'></i> </a>";
							return crear + seleccionar;
						}else{
							return '';
						}
					}
				},
			],
			initComplete: function(settings, data) {
				$('table#listado').append(
					$('<tfoot/>').append( $("table#listado thead tr").clone() )
				);
            },
            // createdRow: function( row, data, dataIndex){
            //     Apps.tipo_periodicidad = data.id_Periodicidad;
            //     Apps.fecha_inicio = data.FechaInicio;
            //     Apps.fecha_fin = data.FechaFin;
                
            //     var band = false;
            //     if(data.Procesado == '0'){
            //         var actual = moment();
            //         var fecha = moment(data.FechaInicio);
            //         if(fecha.isValid()){
            //             if(actual.diff(fecha) > 0){
            //                 band = true;
            //                 $('td', row).css('background-color', '#d4edda');
            //             }
            //         }
            //     }
                
            //     if(band == false){
            //         $('td', row).css('background-color', '#fff');
            //     }
                
            // },
            // fnRowCallback: function(nRow, aData, iDisplayIndex, iDisplayIndexFull) {
            //     $('td', nRow).css('background-color', 'red');
            // }
		});

		var table = $('table#listado').DataTable();
		$('table#listado').on( 'page.dt', function () {
			
			// var info = table.page.info();
			//$('#pageInfo').html( 'Showing page: '+info.page+' of '+info.pages );
		} );
	},

    this.modal_nuevo_periodo = function(){
        $.ajax({
            dataType: "json",
            type: 'get',
            url: PATH + 'nomina/herramientas/periodos/modal_nuevo_periodo',
            data: {},
            success: function (response, status, xhr) {
                $('div#modal_nuevo_periodo div.modal-content').html( atob(response.html) );
                $('div#modal_nuevo_periodo').modal('show');
                Apps.modal_actualizar_fechas();
                
            }
        });
    }

    this.modal_editar_periodo = function(id){
        $('div#modal_nuevo_periodo div.modal-content').empty();
        $.ajax({
            dataType: "json",
            type: 'post',
            url: PATH + 'nomina/herramientas/periodos/modal_editar_periodo',
            data: {id:id},
            success: function (response, status, xhr) {
                
                $('div#modal_nuevo_periodo div.modal-content').html( atob(response.html) );
                $('div#modal_nuevo_periodo').modal('show');
                
            }
        });
    }

    this.modal_nuevo_periodo_save = function(){
        $.ajax({
            dataType: "json",
            type: 'post',
            url: PATH + 'nomina/herramientas/periodos/modal_nuevo_periodo_save',
            data: $('div#modal_nuevo_periodo form#form_content').serializeArray(),
            success: function (response, status, xhr) {
                $('div#modal_nuevo_periodo').modal('hide');
                Swal.fire({
                    icon: 'success',
                    title: '',
                    text: response.message,
                    confirmButtonText: "Aceptar"
                }).then((result) => {

                    var tipo_periodo = $('select#id_tipoperiodo option:selected').val();
                    $('select#Periodicidad option[value='+tipo_periodo+']').prop('selected',true);

                    var table = $('table#listado').DataTable();
                    table.ajax.reload(null, false);
                });
                
            }
        });
    },
    this.modal_editar_periodo_save = function(){
        $.ajax({
            dataType: "json",
            type: 'post',
            url: PATH + 'nomina/herramientas/periodos/modal_editar_periodo_save',
            data: $('div#modal_nuevo_periodo form#form_content').serializeArray(),
            success: function (response, status, xhr) {
                $('div#modal_nuevo_periodo').modal('hide');
                Swal.fire({
                    icon: 'success',
                    title: '',
                    text: response.message,
                    confirmButtonText: "Aceptar"
                }).then((result) => {

                    var tipo_periodo = $('select#id_tipoperiodo option:selected').val();
                    $('select#Periodicidad option[value='+tipo_periodo+']').prop('selected',true);

                    var table = $('table#listado').DataTable();
                    table.ajax.reload(null, false);
                });
                
            }
        });
    }

    this.modal_actualizar_fechas = function(){
        $('div#modal_nuevo_periodo input#fecha_inicio').val( $('select#id_tipoperiodo option:selected').attr('fechainicio') );
        $('div#modal_nuevo_periodo input#fecha_fin').val( $('select#id_tipoperiodo option:selected').attr('fechafin') );
        Apps.modal_actualizar_fechas_corte();
    },

    this.modal_actualizar_fechas_corte = function(){
        var fecha_fin = $('div#modal_nuevo_periodo input#fecha_fin').val();
        var fecha = moment(fecha_fin);
        if(fecha.isValid()){
            var new_date = fecha.add(5, 'days');
            $('div#modal_nuevo_periodo input#fecha_corte').val(new_date.format('YYYY-MM-DD'));
        }
        
    },

    this.seleccionar_periodo = function(id_periodo){
        $.ajax({
            dataType: "json",
            type: 'post',
            url: PATH + 'nomina/herramientas/periodos/guarda_periodo_actual',
            data: {
                'periodo': id_periodo
            },
            success: function (response, status, xhr) {
                try {
                    window.location.href = url_redirect;    
                } catch (error) {
                    window.location.href = PATH+'/nomina/inicio/trabajador';
                }
                
            }
        });
    },

    this.creacion_siguiente_periodo = function(id_periodo){
       window.location.href = PATH+'/nomina/herramientas/periodos/creacion_siguiente_periodo/'+btoa(id_periodo);
    }
}

var Apps;
$(function () {
	Apps = new _appsFunction();
    Apps.get();
    
    $('div#exampleModal').modal('hide');
        $('div#exampleModal').on('show.bs.modal', function (event) {
            setTimeout(() => {
                $('select#periodicidad option[value='+Apps.tipo_periodicidad+']').prop('selected',true);

                var fecha = moment(Apps.fecha_inicio);
                if(fecha.isValid()){
                    $('input#fecha_inicio').val(fecha.format('DD-MM-YYYY'));
                }

                var fecha = moment(Apps.fecha_fin);
                if(fecha.isValid()){
                    $('input#fecha_fin').val(fecha.format('DD-MM-YYYY'));
                }

            },300);
         })

});
