var _appsFunction = function () {
    this.init = function () {},
   
    this.guardar = function(){
        $.ajax({
            dataType: "json",
            type: 'post',
            url: PATH + 'nomina/herramientas/periodos/guarda_periodo_actual',
            data: {
                'periodo': $('select#Periodicidad option:selected').val(),
                'fecha_inicio': $('select#Periodicidad option:selected').attr('fecha_inicio'),
                'fecha_fin': $('select#Periodicidad option:selected').attr('fecha_fin'),
                'title': $('select#Periodicidad option:selected').attr('title'),
                'date': $('select#Periodicidad option:selected').attr('date')
            },
            success: function (response, status, xhr) {
                try {
                    window.location.href = url_redirect;    
                } catch (error) {
                    window.location.href = PATH+'/nomina/inicio/trabajador';
                }
                
            }
        });
    }

}

var Apps;
$(function () {
	Apps = new _appsFunction();
    
});
