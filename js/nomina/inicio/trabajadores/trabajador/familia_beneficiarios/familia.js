var _appsFunction = function () {
	this.init = function () {},
	this.delete = function ($this) {
		Swal.fire({
			icon: 'warning',
			title: '¿Esta seguro de eliminar el registro?',
			//text: response.info,
			showCancelButton: true,
			confirmButtonText: "Si",
			cancelButtonText: "No"
		}).then((result) => {
			if (result.value) {

				$.ajax({
					dataType: "json",
					type: 'post',
					url: PATH + '/nomina/inicio/trabajador/familia_beneficiarios_delete',
					data: {
						'identity': $($this).attr('data-id')
					},
					success: function (response, status, xhr) {
						Swal.fire({
							icon: 'success',
							title: '',
							text: response.message,
							confirmButtonText: "Aceptar"
						}).then((result) => {
							var table = $('table#listado').DataTable();
							table.ajax.reload(null, false);
						});
					}

				});
			}
		});
	},
	this.get = function () {
		$('table#listado').dataTable({
			colReorder: true,
			fixedHeader: {
				header: true,
			},
			autoWidth: false,
			stateSave: false,
			ajax: {
				url: PATH + '/nomina/inicio/trabajador/familia_beneficiarios_get?id='+identity,
				type: "GET"
			},
			columns: [
				{
					title: 'Clave',
					data: 'Clave'
				},
				{
					title: 'Nombre',
					data: 'Nombre'
				},
				{
					title: 'Parentesco',
					data: 'Descripcion_Parentesco'
				},
				{
					title: 'Fecha de nacimiento',
					data: 'FechaNacimiento'
				},
				{
					title: 'Sexo',
					data: 'Nombre_Genero'
				},
				{
					title: 'Lab. en emp.',
					data: 'LaberaEmpresa'
				},
				{
					title: '-',
					'data': function (data) {
						return "<a class='btn btn-info btn-sm' href='" + PATH + '/nomina/inicio/trabajador/familia_editar?id=' + data.id + "&trabajador="+identity+"'> Editar </a>";
					}
				},
				{
					title: '-',
					'data': function (data) {
						return "<a class='btn btn-danger btn-sm' onclick='Apps.delete(this);' data-id='"+data.id+"' > Eliminar </a>";
					}
				},
				
			],
			initComplete: function(settings, data) {
				$('table#listado').append(
					$('<tfoot/>').append( $("table#listado thead tr").clone() )
				);
			}
		});
	}
}

var Apps;
$(function () {
	Apps = new _appsFunction();
	Apps.get();
});
