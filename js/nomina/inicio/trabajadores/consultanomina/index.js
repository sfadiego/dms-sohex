var _appsFunction = function () {
	this.init = function () {},
	this.get = function () {
		$('table#listado').dataTable({
			colReorder: true,
			fixedHeader: {
				header: true,
			},
			autoWidth: false,
			stateSave: false,

			ajax: {
				url: PATH + '/nomina/inicio/consultanomina/obtener_listado',
				type: "POST",
				data: function(){
					return {periodo:_ID_PERIODO_};
				}
			},
			columns: [
				{
					title: 'Clave',
					data: 'Clave_Trabajador'
				},
				{
					title: 'Nombre Completo',
					'data': function(data){
						var row = [
							data.Nombre_Trabajador,
							data.Apellido_1_Trabajador,
							data.Apellido_2_Trabajador
						];
						return $.trim(row.join(' '));
					}
				},
				{
					title: 'Total de percepciones',
					'data': 'TotalPercepcion',
					render: function ( data, type, row, meta ) {
						var formatter = new Intl.NumberFormat('en-US', {
							style: 'currency',
							currency: 'USD',
						  });
						return formatter.format(data);
					}
				},
				{
					title: 'Total de deducciones',
					'data': 'TotalDeduccion',
					render: function ( data, type, row, meta ) {
						var formatter = new Intl.NumberFormat('en-US', {
							style: 'currency',
							currency: 'USD',
						  });
						return formatter.format(data);
					}
				},
				{
					title: 'Neto a pagar',
					'data': 'Total',
					render: function ( data, type, row, meta ) {
						var formatter = new Intl.NumberFormat('en-US', {
							style: 'currency',
							currency: 'USD',
						  });
						return formatter.format(data);
					}
				}
			],
			initComplete: function(settings, data) {
				$('table#listado').append(
					$('<tfoot/>').append( $("table#listado thead tr").clone() )
				);
			}
		});
	}
}

var Apps;
$(function () {
	Apps = new _appsFunction();
	Apps.get();
});
