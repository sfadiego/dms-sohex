var _appsFunction = function () {
	this.init = function () {},
	this.get = function () {
		if(periodo == ''){
			var opcion=$('select#periodo_trabajador option:selected').val(); 
			window.location.href = PATH+'/nomina/inicio/nominaTrabajador/detalle/'+identity+'/'+opcion;
		}else{
			this.get_percepciones();
			this.get_deducciones();
		}
	},
	this.get_percepciones = function () {
		$('table#table_content_percepcion').dataTable({
			colReorder: true,
			bInfo: false,
			searching: false,
			bLengthChange: false,
			pageLength: 50,
			
			fixedHeader: {
				header: true,
			},
			autoWidth: false,
			stateSave: false,
			order: [[ 0, "asc" ]],
			ajax: {
				url: PATH + '/nomina/inicio/nominaTrabajador/detalle_listado_percepciones',
				type: "GET",
				data: function(){
					return {
						id_trabajador: identity,
						id_periodo: $('select#periodo_trabajador option:selected').val()
					};
				}
			},
			columns: [
				{
					title: 'No',
					data: 'Clave'
				},
				{
					title: 'Precepciones',
					data: 'Descripcion'
				},
				{
					title: 'Monto',
					data: 'Monto',
					render: function ( data, type, row, meta ) {
						return '$ ' + (new Intl.NumberFormat('en-US').format(data));
					}
                }
			],
			initComplete: function(settings, data) {
				$('table#table_content_percepcion').append(
					$('<tfoot/>').append( $("table#table_content_percepcion thead tr").clone() )
				);
			}
		});
	},
	this.get_deducciones = function () {
		$('table#table_content_deducciones').dataTable({
			colReorder: true,
			bInfo: false,
			searching: false,
			bLengthChange: false,
			pageLength: 50,

			fixedHeader: {
				header: true,
			},
			autoWidth: false,
			stateSave: false,
			order: [[ 0, "asc" ]],
			ajax: {
				url: PATH + '/nomina/inicio/nominaTrabajador/detalle_listado_deducciones',
				type: "GET",
				data: function(){
					return {
						id_trabajador: identity,
						id_periodo: $('select#periodo_trabajador option:selected').val()
					};
				}
			},
			columns: [
				{
					title: 'No',
					data: 'Clave'
				},
				{
					title: 'Deducciones',
					data: 'Descripcion'
				},
				{
					title: 'Monto',
					data: 'Monto',
					render: function ( data, type, row, meta ) {
						return '$ ' + (new Intl.NumberFormat('en-US').format(data));
					}
                }
			],
			initComplete: function(settings, data) {
				$('table#table_content_deducciones').append(
					$('<tfoot/>').append( $("table#table_content_deducciones thead tr").clone() )
				);
			}
		});
	}

}

var Apps;
$(function () {
	Apps = new _appsFunction();
	Apps.get();
});
