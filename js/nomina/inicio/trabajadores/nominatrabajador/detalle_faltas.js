var _appsFunction = function () {
	this.init = function () {},

	this.get = function () {
		
		$('table#table_content').dataTable({
			colReorder: true,
			fixedHeader: {
				header: true,
			},
			autoWidth: false,
			stateSave: false,
			order: [[ 2, "desc" ]],
			ajax: {
				url: PATH + '/nomina/inicio/faltas/index_get',
				type: "POST",
				data: function(){
					return {
						id: identity
					};
				}
			},
			columns: [
				{
					title: 'Número',
					data: 'id_TipoFalta'
				},
				{
					title: 'Descripción',
					data: 'Descripcion_TipoFalta'
				},
				{
					title: 'Fecha inicial',
					data: 'Fecha',
					render: function ( data, type, row, meta ) {
						var fecha = moment(data);
						if(fecha.isValid()){
							return fecha.format('DD/MM/YYYY');
						}
						return '';
					}
				},
				{
					title: 'Días faltados',
					data: function(){
						return '1';
					}
				},
				{
					title: 'A pagar empresa',
					data: function(){
						return '-';
					}
				},
				{
					title: 'Referencia',
					data: function(){
						return '-';
					}
				}
			],
			initComplete: function(settings, data) {
				$('table#table_content').append(
					$('<tfoot/>').append( $("table#table_content thead tr").clone() )
				);
			}
		});
	}
}

var Apps;
$(function () {
	Apps = new _appsFunction();
	Apps.get();
	
});
