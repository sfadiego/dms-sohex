var _appsFunction = function () {
	this.init = function () {
		$('input[name=Dia_1_fecha]').val(dia_1);
		$('input[name=Dia_2_fecha]').val(dia_2);
		$('input[name=Dia_3_fecha]').val(dia_3);
		$('input[name=Dia_4_fecha]').val(dia_4);
		$('input[name=Dia_5_fecha]').val(dia_5);
		$('input[name=Dia_6_fecha]').val(dia_6);
		$('input[name=Dia_7_fecha]').val(dia_7);

		$('input[name=Semana]').val(semana);

		$('input[name=Dia_1]').val(0);
		$('input[name=Dia_2]').val(0);
		$('input[name=Dia_3]').val(0);
		$('input[name=Dia_4]').val(0);
		$('input[name=Dia_5]').val(0);
		$('input[name=Dia_6]').val(0);
		$('input[name=Dia_7]').val(0);
	},

	this.guardar = function () {

		var data_send =  $('form#general_form').serializeArray();
		data_send.push({ 'name': 'id_Trabajador','value':id_trabajador });
		data_send.push({ 'name': 'id_PeriodoPago','value':id_periodo });
		data_send.push({ 'name': 'Procesado','value':0 });

		var fecha = moment( $('input[name=Fecha]').val()  );
		if(fecha.isValid()){
			data_send.push({ 'name': 'Semana','value':fecha.isoWeek() });
		}else{
			data_send.push({ 'name': 'Semana','value':1 });
		}

		 $.ajax({
			 dataType: "json",
			 type: 'POST',
			 url: PATH + '/nomina/inicio/horas_extras/alta_guardar',
			 data: data_send,
			 success: function (response, status, xhr) {
 
				 if (response.status == 'success') {
					Swal.fire({
						 icon: 'success',
						 title: '',
						 text: response.message,
						 confirmButtonText: "Aceptar"
					 }).then((result) => {
						window.location.href = PATH + '/nomina/inicio/horas_extras/index/' +  btoa(id_trabajador);
					 });
				 }
 
			 }
 
		 });
	},	

	this.get = function () {

		

		if(id_trabajador != false){
			$('select[name="trabajador"]').attr('attr-id',id_trabajador);
		}
		if(numSemana != false){
			$('select[name="fechas"]').attr('attr-id',numSemana);
		}

		$('form#general_form select').each(function( index ) {
			try {
				var id = $(this).attr('attr-id').toLowerCase();
			} catch (error) {
				var id = $(this).attr('attr-id');
			}
			if($.trim(id).length > 0){
				$(this).find('option[value="'+id+'"]').attr("selected", "selected");
				$(this).select2({    
					language: {
					noResults: function() { return "No hay resultados"; },
					searching: function() { return "Buscando.."; }
					}
				});
			}else{
				$(this).select2({    
					language: {
					noResults: function() { return "No hay resultados"; },
					searching: function() { return "Buscando.."; }
					}
				}).val('').change();
			}                        
		});

	}
}

var Apps;
$(function () {
	Apps = new _appsFunction();
	// Apps.init();
});
