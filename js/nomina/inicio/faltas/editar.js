var _appsFunction = function () {
	this.init = function () {

		
		
		$('input[name=Fecha]').removeAttr('type');
		
		$('input[name=Fecha]').removeClass('form-control');
		$('input[name=Fecha]').addClass('form-control-plaintext');

		var fecha2 = moment($('input[name=FechaAlta]').val());
		$('input[name=FechaAlta]').val(fecha2.format('DD/MM/YYYY'));
		
		var fecha2 = moment($('input[name=Fecha]').val());
		$('input[name=Fecha]').val(fecha2.format('DD/MM/YYYY'));
		
		$('input[name=FechaAlta]').removeAttr('name');
		$('input[name=Fecha]').removeAttr('name');

		$('form#general_form select').each(function( index ) {
			try {
				var id = $(this).attr('attr-id').toLowerCase();
			} catch (error) {
				var id = $(this).attr('attr-id');
			}
			if($.trim(id).length > 0){
				$(this).find('option[value="'+id+'"]').attr("selected", "selected");
				$(this).select2({    
					language: {
					noResults: function() { return "No hay resultados"; },
					searching: function() { return "Buscando.."; }
					}
				});
			}else{
				$(this).select2({    
					language: {
					noResults: function() { return "No hay resultados"; },
					searching: function() { return "Buscando.."; }
					}
				}).val('').change();
			}                        
		});
	},
	this.addContenido = function(){
		var id = $('select[name=trabajador] option:selected').val();
		var semana = $('select[name=fechas] option:selected').val();
		window.location.href = PATH + '/nomina/inicio/horas_extras/alta/'+id+'/'+semana;
	},
	this.guardar = function () {

		var data_send =  $('form#general_form').serializeArray();
		data_send.push({ 'name': 'id','value':id });

		 $.ajax({
			 dataType: "json",
			 type: 'POST',
			 url: PATH + '/nomina/inicio/faltas/editar_guardar',
			 data: data_send,
			 success: function (response, status, xhr) {
 
				 if (response.status == 'success') {
					Swal.fire({
						 icon: 'success',
						 title: '',
						 text: response.message,
						 confirmButtonText: "Aceptar"
					 }).then((result) => {
						window.location.href = PATH + '/nomina/inicio/faltas/index/' + id_trabajador;
					 });
				 }
 
			 }
 
		 });
	},	

	this.get = function () {

		var fecha = moment()
		$('input[name=Fecha]').attr('max',fecha.format('YYYY-MM-DD'));
		$('input[name=FechaAlta]').val(fecha.format('DD/MM/YYYY'));
		fecha.subtract(15, 'd');
		$('input[name=Fecha]').attr('min',fecha.format('YYYY-MM-DD'));
		

		if(id_trabajador != false){
			$('select[name="trabajador"]').attr('attr-id',id_trabajador);
		}
		if(numSemana != false){
			$('select[name="fechas"]').attr('attr-id',numSemana);
		}

		$('form#general_form select').each(function( index ) {
			try {
				var id = $(this).attr('attr-id').toLowerCase();
			} catch (error) {
				var id = $(this).attr('attr-id');
			}
			if($.trim(id).length > 0){
				$(this).find('option[value="'+id+'"]').attr("selected", "selected");
				$(this).select2({    
					language: {
					noResults: function() { return "No hay resultados"; },
					searching: function() { return "Buscando.."; }
					}
				});
			}else{
				$(this).select2({    
					language: {
					noResults: function() { return "No hay resultados"; },
					searching: function() { return "Buscando.."; }
					}
				}).val('').change();
			}                        
		});

	}
}

var Apps;
$(function () {
	Apps = new _appsFunction();
	Apps.init();
	// Apps.init();
});
