var _appsFunction = function () {

	this.zfill = function(number, width) {
		var numberOutput = Math.abs(number); /* Valor absoluto del número */
		var length = number.toString().length; /* Largo del número */ 
		var zero = "0"; /* String de cero */  
		
		if (width <= length) {
			if (number < 0) {
				 return ("-" + numberOutput.toString()); 
			} else {
				 return numberOutput.toString(); 
			}
		} else {
			if (number < 0) {
				return ("-" + (zero.repeat(width - length)) + numberOutput.toString()); 
			} else {
				return ((zero.repeat(width - length)) + numberOutput.toString()); 
			}
		}
	},


	this.init = function () {
		
		$('form#general_form select').each(function( index ) {
			try {
				var id = $(this).attr('attr-id').toLowerCase();
			} catch (error) {
				var id = $(this).attr('attr-id');
			}
			
            if($.trim(id).length > 0){
                $(this).find('option[value="'+id+'"]').attr("selected", "selected");
                $(this).select2({    
                    language: {
                    noResults: function() { return "No hay resultados"; },
                    searching: function() { return "Buscando.."; }
                    }
                });
            }else{
                $(this).select2({    
                    language: {
                    noResults: function() { return "No hay resultados"; },
                    searching: function() { return "Buscando.."; }
                    }
                }).val('').change();
            }
			
		});

	},
	this.regresar = function () {
		window.location.href = PATH + '/nomina/inicio/percepcionesdeducciones/basefiscales/'+id_pyd;
    },
	this.guardar = function () {
		var data_send =  $('form#general_form').serializeArray();
        data_send.push({ 'name': 'id','value':id_Base_Fiscal });
        
        $.ajax({
            dataType: "json",
            type: 'POST',
            url: PATH + '/nomina/inicio/percepcionesdeducciones/basefiscales_modificar_actualizar',
            data: data_send,
            success: function (response, status, xhr) {

				if (response.status == 'success') {
					Swal.fire({
						icon: 'success',
						title: '',
						text: response.message,
						confirmButtonText: "Aceptar"
					}).then((result) => {
						window.location.href = PATH + '/nomina/inicio/percepcionesdeducciones/basefiscales/'+id_pyd;
					});
				}

            }

        });
    }		
}

var Apps;
$(function () {
	Apps = new _appsFunction();
	Apps.init();
});
