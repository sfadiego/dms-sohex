var _appsFunction = function () {
	this.init = function () {

		var change_input = [];
		$( "form#general_form input[type=text]" ).each(function( index ) {
			var name = $( this ).attr('name');
			change_input[name] = {type:"number",max:"100",min:"0",step:'0.0001'};
		});
		change_input = Object.assign({},change_input);
		update_inputs(change_input);
	},
	this.guardar = function () {

	   var data_send =  $('form#general_form').serializeArray();
	   data_send.push({ 'name': 'id','value':identity });

        $.ajax({
            dataType: "json",
            type: 'POST',
            url: PATH + '/nomina/inicio/parametrosnomina/guardar_imss',
            data: data_send,
            success: function (response, status, xhr) {

				if (response.status == 'success') {
					Swal.fire({
						icon: 'success',
						title: '',
						text: response.message,
						confirmButtonText: "Aceptar"
					});
				}

            }

        });
    }		
}

var Apps;
$(function () {
	Apps = new _appsFunction();
	Apps.init();
});
