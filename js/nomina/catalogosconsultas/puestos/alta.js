var _appsFunction = function () {
	this.init = function () {},

		this.guardar = function () {
			$.ajax({
				dataType: "json",
				type: 'POST',
				url: PATH + '/nomina/api/api/runner/puestos/post',
				data: $('form#formContent').serializeArray(),
				success: function (response, status, xhr) {
					if (response.status == 'success') {
						Swal.fire({
							icon: 'success',
							title: '',
							text: response.message,
							confirmButtonText: "Aceptar"
						}).then((result) => {
							window.location.href = PATH + '/nomina/catalogosconsultas/puestos/index?id=' + response.data.id;
						});
					}
				}

			});
		},
		this.render = function(){

			$.ajax({
				dataType: "json",
				type: 'GET',
				url: PATH + '/nomina/api/api/runner/puestos/find',
				success: function (response, status, xhr) {
					if(response.status == 'success'){

						var RiesgoPuesto = [];
						if(response.extends.RiesgosPuesto.length){
							$.each(response.extends.RiesgosPuesto, function( index, value ) {
								RiesgoPuesto.push({
									id: value.id,
									Numero: value.Numero,
									Descripcion: value.Descripcion,
									selected: false
								});
							});
						}
						
						var template = $('script#template').html()
						var storange = {
							Clave: response.extends.Clave,
							RiesgoPuesto: RiesgoPuesto
						};
						console.log(storange);
						var renderedContent = Mustache.render(template,storange);
						$('form#formContent > div#contenedor').html(renderedContent);
					}
				}

			});
		}
}

var Apps;
$(function () {
	Apps = new _appsFunction();
	Apps.render();
});
