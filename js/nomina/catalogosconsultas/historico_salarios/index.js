var _appsFunction = function () {
	this.init = function () {},
	this.addContenido = function(){
		var id = $('select[name=trabajador] option:selected').val();
		//var semana = $('select[name=fechas] option:selected').val();
		window.location.href = PATH + '/nomina/catalogosconsultas/historico_salarios/alta/'+id;
	},
	this.cargarDatos = function(){
		var table = $('table#listado').DataTable();
		table.ajax.reload(null, false);
	},

	this.delete = function ($this) {
		Swal.fire({
			icon: 'warning',
			title: '¿Esta seguro de eliminar el registro?',
			//text: response.info,
			showCancelButton: true,
			confirmButtonText: "Si",
			cancelButtonText: "No"
		}).then((result) => {
			if (result.value) {

				$.ajax({
					dataType: "json",
					type: 'post',
					url: PATH + '/nomina/inicio/horas_extras/delete',
					data: {
						'id': $($this).attr('data-id')
					},
					success: function (response, status, xhr) {
						Swal.fire({
							icon: 'success',
							title: '',
							text: response.message,
							confirmButtonText: "Aceptar"
						}).then((result) => {
							var table = $('table#listado').DataTable();
							table.ajax.reload(null, false);
						});
					}

				});
			}
		});
	},

	this.get = function () {
		
		if(id_trabajador != false){
			$('select[name="trabajador"]').attr('attr-id',id_trabajador);
		}
		
		$('form#general_form select').each(function( index ) {
			try {
				var id = $(this).attr('attr-id').toLowerCase();
			} catch (error) {
				var id = $(this).attr('attr-id');
			}
			if($.trim(id).length > 0){
				$(this).find('option[value="'+id+'"]').attr("selected", "selected");
			}                        
		});

		
		$('table#listado').dataTable({
			colReorder: true,
			fixedHeader: {
				header: true,
			},
			autoWidth: false,
			stateSave: false,
			order: [[ 0, "desc" ]],
			ajax: {
				url: PATH + '/nomina/catalogosconsultas/historico_salarios/index_get',
				type: "POST",
				data: function(){
					return {
						id: $('select[name=trabajador] option:selected').val()
					};
				}
			},
			columns: [
				{
					title: 'Tipo de movimiento',
					data: 'Descripcion_HistTipoMov'
				},
				{
					title: 'Fecha de movimiento',
					data: 'FechaMovimiento',
					render: function ( data, type, row, meta ) {
						var fecha = moment(data);
						if(fecha.isValid()){
							return fecha.format('DD/MM/YYYY');
						}
						return '';
					}
				},
				{
					title: 'Salario por dìa',
					data: 'SalarioDiario',
					render: function ( data, type, row, meta ) {
						return '$'+ (new Intl.NumberFormat('en-US').format(data));
					}
				},
				{
					title: 'SDI IMSS',
					data: 'SdiImss',
					render: function ( data, type, row, meta ) {
						return '$'+ (new Intl.NumberFormat('en-US').format(data));
					}
				},
				{
					title: '-',
					'data': function (data) {
						return "<a class='btn btn-info btn-sm' href='" + PATH + '/nomina/inicio/horas_extras/editar/'+data.id+ '/' + data.id_Trabajador + "' title='' > <i class='fas fa-pencil-alt'></i> </a>";
					}
				},
				{
					title: '-',
					'data': function (data) {
						return "<button type='button' onclick='Apps.delete(this);' class='btn-borrar btn btn-danger btn-sm' data-id=" + data.id + "><i class='fas fa-trash'></i></button>";
					}
				}
			],
			initComplete: function(settings, data) {
				$('table#listado').append(
					$('<tfoot/>').append( $("table#listado thead tr").clone() )
				);
			}
		});
	}
}

var Apps;
$(function () {
	Apps = new _appsFunction();
	Apps.get();
});
