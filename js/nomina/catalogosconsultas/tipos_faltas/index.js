var _appsFunction = function () {
	this.init = function () {},
	this.addContenido = function(){
		window.location.href = PATH + '/nomina/catalogosconsultas/tipos_faltas/alta';
	},
	this.cargarDatos = function(){
		var table = $('table#listado').DataTable();
		table.ajax.reload(null, false);
	},

	this.delete = function ($this) {
		Swal.fire({
			icon: 'warning',
			title: '¿Esta seguro de eliminar el registro?',
			//text: response.info,
			showCancelButton: true,
			confirmButtonText: "Si",
			cancelButtonText: "No"
		}).then((result) => {
			if (result.value) {

				$.ajax({
					dataType: "json",
					type: 'post',
					url: PATH + '/nomina/catalogosconsultas/tipos_faltas/delete',
					data: {
						'id': $($this).attr('data-id')
					},
					success: function (response, status, xhr) {
						Swal.fire({
							icon: 'success',
							title: '',
							text: response.message,
							confirmButtonText: "Aceptar"
						}).then((result) => {
							var table = $('table#listado').DataTable();
							table.ajax.reload(null, false);
						});
					}

				});
			}
		});
	},

	this.get = function () {

		$('table#listado').dataTable({
			colReorder: true,
			fixedHeader: {
				header: true,
			},
			autoWidth: false,
			stateSave: false,
			ajax: {
				url: PATH + '/nomina/catalogosconsultas/tipos_faltas/index_get',
				type: "POST"
			},
			columns: [
				{
					title: 'Clave',
					data: 'Clave'
				},
				{
					title: 'Descripción',
					data: 'Descripcion'
				},
				{
					title: '7o. Día',
					data: '7oDia'
				},
				{
					title: 'PTU',
					data: 'PTU'
				},
				{
					title: 'INFONAVIT',
					data: 'Infonavit'
				},
				{
					title: 'IMSS',
					data: 'Descripcion_AfectaImss'
				},
				{
					title: '-',
					'data': function (data) {
						return "<a class='btn btn-info btn-sm' href='" + PATH + '/nomina/catalogosconsultas/tipos_faltas/editar/'+data.id+ "' title='' > <i class='fas fa-pencil-alt'></i> </a>";
					}
				},
				{
					title: '-',
					'data': function (data) {
						return "<button type='button' onclick='Apps.delete(this);' class='btn-borrar btn btn-danger btn-sm' data-id=" + data.id + "><i class='fas fa-trash'></i></button>";
					}
				}
			],
			initComplete: function(settings, data) {
				$('table#listado').append(
					$('<tfoot/>').append( $("table#listado thead tr").clone() )
				);
			}
		});
	}
}

var Apps;
$(function () {
	Apps = new _appsFunction();
	Apps.get();
});
