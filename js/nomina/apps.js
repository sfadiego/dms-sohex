$(function(){

    if (!(typeof(periodo_libre) !== 'undefined' && periodo_libre === true)) {
        $.ajax({
            dataType: "json",
            type: 'post',
            url: PATH + '/nomina/herramientas/periodos/datos_periodo',
            data: {},
            success: function (response, status, xhr) {
                if(response == null){
                    var href = btoa(window.location.href);
                    window.location.href = PATH+'/nomina/herramientas/periodos/index?url='+href;
                }
            }

        });
    }

    $.ajaxSetup({
        beforeSend: function (xhr) {
            if($('small.form-text.text-danger').length > 0){
                $('small.form-text.text-danger').each(function() {
                    $( this ).html( "" );
                });
            }
        },
        error: function (xhr, status, errors) {
            toastr.clear(Apps.toastrMsg);
            
            if(xhr.status == 406){
                // try {
                    toastr["error"]("Es necesario verificar la información");
                    if($('small.form-text.text-danger').length > 0){
                        $('small.form-text.text-danger').each(function() {
                            $( this ).empty();
                        });
                    }
                    var response = JSON.parse(xhr.responseText);
                    if (response.status == 'error') {
                        if($.type(response.message) == 'object'){
                            $.each(response.message, function(index, value) {
                                if ($('small#msg_' + index).length) {
                                    $('small#msg_' + index).html(value);
                                }else{
                                    toastr["error"](value);
                                }
                            });
                        }
                        
                    }
                // } catch (error) {
                //     toastr["info"]("Ha ocurrido un error al momento de procesar su última acción");
                // }
            }else if(xhr.status == 400){
                try {
                    var response = JSON.parse(xhr.responseText);
                    toastr["error"](response.info);
                } catch (error) {
                    toastr["error"]("Ha ocurrido un error al momento de procesar su última acción");
                }
            }else if(xhr.status == 409){
                // try {
                //     var response = JSON.parse(xhr.responseText);
                //     toastr["warning"](response.message);
                // } catch (error) {
                //     // toastr["error"]("Ha ocurrido un error al momento de procesar su última acción");
                // }

                
                    if($('small.form-text.text-danger').length > 0){
                        $('small.form-text.text-danger').each(function() {
                            $( this ).empty();
                        });
                    }
                    var response = JSON.parse(xhr.responseText);
                    if (response.status == 'error') {

                        if($.type(response.message) == 'object'){
                            toastr["error"]("Es necesario verificar la información");
                            $.each(response.message, function(index, value) {
                                if ($('small#msg_' + index).length) {
                                    $('small#msg_' + index).html(value);
                                }else{
                                    toastr["error"](value);
                                }
                            });
                        }else if($.type(response.message) == 'string'){
                            toastr["error"](response.message);
                        }    
                    }

            }else if(xhr.status == 500){
                try {
                    toastr["error"]('Ha ocurrido un error, por favor intente nuevamente');
                } catch (error) {
                    // toastr["error"]("Ha ocurrido un error al momento de procesar su última acción");
                }
            }
        }
    });


    $.extend( true, $.fn.dataTable.defaults, {
        "language": {
            "sProcessing":     "Procesando...",
            "sLengthMenu":     "Mostrar _MENU_ registros",
            "sZeroRecords":    "No se encontraron resultados",
            "sEmptyTable":     "Ningún dato disponible en esta tabla",
            "sInfo":           "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
            "sInfoEmpty":      "Mostrando registros del 0 al 0 de un total de 0 registros",
            "sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
            "sInfoPostFix":    "",
            "sSearch":         "Buscar:",
            "sUrl":            "",
            "sInfoThousands":  ",",
            "sLoadingRecords": "Cargando...",
            "oPaginate": {
                "sFirst":    "Primero",
                "sLast":     "Último",
                "sNext":     "Siguiente",
                "sPrevious": "Anterior"
            },
            "oAria": {
                "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
                "sSortDescending": ": Activar para ordenar la columna de manera descendente"
            },
            "buttons": {
                "copy": "Copiar",
                "colvis": "Visibilidad"
            }
        },
        createdRow: function( row, data, dataIndex){  
            $('td', row).css('background-color', '#fff');
        }         
    } );   
})

function update_inputs(change_input){
    $.each(change_input, function( key, items ) {
        if($('input[name='+key+']').length){

            $.each(items, function( index, value ) {
                $('input[name='+key+']').attr(index,value);
                if(value == 'number'){
                    $('input[name='+key+']').keyup(function (){
                        this.value = (this.value + '').replace(/[^.0-9]/g, '');
                    });
                }
            });
            
        }
    });
}

function cambiar_periodo(){
    window.location.href = PATH+'/nomina/herramientas/periodos';
}

function update_select2(formid){
    $(formid).each(function( index ) {
        try {
            var id = $(this).attr('attr-id').toLowerCase();
        } catch (error) {
            var id = $(this).attr('attr-id');
        }
        if($.trim(id).length > 0){
            $(this).find('option[value="'+id+'"]').attr("selected", "selected");
            $(this).select2({    
                language: {
                noResults: function() { return "No hay resultados"; },
                searching: function() { return "Buscando.."; }
                }
            });
        }else{
            $(this).select2({    
                language: {
                noResults: function() { return "No hay resultados"; },
                searching: function() { return "Buscando.."; }
                }
            }).val('').change();
        }                        
    });
}

function curpValida(curp) {
    var re = /^([A-Z][AEIOUX][A-Z]{2}\d{2}(?:0[1-9]|1[0-2])(?:0[1-9]|[12]\d|3[01])[HM](?:AS|B[CS]|C[CLMSH]|D[FG]|G[TR]|HG|JC|M[CNS]|N[ETL]|OC|PL|Q[TR]|S[PLR]|T[CSL]|VZ|YN|ZS)[B-DF-HJ-NP-TV-Z]{3}[A-Z\d])(\d)$/,
        validado = curp.match(re);
	
    if (!validado)  //Coincide con el formato general?
    	return false;
    
    //Validar que coincida el dígito verificador
    function digitoVerificador(curp17) {
        //Fuente https://consultas.curp.gob.mx/CurpSP/
        var diccionario  = "0123456789ABCDEFGHIJKLMNÑOPQRSTUVWXYZ",
            lngSuma      = 0.0,
            lngDigito    = 0.0;
        for(var i=0; i<17; i++)
            lngSuma = lngSuma + diccionario.indexOf(curp17.charAt(i)) * (18 - i);
        lngDigito = 10 - lngSuma % 10;
        if (lngDigito == 10) return 0;
        return lngDigito;
    }
  
    if (validado[2] != digitoVerificador(validado[1])) 
    	return false;
        
    return true; //Validado
}

function rfcValido(rfc, aceptarGenerico = true) {
    const re       = /^([A-ZÑ&]{3,4}) ?(?:- ?)?(\d{2}(?:0[1-9]|1[0-2])(?:0[1-9]|[12]\d|3[01])) ?(?:- ?)?([A-Z\d]{2})([A\d])$/;
    var   validado = rfc.match(re);

    if (!validado)  //Coincide con el formato general del regex?
        return false;

    //Separar el dígito verificador del resto del RFC
    const digitoVerificador = validado.pop(),
          rfcSinDigito      = validado.slice(1).join(''),
          len               = rfcSinDigito.length,

    //Obtener el digito esperado
          diccionario       = "0123456789ABCDEFGHIJKLMN&OPQRSTUVWXYZ Ñ",
          indice            = len + 1;
    var   suma,
          digitoEsperado;

    if (len == 12) suma = 0
    else suma = 481; //Ajuste para persona moral

    for(var i=0; i<len; i++)
        suma += diccionario.indexOf(rfcSinDigito.charAt(i)) * (indice - i);
    digitoEsperado = 11 - suma % 11;
    if (digitoEsperado == 11) digitoEsperado = 0;
    else if (digitoEsperado == 10) digitoEsperado = "A";

    //El dígito verificador coincide con el esperado?
    // o es un RFC Genérico (ventas a público general)?
    if ((digitoVerificador != digitoEsperado)
     && (!aceptarGenerico || rfcSinDigito + digitoVerificador != "XAXX010101000"))
        return false;
    else if (!aceptarGenerico && rfcSinDigito + digitoVerificador == "XEXX010101000")
        return false;
    return rfcSinDigito + digitoVerificador;
}