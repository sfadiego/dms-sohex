function inicializaTabla() {
	$('#tbl_abonos').DataTable({
		language: {
			url: PATH_LANGUAGE
		},
		order: 2,
		"ajax": {
			url: PATH_API + "api/abonos-por-pagar/listado-abonos-by-orden-entrada?orden_entrada_id=" + cuenta_por_pagar_id,
			type: 'GET',
		},
		columns: [{
				title: "Num pago",
				render: function (data, type, row) {
					if (row.tipo_abono_id == 2) {
						let pagonum = count++;
						return pagonum + '/' + cantidad_mes;
					} else {
						return '-';
					}
				}
			},
			{
				title: "Tipo abono",
				data: "tipo_abono"
			},
			{
				title: "Fecha vencimiento",
				render: function (data, type, row) {
					return obtenerFechaMostrar(row.fecha_vencimiento);
				}
			},
			{
				title: "Monto a abonar",
				render: function (data, type, row) {
					return parseFloat(row.total_abono).toFixed(2).toLocaleString();
				}
			},
			{
				title: "Fecha pago",
				render: function (data, type, row) {
					return utils.isDefined(row.fecha_pago) ? obtenerFechaMostrar(row.fecha_pago) : null;
				}
			},
			{
				title: "Monto pagado",
				render: function (data, type, row) {
					return utils.isDefined(row.total_pago) ? parseFloat(row.total_pago).toFixed(2).toLocaleString() : null;
				}
			},
			{
				title: "Estatus abono",
				data: "estatus_abono"
			},
			{
                title: "CFDI",
                render: function(data, type, row) {
                    return utils.isDefined(row.clave_cfdi) ? row.clave_cfdi + ' - ' + row.descripcion_cfdi : null;
                }
            },
			{
				title: "Acciones",
				render: function (data, type, row) {
					var btn_enganche = '';
					var btn_abonar = '';
					var btn_comprobante = '';
					if (estatus_cuentas_por_cobrar != 4) {
						if (row.tipo_abono_id == 1) {
							if (row.estatus_abono_id == 1) {
								var btn_enganche = '<button title="Pagar enganche" onclick="open_modal_enganche(this)" data-enganche_id="' + row.id + '" class="btn btn-default"><i class="fas fa-cash-register"></i></button>';
							} else if (row.estatus_abono_id == 3) {
								var btn_comprobante = '<button title="Imprimir comprobante"  onclick="imprimir_comprobante(this)" data-abono_id="' + row.id + '" data-cuenta_por_pagar_id="' + row.cuenta_por_pagar_id + '" class="btn btn-default"><i class="fas fa-file-pdf"></i></button>';
							}
						} else if (row.tipo_abono_id == 2) {
							if (row.estatus_abono_id == 1 || row.estatus_abono_id == 2) {
								var btn_abonar = '<button title="Realizar pago" onclick="open_modal_abono(this)" data-abono_id="' + row.id + '"  data-monto_moratorio="' + row.monto_moratorio + '" data-total_abono="' + row.total_abono + '" class="btn btn-default"><i class="fas fa-cash-register"></i></button>';
							} else if (row.estatus_abono_id == 3) {
								var btn_comprobante = '<button title="Imprimir comprobante"  onclick="imprimir_comprobante(this)" data-abono_id="' + row.id + '" data-cuenta_por_pagar_id="' + row.cuenta_por_pagar_id + '" class="btn btn-default"><i class="fas fa-file-pdf"></i></button>';
							}
						}
					} else {
						if (row.tipo_abono_id == 1) {
							if (row.estatus_abono_id == 3) {
								var btn_comprobante = '<button title="Imprimir comprobante"   onclick="imprimir_comprobante(this)" data-abono_id="' + row.id + '" data-cuenta_por_pagar_id="' + row.cuenta_por_pagar_id + '"  class="btn btn-default"><i class="fas fa-file-pdf"></i></button>';
							}
						} else if (row.tipo_abono_id == 2) {
							if (row.estatus_abono_id == 3) {
								var btn_comprobante = '<button title="Imprimir comprobante"   onclick="imprimir_comprobante(this)" data-abono_id="' + row.id + '" data-cuenta_por_pagar_id="' + row.cuenta_por_pagar_id + '"  class="btn btn-default"><i class="fas fa-file-pdf"></i></button>';
							}
						}
					}
					// let numero = row.id;
					// var numstring = numero.toString();
					//return row.estatus_abono_id == 3 ? numstring.padStart(10, "0") : null;
					return btn_enganche + btn_abonar + btn_comprobante;
				}
			},

		],
		"createdRow": function (row, data, dataIndex) {
			switch (data['estatus_abono_id']) {
				case 2:
					$(row).find('td:eq(6)').css('background-color', '#f6ffa4');
					break;
				case 3:
					$(row).find('td:eq(6)').css('background-color', '#8cdd8c');
					break;
				default:
					break;
			}
		}
	});
}

function  imprimir_comprobante(_this) {
    abono_id = $(_this).data('abono_id');
    window.location.href = PATH + '/caja/salidas/imprime_comprobante?abono_id=' + window.btoa(abono_id);
}

function  imprimir_estado_cuenta(_this) {
    cuenta_id = $(_this).data('cuenta_por_pagar_id');
    window.location.href = PATH + '/caja/salidas/imprime_estado_cuenta?cuenta_id=' + window.btoa(cuenta_id);
}

function obtenerFechaMostrar(fecha) {
	const dia = 2,
		mes = 1,
		anio = 0;
	fecha = fecha.split('T');
	fecha = fecha[0].split('-');
	return fecha[dia] + '/' + fecha[mes] + '/' + fecha[anio];
}


$("#pago").on('blur', function () {
	let pago = $("#pago").val();
	let total_pago = $("#total_pago").val();
	if (parseFloat(pago) < parseFloat(total_pago)) {
		toastr.error("El pago es menor al total a abonar")
		$("#btn-modal-abonar").attr('disabled', true);
		$("#pago").val(0);
	} else {
		let cambio = parseFloat(pago) - parseFloat(total_pago);
		$("#cambio").val(cambio.toFixed(2));
		$("#btn-modal-abonar").attr('disabled', false);
	}
});

$("#pago_enganche").on('blur', function () {
	let pago = $("#pago_enganche").val();
	let total_pago = $("#total_pago_enganche").val();
	if (parseFloat(pago) < parseFloat(total_pago)) {
		toastr.error("El pago es menor al total del enganche")
		$("#btn-modal-enganche").attr('disabled', true);
		$("#pago_enganche").val(0);
	} else {
		let cambio = parseFloat(pago) - parseFloat(total_pago);
		$("#cambio_enganche").val(cambio.toFixed(2));
		$("#btn-modal-enganche").attr('disabled', false);
	}
});


let form_abono = function () {
	let moratorio = $("#monto_moratorio").val() >= 1 ?  $("#monto_moratorio").val() : 0;
    let total =  (parseFloat(document.getElementById("total_pago").value) - parseFloat(moratorio));
	return {
		cuenta_por_pagar_id: cuenta_por_pagar_id,
		tipo_abono_id: document.getElementById("tipo_abono_id").value,
		total_pago: total,
		cfdi_id: document.getElementById("cfdi_id").value,
		tipo_pago_id: document.getElementById("tipo_pago_id").value,
		fecha_pago: document.getElementById("fecha_pago").value,
		estatus_abono_id: 3 //SE MARCA COMO PAGADO
	};
}

$("#btn-modal-abonar").on('click', function () {
	let total_pago = $("#total_pago").val();
	let minimo_abonar = $("#minimo_abonar").val();
	if (parseFloat(minimo_abonar) > parseFloat(total_pago)) {
		toastr.error("El monto minimo abonar es de: " + minimo_abonar);
		return false;
	}
	$(".invalid-feedback").html("");
	toastr.info("Realizando abono..");
	$.isLoading({
		text: "Realizando abono...."
	});
	let abono_id = $("#abono_id").val();
	ajax.put(`api/abonos-por-pagar/${abono_id}`, form_abono(), function (response, headers) {
		if (headers.status == 400) {
			$.isLoading("hide");
			return ajax.showValidations(headers);
		}
		let total_pago = document.getElementById("total_pago").value;
		if (parseFloat(total_pago) >= parseFloat(saldo_actual)) {
			ajax.put(`/api/cuentas-por-pagar/${cuenta_por_pagar_id}`, {
				estatus_cuenta_id: 2
			}, function (response, header) {
				if (header.status == 400) {
					$.isLoading("hide");
					return ajax.showValidations(header);
				}
				$.isLoading("hide");
				utils.displayWarningDialog(header.message, "success", function (data) {
					$('#modal-abonar').modal('hide')
					window.location.reload();
				})
			})
		} else {
			$.isLoading("hide");
			utils.displayWarningDialog(headers.message, "success", function (data) {
				$('#modal-abonar').modal('hide')
				window.location.reload();
			})
		}
	})
})

let form_enganche = function () {
	return {
		cuenta_por_pagar_id: cuenta_por_pagar_id,
		tipo_abono_id: document.getElementById("tipo_abono_id_enganche").value,
		total_pago: document.getElementById("total_pago_enganche").value,
		tipo_pago_id: document.getElementById("tipo_pago_id_enganche").value,
		fecha_pago: document.getElementById("fecha_pago_enganche").value,
		estatus_abono_id: 3 //SE MARCA COMO PAGADO
	};
}

$("#btn-modal-enganche").on('click', function () {
	$(".invalid-feedback").html("");
	toastr.info("Realizando enganche..");
	$.isLoading({
		text: "Realizando enganche...."
	});
	let enganche_id = $("#enganche_id").val();
	ajax.put(`api/abonos-por-pagar/${enganche_id}`, form_enganche(), function (response, headers) {
		if (headers.status == 400) {
			$.isLoading("hide");
			return ajax.showValidations(headers);
		}
		$.isLoading("hide");
		utils.displayWarningDialog(headers.message, "success", function (data) {
			$('#modal-enganche').modal('hide')
			window.location.reload();
		})
	})
})

function open_modal_abono(_this) {
	$('#modal-abonar').modal('show');

    $("#abono_id").val($(_this).data('abono_id'));
    let tot= $(_this).data('total_abono');
    let monto = $(_this).data('monto_moratorio');
    let moratorio = monto != null ? monto : 0;
    $("#monto_moratorio").val(monto);
    let total_pago = parseFloat(tot) + parseFloat(moratorio);
    $("#total_pago").val(total_pago.toFixed(2));
    $("#minimo_abonar").val(total_pago.toFixed(2));
}

function open_modal_enganche(_this) {
	$("#enganche_id").val($(_this).data('enganche_id'));
	$('#modal-enganche').modal('show');
}

this.inicializaTabla();
$("#proveedor_id").select2();
