<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
function money_format($value) {
	if ($value<0) return "-".asDollars(-$value);
	return '$' . $value;
  }