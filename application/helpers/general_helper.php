<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

function procesarResponseApiJsonToArray($response_data)
{
	return json_decode($response_data);
}


function accessToDashboard()
{
	$ci = &get_instance();
	if (isset($ci->session->all_userdata()['menu'])) {
		$menu = $ci->session->all_userdata()['menu'];
		foreach ($menu as $key => $item) {
			if ($item->modulo_id == 1) {
				return 1;
			}
		}
	}

	return 0;
}

function renderInputText($type, $name, $label, $value, $reaonly = false)
{
	echo "<div class='form-group'>";
	echo "<label for='" . $name . "'>" . $label . "</label>";
	if ($reaonly) {
		echo "<input id='" . $name . "' name='" . $name . "' readonly type='" . $type . "' class='form-control' value='" . $value . "'>";
	} else {
		echo "<input id='" . $name . "' name='" . $name . "' type='" . $type . "' class='form-control' value='" . $value . "'>";
	}
	echo "<div id='" . $name . "_error' class='invalid-feedback'></div>";
	echo "</div>";
}

function renderInputTextArea($name, $label, $value, $reaonly = false)
{
	echo "<div class='form-group'>";
	echo "<label for='" . $name . "'>" . $label . "</label>";
	if ($reaonly) {
		echo form_textarea('', $value, 'class="form-control" readonly');
	} else {
		echo form_textarea(['id' => $name, 'name' => $name], $value, 'class="form-control"');
	}
	echo "<div id='" . $name . "_error' class='invalid-feedback'></div>";
	echo "</div>";
}

function porcentajeProducto($descuento, $precio_producto, $cantidad = 1)
{
	$des = (int) $descuento / 100;
	$precio_total = (100 * $des) + $precio_producto;
	return $precio_total * $cantidad;
}

function obtenerPorcentaje($porcentaje_descuento, $precio_producto, $cantidad = 1)
{

	$descuento = (int) $porcentaje_descuento / 100;
	$tot_porcentaje = $precio_producto * $descuento;
	return $precio_producto + $tot_porcentaje;
}

function totalPrecioCantidadProducto($precio_producto, $cantidad)
{
	return $precio_producto * $cantidad;
}

function obtenerFechaEnLetra($fecha)
{
	$num = date("j", strtotime($fecha));
	$anno = date("Y", strtotime($fecha));
	$mes = array('enero', 'febrero', 'marzo', 'abril', 'mayo', 'junio', 'julio', 'agosto', 'septiembre', 'octubre', 'noviembre', 'diciembre');
	$mes = $mes[(date('m', strtotime($fecha)) * 1) - 1];

	return  $num . ' de ' . $mes . ' del ' . $anno;
}

function renderSelectArray($name, $label = null, $dataArray, $nameValue, $nameOption, $selected = null, $disabled = false)
{
	echo "<div class='form-group'>";
	if ($label != null) {
		echo "<label for='" . $name . "'>" . $label . "</label>";
	}
	$disabled = $disabled ? "disabled" : null;
	echo "<select " . $disabled . " class='form-control' name='" . $name . "' id='" . $name . "'>";
	if (is_array($dataArray)) {
		echo "<option value=''> Seleccionar ..</option>";
		foreach ($dataArray as $row) :
			if ($selected != null) {
				if ($row->{$nameValue} == $selected) {
					echo "<option value='" . $row->{$nameValue} . "' selected>" . $row->{$nameOption} . "</option>";
				} else {
					echo "<option value='" . $row->{$nameValue} . "'>" . $row->{$nameOption} . "</option>";
				}
			} else {
				echo "<option value='" . $row->{$nameValue} . "'>" . $row->{$nameOption} . "</option>";
			}
		endforeach;
	}
	echo "</select>";
	echo "<div id='" . $name . "_error' class='invalid-feedback'></div>";
	echo "</div>";
}
