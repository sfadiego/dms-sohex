@layout('tema_luna/layout')
@section('contenido')
<div class="container-fluid panel-body">
    <h1 class="mt-4"><?php echo isset($modulo) ? $modulo : "" ?></h1>
    <ol class="breadcrumb mb-4">
        <li class="breadcrumb-item"><?php echo isset($submodulo) ? $submodulo : "" ?></li>
        <li class="breadcrumb-item"><?php echo isset($titulo) ? $titulo : "" ?></li>
        <li class="breadcrumb-item active"><?php echo isset($subtitulo) ? $subtitulo : "" ?></li>
    </ol>

    <div class="row">
        <div class="col-md-12">
            <form id="recepcion_unidad" data-id="{{ isset($data->id) ? $data->id : '' }}"> <!-- enctype="multipart/form-data"  -->
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="">Usuario que recibe:</label>
                            <input type="text" class="form-control" value="<?php if(isset($data->usuario_recibe)){print_r($data->usuario_recibe);}else{echo $usuario_recibe;} ?>"  id="usuario_recibe" name="usuario_recibe" placeholder="">
                            <div id="usuario_recibe_error" class="invalid-feedback"></div>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="form-group">
                            <label for="">Fecha de recepción:</label>
                            <input type="date" class="form-control" value="<?php if(isset($data->fecha_recepcion)){print_r($data->fecha_recepcion);}else {echo date('Y-m-d');} ?>" id="fecha_recepcion" name="fecha_recepcion" placeholder="">
                            <div id="fecha_recepcion_error" class="invalid-feedback"></div>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="form-group">
                            <label for="">Estado:</label>
                            <select class="form-control" id="id_estado" name="id_estado" style="width: 100%;">
                                <option value="2">Seminuevo</option>
                            </select>
                            <div id="id_estado_error" class="invalid-feedback"></div>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-6">
                        <label for="" style="font-size: 13px;">Unidad (Descripción Larga):</label>
                        <textarea type="text" class="form-control" id="unidad_descripcion" name="unidad_descripcion" rows="2"><?php if(isset($data->unidad_descripcion)){print_r($data->unidad_descripcion);} ?></textarea>
                        <div id="unidad_descripcion_error" class="invalid-feedback"></div>
                    </div>
                    <div class="col-md-3">
                        <div class="form-group">
                            <label for="">Kilometraje:</label>
                            <input type="text" maxlength="5" onkeypress='return event.charCode >= 48 && event.charCode <= 57' class="form-control" value="<?php if(isset($data->kilometraje)){print_r($data->kilometraje);} ?>"  id="kilometraje" name="kilometraje" placeholder="">
                            <div id="kilometraje_error" class="invalid-feedback"></div>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="form-group">
                            <label for="">Catálogo:</label>
                            <select class="form-control" id="catalogo_id" name="catalogo_id" onchange="formularEconomico()" style="width: 100%;" <?php if(isset($data->catalogo_id)){echo "disabled";} ?> >
                                <option value="">Selecionar ...</option>
                                @if(!empty($cat_unidades))
                                    @foreach ($cat_unidades as $catalogo)
                                        <option value="{{$catalogo->id}}" <?php if(isset($data->catalogo_id)){if($data->catalogo_id == $catalogo->id) echo "selected";} ?>>[{{$catalogo->clave}}]. {{$catalogo->nombre}}</option>
                                    @endforeach
                                @endif
                            </select>
                            <div id="catalogo_id_error" class="invalid-feedback"></div>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="">Marca:</label>
                            <select class="form-control" id="marca_id" name="marca_id" style="width: 100%;">
                                <option value="">Selecionar ...</option>
                                @if(!empty($cat_marcas))
                                    @foreach ($cat_marcas as $marca)
                                        <option value="{{$marca->id}}" <?php if(isset($data->marca_id)){if($data->marca_id == $marca->id) echo "selected";} ?>>{{$marca->nombre}}</option>
                                    @endforeach
                                @endif
                            </select>
                            <div id="id_marca_error" class="invalid-feedback"></div>
                        </div>
                    </div> 
                    <!--<div class="col-md-4">
                        <div class="form-group">
                            <label for="">Modelo:</label>
                            <select class="form-control" id="modelo_id" name="modelo_id" style="width: 100%;">
                                <option value="">Selecionar ...</option>
                                @if(!empty($cat_modelo))
                                    @foreach ($cat_modelo as $modelo)
                                        <option value="{{$modelo->id}}" <?php if(isset($data->modelo_id)){if($data->modelo_id == $modelo->id) echo "selected";} ?> >{{$modelo->nombre}}</option>
                                    @endforeach
                                @endif
                            </select>
                            <div id="id_modelo_error" class="invalid-feedback"></div>
                        </div>
                    </div>-->
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="">Año:</label>
                            <select class="form-control" id="anio_id" name="anio_id" style="width: 100%;">
                                <option value="">Selecionar ...</option>
                                @if(!empty($cat_anio))
                                    @foreach ($cat_anio as $anio)
                                        <option value="{{$anio->id}}" <?php if(isset($data->anio_id)){if($data->anio_id == $anio->id) echo "selected";} ?> >{{$anio->nombre}}</option>
                                    @endforeach
                                @endif
                            </select>
                            <div id="anio_id_error" class="invalid-feedback"></div>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="">Combustible:</label>
                            <select class="form-control" id="combustible" name="combustible" style="width: 100%;">
                                <option value="">Selecionar ...</option>
                                @if(!empty($cat_combustible))
                                    @foreach ($cat_combustible as $combustible)
                                        <option value="{{$combustible->id}}" <?php if(isset($data->combustible)){if($data->combustible == $combustible->id) echo "selected";} ?>>{{$combustible->nombre}}</option>
                                    @endforeach
                                @endif
                            </select>
                            <div id="combustible_error" class="invalid-feedback"></div>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="">Color:</label>
                            <select class="form-control" id="color_id" name="color_id" style="width: 100%;">
                                <option value="">Selecionar ...</option>
                                @if(!empty($cat_color))
                                    @foreach ($cat_color as $color)
                                        <option value="{{$color->id}}" <?php if(isset($data->color_id)){if($data->color_id == $color->id) echo "selected";} ?> > [ {{$color->clave}} ]&nbsp;&nbsp;  {{$color->nombre}}</option>
                                    @endforeach
                                @endif
                            </select>
                            <div id="color_id_error" class="invalid-feedback"></div>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="">Motor:</label>
                            <input type="text" class="form-control" value="<?php if(isset($data->motor)){print_r($data->motor);} ?>"  id="motor" name="motor" placeholder="">
                            <div id="motor_error" class="invalid-feedback"></div>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="">N° Motor:</label>
                            <input type="text" class="form-control" value="<?php if (isset($data->n_motor)) {print_r($data->n_motor);} ?>" id="n_motor" name="n_motor" placeholder="">
                            <div id="n_motor_error" class="invalid-feedback"></div>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="">Transmisión:</label>
                            <!--<input type="text" class="form-control" value="<?php if(isset($data->transmision)){print_r($data->transmision);} ?>"  id="transmision" name="transmision" placeholder="">-->
                            <select class="form-control" id="transmision" name="transmision" style="width: 100%;">
                                <option value="">Selecionar ...</option>
                                <option value="AUTOMÁTICA" <?php if(isset($data->transmision)){ if($data->transmision == "AUTOMÁTICA") echo "selected";} ?> >AUTOMÁTICA </option>
                                <option value="ESTÁNDAR" <?php if(isset($data->transmision)){ if($data->transmision == "ESTÁNDAR") echo "selected";} ?> >ESTÁNDAR </option>
                                <option value="CVT" <?php if(isset($data->transmision)){ if($data->transmision == "CVT") echo "selected";} ?> >CVT </option>
                                <option value="SEMI-AUTOMÁTICA" <?php if(isset($data->transmision)){ if($data->transmision == "SEMI-AUTOMÁTICA") echo "selected";} ?> >SEMI-AUTOMÁTICA </option>
                            </select>
                            <div id="transmision_error" class="invalid-feedback"></div>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="">N° cilindros:</label>
                            <input type="text" class="form-control" maxlength="2" onkeypress='return event.charCode >= 48 && event.charCode <= 57' value="<?php if(isset($data->numero_cilindros)){print_r($data->numero_cilindros);} ?>"  id="numero_cilindros" name="numero_cilindros" placeholder="">
                            <div id="numero_cilindros_error" class="invalid-feedback"></div>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="">Capacidad:</label>
                            <input type="text" class="form-control" value="<?php if(isset($data->capacidad)){print_r($data->capacidad);} ?>"  id="capacidad" name="capacidad" placeholder="">
                            <div id="capacidad_error" class="invalid-feedback"></div>
                        </div>
                    </div>
                    
                </div>

                <div class="row">
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="">N° de puertas:</label>
                            <input type="text" class="form-control" onkeypress='return event.charCode >= 48 && event.charCode <= 57' value="<?php if(isset($data->numero_puertas)){print_r($data->numero_puertas);} ?>" id="numero_puertas" name="numero_puertas" placeholder="">
                            <div id="numero_puertas_error" class="invalid-feedback"></div>
                        </div>
                    </div>
                    
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="">Precio Costo:</label>
                            <input type="text" class="form-control" onkeypress='return event.charCode >= 46 && event.charCode <= 57' value="<?php if(isset($data->precio_costo)){print_r($data->precio_costo);} ?>" id="precio_costo" name="precio_costo" placeholder="">
                            <div id="precio_costo_error" class="invalid-feedback"></div>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="">Precio Venta:</label>
                            <input type="text" class="form-control" onkeypress='return event.charCode >= 48 && event.charCode <= 57' value="<?php if(isset($data->precio_venta)){print_r($data->precio_venta);} ?>" id="precio_venta" name="precio_venta" placeholder="">
                            <div id="precio_venta_error" class="invalid-feedback"></div>
                        </div>
                    </div>
                </div>
                
                <div class="row">
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="">VIN:</label>
                            <input type="text" class="form-control" value="<?php if(isset($data->vin)){print_r($data->vin);} ?>"  id="vin" name="vin" placeholder="">
                            <div id="vin_error" class="invalid-feedback"></div>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="">Serie Corta:</label>
                            <input type="text" class="form-control" value="<?php if(isset($data->serie_corta)){print_r($data->serie_corta);} ?>"  id="serie_corta" name="serie_corta" placeholder="">
                            <div id="serie_corta_error" class="invalid-feedback"></div>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for=""># Económico:</label>
                            <input type="text" class="form-control" value="<?php if(isset($data->numero_economico)){print_r($data->numero_economico);} ?>" id="numero_economico" name="numero_economico" placeholder="">
                            <div id="numero_economico_error" class="invalid-feedback"></div>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="">Ubitación:</label>
                            <select class="form-control" id="id_ubicacion" name="id_ubicacion" style="width: 100%;">
                                <option value="">Selecionar ...</option>
                                @if(!empty($cat_ubicacion))
                                    @foreach ($cat_ubicacion as $ubicacion)
                                        <option value="{{$ubicacion->id}}" <?php if(isset($data->id_ubicacion)){if($data->id_ubicacion == $ubicacion->id) echo "selected";} ?>>{{$ubicacion->nombre}}</option>
                                    @endforeach
                                @endif
                            </select>
                            <div id="id_ubicacion_error" class="invalid-feedback"></div>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="">Ubicación Llaves:</label>
                            <select class="form-control" id="id_ubicacion_llaves" name="id_ubicacion_llaves" style="width: 100%;">
                                <option value="">Selecionar ...</option>
                                @if(!empty($cat_llaves))
                                    @foreach ($cat_llaves as $llaves)
                                        <option value="{{$llaves->id}}" <?php if(isset($data->id_ubicacion_llaves)){if($data->id_ubicacion_llaves == $llaves->id) echo "selected";} ?>>{{$llaves->nombre}}</option>
                                    @endforeach
                                @endif
                            </select>
                            <div id="id_ubicacion_llaves_error" class="invalid-feedback"></div>
                        </div>
                    </div>
                    
                </div>

                <div class="row">
                    <div class="col-md-12" align="center">
                        <br>
                        @if (isset($data->id))
                            <button type="button" id="editar_registro" class="btn btn-success col-md-4"> Actualizar </button>
                        @else
                            <button type="button" id="guardar_registro" class="btn btn-success col-md-4"> Guardar </button>
                        @endif

                        <input type="hidden" id="consecutivo" value="<?php if (isset($consecutivo)) {echo $consecutivo;} else {echo '0';} ?>">
                        
                        <div id="mensaje" class="alert alert-success" role="alert" style="display:none;" ></div>
                    </div>
                </div>
                        
            </form>
        </div>
    </div>
</div>
@endsection

@section('scripts')    
    <script>
        //Para guardar registro por primera vez
        $("#guardar_registro").on('click', function() {
            $(".invalid-feedback").html("");

            var validar_costo = costos_validar();
            if (validar_costo) {
                ajax.post('api/seminuevos', procesarRegistro(), function(response, headers) {
                    if (headers.status == 400) {
                        return ajax.showValidations(headers);
                    }

                    //var titulo = (headers.status != 200) ? headers.message : "Proceso completado con éxito";
                    //var alerta = (headers.status != 200) ? "warning" : "success";
                    utils.displayWarningDialog("Proceso completado con éxito", "success", function(data) {
                        return window.location.href = base_url + 'seminuevos/unidades/index';
                    })
                })
            } 
        });

        //Para actualizar un registro
        $("#editar_registro").on('click', function() {
            $(".invalid-feedback").html("");
            var id = $("#recepcion_unidad").data('id');

            var validar_costo = costos_validar();
            if (validar_costo) { 
                ajax.put('api/seminuevos/'+id, procesarRegistro(), function(response, headers) {
                    if (headers.status == 400) {
                        return ajax.showValidations(headers);
                    }
                    
                    //var titulo = (headers.status != 200) ? headers.message : "Proceso completado con éxito";
                    utils.displayWarningDialog("Proceso completado con éxito", "success", function(data) {
                        return window.location.href = base_url + 'seminuevos/unidades/index';
                    })
                })
            }
        });

        function costos_validar() {
            var precio_costo = parseFloat($("#precio_costo").val());
            var precio_venta = parseFloat($("#precio_venta").val());
            
            var validacion = true;

            if (precio_costo >= precio_venta) {
                var error = $("#precio_venta_error");
                $("#precio_venta_error").css("display","inline-block");
                error.append('<label class="form-text text-danger">Costos incoherentes, verificar montos.</label>');
                validacion = false;
            }else{
                var error = $("#precio_venta_error");
                error.empty();
            }

            return validacion;
        }

        //Armamos el formulario a enviar
        let procesarRegistro = function() {
            let newArray = {
                usuario_recibe: document.getElementById("usuario_recibe").value,
                fecha_recepcion: document.getElementById("fecha_recepcion").value,
                id_estado: document.getElementById("id_estado").value,
                marca_id: document.getElementById("marca_id").value,
                modelo_id: "1", //document.getElementById("modelo_id").value,
                anio_id: document.getElementById("anio_id").value,
                color_id: document.getElementById("color_id").value,
                unidad_descripcion: document.getElementById("unidad_descripcion").value,
                numero_puertas: document.getElementById("numero_puertas").value,
                combustible: document.getElementById("combustible").value,
                n_motor: document.getElementById("n_motor").value,
                motor: document.getElementById("motor").value,
                transmision: document.getElementById("transmision").value,
                numero_cilindros: document.getElementById("numero_cilindros").value,
                catalogo_id: document.getElementById("catalogo_id").value,
                capacidad: document.getElementById("capacidad").value,
                vin: document.getElementById("vin").value,
                serie_corta: document.getElementById("serie_corta").value,
                numero_economico: document.getElementById("numero_economico").value,
                id_ubicacion: document.getElementById("id_ubicacion").value,
                id_ubicacion_llaves: document.getElementById("id_ubicacion_llaves").value,

                precio_costo: document.getElementById("precio_costo").value,
                precio_venta: document.getElementById("precio_venta").value,

            };

            return newArray;
        }

        //Creamos clave para numero economico
        function formularEconomico() {
            var contenedor = document.getElementById("catalogo_id");
            var etiqueta = contenedor.options[contenedor.selectedIndex].text;
            //console.log(etiqueta);

            var datos = etiqueta.split(".");
            var unidad = datos[0].replace(/[[\]\\]/gi, '');
            //console.log(unidad);
            //recuperar_consecutivo();

            //Formamos el numero economico
            var consecutivo = parseInt($("#consecutivo").val());
            var folio = "";
            consecutivo++;

            if (consecutivo < 10) {
                folio = "S000" + consecutivo;
            } else if ((consecutivo > 9) && (consecutivo < 100)) {
                folio = "S00" + consecutivo;
            } else if ((consecutivo > 99) && (consecutivo < 1000)) {
                folio = "S0" + consecutivo;
            } else {
                folio = "S" + consecutivo;
            }

            var numero_economico = unidad + "" + folio;
            $("#numero_economico").val(numero_economico);
            //console.log(n_economico);
        }

        function recuperar_consecutivo() {
            console.log("entro en la funcion");
            console.log(base_url);

            $.ajax({
                url: base_url + "autos/Recepcion/ultimo_id",
                method: 'post',
                data: {
                    dato: "",
                },
                success: function(resp) {
                    console.log("respuesta: " + resp);

                    if (resp.indexOf("handler           </p>") < 1) {
                        resultado = resp;
                    } else {
                        resultado = "0";
                    }

                    $('#consecutivo').val(resultado);
                    //Cierre de success
                },
                error: function(error) {
                    console.log(error);
                    //Cierre del error
                }
                //Cierre del ajax
            });

            return true;
        }
    </script>
@endsection