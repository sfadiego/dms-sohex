<?php

defined('BASEPATH') or exit('No direct script access allowed');
class Reportes extends MX_Controller
{
  public function __construct()
  {
    parent::__construct();
    date_default_timezone_set('America/Mexico_City');
  }

  public function diario()
  {
  	$data['titulo'] = "Contabilidad/Reportes/Diario de movimientos";
    $this->blade->render('reportes/diario',$data);
  }

  public function mayor()
  {
    $data['titulo'] = "Contabilidad/Reportes/Mayor mensual";
    $this->blade->render('reportes/mayor',$data);
  }

  public function balanza()
  {
    $data['titulo'] = "Contabilidad/Reportes/Balanza de Comprobación";
    $this->blade->render('reportes/balanza',$data);
  }


   public function cuentas()
  {
    $data['titulo'] = "Contabilidad/Reportes/Catalogo cuentas";
    $this->blade->render('reportes/cuentas',$data);
  }


  public function historico()
  {
    $data['titulo'] = "Contabilidad/Reportes/Historico Mayor";
    $data['historico'] = "menu_cont_rep_hist";
    $this->blade->render('reportes/historico',$data);
  }

  public function aux()
  {
    $data['titulo'] = "Contabilidad/Reportes/Aux. VS Conta";
    $data['historico'] = "menu_cont_rep_aux";
    $this->blade->render('reportes/historico',$data);
  }

  public function estado_financiero()
  {
    $data['titulo'] = "Contabilidad/Reportes/Estado financiero";
    $data['historico'] = "menu_cont_rep_estad";
    $this->blade->render('reportes/historico',$data);
  }

  
}