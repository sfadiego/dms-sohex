@layout('tema_luna/layout')
@section('contenido')
<div class="container-fluid panel-body">
    <h1 class="mt-4"><?php echo isset($modulo) ? $modulo : "" ?></h1>
    <ol class="breadcrumb mb-4">
        <li class="breadcrumb-item"><?php echo isset($submodulo) ? $submodulo : "" ?></li>
        <li class="breadcrumb-item active"><?php echo isset($subtitulo) ? $subtitulo : "" ?></li>
    </ol>
    
    <div class="row">
        <div class="col-md-12">
            <table class="table table-bordered" id="tbl_salidas" width="100%" cellspacing="0">
                <thead>
                    <tr>
                        <th>#</th>
                        <th>Nombre</th>
                        <th>usuario</th>
                        <th>Telefono</th>
                        <th>Rol</th>
                        <th>Activo</th>
                        <th>-</th>
                    </tr>
                </thead>
                <tbody>
                    @if(!empty($usuarios))
                        @foreach ($usuarios as $key => $usuario)
                            <tr>
                                <td>{{ $usuario->id }} </td>
                                <td>{{ $usuario->nombre .' '.$usuario->apellido_paterno }} </td>
                                <td>{{ $usuario->usuario }} </td>
                                <td>{{ $usuario->telefono }} </td>
                                <td>{{ $usuario->rol }} </td>
                                <td>{{ $usuario->activo == 1 ? "activo" : '--'  }} </td>
                                <td>
                                    <a href="{{ base_url('usuarios/editarUsuario/'.$usuario->id) }}" class="btn btn-warning">
                                        <i class="fa fa-list"></i>
                                    </a>
                                </td>
                            </tr>
                        @endforeach
                    @else
                        <tr>
                            <td>
                                No se encontraron resultados
                            </td>
                        </tr>
                    @endif
                </tbody>
                <tfoot>
                    <tr>
                        <th>#</th>
                        <th>Nombre</th>
                        <th>usuario</th>
                        <th>Telefono</th>
                        <th>Rol</th>
                        <th>Activo</th>
                        <th>-</th>
                    </tr>
                </tfoot>
            </table>
        </div>
    </div>
</div>
@endsection

@section('scripts')
<script>
     $("#rol_id").on('change', function(){
            $('input[type=checkbox]').each(function() {
                $(this).prop('checked',false);
            });

            let rol = $("#rol_id").val();
            ajax.get('api/roles/modulos-by-rol?rol_id='+rol, {}, function(response, headers){
                if(response.length > 0){
                    response.map(item => {
                        if(item.modulo_id){
                            document.getElementById("modulo_"+item.modulo_id).checked = true;
                        }
                    })
                }
            })
        });
</script>
@endsection