@layout('tema_luna/layout')
@section('contenido')
<div class="container-fluid panel-body">
    <h1 class="mt-4"><?php echo isset($modulo) ? $modulo : "" ?></h1>
    <ol class="breadcrumb mb-4">
        <li class="breadcrumb-item"><?php echo isset($titulo) ? $titulo : "" ?></li>
        <li class="breadcrumb-item active"><?php echo isset($subtitulo) ? $subtitulo : "" ?></li>
    </ol>

    <div class="row">
        <div class="col-md-12" align="center">
            <form id="form-modelos" data-id="<?php echo isset($data->id) ? $data->id : ''?>">
                <div class="row">
                    <div class="col-md-3">                        
                        <div class="form-group">
                            <label for="">Clave:</label>
                            <input type="text" class="form-control" value="<?php if(isset($data->clave)) {print_r($data->clave);} ?>" id="clave" name="clave" style="text-transform: uppercase;" >
                            <div id="clave_error" class="invalid-feedback"></div>
                        </div>
                    </div>
                    <div class="col-md-9">                        
                        <div class="form-group">
                            <label for="">Modelo:</label>
                            <input type="text" class="form-control" value="<?php if(isset($data->nombre)) {print_r($data->nombre);} ?>" id="nombre" name="nombre" style="text-transform: uppercase;" >
                            <div id="nombre_error" class="invalid-feedback"></div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-3">                        
                        <div class="form-group">
                            <label for="">Tiempo de lavado:</label>
                            <input type="text" class="form-control" value="<?php if(isset($data->tiempo_lavado)) {print_r($data->tiempo_lavado);}else {echo '0';} ?>" id="tiempo_lavado" name="tiempo_lavado" >
                            <div id="tiempo_lavado_error" class="invalid-feedback"></div>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="">Categoría:</label>
                            <select class="form-control" id="categoria_id" name="categoria_id" style="width: 100%;">
                                <option value="">Selecionar ...</option>
                                @if(!empty($cat_unidades))
                                    @foreach ($cat_unidades as $categorias)
                                        <option value="{{$categorias->id}}" <?php if(isset($data->categoria_id)){if($data->categoria_id == $categorias->id) echo "selected";} ?>>{{$categorias->nombre}}</option>
                                    @endforeach
                                @endif
                            </select>
                            <div id="id_marca_error" class="invalid-feedback"></div>
                        </div>
                    </div> 
                </div>

                <div class="row">
                    <div class="col-md-12" align="center">
                        <br>
                        @if (isset($data->id))
                            <button id="btn-actualizar" class="btn btn-success" type="button">Actualizar</button>
                        @else
                            <button id="btn-guardar" class="btn btn-success" type="button">Guardar</button>
                        @endif
                        
                        <div id="mensaje" class="alert alert-success" role="alert" style="display:none;" ></div>
                    </div>
                </div>
            </form>
            <hr>
        </div>
    </div>
</div>
@endsection

@section('scripts')
<script>
   
    //Para guardar registro por primera vez
    $("#btn-guardar").on('click', function() {
        $(".invalid-feedback").html("");
        ajax.post('api/catalogo-autos', form(), function(response, headers) {
            if (headers.status == 400) {
                return ajax.showValidations(headers);
            }

            var titulo = (headers.status != 200) ? headers.message : "Proceso completado con éxito";
            utils.displayWarningDialog("Proceso completado con éxito", "success", function(data) {
                return window.location.href = base_url + 'catalogos/AutoController/index';
            })
        })
    });

    //Para actualizar un registro
    $("#btn-actualizar").on('click', function() {
        $(".invalid-feedback").html("");
        var id = $("#form-modelos").data('id');
        ajax.put('api/catalogo-autos/'+id, form(), function(response, headers) {
            if (headers.status == 400) {
                return ajax.showValidations(headers);
            }
            
            var titulo = (headers.status != 200) ? headers.message : "Proceso completado con éxito";
            utils.displayWarningDialog("Proceso completado con éxito", "success", function(data) {
                return window.location.href = base_url + 'catalogos/AutoController/index';
            })
        })

    });

    let form = function() {
         return  {
            nombre: document.getElementById("nombre").value.toUpperCase(),
            clave: document.getElementById("clave").value.toUpperCase(),
            tiempo_lavado: document.getElementById("tiempo_lavado").value,
            categoria_id: document.getElementById("categoria_id").value,
        };
    }

</script>
@endsection