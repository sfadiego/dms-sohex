@layout('tema_luna/layout')
@section('contenido')
<div class="container-fluid panel-body">
    <h1 class="mt-4"><?php echo isset($modulo) ? $modulo : "" ?></h1>
    <ol class="breadcrumb mb-4">
        <li class="breadcrumb-item"><?php echo isset($titulo) ? $titulo : "" ?></li>
        <li class="breadcrumb-item active"><?php echo isset($subtitulo) ? $subtitulo : "" ?></li>
    </ol>

    <div class="row">
        <div class="col-md-12" align="center">
            <form id="form-registro" data-id="<?php echo isset($data->id) ? $data->id : ''?>">
                <div class="row">
                    <div class="col-md-3">                        
                        <div class="form-group">
                            <label for="">Clave:</label>
                            <input type="text" class="form-control" value="<?php if(isset($data->clave)) {print_r($data->clave);} ?>" id="clave" name="clave" style="text-transform: uppercase;" >
                            <div id="clave_error" class="invalid-feedback"></div>
                        </div>
                    </div>
                    <div class="col-md-9">                        
                        <div class="form-group">
                            <label for="">Descripcion:</label>
                            <input type="text" class="form-control" value="<?php if(isset($data->nombre)) {print_r($data->nombre);} ?>" id="nombre" name="nombre" style="text-transform: uppercase;" >
                            <div id="nombre_error" class="invalid-feedback"></div>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-12" align="center">
                        <br>
                        @if (isset($data->id))
                            <button id="btn-actualizar" class="btn btn-success" type="button">Actualizar</button>
                        @else
                            <button id="btn-guardar" class="btn btn-success" type="button">Guardar</button>
                        @endif
                        
                        <div id="mensaje" class="alert alert-success" role="alert" style="display:none;" ></div>
                    </div>
                </div>
            </form>
            <hr>
        </div>
    </div>
</div>
@endsection

@section('scripts')
<script>
   
    //Para guardar registro por primera vez
    $("#btn-guardar").on('click', function() {
        $(".invalid-feedback").html("");
        ajax.post('api/catalogo-clave-cliente', form(), function(response, headers) {
            if (headers.status == 400) {
                return ajax.showValidations(headers);
            }

            var titulo = (headers.status != 200) ? headers.message : "Proceso completado con éxito";
            utils.displayWarningDialog("Proceso completado con éxito", "success", function(data) {
                return window.location.href = base_url + 'catalogos/ClavesClienteController/index';
            })
        })
    });

    //Para actualizar un registro
    $("#btn-actualizar").on('click', function() {
        $(".invalid-feedback").html("");
        var id = $("#form-registro").data('id');
        ajax.put('api/catalogo-clave-cliente/'+id, form(), function(response, headers) {
            if (headers.status == 400) {
                return ajax.showValidations(headers);
            }
            
            var titulo = (headers.status != 200) ? headers.message : "Proceso completado con éxito";
            utils.displayWarningDialog("Proceso completado con éxito", "success", function(data) {
                return window.location.href = base_url + 'catalogos/ClavesClienteController/index';
            })
        })

    });

    let form = function() {
         return  {
            nombre: document.getElementById("nombre").value.toUpperCase(),
            clave: document.getElementById("clave").value.toUpperCase(),
        };
    }

</script>
@endsection