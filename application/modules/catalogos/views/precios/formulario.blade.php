@layout('tema_luna/layout')
@section('contenido')
<div class="container-fluid panel-body">
    <h1 class="mt-4"><?php echo isset($titulo) ? $titulo : "" ?></h1>
    <ol class="breadcrumb mb-4">
        <li class="breadcrumb-item active"><?php echo isset($titulo) ? $titulo : "" ?></li>
    </ol>

    <div class="row">
        <div class="col-md-12">
            <form id="form-precios" data-id="<?php echo isset($data->id) ? $data->id : ''?>" method="post">
                <h3>Vendedor</h3>
                <?php echo renderInputText("text", "precio_publico", "Precio publico", isset($data->precio_publico) ? $data->precio_publico : '', false); ?>
                <?php echo renderInputText("text", "precio_mayoreo", "Precio mayoreo", isset($data->precio_mayoreo) ? $data->precio_mayoreo : '', false); ?>
                <?php echo renderInputText("text", "precio_interno", "Precio interno", isset($data->precio_interno) ? $data->precio_interno : '', false); ?>
                <?php echo renderInputText("text", "precio_taller", "Precio Taller", isset($data->precio_taller) ? $data->precio_taller : '', false); ?>
                <?php echo renderInputText("text", "precio_otras_distribuidoras", "Otras distribuidoras", isset($data->precio_otras_distribuidoras) ? $data->precio_otras_distribuidoras : '', false); ?>
                <?php echo renderInputText("text", "impuesto", "Impuesto", isset($data->impuesto) ? $data->impuesto : '', false); ?>
                @if (isset($data->id))
                <button id="btn-actualizar" class="btn btn-primary" type="button">Actualizar</button>
                    @else
                <button id="btn-guardar" class="btn btn-primary" type="button">Guardar</button>
                @endif
            </form>
            <hr>
        </div>
    </div>
</div>
@endsection

@section('scripts')
<script>
   
    $("#btn-guardar").on('click', function() {
        $(".invalid-feedback").html("");
        ajax.post(`/api/precios/`,form(), function(response, headers) {
            if (headers.status == 400) {
                return ajax.showValidations(headers);
            }

            utils.displayWarningDialog(headers.message,"success",function(data){
                return window.location.href = base_url + `catalogos/preciosController`;
            })
        })
    })

    $("#btn-actualizar").on('click', function() {
        $(".invalid-feedback").html("");
        var id = $("#form-precios").data('id');
        ajax.put(`/api/precios/${id}`,form(), function(response, headers) {
           if (headers.status == 400) {
               return ajax.showValidations(headers);
           }

           utils.displayWarningDialog(headers.message,"success",function(data){
               return window.location.href = base_url + `catalogos/preciosController`;
           })
        })

   })

    let form = function() {
         return  {
            precio_publico: document.getElementById("precio_publico").value,
            precio_mayoreo: document.getElementById("precio_mayoreo").value,
            precio_interno: document.getElementById("precio_interno").value,
            precio_taller: document.getElementById("precio_taller").value,
            precio_otras_distribuidoras: document.getElementById("precio_otras_distribuidoras").value,
            impuesto: document.getElementById("impuesto").value,
        };
    }


   
</script>
@endsection