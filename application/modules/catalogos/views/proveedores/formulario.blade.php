@layout('tema_luna/layout')
@section('contenido')
<div class="container-fluid panel-body">
    <h1 class="mt-4"><?php echo isset($modulo) ? $modulo : "" ?></h1>
    <ol class="breadcrumb mb-4">
        <li class="breadcrumb-item"><?php echo isset($titulo) ? $titulo : "" ?></li>
        <li class="breadcrumb-item active"><?php echo isset($subtitulo) ? $subtitulo : "" ?></li>
    </ol>

    <div class="row">
        <div class="col-md-12">
            <form id="alta_registro" data-id="{{ isset($data->id) ? $data->id : '' }}"> <!-- enctype="multipart/form-data"  -->
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="">Identificador:</label>
                            <input type="text" class="form-control" value="<?php if(isset($data->clave_identificador)){print_r($data->clave_identificador);}else{ echo $clave;} ?>" id="clave_identificador" name="clave_identificador" placeholder="">
                            <div id="clave_identificador_error" class="invalid-feedback"></div>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <label for="">Tipo de registro:</label>
                        <select class="form-control" id="tipo_registro" name="tipo_registro" style="width: 100%;">
                            <option value="">Selecionar ...</option>
                            <option value="Contado" <?php if(isset($data->tipo_registro)){ if($data->tipo_registro == "Contado") echo "selected";} ?> >Contado </option>
                            <option value="Credito" <?php if(isset($data->tipo_registro)){ if($data->tipo_registro == "Credito") echo "selected";} ?> >Crédito </option>
                            <option value="Credito" <?php if(isset($data->tipo_registro)){ if($data->tipo_registro == "Ambos") echo "selected";} ?> >Ambos </option>
                        </select>
                        <div id="tipo_registro_error" class="invalid-feedback"></div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-8">
                        <div class="form-group">
                            <label for="">Razón Social:</label>
                            <input type="text" class="form-control" value="<?php if(isset($data->razon_social)){print_r($data->razon_social);} ?>" id="razon_social" name="razon_social" placeholder="">
                            <div id="razon_social_error" class="invalid-feedback"></div>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="">RFC:</label>
                            <input type="text" class="form-control" maxlength="19" value="<?php if(isset($data->proveedor_rfc)){print_r($data->proveedor_rfc);} ?>" id="proveedor_rfc" name="proveedor_rfc" placeholder="">
                            <div id="proveedor_rfc_error" class="invalid-feedback"></div>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="">Nombre:</label>
                            <input type="text" class="form-control" value="<?php if(isset($data->proveedor_nombre)){print_r($data->proveedor_nombre);} ?>" id="proveedor_nombre" name="proveedor_nombre" placeholder="">
                            <div id="proveedor_nombre_error" class="invalid-feedback"></div>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="">Apellido Paterno:</label>
                            <input type="text" class="form-control" value="<?php if(isset($data->apellido_materno)){print_r($data->apellido_materno);} ?>" id="apellido_materno" name="apellido_materno" placeholder="">
                            <div id="apellido_materno_error" class="invalid-feedback"></div>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="">Apellido Materno:</label>
                            <input type="text" class="form-control" value="<?php if(isset($data->apellido_paterno)){print_r($data->apellido_paterno);} ?>" id="apellido_paterno" name="apellido_paterno" placeholder="">
                            <div id="apellido_paterno_error" class="invalid-feedback"></div>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="">Calle:</label>
                            <input type="text" class="form-control" value="<?php if(isset($data->proveedor_calle)){print_r($data->proveedor_calle);} ?>" id="proveedor_calle" name="proveedor_calle" placeholder="">
                            <div id="proveedor_calle_error" class="invalid-feedback"></div>
                        </div>
                    </div>
                    <div class="col-md-2">
                        <div class="form-group">
                            <label for="">Número:</label>
                            <input type="text" class="form-control" value="<?php if(isset($data->proveedor_numero)){print_r($data->proveedor_numero);} ?>" id="proveedor_numero" name="proveedor_numero" placeholder="">
                            <div id="proveedor_numero_error" class="invalid-feedback"></div>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="">Colonial:</label>
                            <input type="text" class="form-control" value="<?php if(isset($data->proveedor_colonia)){print_r($data->proveedor_colonia);} ?>" id="proveedor_colonia" name="proveedor_colonia" placeholder="">
                            <div id="proveedor_colonia_error" class="invalid-feedback"></div>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="">Estado:</label>
                            <input type="text" class="form-control" value="<?php if(isset($data->proveedor_estado)){print_r($data->proveedor_estado);} ?>" id="proveedor_estado" name="proveedor_estado" placeholder="">
                            <div id="proveedor_estado_error" class="invalid-feedback"></div>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="">País:</label>
                            <input type="text" class="form-control" value="<?php if(isset($data->proveedor_pais)){print_r($data->proveedor_pais);}else{ echo 'México';} ?>" id="proveedor_pais" name="proveedor_pais" placeholder="">
                            <div id="proveedor_pais_error" class="invalid-feedback"></div>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="">Código postal:</label>
                            <input type="text" maxlength="5" onkeypress='return event.charCode >= 48 && event.charCode <= 57' class="form-control" value="<?php if(isset($data->codigo_postal)){print_r($data->codigo_postal);} ?>" id="codigo_postal" name="codigo_postal" placeholder="">
                            <div id="codigo_postal_error" class="invalid-feedback"></div>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="">Telefono 1:</label>
                            <input type="text" maxlength="10" onkeypress='return event.charCode >= 48 && event.charCode <= 57' class="form-control" value="<?php if(isset($data->telefono)){print_r($data->telefono);} ?>" id="telefono" name="telefono" placeholder="">
                            <div id="telefono_error" class="invalid-feedback"></div>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="">Telefono 2:</label>
                            <input type="text" maxlength="10" onkeypress='return event.charCode >= 48 && event.charCode <= 57' class="form-control" value="<?php if(isset($data->telefono_secundario)){print_r($data->telefono_secundario);} ?>" id="telefono_secundario" name="telefono_secundario" placeholder="">
                            <div id="telefono_secundario_error" class="invalid-feedback"></div>
                        </div>
                    </div>
                    <!--<div class="col-md-4">
                        <div class="form-group">
                            <label for="">Telefono Oficina:</label>
                            <input type="text" maxlength="10" onkeypress='return event.charCode >= 48 && event.charCode <= 57' class="form-control" value="<?php if(isset($data->telefono_oficina)){print_r($data->telefono_oficina);} ?>" id="telefono_oficina" name="telefono_oficina" placeholder="">
                            <div id="telefono_oficina_error" class="invalid-feedback"></div>
                        </div>
                    </div>-->
                </div>

                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="">Correo Electrónico 1:</label>
                            <input type="text" class="form-control" value="<?php if(isset($data->correo_electronico)){print_r($data->correo_electronico);} else{ echo 'notienecorreo@notienecorreo.com.mx'; } ?>" id="correo_electronico" name="correo_electronico" placeholder="">
                            <div id="correo_electronico_error" class="invalid-feedback"></div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="">Correo Electrónico 2:</label>
                            <input type="text" class="form-control" value="<?php if(isset($data->correo_electronico_secundario)){print_r($data->correo_electronico_secundario);} else{ echo 'notienecorreo@notienecorreo.com.mx'; } ?>" id="correo_electronico_secundario" name="correo_electronico_secundario" placeholder="">
                            <div id="correo_electronico_secundario_error" class="invalid-feedback"></div>
                        </div>
                    </div>
                </div>

                <br>
                <h4>Datos de pago</h4>
                <hr>

                <div class="row">
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="">Forma de pago:</label>
                            <select class="form-control" id="metodo_pago_id" name="metodo_pago_id" style="width: 100%;">
                                <option value="">Selecionar ...</option>
                                @if(!empty($cat_tipo_pago))
                                    @foreach ($cat_tipo_pago as $pago)
                                        <option value="{{$pago->id}}" <?php if(isset($data->metodo_pago_id)){if($data->metodo_pago_id == $pago->id) echo "selected";} ?>>[{{$pago->clave}}] {{$pago->nombre}}</option>
                                    @endforeach
                                @endif
                            </select>
                            <div id="metodo_pago_id_error" class="invalid-feedback"></div>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="">Método de pago:</label>
                            <select class="form-control" id="forma_pago" name="forma_pago" style="width: 100%;">
                                <option value="">Selecionar ...</option>
                                <option value="PUE" <?php if(isset($data->forma_pago)){ if($data->forma_pago == "PUE") echo "selected";} ?>>Pago en una sola exhibición</option>
                                <option value="PPD" <?php if(isset($data->forma_pago)){ if($data->forma_pago == "PPD") echo "selected";} ?>>Pago en parcialidades o diferido</option>
                            </select>
                            <div id="forma_pago_error" class="invalid-feedback"></div>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="">CFDI:</label>
                            <select class="form-control" id="cfdi_id" name="cfdi_id" style="width: 100%;">
                                <option value="">Selecionar ...</option>
                                @if(!empty($cat_cfdi))
                                    @foreach ($cat_cfdi as $cfdi)
                                        <option value="{{$cfdi->id}}" <?php if(isset($data->cfdi_id)){if($data->cfdi_id == $cfdi->id) echo "selected";} ?>>[{{$cfdi->clave}}] {{$cfdi->descripcion}}</option>
                                    @endforeach
                                @endif
                            </select>
                            <div id="cfdi_id_error" class="invalid-feedback"></div>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="">Límite de crédito:</label>
                            <input type="text" class="form-control" value="<?php if(isset($data->limite_credito)){print_r($data->limite_credito);} else{ echo '0.00'; } ?>" id="limite_credito" name="limite_credito" placeholder="">
                            <div id="limite_credito_error" class="invalid-feedback"></div>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="">Saldo:</label>
                            <input type="text" class="form-control" value="<?php if(isset($data->saldo)){print_r($data->saldo);} else{ echo '0.00'; } ?>" id="saldo" name="saldo" placeholder="">
                            <div id="saldo_error" class="invalid-feedback"></div>
                        </div>
                    </div>
                    <!--<div class="col-md-4">
                        <div class="form-group">
                            <label for="">Moneda:</label>
                            <select class="form-control" id="modena" name="modena" style="width: 100%;">
                                <option value="">Selecionar ...</option>
                                <option value="MXN">MXN (Pesos Mexicanos)</option>
                            </select>
                            <div id="modena_error" class="invalid-feedback"></div>
                        </div>
                    </div>-->
                </div>
                
                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group">
                            <label for="">NOTAS:</label>
                            <textarea class="form-control" cols="4" id="notas" name="notas"><?php if(isset($data->notas)){print_r($data->notas);} ?></textarea>
                            <div id="notas_error" class="invalid-feedback"></div>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-12" align="center">
                        <br>
                        @if (isset($data->id))
                            <button type="button" id="editar_registro" class="btn btn-success col-md-4"> Actualizar</button>
                        @else
                            <button type="button" id="guardar_registro" class="btn btn-success col-md-4"> Guardar</button>
                        @endif
                        
                        <div id="mensaje" class="alert alert-success" role="alert" style="display:none;" ></div>
                    </div>
                </div>
                        
            </form>
        </div>
    </div>
</div>
@endsection

@section('scripts')
    <script>
        //Para guardar registro por primera vez
        $("#guardar_registro").on('click', function() {
            $(".invalid-feedback").html("");
            //Validamos campos de telefono y correo
            var validaciones = validar_campos();
            if (validaciones) {
                ajax.post('api/catalogo-proveedor', procesarRegistro(), function(response, headers) {
                    console.log(response);
                    console.log(headers);

                    if (headers.status == 400) {
                        return ajax.showValidations(headers);
                    }

                    //var titulo = (headers.status != 200) ? headers.message : "Proceso completado con éxito";
                    utils.displayWarningDialog("Proceso completado con éxito", "success", function(data) {
                        return window.location.href = base_url + 'catalogos/ProveedoresController/index';
                    })
                })
            }
        });

        //Para actualizar un registro
        $("#editar_registro").on('click', function() {
            $(".invalid-feedback").html("");
            var id = $("#alta_registro").data('id');
            //Validamos campos de telefono y correo
            var validaciones = validar_campos();
            if (validaciones) {
                ajax.put('api/catalogo-proveedor/'+id, procesarRegistro(), function(response, headers) {
                    if (headers.status == 400) {
                        return ajax.showValidations(headers);
                    }
                    
                    //var titulo = (headers.status != 200) ? headers.message : "Proceso completado con éxito";
                    utils.displayWarningDialog("Proceso completado con éxito", "success", function(data) {
                        return window.location.href = base_url + 'catalogos/ProveedoresController/index';
                    })
                })
            }
        });

        //Armamos el formulario a enviar
        let procesarRegistro = function() {
            let form = $('#alta_registro').serializeArray();

            let newArray = {
                clave_identificador : document.getElementById("clave_identificador").value,
                razon_social : document.getElementById("razon_social").value,
                proveedor_rfc : document.getElementById("proveedor_rfc").value,
                tipo_registro : document.getElementById("tipo_registro").value !== '' ? document.getElementById("tipo_registro").value : 'Sr',
                proveedor_nombre : document.getElementById("proveedor_nombre").value,
                apellido_materno : document.getElementById("apellido_materno").value,
                apellido_paterno : document.getElementById("apellido_paterno").value,
                
                direccion : document.getElementById("proveedor_calle").value, //document.getElementById("direccion").value,

                proveedor_calle : document.getElementById("proveedor_calle").value,
                proveedor_numero : document.getElementById("proveedor_numero").value,
                proveedor_estado : document.getElementById("proveedor_estado").value,
                //municipio : document.getElementById("municipio").value,
                proveedor_colonia : document.getElementById("proveedor_colonia").value,
                proveedor_pais : document.getElementById("proveedor_pais").value,
                codigo_postal : document.getElementById("codigo_postal").value,
                telefono : document.getElementById("telefono").value,
                telefono_secundario : document.getElementById("telefono_secundario").value,
                //telefono_oficina : document.getElementById("telefono_oficina").value,
                correo : document.getElementById("correo_electronico").value,
                correo_secundario : document.getElementById("correo_electronico_secundario").value,
                notas : document.getElementById("notas").value,

                forma_pago : document.getElementById("forma_pago").value !== '' ? document.getElementById("forma_pago").value : '0',
                metodo_pago_id : document.getElementById("metodo_pago_id").value !== '' ? document.getElementById("metodo_pago_id").value : '0',
                cfdi_id : document.getElementById("cfdi_id").value !== '' ? document.getElementById("cfdi_id").value : '0',

                limite_credito : document.getElementById("limite_credito").value,
                saldo : document.getElementById("saldo").value,
            };

            return newArray;
        }

        function validar_campos() {
            var validacion = true;

            var correo_1 = $("#correo_electronico").val();
            if (!validar_email(correo_1)) {
                var error_1 = $("#correo_electronico_error");
                $("#correo_electronico_error").css("display","inline-block");
                error_1.append('<label class="form-text text-danger">Formato invalido.</label>');
                validacion = false;
            }else{
                var error_1 = $("#correo_electronico_error");
                error_1.empty();
            }

            var correo_2 = $("#correo_electronico_secundario").val();
            if (!validar_email(correo_2)) {
                var error_2 = $("#correo_electronico_secundario_error");
                $("#correo_electronico_secundario_error").css("display","inline-block");
                error_2.append('<label class="form-text text-danger">Formato invalido.</label>');
                validacion = false;
            }else{
                var error_2 = $("#correo_electronico_secundario_error");
                error_2.empty();
            }

            var telefono_1 = document.getElementById('telefono').value;
            if (telefono_1.length < 10) {
                var error_3 = $("#telefono_error");
                $("#telefono_error").css("display","inline-block");
                error_3.append('<label class="form-text text-danger">Formato invalido.</label>');
                validacion = false;
            }else{
                var error_3 = $("#telefono_error");
                error_3.empty();
            }

            var telefono_2 = document.getElementById('telefono_secundario').value;
            if (telefono_2.length < 10) {
                var error_4 = $("#telefono_secundario_error");
                $("#telefono_secundario_error").css("display","inline-block");
                error_4.append('<label class="form-text text-danger">Formato invalido.</label>');
                validacion = false;
            }else{
                var error_4 = $("#telefono_secundario_error");
                error_4.empty();
            }

            return validacion;
        }

        function validar_email( email ) 
        {
            var regex = /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
            return regex.test(email) ? true : false;
        }
    </script>
@endsection