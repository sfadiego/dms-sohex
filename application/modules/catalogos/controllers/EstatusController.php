<?php
defined('BASEPATH') or exit('No direct script access allowed');

class EstatusController extends MX_Controller
{
    public function __construct()
    {
        parent::__construct();
        date_default_timezone_set('America/Mexico_City');
        //300 segundos  = 5 minutos
        ini_set('max_execution_time',300);

       
        
    }

    public function index()
    {
        $this->load->library('curl');
        $this->load->helper('general');
        $dataFromApi = $this->curl->curlGet('api/estatus');
        
        $dataregistros = procesarResponseApiJsonToArray($dataFromApi);
        $data['data'] = isset($dataregistros) ? $dataregistros : [];
        $data['titulo'] = "Estatus";
        $this->blade->render('estatus/listado', $data);
    }

    public function crear()
    {
        $this->load->library('curl');
        $this->load->helper('general');
        $data['titulo'] = "Registrar estatus";
        $this->blade->render('estatus/formulario', $data);
    }

    public function editar($id)
    {
        if ($id) {
            $this->load->library('curl');
            $this->load->helper('general');
            $dataFromApi = $this->curl->curlGet('api/estatus/' . $id);
            $dataregistro = procesarResponseApiJsonToArray($dataFromApi);
            $data['data'] = isset($dataregistro)  ? $dataregistro : [];
            $data['titulo'] = "Editar estatus";
            $this->blade->render('estatus/formulario', $data);
        }
    }
}

/* End of file VendedoresController.php */
