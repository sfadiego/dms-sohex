<?php
defined('BASEPATH') or exit('No direct script access allowed');

class ClavesClienteController extends MX_Controller
{
    public function __construct()
    {
        parent::__construct();
        date_default_timezone_set('America/Mexico_City');
        //300 segundos  = 5 minutos
        ini_set('max_execution_time',300);
    }

    public function index()
    {
        $this->load->library('curl');
        $this->load->helper('general');

        $dataFromApi = $this->curl->curlGet('api/catalogo-clave-cliente');
        $dataregistros = procesarResponseApiJsonToArray($dataFromApi);
        $data['data'] = isset($dataregistros) ? $dataregistros : [];

        $data['modulo'] = "Catálogos";
        $data['titulo'] = "Claves Clientes";
        $data['subtitulo'] = "Listado";

        $this->blade->render('clave_clientes/listado', $data);
    }

    public function crear()
    {
        $this->load->library('curl');
        $this->load->helper('general');

        $data['modulo'] = "Catálogos";
        $data['titulo'] = "Claves Clientes";
        $data['subtitulo'] = "Registro";

        $this->blade->render('clave_clientes/formulario', $data);
    }

    public function editar($id)
    {
        if ($id) {
            $this->load->library('curl');
            $this->load->helper('general');
            
            $dataFromApi = $this->curl->curlGet('api/catalogo-clave-cliente/' . $id);
            $dataregistro = procesarResponseApiJsonToArray($dataFromApi);

            $data['data'] = isset($dataregistro)  ? $dataregistro : [];
            
            $data['modulo'] = "Catálogos";
            $data['titulo'] = "Claves Clientes";
            $data['subtitulo'] = "Editar";
            
            $this->blade->render('clave_clientes/formulario', $data);
        }
    }

    public function ajax_catalogo()
    {
        $this->load->helper('general');
        $this->load->library('curl');

        $responseData = $this->curl->curlGet('api/catalogo-clave-cliente');
        $response = procesarResponseApiJsonToArray($responseData);
        $data['data'] = count($response) > 0 ? $response : [];
        echo json_encode($data);
    }
}

/* End of file ColoresController.php */
