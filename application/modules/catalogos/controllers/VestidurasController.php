<?php
defined('BASEPATH') or exit('No direct script access allowed');

class VestidurasController extends MX_Controller
{
    public function __construct()
    {
        parent::__construct();
        date_default_timezone_set('America/Mexico_City');
        //300 segundos  = 5 minutos
        ini_set('max_execution_time',300);
    }

    public function index()
    {
        $this->load->library('curl');
        $this->load->helper('general');

        $dataFromApi = $this->curl->curlGet('api/catalogo-vestiduras');
        $dataregistros = procesarResponseApiJsonToArray($dataFromApi);
        $data['data'] = isset($dataregistros) ? $dataregistros : [];

        $data['modulo'] = "Catálogos";
        $data['titulo'] = "Colores Interiores (Vestiduras)";
        $data['subtitulo'] = "Listado";

        $this->blade->render('vestiduras/listado', $data);
    }

    public function crear()
    {
        $this->load->library('curl');
        $this->load->helper('general');

        $data['modulo'] = "Catálogos";
        $data['titulo'] = "Colores Interiores (Vestiduras)";
        $data['subtitulo'] = "Registro";

        $this->blade->render('vestiduras/formulario', $data);
    }

    public function editar($id)
    {
        if ($id) {
            $this->load->library('curl');
            $this->load->helper('general');
            
            $dataFromApi = $this->curl->curlGet('api/catalogo-vestiduras/' . $id);
            $dataregistro = procesarResponseApiJsonToArray($dataFromApi);

            $data['data'] = isset($dataregistro)  ? $dataregistro : [];
            
            $data['modulo'] = "Catálogos";
            $data['titulo'] = "Colores Interiores (Vestiduras)";
            $data['subtitulo'] = "Editar";
            
            $this->blade->render('vestiduras/formulario', $data);
        }
    }

    public function ajax_catalogo()
    {
        $this->load->helper('general');
        $this->load->library('curl');

        $responseData = $this->curl->curlGet('api/catalogo-vestiduras');
        $response = procesarResponseApiJsonToArray($responseData);
        $data['data'] = count($response) > 0 ? $response : [];
        echo json_encode($data);
    }
}

/* End of file vestidurasController.php */
