<?php defined('BASEPATH') or exit('No direct script access allowed');

class Nomina extends CI_Controller
{
    public $title = 'Nomina';
    public $breadcrumb = array(
        'submodulo' => false,
        'titulo' => false,
        'subtitulo' => false,
        'metodo' => false,
    );
    public $scripts = array();
    public $pathScript;
    public $pathBase;

    public function __construct()
    {
        parent::__construct();
        $this->title = 'Nómina';
        $this->breadcrumb['submodulo'] = array('name'=>'Menú principal','url'=>site_url('nomina'));
    }

    public function construccion(){
        $html = $this->load->view('/nomina/construccion',false,true);
        $this->output($html);
    }

    public function menu(){
        $this->scripts[] = script_tag('js/nomina/jstree/jstree.js');
        $this->scripts[] = script_tag('js/nomina/menu.js');
        

        $id = $this->input->get('menu');
        $dataContent = array(
            'id' => $id
        );

        $this->load->library('parser');
        $html = $this->parser->parse('/nomina/menu',$dataContent,true);
        $this->output($html);
    }

    public function index()
    {
        redirect('nomina/menu');
    }

    public function catalogosyconsultas(){
        $this->breadcrumb['titulo'] = array('name'=>'Catálogos y consultas','url'=>site_url('nomina'));
        $html = $this->load->view('/nomina/catalogosyconsultas',false,true);
        $this->output($html);
    }

    public function catalogossistema(){
        $this->breadcrumb['titulo'] = array('name'=>'Catálogos y consultas','url'=>site_url('nomina'));
        $html = $this->load->view('/nomina/catalogosyconsultas',false,true);
        $this->output($html);
    }

    public function output($html){
		$this->blade->render('build', array(
            'scripts' => $this->scripts,
            'modulo' => $this->title,

            'submodulo' => $this->breadcrumb['submodulo'],
            'titulo' => $this->breadcrumb['titulo'],
            'subtitulo' => $this->breadcrumb['subtitulo'],
            'metodo' => $this->breadcrumb['metodo'],
            
            'content'=>$html
        ));
    }

    
}
