<?php defined('BASEPATH') or exit('No direct script access allowed');

class Tablero_total_percepciones extends MY_Controller
{
    public $title;
    public $breadcrumb;
    public $scripts = array();
    public $pathScript;
    public $pathBase;

    public function __construct()
    {
        parent::__construct();
        $this->pathScript = base_url('js/nomina/inicio/tablero_total_percepciones/');
        $this->breadcrumb = array(
            'Nomina',
            'Tablero de nomina',
            array('name'=>'Total de percepciones','url'=>site_url('omina/inicio/tablero_total_percepciones'))
        );
    }

    public function index(){

        redirect('nomina/inicio/tablero_total_percepciones/tablero_pyd');
    }

    public function tablero_pyd($id_tipo_perdiodo = 1 , $id_periodo = false){

        $this->scripts[] = script_tag($this->pathScript.'tablero_pyd.js');

        $this->load->model('General_model');
        $dataForm = $this->General_model->call_api('catalogos/percepciones_deducciones/cantidad_percepciones_deducciones',array('id_periodo'=>$id_periodo),'get');

        $dataContent = array(
            'contenido' => $dataForm['data'],
        );

        $this->load->library('parser');
        $html_content = $this->parser->parse('/inicio/tablero_total_percepciones/tablero_pyd', $dataContent,true);


        $this->load->model('General_model');
        $tipo_periodos = $this->General_model->call_api('catalogos/tipo_periodos/store',array(),'get');
        $periodos = $this->General_model->call_api('catalogos/periodos/periodos_cerrados_anio_transacciones',array('id_tipo_periodo'=>$id_tipo_perdiodo,'anio'=>date('Y')),'get');
        $total = $this->General_model->call_api('catalogos/trabajadores/cantidad_total_percepciones',array('id_periodo'=>$id_periodo),'get');

        $content = array(
            'tab1' => 'active',
            'contenido' => $html_content,
            'id_tipo_perdiodo' => $id_tipo_perdiodo,
            'tipo_periodos' => $tipo_periodos['data'],
            'id_perdiodo' => $id_periodo,
            'perdiodo' => $periodos['data'],
            'total' => $total['data']
        );

        $this->load->library('parser');
        $html = $this->parser->parse('/inicio/tablero_total_percepciones/index', $content,true);
        $this->output( $html );

    }

    public function periodos_cerrados_anio_transacciones(){

        $id_tipo_periodo = $this->input->get('id_tipo_periodo');

        $this->load->model('General_model');
        $dataForm = $this->General_model->call_api('catalogos/periodos/periodos_cerrados_anio_transacciones',array('id_tipo_periodo'=>$id_tipo_periodo,'anio'=>date('Y')),'get');

        $http_estatus = (array_key_exists('code',$dataForm))? $dataForm['code'] : 200;
        $this->response($dataForm,$http_estatus);
    }

    public function total_percepciones(){

        $id_periodo = $this->input->get('id_periodo');

        $this->load->model('General_model');
        $dataForm = $this->General_model->call_api('catalogos/trabajadores/cantidad_total_percepciones',array('id_periodo'=>$id_periodo),'get');

        $http_estatus = (array_key_exists('code',$dataForm))? $dataForm['code'] : 200;
        $this->response($dataForm,$http_estatus);
    }

    public function tablero_puestos($id_tipo_perdiodo = 1 , $id_periodo = false){

        $this->scripts[] = script_tag($this->pathScript.'tablero_puestos.js');

        $this->load->model('General_model');
        $dataForm = $this->General_model->call_api('catalogos/trabajadores/trabajadores_puestos_pyd',array('id_periodo'=>$id_periodo),'get');
        $dataContent = array(
            'contenido' => $dataForm['data'],
        );

        $this->load->library('parser');
        $html_content = $this->parser->parse('/inicio/tablero_total_percepciones/tablero_puestos', $dataContent,true);


        $this->load->model('General_model');
        $tipo_periodos = $this->General_model->call_api('catalogos/tipo_periodos/store',array(),'get');
        $periodos = $this->General_model->call_api('catalogos/periodos/periodos_cerrados_anio_transacciones',array('id_tipo_periodo'=>$id_tipo_perdiodo,'anio'=>date('Y')),'get');
        $total = $this->General_model->call_api('catalogos/trabajadores/cantidad_total_percepciones',array('id_periodo'=>$id_periodo),'get');

        $content = array(
            'tab3' => 'active',
            'contenido' => $html_content,
            'id_tipo_perdiodo' => $id_tipo_perdiodo,
            'tipo_periodos' => $tipo_periodos['data'],
            'id_perdiodo' => $id_periodo,
            'perdiodo' => $periodos['data'],
            'total' => $total['data']
        );

        $this->load->library('parser');
        $html = $this->parser->parse('/inicio/tablero_total_percepciones/index', $content,true);
        $this->output( $html );
    }

    public function tablero_clasificaciones($id_tipo_perdiodo = 1 , $id_periodo = false){
        $this->scripts[] = script_tag($this->pathScript.'tablero_clasificaciones.js');

        $this->load->model('General_model');
        $dataForm = $this->General_model->call_api('catalogos/trabajadores/trabajadores_clasificaciones_pyd',array('id_periodo'=>$id_periodo),'get');
        $dataContent = array(
            'contenido' => $dataForm['data'],
        );

        $this->load->library('parser');
        $html_content = $this->parser->parse('/inicio/tablero_total_percepciones/tablero_clasificaciones', $dataContent,true);


        $this->load->model('General_model');
        $tipo_periodos = $this->General_model->call_api('catalogos/tipo_periodos/store',array(),'get');
        $periodos = $this->General_model->call_api('catalogos/periodos/periodos_cerrados_anio_transacciones',array('id_tipo_periodo'=>$id_tipo_perdiodo,'anio'=>date('Y')),'get');
        $total = $this->General_model->call_api('catalogos/trabajadores/cantidad_total_percepciones',array('id_periodo'=>$id_periodo),'get');

        $content = array(
            'tab4' => 'active',
            'contenido' => $html_content,
            'id_tipo_perdiodo' => $id_tipo_perdiodo,
            'tipo_periodos' => $tipo_periodos['data'],
            'id_perdiodo' => $id_periodo,
            'perdiodo' => $periodos['data'],
            'total' => $total['data']
        );

        $this->load->library('parser');
        $html = $this->parser->parse('/inicio/tablero_total_percepciones/index', $content,true);
        $this->output( $html );
    }

    public function tablero_antiguedad($id_tipo_perdiodo = 1 , $id_periodo = false){

        $this->scripts[] = script_tag($this->pathScript.'tablero_antiguedad.js');

        $this->load->model('General_model');
        $dataForm = $this->General_model->call_api('catalogos/trabajadores/trabajadores_antiguedad_pyd',array('id_periodo'=>$id_periodo),'get');
        $dataContent = array(
            'contenido' => $dataForm['data'],
        );

        $this->load->library('parser');
        $html_content = $this->parser->parse('/inicio/tablero_total_percepciones/tablero_antiguedad', $dataContent,true);


        $this->load->model('General_model');
        $tipo_periodos = $this->General_model->call_api('catalogos/tipo_periodos/store',array(),'get');
        $periodos = $this->General_model->call_api('catalogos/periodos/periodos_cerrados_anio_transacciones',array('id_tipo_periodo'=>$id_tipo_perdiodo,'anio'=>date('Y')),'get');
        $total = $this->General_model->call_api('catalogos/trabajadores/cantidad_total_percepciones',array('id_periodo'=>$id_periodo),'get');

        $content = array(
            'tab5' => 'active',
            'contenido' => $html_content,
            'id_tipo_perdiodo' => $id_tipo_perdiodo,
            'tipo_periodos' => $tipo_periodos['data'],
            'id_perdiodo' => $id_periodo,
            'perdiodo' => $periodos['data'],
            'total' => $total['data']
        );

        $this->load->library('parser');
        $html = $this->parser->parse('/inicio/tablero_total_percepciones/index', $content,true);
        $this->output( $html );
    }

    public function tablero_departamentos($id_tipo_perdiodo = 1 , $id_periodo = false){

        $this->scripts[] = script_tag($this->pathScript.'tablero_departamentos.js');

        $this->load->model('General_model');
        $dataForm = $this->General_model->call_api('catalogos/trabajadores/trabajadores_departamentos_pyd',array('id_periodo'=>$id_periodo),'get');
        $dataContent = array(
            'contenido' => $dataForm['data'],
        );

        $this->load->library('parser');
        $html_content = $this->parser->parse('/inicio/tablero_total_percepciones/tablero_departamentos', $dataContent,true);


        $this->load->model('General_model');
        $tipo_periodos = $this->General_model->call_api('catalogos/tipo_periodos/store',array(),'get');
        $periodos = $this->General_model->call_api('catalogos/periodos/periodos_cerrados_anio_transacciones',array('id_tipo_periodo'=>$id_tipo_perdiodo,'anio'=>date('Y')),'get');
        $total = $this->General_model->call_api('catalogos/trabajadores/cantidad_total_percepciones',array('id_periodo'=>$id_periodo),'get');

        $content = array(
            'tab2' => 'active',
            'contenido' => $html_content,
            'id_tipo_perdiodo' => $id_tipo_perdiodo,
            'tipo_periodos' => $tipo_periodos['data'],
            'id_perdiodo' => $id_periodo,
            'perdiodo' => $periodos['data'],
            'total' => $total['data'],
        );

        $this->load->library('parser');
        $html = $this->parser->parse('/inicio/tablero_total_percepciones/index', $content,true);
        $this->output( $html );
    }


}