<?php defined('BASEPATH') or exit('No direct script access allowed');

class Tablero_nomina_activos extends MY_Controller
{
    public $title;
    public $breadcrumb;
    public $scripts = array();
    public $pathScript;
    public $pathBase;

    public function __construct()
    {
        parent::__construct();
        $this->pathScript = base_url('js/nomina/inicio/tablero_nomina_activos/');
        $this->breadcrumb = array(
            'Nomina',
            'Tablero de nomina',
            array('name'=>'Trabajadores Activos','url'=>site_url('omina/inicio/tablero_nomina_activos'))
        );
    }

    public function index(){

        redirect('nomina/inicio/tablero_nomina_activos/tablero_estatus');
    }

    public function tablero_estatus(){
        

        $this->load->model('General_model');
        $dataForm = $this->General_model->call_api('catalogos/trabajadores/cantidad_trabajadores_estatus',array(),'get');

        $dataContent = array(
            'contenido' => $dataForm['data']
        );

        $this->load->library('parser');
        $html_content = $this->parser->parse('/inicio/tablero_nomina_activos/tablero_activos', $dataContent,true);

        $content = [
            'tab1' => 'active',
            'contenido' => $html_content
        ];
        $this->load->library('parser');
        $html = $this->parser->parse('/inicio/tablero_nomina_activos/index', $content,true);
        $this->output( $html );
        
        
    }

    public function tablero_departamentos(){
        

        $this->load->model('General_model');
        $dataForm = $this->General_model->call_api('catalogos/trabajadores/cantidad_trabajadores_departamentos',array(),'get');

        $dataContent = array(
            'contenido' => $dataForm['data']
        );

        $this->load->library('parser');
        $html_content = $this->parser->parse('/inicio/tablero_nomina_activos/tablero_departamentos', $dataContent,true);
        
        $content = [
            'tab2' => 'active',
            'contenido' => $html_content
        ];
        $this->load->library('parser');
        $html = $this->parser->parse('/inicio/tablero_nomina_activos/index', $content,true);
        $this->output( $html );
    }

    public function tablero_puestos(){
        

        $this->load->model('General_model');
        $dataForm = $this->General_model->call_api('catalogos/trabajadores/cantidad_trabajadores_puestos',array(),'get');

        $dataContent = array(
            'contenido' => $dataForm['data']
        );

        $this->load->library('parser');
        $html_content = $this->parser->parse('/inicio/tablero_nomina_activos/tablero_puestos', $dataContent,true);
        
        $content = [
            'tab3' => 'active',
            'contenido' => $html_content
        ];
        $this->load->library('parser');
        $html = $this->parser->parse('/inicio/tablero_nomina_activos/index', $content,true);
        $this->output( $html );
    }

    public function tablero_clasificaciones(){
        

        $this->load->model('General_model');
        $dataForm = $this->General_model->call_api('catalogos/trabajadores/cantidad_trabajadores_clasificaciones',array(),'get');

        $dataContent = array(
            'contenido' => $dataForm['data']
        );

        $this->load->library('parser');
        $html_content = $this->parser->parse('/inicio/tablero_nomina_activos/tablero_clasificaciones', $dataContent,true);
        
        $content = [
            'tab4' => 'active',
            'contenido' => $html_content
        ];
        $this->load->library('parser');
        $html = $this->parser->parse('/inicio/tablero_nomina_activos/index', $content,true);
        $this->output( $html );
    }

    public function tablero_antiguedad(){
        

        $this->load->model('General_model');
        $dataForm = $this->General_model->call_api('catalogos/trabajadores/cantidad_trabajadores_antiguedad',array(),'get');

        $dataContent = array(
            'contenido' => $dataForm['data']
        );

        $this->load->library('parser');
        $html_content = $this->parser->parse('/inicio/tablero_nomina_activos/tablero_antiguedad', $dataContent,true);
        
        $content = [
            'tab5' => 'active',
            'contenido' => $html_content
        ];
        $this->load->library('parser');
        $html = $this->parser->parse('/inicio/tablero_nomina_activos/index', $content,true);
        $this->output( $html );
    }


}