<?php defined('BASEPATH') or exit('No direct script access allowed');

class Puestos extends CI_Controller
{
    public $title;
    public $breadcrumb;
    public $scripts = array();
    public $pathScript;
    public $pathBase;

    public function __construct()
    {
        parent::__construct();
        $this->pathScript = 'js/nomina/catalogosconsultas/puestos/';
        $this->pathBase = '/nomina/catalogosconsultas/puestos';

        $this->breadcrumb = array(
            'Catalogos y consultas',
            array('name'=>'Organización','url'=>site_url('nomina/catalogosconsultas/organizacion')),
            array('name'=>'Puestos','url'=>site_url('nomina/catalogosconsultas/puestos'))
        );
    }

    public function index()
    {
        $this->title = 'Puestos';
        $this->breadcrumb[] = 'Listado';
        $this->scripts[] = utils::script_tag($this->pathScript.'index.js');
        $html = $this->load->view('/catalogosconsultas/puestos/index',false,true);
        $this->output($html);
    }

    public function alta()
    {
        $this->title = 'Alta de puestos';
        $this->breadcrumb[] = 'Alta de puestos';
        $this->scripts[] = utils::script_tag($this->pathScript.'alta.js');

        
        $dataForm = array(
            'identity' => ''
        );
        $this->load->library('parser');
        $html = $this->parser->parse('/catalogosconsultas/puestos/form',$dataForm,true);

        $this->output($html);
    }

    public function editar()
    {
        $this->title = 'Cambios de puestos';
        $this->breadcrumb[] = 'Cambios de puestos';
        $this->scripts[] = utils::script_tag($this->pathScript.'editar.js');

        $id = $this->input->get('id');
        $dataForm = array(
            'identity' => $id
        );

        $this->load->library('parser');
        $html = $this->parser->parse('/catalogosconsultas/puestos/form',$dataForm,true);
        $this->output($html);
    }

    public function output($html){
		$this->blade->render('build', array(
            'scripts' => $this->scripts,
            'modulo' => 'Nomina',
            'breadcrumb' => $this->breadcrumb,
            'titulo_modulo' => $this->title,
            'content'=>$html
        ));
    }

}
