<?php defined('BASEPATH') or exit('No direct script access allowed');

class Organizacion extends CI_Controller
{
    public $title = 'Nomina';
    public $breadcrumb;
    public $scripts = array();
    public $pathScript;
    public $pathBase;

    public function __construct()
    {
        parent::__construct();
        $this->title = 'Nomina';
        $this->breadcrumb = array(
            'Catalogos y consultas',
            array('name'=>'Organización','url'=>site_url('nomina/catalogosconsultas/organizacion'))
        );
    }

    public function index()
    {
        $this->breadcrumb[] = 'Menú';
        $this->title = 'Organización';
        $html = $this->load->view('/catalogosconsultas/organizacion/index',false,true);
        $this->output($html);
    }

    public function output($html){
		$this->blade->render('build', array(
            'scripts' => $this->scripts,
            'titulo_modulo' => $this->title,
            'modulo' => 'Nomina',
            'breadcrumb' => $this->breadcrumb,
            'content'=>$html
        ));
    }

    
}
