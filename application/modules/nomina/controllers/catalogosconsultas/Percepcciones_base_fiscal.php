<?php defined('BASEPATH') or exit('No direct script access allowed');

class Percepcciones_base_fiscal extends MY_Controller
{
    public $title;
    public $breadcrumb;
    public $scripts = array();
    public $pathScript;
    public $pathBase;

    public function __construct()
    {
        parent::__construct();
        $this->pathScript = base_url('js/nomina/catalogosconsultas/percepcciones_base_fiscal/');
        $this->breadcrumb = array(
            'Catalogos y consultas',
            'Percepciones y deducciones',
            array('name'=>'Percepciones por base fiscal','url'=>site_url('nomina/catalogosconsultas/percepcciones_base_fiscal'))
        );
    }

    public function index($id = false){
        $this->scripts[] = script_tag($this->pathScript.'index.js');

        $this->load->model('General_model');
        $basesFiscales = $this->General_model->call_api('catalogos/basesFiscalesTipos/store',array(),'get');

        $dataContent = array(
            'id' => $id,
            'basesFiscales' => $basesFiscales['data'],
        );

        $this->load->library('parser');
        $html = $this->parser->parse('/catalogosconsultas/percepcciones_base_fiscal/index', $dataContent,true);
        
        $this->output($html);
    }


    public function index_get()
    {
        $identity = $this->input->get_post('id');

        $this->load->model('General_model');
        $dataForm = $this->General_model->call_api('nomina/pydbasesfiscales/listbasefiscal',array('id'=>$identity),'get');
        $http_estatus = (array_key_exists('code',$dataForm))? $dataForm['code'] : 200;

        $this->response($dataForm,$http_estatus);
    }

    public function editar($id,$base_fiscal_tipo){
        
        $this->scripts[] = script_tag($this->pathScript.'editar.js');
        $this->breadcrumb[] = 'Modificar';

        $this->load->model('General_model');
        $registro = $this->General_model->call_api('nomina/pydbasesfiscales/store_find',array('id'=>$id),'get');

        $registro2 = $this->General_model->call_api('nomina/percepcionesdeducciones/store_find',array('id'=>$registro['data']['id_PyD']),'get');
        

        $dataForm = array(
            'id' => $id,
            'base_fiscal_tipo' => $base_fiscal_tipo,
            'registros' => $registro['data'],
            'registros2' => $registro2['data'],
            'montos' => $registro['extends']['BasesFiscalesMontos'],
        );
        
        $this->load->library('parser');
        $html = $this->parser->parse('/catalogosconsultas/percepcciones_base_fiscal/editar', $dataForm,true);
        
        $this->output($html);
    }

    public function editar_guardar(){

        $parametros = $this->input->post();
        
        $this->load->model('General_model');
        $response = $this->General_model->call_api('nomina/pydbasesfiscales/store',$parametros,'put');
        $code = (array_key_exists('code',$response))? $response['code'] : 200;
        
        $this->response($response,$code);
    }

}