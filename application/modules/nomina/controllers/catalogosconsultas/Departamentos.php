<?php defined('BASEPATH') or exit('No direct script access allowed');

class Departamentos extends CI_Controller
{
    public $title;
    public $breadcrumb;
    public $scripts = array();
    public $pathScript;
    public $pathBase;

    public function __construct()
    {
        parent::__construct();
        $this->breadcrumb = array(
            'Catalogos y consultas',
            array('name'=>'Organización','url'=>site_url('nomina/catalogosconsultas/organizacion')),
            array('name'=>'Departamentos','url'=>site_url('nomina/catalogosconsultas/departamentos'))
        );

        $this->pathScript = 'js/nomina/catalogosconsultas/departamentos/';
        $this->pathBase = '/nomina/catalogosconsultas/departamentos';
    }

    public function index()
    {
        $this->title = 'Departamentos';
        $this->breadcrumb[] = 'Listado';
        $this->scripts[] = utils::script_tag($this->pathScript.'index.js');
        $html = $this->load->view('/catalogosconsultas/departamentos/index',false,true);
        $this->output($html);
    }

    public function alta()
    {
        $this->title = 'Registro';
        $this->breadcrumb[] = 'Registro';
        $this->scripts[] = utils::script_tag($this->pathScript.'alta.js');


        $dataForm = array(
            'id' => '',
            'Clave' => '',
            'Descripcion' => '',
            'CuentaCOI' => '',
            'DepartamentoCOI' => ''
        );
        $this->load->library('parser');
        $html = $this->parser->parse('/catalogosconsultas/departamentos/form',$dataForm,true);

        $this->output($html);
    }

    public function editar()
    {
        $this->title = 'Editar';
        $this->breadcrumb[] = 'Editar';
        $this->scripts[] = utils::script_tag($this->pathScript.'editar.js');

        $id = $this->input->get('id');
        $dataForm = array(
            'id' => $id
        );
        $this->load->library('parser');
        $html = $this->parser->parse('/catalogosconsultas/departamentos/form',$dataForm,true);
        $this->output($html);
    }

    public function output($html){
		$this->blade->render('build', array(
            'scripts' => $this->scripts,
            'modulo' => 'Nomina',
            'titulo_modulo' => $this->title,
            'breadcrumb'=> $this->breadcrumb,
            'content'=>$html
        ));
    }

}
