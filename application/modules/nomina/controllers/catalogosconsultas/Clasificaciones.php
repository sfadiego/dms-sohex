<?php defined('BASEPATH') or exit('No direct script access allowed');

class Clasificaciones extends CI_Controller
{
    public $title;
    public $breadcrumb;
    public $scripts = array();
    public $pathScript;
    public $pathBase;

    public function __construct()
    {
        parent::__construct();
        $this->breadcrumb = array(
            'Catalogos y consultas',
            array('name'=>'Organización','url'=>site_url('nomina/catalogosconsultas/organizacion')),
            array('name'=>'Clasificaciones','url'=>site_url('nomina/catalogosconsultas/clasificaciones'))
        );

        $this->pathScript = 'js/nomina/catalogosconsultas/clasificaciones/';
        $this->pathBase = '/nomina/catalogosconsultas/clasificaciones';
    }

    public function index()
    {
        $this->title = 'Clasificaciones';
        $this->breadcrumb[] = 'Listado';
        $this->scripts[] = utils::script_tag($this->pathScript.'index.js');

        $html = $this->load->view('/catalogosconsultas/clasificaciones/index',false,true);
        $this->output($html);
    }

    public function alta()
    {
        $this->title = 'Alta de clasificaciones';
        $this->breadcrumb[] = 'Alta de clasificaciones';
        $this->scripts[] = utils::script_tag($this->pathScript.'alta.js');

        $dataForm = array(
            'identity' => ''
        );
        $this->load->library('parser');
        $html = $this->parser->parse('/catalogosconsultas/clasificaciones/form',$dataForm,true);

        $this->output($html);
    }

    public function editar()
    {
        $this->title = 'Cambios de clasificaciones';
        $this->breadcrumb[] = 'Cambios de clasificaciones';
        $this->scripts[] = utils::script_tag($this->pathScript.'editar.js');

        $id = $this->input->get('id');
        
        $dataForm = array(
            'identity' => $id
        );
        $this->load->library('parser');
        $html = $this->parser->parse('/catalogosconsultas/clasificaciones/form',$dataForm,true);
        $this->output($html);
    }

    public function output($html){
		$this->blade->render('build', array(
            'modulo' => 'Nomina',
            'scripts' => $this->scripts,
            'breadcrumb' => $this->breadcrumb,
            'titulo_modulo' => $this->title,
            'content'=>$html
        ));
    }

}
