<?php defined('BASEPATH') or exit('No direct script access allowed');

class Periodos extends MY_Controller
{
    public $title;
    public $breadcrumb;
    public $scripts = array();
    public $pathScript;
    public $pathBase;

    public function __construct()
    {
        parent::__construct();
        $this->pathScript = base_url('js/nomina/herramientas/periodos/');
        $this->breadcrumb = array(
            'Nomina',
            'Herramientas',
            array('name'=>'Periodos de la nomina','url'=>site_url('nomina/herramientas/periodos'))
        );
    }

    public function index(){
        $this->scripts[] = script_tag($this->pathScript.'index.js');

        $this->load->model('General_model');
        $dataForm = $this->General_model->call_api('procesos/transacciones/store',array(),'get');
        

        $dataContent = array(
            'exite_periodos' => ($dataForm['data'] == false)? false : true
        );
        
        $this->load->library('parser');
        $html = $this->parser->parse('/herramientas/periodos/index', $dataContent,true);
        
        $this->output($html);
    }

    public function index_get()
    {
        $id_Periodicidad = $this->input->post();

        $this->load->model('General_model');
        $dataForm = $this->General_model->call_api('procesos/transacciones/store',array(),'get');
        $http_estatus = (array_key_exists('code',$dataForm))? $dataForm['code'] : 200;
        $this->response($dataForm,$http_estatus);
    }

    public function index_recalcular_get()
    {
        $periodo = $this->input->post('periodo');
        $this->load->model('General_model');
        $dataForm = $this->General_model->call_api('procesos/transacciones/precalculo_periodo_actual',array('periodo'=>$periodo),'get');
        $http_estatus = (array_key_exists('code',$dataForm))? $dataForm['code'] : 200;
        $this->response($dataForm,$http_estatus);
    }

    public function index_guardar()
    {
        $this->load->library('form_validation');
		$this->form_validation->set_rules('fecha_inicio', 'Fecha de inicio', 'trim|required');
		$this->form_validation->set_rules('fecha_fin', 'Fecha de la siguiente nómina', 'trim|required');

		if ($this->form_validation->run() == true) {

            $id_periodo = $this->input->post('id_periodo');
            $clave = 0;

            $this->load->model('General_model');
            
            if($id_periodo == 0 || $id_periodo == false){
                $dataForm = $this->General_model->call_api('nomina/trabajador/parametrosnominagen/store_find',array('id'=>1),'get');
            }else{
                $dataForm = $this->General_model->call_api('catalogos/periodos/store_find',array('id' => $id_periodo),'get');
                $dataForm2 = $this->General_model->call_api('catalogos/periodos/clave_consecutiva',array(),'get');
                $clave = $dataForm2['data'];
            }
            $paramentros_generales = (array_key_exists('data',$dataForm))? $dataForm['data'] : array();

            $data_content = array(
                // 'id_periodo' => $this->input->post('id_periodo'),
                'Clave' => array_key_exists('NumeroNomina',$paramentros_generales)? $paramentros_generales['NumeroNomina'] : $clave,
                'id_Periodicidad' => array_key_exists('Periodicidad',$paramentros_generales)? $paramentros_generales['Periodicidad'] : $paramentros_generales['id_Periodicidad'],
                'FechaInicio' => $this->input->post('fecha_inicio'),
                'FechaFin' => $this->input->post('fecha_fin'),
                'FechaProceso' => $this->input->post('fecha_fin'),
                'Procesado' => 0,

                "DiasPago" => $paramentros_generales['DiasPago'],
                // "PrimerDia" => $paramentros_generales['PrimerDia'],
                "DiasAnio" => $paramentros_generales['DiasAnio'],
                "DiasSemana" => $paramentros_generales['DiasSemana'],
                "TipoCalculo" => $paramentros_generales['TipoCalculo'],
                "SalarioMinimoSM" => $paramentros_generales['SalarioMinimoSM'],
                "UMA" => $paramentros_generales['UMA'],
                "CalculoSubsEmpleo" => $paramentros_generales['CalculoSubsEmpleo'],
                "NominaEspecial" => $paramentros_generales['NominaEspecial'],
                "IncrementoSueldosMitadNomina" => $paramentros_generales['IncrementoSueldosMitadNomina'],
                "ISRMes" => $paramentros_generales['ISRMes'],
                "ISRAnio" => $paramentros_generales['ISRAnio'],
                "SubsidioEmpleo" => $paramentros_generales['SubsidioEmpleo'],
                "SalarioDiarioIntegrado" => $paramentros_generales['SalarioDiarioIntegrado'],
                "Vacaciones" => $paramentros_generales['Vacaciones'],
                "CalculoFactorMensualizacion" => $paramentros_generales['CalculoFactorMensualizacion'],
                "LimiteImpuestoLoc" => $paramentros_generales['LimiteImpuestoLoc'],
                "LimiteVecesUMA" => $paramentros_generales['LimiteVecesUMA'],
                "ISRIncluyeLimiteExencion" => $paramentros_generales['ISRIncluyeLimiteExencion'],
                "DesglosarPagoVacacionesRecibo" => $paramentros_generales['DesglosarPagoVacacionesRecibo'],
                "PagoAutomatico" => $paramentros_generales['PagoAutomatico'],
                "PagoAutomaticoTipo" => $paramentros_generales['PagoAutomaticoTipo'],
                "HorasTrabajo" => $paramentros_generales['HorasTrabajo'],
                "id_Periodicidad" => $paramentros_generales['id_Periodicidad'],
                "Clave_Periodicidad" => $paramentros_generales['Clave_Periodicidad'],
                "Descripcion_Periodicidad" => $paramentros_generales['Descripcion_Periodicidad']
            );
            
            $this->load->model('General_model');
            $dataForm = $this->General_model->call_api('catalogos/periodos/store',$data_content,'post');
            $dataForm_periodo = $dataForm;
            
            if($id_periodo != false && $id_periodo > 0){
                $periodo_nuevo = $dataForm['data'];

                $dataForm = $this->General_model->call_api('nomina/trabajador/datosgenerales/store_findAll',array('id' => $id_periodo),'get');
                $trabajador = $dataForm['data'];
                if($trabajador != false){
                    foreach ($trabajador as $key => $value) {
                        $id_trabajador_ant = $value['id'];
                        $datos_trabajador = $value;
                        unset($datos_trabajador['id']);
                        $datos_trabajador['id_periodo'] = $periodo_nuevo['id'];
                        $nuevo_registro_rest = $this->General_model->call_api('nomina/trabajador/datosgenerales/store',$datos_trabajador,'post');
                        $nuevo_registro = $nuevo_registro_rest['data'];

                        if($nuevo_registro != false){
                            $id_trabajador_nuevo = $nuevo_registro['id'];

                            #salario
                            $datos_salario_rest = $this->General_model->call_api('nomina/trabajador/salario/store_find',array('id_Trabajador' => $id_trabajador_ant),'get');
                            $datos_salario = $datos_salario_rest['data'];
                            unset($datos_salario['id']);
                            $datos_salario['id_Trabajador'] = $id_trabajador_nuevo;
                            $reg_salario_rest = $this->General_model->call_api('nomina/trabajador/salario/store',$datos_salario,'post');

                            #personales
                            $datos_rest = $this->General_model->call_api('nomina/trabajador/datospersonales/store_find',array('id_Trabajador' => $id_trabajador_ant),'get');
                            $datos_find = $datos_rest['data'];
                            unset($datos_find['id']);
                            $datos_find['id_Trabajador'] = $id_trabajador_nuevo;
                            $reg_rest = $this->General_model->call_api('nomina/trabajador/datospersonales/store',$datos_find,'post');

                            #imss
                            $datos_rest = $this->General_model->call_api('nomina/trabajador/datosimss/store_find',array('id_Trabajador' => $id_trabajador_ant),'get');
                            $datos_find = $datos_rest['data'];
                            unset($datos_find['id']);
                            $datos_find['id_Trabajador'] = $id_trabajador_nuevo;
                            $reg_rest = $this->General_model->call_api('nomina/trabajador/datosimss/store',$datos_find,'post');

                            #salud
                            $datos_rest = $this->General_model->call_api('nomina/trabajador/datossalud/store_find',array('id_Trabajador' => $id_trabajador_ant),'get');
                            $datos_find = $datos_rest['data'];
                            unset($datos_find['id']);
                            $datos_find['id_Trabajador'] = $id_trabajador_nuevo;
                            $reg_rest = $this->General_model->call_api('nomina/trabajador/datossalud/store',$datos_find,'post');

                            #fiscales
                            $datos_rest = $this->General_model->call_api('nomina/trabajador/datosfiscales/store_find',array('id_Trabajador' => $id_trabajador_ant),'get');
                            $datos_find = $datos_rest['data'];
                            unset($datos_find['id']);
                            $datos_find['id_Trabajador'] = $id_trabajador_nuevo;
                            $reg_rest = $this->General_model->call_api('nomina/trabajador/datosfiscales/store',$datos_find,'post');

                        }
                    }
                }
            }

            $http_estatus = (array_key_exists('code',$dataForm_periodo))? $dataForm_periodo['code'] : 200;
            $this->response($dataForm_periodo,$http_estatus);

		} else {
            $this->httpStatus = 406;
			$this->results['status'] = 'error';
			$this->results['message'] = $this->form_validation->error_array();
		}
        $this->response($this->results,$this->httpStatus);
    }

    

    public function form($form){
        $this->load->helper('form');
        if(is_array($form)){
            foreach ($form as $key => $value) {
                foreach ($value as $key2 => $value2) {
                    if($value2['key'] != 'id_Trabajador' && $value2['key'] != 'Clave'){
                        if($value2['input']['catalog'] != false){
                            $opciones = array();
                            foreach ($value2['input']['catalog'] as $value3) {
                                if(array_key_exists('Clave',$value3)){
                                    $opciones[$value3['id']] = $value3['Clave'].' - '.$value3['Descripcion'];
                                }else{
                                    $opciones[$value3['id']] = $value3['Descripcion'];
                                }
                                
                            }

                            $form[$key][$key2]['input'] = form_dropdown( $value2['input']['name'], $opciones, $value2['input']['value'], 'id="'.$value2['input']['id'].'" class="'.$value2['input']['class'].'" attr-id="'.$value2['input']['value'].'"' );
                        }else{
                            $form[$key][$key2]['input'] = form_input($value2['input']);
                        }
                        $form[$key][$key2]['key'] = $value2['input']['id'];
                    }else{
                        unset($form[$key][$key2]);
                    }
                }
            }
        }
        return $form;

    }

    public function alta($idTrabajador){
        $this->scripts[] = script_tag($this->pathScript.'alta.js');
        $this->breadcrumb[] = 'Alta';

        $this->load->model('General_model');
        $dataForm = $this->General_model->form('nomina/movimientos/store_form',array(),'get');
        $dataForm = $this->form($dataForm);
        $dataForm['id_trabajador'] = $idTrabajador;
        $dataForm['id_Movimiento'] = false;
        
        $this->load->library('parser');
        $html = $this->parser->parse('/inicio/movimientos/form', $dataForm,true);
        
        $this->output($html);
    }

    public function alta_guardar(){
        $parametros = $this->input->post();
        
        $this->load->model('General_model');
        $response = $this->General_model->call_api('nomina/movimientos/store',$parametros,'post');
        $code = (array_key_exists('code',$response))? $response['code'] : 200;
        $this->response($response,$code);
    }

    public function editar($idTrabajador,$idMovimiento){
        $this->scripts[] = script_tag($this->pathScript.'editar.js');
        $this->breadcrumb[] = 'Editar';

        $this->load->model('General_model');
        $dataForm = $this->General_model->form('nomina/movimientos/store_form',array('id'=>$idMovimiento),'get');
        $dataForm = $this->form($dataForm);
        $dataForm['id_trabajador'] = $idTrabajador;
        $dataForm['id_movimiento'] = $idMovimiento;
        

        $this->load->library('parser');
        $html = $this->parser->parse('/inicio/movimientos/form', $dataForm,true);
        
        $this->output($html);
    }

    public function alta_actualizar(){
        $parametros = $this->input->post();
        
        $this->load->model('General_model');
        $response = $this->General_model->call_api('nomina/movimientos/store',$parametros,'put');
        $code = (array_key_exists('code',$response))? $response['code'] : 200;
        $this->response($response,$code);
    }
    


    
    public function modal_nuevo_periodo(){

        $this->load->model('General_model');
        $response = $this->General_model->call_api('catalogos/tipo_periodos/store',array(),'get');
        if(is_array($response) && array_key_exists('data',$response)){
            foreach ($response['data'] as $key => $value) {
                $fechas = $this->General_model->call_api('catalogos/periodos/fechas_nuevo_periodo',array('id_tipo_periodo' => $value['id']),'get');
                $response['data'][$key]['fechaInicio'] = $fechas['data']['fechaInicio'];
                $response['data'][$key]['fechaFin'] = $fechas['data']['fechaFin'];
            }
        }

        $data_content = array(
            'id_tipo_periodo' => $response['data']
        );
        $response = array(
            'html' => base64_encode($this->load->view('/herramientas/periodos/modal_nuevo_periodo',$data_content,true))
        );
        $this->response($response);
    }

    public function modal_editar_periodo(){

        $this->load->model('General_model');
        $response = $this->General_model->call_api('catalogos/tipo_periodos/store',array(),'get');
        if(is_array($response) && array_key_exists('data',$response)){
            foreach ($response['data'] as $key => $value) {
                $fechas = $this->General_model->call_api('catalogos/periodos/fechas_nuevo_periodo',array('id_tipo_periodo' => $value['id']),'get');
                $response['data'][$key]['fechaInicio'] = $fechas['data']['fechaInicio'];
                $response['data'][$key]['fechaFin'] = $fechas['data']['fechaFin'];
            }
        }
        $id = $this->input->post('id');
        $response_data = $this->General_model->call_api('catalogos/periodos/store_find',array('id'=>$id),'get');

        $data_content = array(
            'id_tipo_periodo' => $response['data'],
            'data' => $response_data['data']
        );
        $response = array(
            'html' => base64_encode($this->load->view('/herramientas/periodos/modal_editar_periodo',$data_content,true))
        );
        $this->response($response);
    }

    public function modal_nuevo_periodo_save(){
        $this->load->library('form_validation');
		$this->form_validation->set_rules('id_tipoperiodo', 'Tipo de periodo', 'trim|required');
		$this->form_validation->set_rules('fecha_inicio', 'Fecha de inicio', 'trim|required|callback_verifica_fecha_disponible');
		$this->form_validation->set_rules('fecha_fin', 'Fecha fin', 'trim|required');

		if ($this->form_validation->run() == true) {
       
            $parametros = array(
                'id_Periodicidad' => $this->input->post('id_tipoperiodo'),
                'FechaInicio' => $this->input->post('fecha_inicio'),
                'FechaFin' => $this->input->post('fecha_fin'),
                'FechaProceso' => $this->input->post('fecha_corte'),
                'Procesado' => 0
            );

            $this->load->model('General_model');
            $response = $this->General_model->call_api('catalogos/periodos/store',$parametros,'post');
            $code = (array_key_exists('code',$response))? $response['code'] : 200;
            $this->response($response,$code);

		} else {
            $this->httpStatus = 406;
			$this->results['status'] = 'error';
			$this->results['message'] = $this->form_validation->error_array();
		}
        $this->response($this->results,$this->httpStatus);
    }

    public function modal_editar_periodo_save(){
        $this->load->library('form_validation');
		// $this->form_validation->set_rules('id_tipoperiodo', 'Tipo de periodo', 'trim|required');
		// $this->form_validation->set_rules('fecha_inicio', 'Fecha de inicio', 'trim|required');
		$this->form_validation->set_rules('fecha_fin', 'Fecha fin', 'trim|required');

		if ($this->form_validation->run() == true) {
       
            $parametros = array(
                'id' => $this->input->post('id'),
                //'id_Periodicidad' => $this->input->post('id_tipoperiodo'),
                //'FechaInicio' => $this->input->post('fecha_inicio'),
                'FechaFin' => $this->input->post('fecha_fin'),
                'FechaProceso' => $this->input->post('fecha_corte'),
                // 'Procesado' => 0
            );

            $this->load->model('General_model');
            $response = $this->General_model->call_api('catalogos/periodos/store',$parametros,'put');
            $code = (array_key_exists('code',$response))? $response['code'] : 200;
            $this->response($response,$code);

		} else {
            $this->httpStatus = 406;
			$this->results['status'] = 'error';
			$this->results['message'] = $this->form_validation->error_array();
		}
        $this->response($this->results,$this->httpStatus);
    }

    public function verifica_fecha_disponible($str)
    {
        if(strlen($str)>0){
            $parametros = array(
                'id_Periodicidad' => $this->input->post('id_tipoperiodo'),
                'FechaInicio' => $str
            );
            $this->load->model('General_model');
            $response = $this->General_model->call_api('catalogos/periodos/store_find',$parametros,'get');
            
            if (is_array($response['data'])) {
                $this->form_validation->set_message('verifica_fecha_disponible', 'Ya existe un periodo con esta fecha.');
                return FALSE;
            } else {
                return TRUE;
            }
        }   
    }

    public function modal_nuevo_periodo_delete(){
        $parametros = array(
            'id' => $this->input->post('id')
        );
        
        $this->load->model('General_model');
        $response = $this->General_model->call_api('catalogos/periodos/store',$parametros,'delete');
        $code = (array_key_exists('code',$response))? $response['code'] : 200;
        $this->response($response,$code);
    }

    public function datos_periodo(){
        $response = $this->session->userdata('periodo');
        $this->response($response);
    }

    public function seleccionar_periodo(){
        
        redirect('nomina/herramientas/periodos/index?url='.$this->input->get('url'),'refresh');
        
        $this->scripts[] = script_tag($this->pathScript.'seleccionar_periodo.js');
        $this->breadcrumb[] = 'Seleccionar periodo';

        $url = base64_decode( $this->input->get('url') );

        $this->load->model('General_model');
        $periodos = $this->General_model->call_api('catalogos/periodos/obtiene_periodos_en_curso',array(),'get');
        

        $dataContent = array(
            'url' => $url,
            'periodos' => $periodos['data']
        );
        
        $this->load->library('parser');
        $html = $this->parser->parse('/herramientas/periodos/seleccionar_periodo', $dataContent,true);
        
        $this->output($html);
    }

    public function guarda_periodo_actual(){
        $this->load->library('form_validation');
		$this->form_validation->set_rules('periodo', 'Periodos en curso', 'trim|required');

		if ($this->form_validation->run() == true) {
            $id_periodo = $this->input->post('periodo');

            $this->load->model('General_model');
            $periodos = $this->General_model->call_api('catalogos/periodos/store_find',array('id' => $id_periodo),'get');
            $this->General_model->call_api('nomina/trabajador/transacciones/calcular_nomina_periodo',array('id_periodo' => $id_periodo),'get');
            $data_content = $periodos['data'];
            $this->session->set_userdata('periodo',$data_content);

            $response = $this->session->userdata('periodo');
		    $code = (is_array($response) && count($response) > 0)? 200 : 406;
            $this->response($response,$code);

		} else {
            $this->httpStatus = 406;
			$this->results['status'] = 'error';
			$this->results['message'] = $this->form_validation->error_array();
		}
        $this->response($this->results,$this->httpStatus);
	}

    public function creacion_siguiente_periodo($id_periodo = false){
        $id_periodo = base64_decode($id_periodo);
        
        $this->scripts[] = script_tag($this->pathScript.'creacion_siguiente_periodo.js');
        $this->breadcrumb[] = 'Creacion del siguiente periodo';

        // $this->load->model('General_model');
        // $dataForm = $this->General_model->form('nomina/movimientos/store_form',array('id'=>$idMovimiento),'get');
        // $dataForm = $this->form($dataForm);
        // $dataForm['id_trabajador'] = $idTrabajador;
        // $dataForm['id_movimiento'] = $idMovimiento;

        $this->load->model('General_model');
        
        if($id_periodo > 0){
            $periodos = $this->General_model->call_api('catalogos/periodos/store_find',array('id' => $id_periodo),'get');
            $periodos = $periodos['data'];
        }else{
            $periodos = false;
        }
        
        if($periodos != false){
            $fin_periodo = array_key_exists('FechaFin',$periodos)? $periodos['FechaFin'] : date("Y-m-d");
            $date = new DateTime($fin_periodo);
            $date->modify('+1 day');
            $inicio_periodo = $date->format('Y-m-d');
        }else{
            $this->load->model('General_model');
            $dataForm = $this->General_model->call_api('nomina/trabajador/parametrosnominagen/store_find',array('id'=>1),'get');
            $paramentros_generales = (array_key_exists('data',$dataForm))? $dataForm['data'] : array();
        }

        $id_periodicidad = is_array($periodos) && array_key_exists('id_Periodicidad',$periodos)? $periodos['id_Periodicidad'] : $paramentros_generales['Periodicidad'];
        $fechas = $this->General_model->call_api('catalogos/periodos/fechas_nuevo_periodo',array('id_tipo_periodo' => $id_periodicidad),'get');
        $fin_periodo = array_key_exists('fechaFin',$fechas['data'])? $fechas['data']['fechaFin'] : date("Y-m-d");

        if($periodos == false){
            $inicio_periodo = array_key_exists('fechaInicio',$fechas['data'])? $fechas['data']['fechaInicio'] : date("Y-m-d");
        }

        $datetime1 = new DateTime($inicio_periodo);
        $datetime2 = new DateTime($fin_periodo);
        $interval = $datetime1->diff($datetime2);
        
        $data_content = array(
            'id_periodo' => $id_periodo,
            'inicio_periodo' => $inicio_periodo,
            'fin_periodo' => ($interval->invert != 1)? $fin_periodo : '',
            'id_periodicidad' => $id_periodicidad
        );

        $this->load->library('parser');
        $html = $this->parser->parse('/herramientas/periodos/creacion_siguiente_periodo', $data_content,true);
        
        $this->output($html);

    }

}