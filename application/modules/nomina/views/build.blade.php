@layout('tema_luna/layout')
@section('contenido')

<script>
// const NOMINA_API = "http://localhost/sistema_nomina/respaldo/";
// const NOMINA_API_new = "http://localhost/sistema_nomina/";

const NOMINA_API = "https://www.sohex.net/nomina_dms/respaldo/";
const NOMINA_API_new = "https://www.sohex.net/nomina_dms/";
</script>

<?php 
  echo link_tag('js/nomina/apps.css');
  // echo link_tag('js/nomina/jstree/themes/default/style.min.css');
  echo script_tag('js/nomina/apps.js');
  echo script_tag('js/nomina/mustache.min.js');
  echo script_tag('js/nomina/moment-with-locales.min.js');
  echo script_tag('js/nomina/bootbox.all.min.js');

  echo is_array($scripts)? implode('',$scripts) : $scripts;
  $active = ($this->input->get('menu') != false)? $this->input->get('menu') : $this->uri->segment(2);
?>

<?php if(isset($content) && strlen($content)>0){ ?>
<div class="container-fluid panel-body">    
  <div class="row mt-4">
    <div class="col-sm-4">
      <h1 class="">Nomina</h1>
    </div>
    <div class="col-sm-8">
      <?php
        $periodicidad = $this->session->userdata('periodo');
        if(is_array($periodicidad) && array_key_exists('id',$periodicidad)){
      ?>
        <script>
          var _ID_PERIODO_ = "<?php echo $periodicidad['id']; ?>";
        </script>
        <div class="row justify-content-end">
          <div class="" style="text-align: right;">
            <?php  echo "<small>Número de nómina: <b>".$periodicidad['Clave']."</b><br/>Fecha: <b>".utils::aFecha($periodicidad['FechaInicio'],true)."</b> al <b>".utils::aFecha($periodicidad['FechaFin'],true)."</b></small>"; ?>
          </div>
          <div class="mr-2 ml-2">
            <button type="button" onclick="cambiar_periodo();" class="btn btn-outline-primary float-right btn-lg mt-1" data-toggle="tooltip" data-placement="left" title="Cambiar periodo"> <i class="far fa-calendar-alt"></i> </button>
          </div>
      </div>
      <?php } ?>
    </div>

  </div>
  <div class="row">
    <div class="col-sm-12">
    <nav aria-label="breadcrumb" >
    <?php
      $bread = array();
      if(isset($breadcrumb) && is_array($breadcrumb)){
        $count = 1;
        echo '<ol class="breadcrumb" style="" >';
        foreach ($breadcrumb as $key => $value) {
          $bread[] = '<li class="breadcrumb-item '.(($count == count($breadcrumb))? 'active' : '').'" '.(($count == count($breadcrumb))? 'aria-current="page"' : '').' >'.(is_array($value)? anchor($value['url'], $value['name'],'class=""') : $value).'</li>';
          // $bread[] = '<li class="breadcrumb-item '.(($count == count($breadcrumb))? 'active' : '').'" '.(($count == count($breadcrumb))? 'aria-current="page"' : '').' >'.(is_array($value)? $value['name'] : $value).'</li>';
          $count ++;
        }
          echo implode('',$bread);
        echo '</ol>';
        // echo'<hr class="style-six;display:none;"/>';
      }
    ?>

    </nav>
    </div>
   
  </div>
    <!-- <h2><?php echo isset($titulo_modulo) ? $titulo_modulo : "" ?></h2><br/> -->
    {{$content}}
</div>
<?php } ?>

@endsection
@section('scripts')
@endsection