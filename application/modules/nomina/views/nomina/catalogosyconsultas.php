<div class="icon-bar">
	<a href="<?php echo site_url('nomina/catalogosconsultas/catalogossistema'); ?>" >
		<i class="fa fa-book"></i><br/>
        <h6><small>Catálogos del sistema</small></h6>
	</a>
	<a href="<?php echo site_url('nomina/catalogosconsultas/organizacion'); ?>" >
		<i class="fa fa-book"></i><br/>
        <h6><small>Organización</small></h6>
	</a>
	<a href="<?php echo site_url('nomina/catalogosyconsultas'); ?>">
		<i class="fa fa-book"></i><br/>
        <h6><small>Percepciones y deducciones</small></h6>
	</a>
	<a href="<?php echo site_url('nomina/catalogosyconsultas'); ?>">
		<i class="fa fa-book"></i><br/>
        <h6><small>Consulta de movimientos</small></h6>
	</a>

	<a href="<?php echo site_url('nomina/catalogosyconsultas'); ?>">
		<i class="fa fa-book"></i><br/>
        <h6><small>Personalizar</small></h6>
	</a>
	<!-- <a href="#"><i class="fa fa-envelope"></i></a>
	<a href="#"><i class="fa fa-globe"></i></a>
	<a href="#"><i class="fa fa-trash"></i></a> -->
</div>

<style>
	.icon-bar {
		width: 100%;
		/* Full-width */
		background-color: #f4f5f6;
		/* Dark-grey background */
		overflow: auto;
		/* Overflow due to float */
	}

	.icon-bar a {
		float: left;
		/* Float links side by side */
		text-align: center;
		/* Center-align text */
		width: 20%;
		/* Equal width (5 icons with 20% width each = 100%) */
		padding: 12px 0;
		/* Some top and bottom padding */
		transition: all 0.3s ease;
		/* Add transition for hover effects */
		color: white;
		/* White text color */
		font-size: 36px;
		/* Increased font size */
	}

	.icon-bar a:hover {
		background-color: #f8f9fa;
		/* Add a hover color */
	}

	.active {
		background-color: #4CAF50;
		/* Add an active/current color */
	}

</style>
