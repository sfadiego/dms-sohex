@layout('tema_luna/layout')
@section('contenido')

<script>
// const NOMINA_API = "http://localhost/sistema_nomina/respaldo/";
// const NOMINA_API_new = "http://localhost/sistema_nomina/";

const NOMINA_API = "https://www.sohex.net/nomina_dms/respaldo/";
const NOMINA_API_new = "https://www.sohex.net/nomina_dms/";
</script>

<?php 
  echo link_tag('js/nomina/apps.css');
  echo link_tag('js/nomina/jstree/themes/default/style.min.css');
  echo script_tag('js/nomina/apps.js');
  echo script_tag('js/nomina/mustache.min.js');
  echo script_tag('js/nomina/moment-with-locales.min.js');
  echo is_array($scripts)? implode('',$scripts) : $scripts;
  $active = ($this->input->get('menu') != false)? $this->input->get('menu') : $this->uri->segment(2);
?>

<nav class="navbar navbar-expand-lg navbar-light pr-3 pl-3 mb-3" style="background-image: inherit;background-color: #fff;display:none;">
  <a class="navbar-brand" href="<?php echo site_url('nomina/menu?menu=nomina'); ?>"><?php echo isset($modulo) ? $modulo : "" ?></a>
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>

  <div class="collapse navbar-collapse" id="navbarSupportedContent">
    <ul class="navbar-nav mr-auto">
 
      <li class="nav-item <?php echo ($active == 'catalogosconsultas')? 'active':''; ?>">
        <a class="nav-link" href="<?php echo site_url('nomina/menu?menu=catalogosconsultas'); ?>" style="text-transform: none;">
            Catalogos y consultas
        </a>
      </li>
      <li class="nav-item <?php echo ($active == 'acumulados')? 'active':''; ?>">
        <a class="nav-link" href="<?php echo site_url('nomina/menu?menu=acumulados'); ?>" style="text-transform: none;">
          Acumulados
        </a>
      </li>

      
      <li class="nav-item dropdown">
        <a class="nav-link dropdown-toggle <?php echo ($active == '1')? 'active':''; ?>" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" style="text-transform: none;">
            Fiscales
        </a>
        <div class="dropdown-menu" aria-labelledby="navbarDropdown">
          <a class="dropdown-item" href="#">ISR</a>
          <a class="dropdown-item" href="#">SDI</a>
          <a class="dropdown-item" href="#">Avisos al IMSS</a>
          <a class="dropdown-item" href="#">Enlace SUA</a>
          <a class="dropdown-item" href="#">IMSS</a>
          <a class="dropdown-item" href="#">Bases fiscales por percepción</a>
          <a class="dropdown-item" href="#">Analizador</a>
        </div>
      </li>

      <li class="nav-item dropdown">
        <a class="nav-link dropdown-toggle <?php echo ($active == '1')? 'active':''; ?>" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" style="text-transform: none;">
            Reportes
        </a>
        <div class="dropdown-menu" aria-labelledby="navbarDropdown">
          <a class="dropdown-item" href="#">Nómina</a>
          <a class="dropdown-item" href="#">Pago de nómina</a>
          <a class="dropdown-item" href="#">Movimientos de la nómina</a>
          <a class="dropdown-item" href="#">Catalogos</a>
          <a class="dropdown-item" href="#">Empresa</a>
          <a class="dropdown-item" href="#">Administrador de reportes</a>
          <a class="dropdown-item" href="#">Documentos</a>
        </div>
      </li>

      <li class="nav-item dropdown">
        <a class="nav-link dropdown-toggle <?php echo ($active == '1')? 'active':''; ?>" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" style="text-transform: none;">
            Procesos de nómina
        </a>
        <div class="dropdown-menu" aria-labelledby="navbarDropdown">
          <a class="dropdown-item" href="#">Dispersión</a>
          <a class="dropdown-item" href="#">Incrementos</a>
          <a class="dropdown-item" href="#">Cálculo inverso</a>
          <a class="dropdown-item" href="#">Contratos</a>
          <a class="dropdown-item" href="#">Finiquitos</a>
          <a class="dropdown-item" href="#">Fondo de ahorro</a>
          <a class="dropdown-item" href="#">PTU</a>
          <a class="dropdown-item" href="#">Fonacot</a>
          <a class="dropdown-item" href="#">Cierre de nómina</a>
          <a class="dropdown-item" href="#">Corte anual</a>
        </div>
      </li>

      <li class="nav-item dropdown">
        <a class="nav-link dropdown-toggle <?php echo ($active == '1')? 'active':''; ?>" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" style="text-transform: none;">
            Herramientas
        </a>
        <div class="dropdown-menu" aria-labelledby="navbarDropdown">
          <a class="dropdown-item" href="#">Periodo</a>
          <a class="dropdown-item" href="#">Acumulados</a>
          <a class="dropdown-item" href="#">Importación</a>
          <a class="dropdown-item" href="#">Interfaz</a>
          <a class="dropdown-item" href="#">Exportación</a>
          <a class="dropdown-item" href="#">Eliminación de nómina</a>
          <a class="dropdown-item" href="#">Parámetros de la nómina</a>
          <a class="dropdown-item" href="#">Bitacora</a>
          <a class="dropdown-item" href="#">Exportar Información</a>
        </div>
      </li>

      <li class="nav-item dropdown">
        <a class="nav-link dropdown-toggle <?php echo ($active == '1')? 'active':''; ?>" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" style="text-transform: none;">
            Configuración
        </a>
        <div class="dropdown-menu" aria-labelledby="navbarDropdown">
          <a class="dropdown-item" href="#">Parámetros del sistema</a>
          <a class="dropdown-item" href="#">Empresas</a>
          <a class="dropdown-item" href="#">Consultas</a>
          <a class="dropdown-item" href="#">Respaldo</a>
          <a class="dropdown-item" href="#">Control de archivos</a>
          <a class="dropdown-item" href="#">Usuarios</a>
        </div>
      </li>

    </ul>
    
  </div>
</nav>


<?php if(isset($content) && strlen($content)>0){ ?>
<div class="container-fluid panel-body">    
<h1 class="mt-4">Nomina</h1>
    <?php
      $bread = array();
      if(isset($breadcrumb) && is_array($breadcrumb)){
        $count = 1;
        echo '<ol class="breadcrumb" style="" >';
        foreach ($breadcrumb as $key => $value) {
          // $bread[] = '<li class="breadcrumb-item '.(($count == count($breadcrumb))? 'active' : '').'" '.(($count == count($breadcrumb))? 'aria-current="page"' : '').' >'.(is_array($value)? anchor($value['url'], $value['name'],'class="small"') : '<small>'.$value.'</small>').'</li>';
          $bread[] = '<li class="breadcrumb-item '.(($count == count($breadcrumb))? 'active' : '').'" '.(($count == count($breadcrumb))? 'aria-current="page"' : '').' >'.(is_array($value)? $value['name'] : $value).'</li>';
          $count ++;
        }
          echo implode('',$bread);
        echo '</ol>';
        // echo'<hr class="style-six;display:none;"/>';
      }
    ?>
    <!-- <h2><?php echo isset($titulo_modulo) ? $titulo_modulo : "" ?></h2><br/> -->
    {{$content}}
</div>
<?php } ?>

@endsection
@section('scripts')
@endsection