<script>
	var id_periodicidad = "{id_periodicidad}";
	var id_periodo = "{id_periodo}";
    var periodo_libre = true;
</script>

<div class="row mt-4 mb-3">
	<div class="col-sm-12">
		<h3>Creacion del siguiente periodo</h3>
	</div>
</div>

<div class="row mt-4 mb-3">
	<div class="col-sm-7 mx-auto">
        <div class="card">
            <div class="card-body">
                <form id="form_content">
                    <div class="form-group">
                    <label for="fecha_inicio" class="col-form-label">Fecha de inicio:</label>
                        <input type="date" readonly  class="form-control" id="fecha_inicio" name="fecha_inicio" value="<?php echo $inicio_periodo; ?>">
                        <small id="msg_fecha_inicio" class="form-text text-danger"></small>
                    </div>
                    <div class="form-group">
                        <label for="fecha_fin" class="col-form-label">Fecha de la siguiente nómina:</label>
                        <input type="date" class="form-control" id="fecha_fin" name="fecha_fin" min="<?php echo $inicio_periodo; ?>" value="<?php echo $fin_periodo; ?>" >
                        <small id="msg_fecha_fin" class="form-text text-danger"></small>
                    </div>
                
                    <div class="row">
                        <!-- <div class="col-sm-3">&nbsp;</div> -->
                        <div class="mx-auto">
                            <button type="button" onclick="Apps.cancelar();" class="btn btn-danger" data-dismiss="modal"><i class="fas fa-times"></i> Cancelar</button>
                            <button onclick="Apps.guardar(this);" type="button" class="btn btn-success"><i class="far fa-save"></i> Guardar</button>
                        </div>
                    </div>

                </form>
            </div>
        </div>
    </div>
</div>



<!-- <div class="mx-auto col-sm-4 mt-4">
	<div class="card">
		<div class="card-body">
			<h4 class="card-subtitle mb-2 text-muted">Ayuda</h4>
			<ul class="list-unstyled mt-3">
				<li class="media">
					<i class="mr-3 fas fa-paper-plane text-info" style="font-size: 1.2em;"></i>
					<div class="media-body">
						<h5 class="mt-0 mb-1 text-dark" style="font-size: 1.1em;">Enviar Comprobante Fiscal Digital</h5>
					</div>
				</li>
			</ul>
		</div>
	</div>
</div> -->