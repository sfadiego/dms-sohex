<div class="modal-header">
    <h5 class="text-dark" >Ediar periodo</h5>
    </div>
    <div class="modal-body pl-3 pr-3 pt-3 pb-3">
    <form id="form_content">
        <div class="form-group">
            <label for="id_tipoperiodo" class="col-form-label">Tipo de periodo:</label>
            <select class="form-control" readonly disabled  id="id_tipoperiodo" name="id_tipoperiodo" >
                <?php if(isset($id_tipo_periodo) && is_array($id_tipo_periodo) ){ ?>
                    <?php foreach ($id_tipo_periodo as $key => $value) { ?>
                        <option <?php echo array_key_exists('id_Periodicidad',$data) && $data['id_Periodicidad'] == $value['id'] ? 'selected' : ''; ?> value="<?php echo $value['id']; ?>"  ><?php echo $value['Descripcion']; ?></option>
                    <?php } ?>
                <?php } ?>
            </select>
            <small id="msg_id_tipoperiodo" class="form-text text-danger"></small>
        </div>
        <div class="form-group">
        <label for="fecha_inicio" class="col-form-label">Fecha de inicio:</label>
            <input type="date" class="form-control" readonly                                                                                                                                          value="<?php echo array_key_exists('FechaInicio',$data)? $data['FechaInicio'] : ''; ?>">
            <small id="msg_fecha_inicio" class="form-text text-danger"></small>
        </div>
        <div class="form-group">
            <label for="fecha_fin" class="col-form-label">Fecha fin:</label>
            <input type="hidden" value="<?php echo array_key_exists('id',$data)? $data['id'] : ''; ?>" class="form-control" id="id" name="id">
            <input type="date" onkeyup="Apps.modal_actualizar_fechas_corte();" onchange="Apps.modal_actualizar_fechas_corte();"  min="<?php echo array_key_exists('FechaInicio',$data)? $data['FechaInicio'] : ''; ?>"  value="<?php echo array_key_exists('FechaFin',$data)? $data['FechaFin'] : ''; ?>" class="form-control" id="fecha_fin" name="fecha_fin">
            <small id="msg_fecha_fin" class="form-text text-danger"></small>
        </div>
        <div class="form-group">
            <label for="fecha_corte" class="col-form-label">Fecha de corte programado:</label>
            <input type="date" class="form-control"  id="fecha_corte" name="fecha_corte"  value="<?php echo array_key_exists('FechaProceso',$data)? $data['FechaProceso'] : ''; ?>">
            <small id="msg_fecha_corte" class="form-text text-danger"></small>
        </div>
    </form>
    </div>
    <div class="modal-footer">
    <button type="button" class="btn btn-danger btn-sm" data-dismiss="modal">Cancelar</button>
    <button type="button" class="btn btn-success  btn-sm" onclick="Apps.modal_editar_periodo_save();">Guardar</button>
</div>