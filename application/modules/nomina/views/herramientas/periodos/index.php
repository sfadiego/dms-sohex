<script>
    var periodo_libre = true;
</script>

<div class="row">
	<div class="col-sm-12">
		<h3 class="mt-2 mb-3">Administrador de periodos de la nomina</h3>
	</div>
</div>

<!-- <div class="card">
	<div class="card-body">

		<form id="general_form">
			<div class="row">
				<div class="col-sm-12">
					<div class="form-group row">
						<label for="staticEmail" class="col-sm-2 col-form-label">Periodicidad</label>
						<div class="col-sm-10">
							<select class="custom-select"  id="Periodicidad" name="Periodicidad">
								{periodicidad}
								<option value="{id}">[{Clave}] {Descripcion}</option>
								{/periodicidad}
							</select>
						</div>
					</div>

					<div class="row">
						<div class="col-sm-12">
							<button onclick="Apps.addContenido();" type="button" class="btn btn-success btn-sm  float-right ml-1 mr-1">Buscar</button>
              				<button onclick="location.reload();" type="button" class="btn btn-default btn-sm float-right ml-1 mr-1">Limpiar</button>
						</div>
					</div>

				</div>
			</div>

		</form>

	</div>
</div> -->

<div class="card mt-4">
	<div class="card-body ">

	<!-- <div class="row py-0 pr-0 pl-6 d-flex justify-content-between align-items-center mb-5 mt-0">
		<h6 class="card-title mb-0">&nbsp;</h6>
		<div class="btn-group" role="group" aria-label="Basic example">
			<button onclick="Apps.modal_nuevo_periodo();" type="button" class="btn btn-success btn-sm"><i class="fa fa-plus"></i> Registrar nuevo periodo</button>
		</div>
	</div> -->

	<div class="row">
		<div class="col-sm-12">
			<?php if($exite_periodos == true){ ?>
			<div class="table-responsive">
				<table class="table table-bordered table-sm" id="listado" width="100%" cellspacing="0"></table>
			</div>
			<?php }else{ ?>
				<button type="button" onclick='Apps.creacion_siguiente_periodo(0);' class="btn btn-outline-success btn-lg btn-block"><i class='far fa-calendar-plus'></i> Crear periodo</button>
			<?php } ?>
		</div>
	</div>

	</div>
</div>


<!-- <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
	aria-hidden="true">
	<div class="modal-dialog modal-dialog-centered" role="document">
		<div class="modal-content">
			

			<div class="modal-body">
				<h3 class="mb-3">Editar último periodo</h3>
				<form>

					<div class="form-group">
						<label for="exampleFormControlSelect1">Periodicidad</label>
						<select onchange="Apps.recalcular();" class="form-control" id="periodicidad" name="periodicidad">
							{periodicidad}
							<option value="{id}">{Clave} {Descripcion}</option>
							{/periodicidad}
						</select>
					</div>
					<div class="form-group">
						<label for="exampleFormControlTextarea1">Fecha Inicio</label>
						<input type="" class="form-control-plaintext" readonly id="fecha_inicio" placeholder="">
					</div>
					<div class="form-group">
						<label for="exampleFormControlTextarea1">Fecha Fin</label>
						<input type="" class="form-control-plaintext" readonly id="fecha_fin" placeholder="">
					</div>
				</form>

				<div class="form-group row mt-3">
					<div class="col-sm-3">&nbsp;</div>
					<div class="col-sm-9">
						<button type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>
						<button onclick="Apps.guardar(this);" type="button" class="btn btn-success col-md-4">Guardar</button>
					</div>
				</div>


			</div>

		</div>
	</div>
</div> -->



<div id="modal_nuevo_periodo" class="modal fade bd-example-modal-sm" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
    </div>
  </div>
</div>
