<script>
    var periodo_libre = true;
    var url_redirect = "<?php echo $url; ?>";
</script>

<div class="row">
    <div class="mx-auto col-md-12">
        <div class="card">
            <div class="card-body">

                <form id="general_form">
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="form-group row">
                                <label for="staticEmail" class="col-sm-2 col-form-label">Periodos en curso:</label>
                                <div class="col-sm-10">
                                    <select class="custom-select"  id="Periodicidad" name="Periodicidad" onchange="var opcion = $('select#Periodicidad option:selected').attr('date'); $('small#fecha_corte').html( (opcion != undefined)? 'Fecha de corte programada: <b>'+opcion+'</b>' : ''  );">
                                        <?php if(is_array($periodos)){ ?>
                                            <option value="">Seleccione un periodo</option>
                                            <?php foreach ($periodos as $key => $value) { ?>
                                                <option value="<?php echo $value['id']; ?>" date="<?php echo utils::aFecha($value['FechaProceso'],true) ?>" fecha_fin="<?php echo utils::aFecha($value['FechaFin'],true) ?>" fecha_inicio="<?php echo utils::aFecha($value['FechaInicio'],true) ?>" title="<?php echo $value['tipo_periodo']; ?>" ><?php echo 'Periodo '.$value['tipo_periodo'].' [ '.utils::aFecha($value['FechaInicio'],true).' al '.utils::aFecha($value['FechaFin'],true).' ]'; ?></option>
                                            <?php } ?>
                                        <?php } ?>
                                    </select>
                                    <small id="msg_periodo" class="form-text text-danger"></small>
                                    <small id="fecha_corte" class="form-text text-dark mt-2"></small>
                                </div>
                            </div>

                            <div class="row mt-2">
                                <div class="mx-auto col-md-4">
                                    <button onclick="Apps.guardar();" type="button" class="btn btn-secondary  btn-sm btn-block float-right ml-1 mr-1">Guardar</button>
                                </div>
                            </div>

                        </div>
                    </div>

                </form>

            </div>
        </div>
    </div>
</div>
