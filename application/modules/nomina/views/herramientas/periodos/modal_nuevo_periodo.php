<div class="modal-header">
    <h5 class="text-dark" >Registrar nuevo periodo</h5>
    </div>
    <div class="modal-body pl-3 pr-3 pt-3 pb-3">
    <form id="form_content">
        <div class="form-group">
            <label for="id_tipoperiodo" class="col-form-label">Tipo de periodo:</label>
            <select class="form-control"  id="id_tipoperiodo" name="id_tipoperiodo" onclick="Apps.modal_actualizar_fechas();">
                <?php if(isset($id_tipo_periodo) && is_array($id_tipo_periodo) ){ ?>
                    <?php foreach ($id_tipo_periodo as $key => $value) { ?>
                        <option value="<?php echo $value['id']; ?>" fechaInicio="<?php echo $value['fechaInicio']; ?>" fechaFin="<?php echo $value['fechaFin']; ?>" ><?php echo $value['Descripcion']; ?></option>
                    <?php } ?>
                <?php } ?>
            </select>
            <small id="msg_id_tipoperiodo" class="form-text text-danger"></small>
        </div>
        <div class="form-group">
        <label for="fecha_inicio" class="col-form-label">Fecha de inicio:</label>
            <input type="date" class="form-control" id="fecha_inicio" name="fecha_inicio">
            <small id="msg_fecha_inicio" class="form-text text-danger"></small>
        </div>
        <div class="form-group">
            <label for="fecha_fin" class="col-form-label">Fecha fin:</label>
            <input type="date" onkeyup="Apps.modal_actualizar_fechas_corte();" onchange="Apps.modal_actualizar_fechas_corte();" class="form-control" id="fecha_fin" name="fecha_fin">
            <small id="msg_fecha_fin" class="form-text text-danger"></small>
        </div>
        <div class="form-group">
            <label for="fecha_corte" class="col-form-label">Fecha de corte programado:</label>
            <input type="date" class="form-control"  id="fecha_corte" name="fecha_corte">
            <small id="msg_fecha_corte" class="form-text text-danger"></small>
        </div>
    </form>
    </div>
    <div class="modal-footer">
    <button type="button" class="btn btn-danger btn-sm" data-dismiss="modal">Cancelar</button>
    <button type="button" class="btn btn-success  btn-sm" onclick="Apps.modal_nuevo_periodo_save();">Guardar</button>
</div>