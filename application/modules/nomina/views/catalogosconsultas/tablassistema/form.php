<script>
	const uuid_temp = "{uuid_temp}";
	const identity = "{id}";
</script>

<script id="template" type="x-tmpl-mustache">
	<div class="form-group row">
		<label for="Clave" class="col-sm-3 col-form-label">Clave:</label>
		<div class="col-sm-9">
			<input type="number" class="form-control-plaintext" readonly="true" id="Clave" name="Clave" value="{{Clave}}">
			<small id="msg_Clave" class="form-text text-danger"></small>
		</div>
	</div>

	<div class="form-group row">
		<label for="id_TipoCatalogo" class="col-sm-3 col-form-label">Tipo:</label>
		<div class="col-sm-9">
			<select class="custom-select" id="id_TipoCatalogo" name="id_TipoCatalogo" onchange="Apps.getLabelTable();" {{TipoCatalogoOption}} >
				{{#TipoCatalogo}}
				<option value="{{id}}" campo_1="{{Campo_1}}" campo_2="{{Campo_2}}" campo_3="{{Campo_3}}" {{selected}}>{{Descripcion}}</option>
				{{/TipoCatalogo}}
			</select>
			<small id="msg_id_TipoCatalogo" class="form-text text-danger"></small>
		</div>
	</div>

	<div class="form-group row">
		<label for="Descripcion" class="col-sm-3 col-form-label">Descripción:</label>
		<div class="col-sm-9">
			<input type="text" class="form-control" id="Descripcion" name="Descripcion" value="{{Descripcion}}">
			<small id="msg_Descripcion" class="form-text text-danger"></small>
		</div>
	</div>

	<div class="form-group row">
		<label for="Descripcion" class="col-sm-3 col-form-label">Contenido:</label>
		<div class="col-sm-9">
			<div class="row">
				<div class="col-md-12">
					<button onclick="Apps.addContenido();" type="button" class="btn btn-success">Agregar</button>
					<small id="msg_Contenido" class="form-text text-danger"></small>
				</div>

				<div class="col-md-12 mt-3">
					<div class="table-responsive">

						<table class="table table-bordered" id="listado" width="100%" cellspacing="0">
							<thead>
								<tr>
									<th id="valor_1h"></th>
									<th id="valor_2h"></th>
									<th id="valor_3h"></th>
									<th>-</th>
									<th>-</th>
								</tr>
							</thead>
							<tbody>
							</tbody>
							<tfoot>
								<tr>
									<th id="valor_1f"></th>
									<th id="valor_2f"></th>
									<th id="valor_3f"></th>
									<th>-</th>
									<th>-</th>
								</tr>
							</tfoot>
						</table>
					</div>
				</div>
			</div>

		</div>
	</div>
</script>

<script id="templateEdit" type="x-tmpl-mustache">
	<div class="form-group row">
		<label for="Clave" class="col-sm-3 col-form-label">Clave:</label>
		<div class="col-sm-9">
			<input  class="form-control-plaintext" readonly="true" id="Clave" name="Clave" value="{{Clave}}">
			<small id="msg_Clave" class="form-text text-danger"></small>
		</div>
	</div>

	<div class="form-group row">
		<label for="id_TiposCatalogo" class="col-sm-3 col-form-label">Clave:</label>
		<div class="col-sm-9">
			<input  value="{{TipoCatalogo.Nombre}}" campo_1="{{TipoCatalogo.Campo_1}}" campo_2="{{TipoCatalogo.Campo_2}}" campo_3="{{TipoCatalogo.Campo_3}}" class="form-control-plaintext" readonly="true" id="id_TiposCatalogo" name="id_TiposCatalogo">
			<small id="msg_id_TiposCatalogo" class="form-text text-danger"></small>
		</div>
	</div>

	<div class="form-group row">
		<label for="Descripcion" class="col-sm-3 col-form-label">Descripción:</label>
		<div class="col-sm-9">
			<input type="text" class="form-control" id="Descripcion" name="Descripcion" value="{{Descripcion}}">
			<small id="msg_Descripcion" class="form-text text-danger"></small>
		</div>
	</div>

	<div class="form-group row">
		<label for="Descripcion" class="col-sm-3 col-form-label">Contenido:</label>
		<div class="col-sm-9">
			<div class="row">
				<div class="col-md-12">
					<button onclick="Apps.addContenido();" type="button" class="btn btn-success">Agregar</button>
					<small id="msg_Contenido" class="form-text text-danger"></small>
				</div>

				<div class="col-md-12 mt-3">
					<div class="table-responsive">

						<table class="table table-bordered" id="listado" width="100%" cellspacing="0">
							<thead>
								<tr>
									<th id="valor_1h"></th>
									<th id="valor_2h"></th>
									<th id="valor_3h"></th>
									<th>-</th>
									<th>-</th>
								</tr>
							</thead>
							<tbody>
							</tbody>
							<tfoot>
								<tr>
									<th id="valor_1f"></th>
									<th id="valor_2f"></th>
									<th id="valor_3f"></th>
									<th>-</th>
									<th>-</th>
								</tr>
							</tfoot>
						</table>
					</div>
				</div>
			</div>

		</div>
	</div>
</script>

<form id="formContent">
	<div id="contentDiv"></div>

	<div class="form-group row mt-5">
		<div class="col-sm-3">&nbsp;</div>
		<div class="col-sm-9">
			<a href="<?php echo site_url('nomina/catalogosconsultas/tablassistema'); ?>" type="button" class="btn btn-danger col-md-4">Cancelar</a>
			<button onclick="Apps.guardar();" type="button" class="btn btn-success col-md-4">Guardar</button>
		</div>
	</div>
</form>


<div class="modal fade" id="modalContenido" tabindex="-1" role="dialog" aria-labelledby=""
	aria-hidden="true">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-body">
				<form id="modalContenidoForm">
					<h3 class="mb-3">Agregar contenido</h3>
					<div class="form-group row">
						<label for="Valor_1" class="col-sm-5 col-form-label"></label>
						<div class="col-sm-7">
							<input type="text" class="form-control" id="Valor_1" name="Valor_1" value="">
							<small id="msg_Valor_1" class="form-text text-danger"></small>
						</div>
					</div>

					<div class="form-group row">
						<label for="Valor_2" class="col-sm-5 col-form-label"></label>
						<div class="col-sm-7">
							<input type="text" class="form-control" id="Valor_2" name="Valor_2" value="">
							<small id="msg_Valor_2" class="form-text text-danger"></small>
						</div>
					</div>

					<div class="form-group row">
						<label for="Valor_3" class="col-sm-5 col-form-label"></label>
						<div class="col-sm-7">
							<input type="text" class="form-control" id="Valor_3" name="Valor_3" value="">
							<small id="msg_Valor_3" class="form-text text-danger"></small>
						</div>
					</div>

					<div class="form-group row mt-2">
						<div class="col-sm-3">&nbsp;</div>
						<div class="col-sm-9">
							<button type="button" onclick="Apps.guardarContenido(this);" class="btn btn-success">Guardar</button>
							<button type="button" class="btn btn-danger" data-dismiss="modal">Cancelar</button>
						</div>
					</div>
				</form>
			</div>
		</div>
	</div>
</div>

<div class="modal fade" id="modalContenidoEdit" tabindex="-1" role="dialog" aria-labelledby=""
	aria-hidden="true">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-body">
				<form id="modalContenidoEditForm">
					<h3 class="mb-3">Editar contenido</h3>
					<div class="form-group row">
						<label for="Valor_1" class="col-sm-3 col-form-label"></label>
						<div class="col-sm-9">
							<input type="text" class="form-control" id="Valor_1" name="Valor_1" value="">
							<small id="msg_Valor_1" class="form-text text-danger"></small>
						</div>
					</div>

					<div class="form-group row">
						<label for="Valor_2" class="col-sm-3 col-form-label"></label>
						<div class="col-sm-9">
							<input type="text" class="form-control" id="Valor_2" name="Valor_2" value="">
							<small id="msg_Valor_2" class="form-text text-danger"></small>
						</div>
					</div>

					<div class="form-group row">
						<label for="Valor_3" class="col-sm-3 col-form-label"></label>
						<div class="col-sm-9">
							<input type="text" class="form-control" id="Valor_3" name="Valor_3" value="">
							<small id="msg_Valor_3" class="form-text text-danger"></small>
						</div>
					</div>

					<div class="form-group row mt-2">
						<div class="col-sm-3">&nbsp;</div>
						<div class="col-sm-9">
							<button type="button" data-id='' id="guardarEditButton" onclick="Apps.guardarEditarContenido(this);" class="btn btn-success">Guardar</button>
							<button type="button" class="btn btn-danger" data-dismiss="modal">Cancelar</button>
						</div>
					</div>
				</form>
			</div>
		</div>
	</div>
</div>


