<script>
	const identity = "{identity}";
</script>

<script id="template" type="x-tmpl-mustache">
	<div class="form-group row">
		<label for="clave" class="col-sm-3 col-form-label">Clave:</label>
		<div class="col-sm-9">
			<input type="number" class="form-control-plaintext" readonly="true" id="clave" name="clave" value="{{Clave}}">
            <small id="msg_clave" class="form-text text-danger"></small>
		</div>
	</div>
	<div class="form-group row">
		<label for="Descripcion" class="col-sm-3 col-form-label">Descripción:</label>
		<div class="col-sm-9">
			<input type="text" class="form-control" id="Descripcion" name="Descripcion" value="{{Descripcion}}">
            <small id="msg_Descripcion" class="form-text text-danger"></small>
		</div>
	</div>
</script>

<form id="formContent">
	<div id="contenedor"></div>
	<div class="form-group row">
		<div class="col-sm-3">&nbsp;</div>
		<div class="col-sm-9">
			<button onclick="Apps.guardar();" type="button" class="btn btn-success col-md-4">Guardar</button>
		</div>
	</div>
</form>
