<script>
    var contenido = [ ['Puestos', 'Total'], <?php foreach ($contenido as $key => $value) { echo '["'.$key.'",'.$value.'.],';  } ?>];
</script>

<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
<script type="text/javascript">

    

    google.charts.load('current', {'packages':['bar']});
    google.charts.setOnLoadCallback(drawStuff);

    function drawStuff() {
    var data = new google.visualization.arrayToDataTable(contenido);


    var options = {
        title: 'Trabajadores Activos por puestos',
        width: 900,
        legend: { position: 'none' },
        chart: { title: 'Trabajadores Activos por puestos',
                subtitle: '' },
        bars: 'vertical', // Required for Material Bar Charts.
        axes: {
        y: {
            0: { side: 'buttom', label: 'Total de trabajadores'} // Top x-axis.
        }
        },
        bar: { groupWidth: "90%" }
    };

    var chart = new google.charts.Bar(document.getElementById('top_x_div'));
    chart.draw(data, options);
    };
</script>

<div class="card">
  <div class="card-body">
    <div id="top_x_div" style="width: 900px; height: 500px;"></div>
  </div>
</div>