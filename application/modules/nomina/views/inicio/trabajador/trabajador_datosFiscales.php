<input type="hidden" class="form-control" name="id" value="{{id}}">

<div class="row">
	
	<div class="col-sm-12">
		<div class="card">
			<div class="card-body">

				<h6 class="text-dark">Régimen de contratación:</h6>
				<hr class="style-six" />
			
				<div class="form-group row">
					<label for="id_RegimenContrato" class="col-sm-4 col-form-label">Régimen para recibos electrónicos:</label>
					<div class="col-sm-8">
						<select class="form-control" id="id_RegimenContrato" name="id_RegimenContrato" attr-id="{{id_RegimenContrato}}">
							{{#RegimenContrato}}<option value="{{id}}" {select}>{{Clave}} - {{Descripcion}}</option>{{/RegimenContrato}}
						</select>
						<small id="msg_id_RegimenContrato" class="form-text text-danger"></small>
					</div>
				</div>

				<div class="form-group row">
					<label for="id_EntidadFederativa" class="col-sm-4 col-form-label">Entidad federativa donde se presta el servicio:</label>
					<div class="col-sm-8">
						<select class="form-control" id="id_EntidadFederativa" name="id_EntidadFederativa" attr-id="{{id_EntidadFederativa}}">
							{{#EntidadesFederativa}}<option value="{{id}}" {select}>{{Clave}} - {{Nombre}}</option>{{/EntidadesFederativa}}
						</select>
						<small id="msg_id_EntidadFederativa" class="form-text text-danger"></small>
					</div>
				</div>

				<div class="form-group row">
					<label for="id_TipoContrato" class="col-sm-4 col-form-label">Tipo de contrato:</label>
					<div class="col-sm-8">
						<select class="form-control" id="id_TipoContrato" name="id_TipoContrato" attr-id="{{id_TipoContrato}}">
							{{#TipoContrato}}<option value="{{id}}" {select}>{{Clave}} - {{Descripcion}}</option>{{/TipoContrato}}
						</select>
						<small id="msg_id_TipoContrato" class="form-text text-danger"></small>
					</div>
				</div>

				<div class="form-group row">
					<label for="id_TipoJornada" class="col-sm-4 col-form-label">Tipo jornada:</label>
					<div class="col-sm-8">
						<select class="form-control" id="id_TipoJornada" name="id_TipoJornada" attr-id="{{id_TipoJornada}}">
							{{#TipoJornada}}<option value="{{id}}" {select}>{{Clave}} - {{Nombre}}</option>{{/TipoJornada}}
						</select>
						<small id="msg_id_TipoJornada" class="form-text text-danger"></small>
					</div>
				</div>

			</div>
		</div>
	</div>

</div>