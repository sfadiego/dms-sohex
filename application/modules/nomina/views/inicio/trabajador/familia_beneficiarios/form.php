<script>
    var  identity = "{identity}";
</script>

<div class="row mt-5 mb-3">
	<div class="col-sm-12">
		<h2>Familia y beneficiarios</h2>
	</div>
</div>

<div class="row">
    <div class="col-sm-12">
        <nav class="nav nav-pills nav-fill">
            <a class="nav-item nav-link {tab_1}" href="{url_1}">Familia</a>
            <a class="nav-item nav-link {tab_2}" href="{url_2}">Beneficiarios</a>
        </nav>
    </div>

    <div class="col-sm-12">
        <div class="mt-1" >
        <div class="card">
  <div class="card-body">
            {content_form}
        </div>
        </div>
        </div>
    </div>

    <!-- <div class="col-sm-12">
        <div class="form-group row mt-3">
            <div class="col-sm-3">&nbsp;</div>
            <div class="col-sm-9">
                <button onclick="Apps.guardar(this);" type="button" class="btn btn-success col-md-4">Guardar</button>
            </div>
        </div>
    </div> -->
</div>