
<script>
    var identity = "{identity}";
    var trabajador = "{trabajador}";
</script>

<div class="card">
  <div class="card-body">
  <h4 class="card-title text-dark">Datos</h4>
<?php echo form_open('#', 'class="" id="general_form"'); ?>
<div class="row mt-4">
    {grupo1}
    <div class="col-sm-6">
        <div class="form-group">
                <label for="{key}">{label}</label>
                {input}
                <small id="msg_{key}" class="form-text text-danger"></small>
        </div>
    </div>
    {/grupo1}

    <div class="col-sm-12">
        <div class="col-sm-12">
            <div class="form-group row mt-3">
                <div class="col-sm-3">&nbsp;</div>
                <div class="col-sm-9">
                    <button onclick="Apps.guardar(this);" type="button" class="btn btn-success col-md-4">Guardar</button>
                </div>
            </div>
        </div>
    </div> 

</div>
<?php echo form_close(); ?>
</div>
</div>