<div class="row mt-4 mb-3">
	<div class="col-sm-12">
		<h3>Catálogo de trabajadores</h3>
	</div>
</div>

<div class="row mb-2">
	<div class="col-md-8"></div>
	<div class="col-md-4" align="right">
		<a class="btn btn-primary" href="<?php echo site_url('nomina/inicio/trabajador/alta') ?>">Registrar</a>
	</div>
</div>

<div class="row">
	<div class="col-md-12">
		<div class="table-responsive">
			<table class="table table-bordered dataTable" id="listado" width="100%" cellspacing="0">
			</table>
		</div>
	</div>
</div>

<script>
	var NOMINA_API_IMAGEN = "<?php echo NOMINA_API; ?>";
</script>
