<style>
table {
  border-collapse: collapse;
  width: 100%;
}

th, td {
  text-align: left;
  padding: 8px;
}

tr:nth-child(even) {background-color: #f2f2f2;}
</style>

<br/>
<div style="">
    <table style="" class="" cellpadding="5">
        <tr>
            <th width="150px" bgcolor="">
                Clasificación:
            </th>
            <td>
                <?php echo (isset($clasificacion))? $clasificacion : 'Todos' ?>
            </td>
        </tr>

        <tr>
            <th width="150px" bgcolor="">
                Departamentos:
            </th>
            <td>
            <?php echo (isset($departamentos))? $departamentos : 'Todos' ?>
            </td>
        </tr>

        <tr>
            <th width="150px" bgcolor="">
                Periodo de pago:
            </th>
            <td>
                <?php echo (isset($periodo_desc))? $periodo_desc : '' ?>
            </td>
        </tr>

        <tr>
            <th width="150px" bgcolor="">
                Periodicidad:
            </th>
            <td>
            <?php echo (isset($parametros_desc))? $parametros_desc : '' ?>
            </td>
        </tr>

        <tr>
            <th width="150px" bgcolor="">
                Forma de pago:
            </th>
            <td>
                <?php echo (isset($formapago_desc))? $formapago_desc : '' ?>
            </td>
        </tr>
    </table>
</div>
<br/>