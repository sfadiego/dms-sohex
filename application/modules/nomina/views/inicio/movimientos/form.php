<script>
    var  id_trabajador = "{id_trabajador}";
    var  id_movimiento = "{id_movimiento}";
</script>

<div class="row mt-2 ">
<div class="col-sm-12">
<h2><?php echo $titulo; ?></h2>
</div>
</div>

<div class="row mb-3">
<div class="col-sm-12">
<div class="card mt-4">
	<div class="card-body ">


		<form id="general_form">
            <div class="row">
                {grupo1}
                <div id="content_{key}" class="col-sm-4">
                    <div class="form-group">
                        <label for="{key}" class="">{label}</label>
                        {input}
                        <small id="msg_{key}" class="form-text text-danger"></small>
                    </div>
                </div>
                {/grupo1}
            </div>

            <div class="col-sm-12">
                <div class="form-group row mt-3">
                    <div class="mx-auto col-sm-5">
                        <button onclick="window.location.href = '<?php echo site_url('nomina/inicio/movientos_nomina'); ?>';" type="button"
                            class="btn btn-danger col-md-4">Regresar</button>
                        <button onclick="Apps.guardar(this);" type="button"
                            class="btn btn-success col-md-4">Guardar</button>
                    </div>
                </div>
            </div>

		</form>

	</div>
</div>

</div>
</div>
