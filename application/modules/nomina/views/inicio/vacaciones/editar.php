<script>
	var id_trabajador = "{id_trabajador}";
	var id = "{id}";

</script>

<div class="row mt-2 mb-3">
	<div class="col-sm-12">
		<h3>Edición de vacaciones</h3>
	</div>
</div>

<div class="card mt-4">
	<div class="card-body ">

		<div class="row">
			<div class="col-sm-2">
				<div class="form-group">
					<label for="">Clave:</label>
					<input type="" class="form-control-plaintext" readonly value="<?php echo $trabajador['Clave']; ?>">
				</div>
			</div>
			<div class="col-sm-5">
				<div class="form-group">
					<label for="" class="">Nombre:</label>
					<input type="" class="form-control-plaintext" readonly
						value="<?php echo $trabajador['Nombre'].' '.$trabajador['Apellido_1'].' '.$trabajador['Apellido_2']; ?>">
				</div>
			</div>
			<div class="col-sm-5">
				<div class="form-group">
					<label for="">Departamento:</label>
					<input type="" class="form-control-plaintext" readonly
						value="<?php echo $trabajador['Descripcion_Departamento']; ?>">
				</div>
			</div>
		</div>
	</div>
</div>
<div class="card mt-4">
	<div class="card-body ">

		<form id="general_form">
			<div class="col-sm-12">
				<div class="form-group row">
					<label for="FechaDisfrute" class="col-sm-2 col-form-label">Fecha disfrute</label>
					<div class="col-sm-5">
						<input type="" id="FechaDisfrute" class="form-control-plaintext"
							value="<?php echo utils::aFecha($registro['FechaDisfrute'],true); ?>">
						<small id="msg_FechaDisfrute" class="form-text text-danger"></small>
					</div>
				</div>
			</div>

			<div class="col-sm-12">
				<div class="form-group row">
					<label for="DiasDisfrute" class="col-sm-2 col-form-label">Días disfrute</label>
					<div class="col-sm-5">
						<input type="number" id="DiasDisfrute" class="form-control" name="DiasDisfrute"
							value="<?php echo $registro['DiasDisfrute']; ?>">
						<small id="msg_DiasDisfrute" class="form-text text-danger"></small>
					</div>
				</div>
			</div>

			<div class="col-sm-12">
				<div class="form-group row">
					<label for="FechaPagoPV" class="col-sm-2 col-form-label">F. pago P.V.</label>
					<div class="col-sm-5">
						<input type="date" id="FechaPagoPV" class="form-control" name="FechaPagoPV"
							value="<?php echo $registro['FechaPagoPV']; ?>">
						<small id="msg_FechaPagoPV" class="form-text text-danger"></small>
					</div>
				</div>
			</div>

			<div class="col-sm-12">
				<div class="form-group row">
					<label for="DiasPrimaVac" class="col-sm-2 col-form-label">Días prima vac</label>
					<div class="col-sm-5">
						<input type="number" min="0" id="DiasPrimaVac" class="form-control" name="DiasPrimaVac"
							value="<?php echo $registro['DiasPrimaVac']; ?>">
						<small id="msg_DiasPrimaVac" class="form-text text-danger"></small>
					</div>
				</div>
			</div>

			<div class="col-sm-12">
				<div class="form-group row mt-3">
					<div class="mx-auto col-sm-5">
						<button onclick="window.location.href = '<?php echo site_url('nomina/inicio/vacaciones'); ?>';"
							type="button" class="btn btn-danger col-md-4">Regresar</button>
						<button onclick="Apps.guardar(this);" type="button"
							class="btn btn-success col-md-4">Guardar</button>
					</div>
				</div>
			</div>

		</form>

	</div>
</div>
