<script>
    var  identity = "{identity}";
</script>

<div class="row">

    <div class="col-sm-12">
        <div class="mt-1" >
            <div class="row">
                <div class="col-sm-12 mb-2">
                    <h3>Datos Generales</h3>
                </div>
            </div>
            {content_form}
        </div>
    </div>

    <div class="col-sm-12">
        <div class="form-group row mt-3">
            <div class="col-sm-3">&nbsp;</div>
            <div class="col-sm-9">
                <button onclick="Apps.regresar(this);" type="button" class="btn btn-danger col-md-4">Cancelar</button>
                <button onclick="Apps.guardar(this);" type="button" class="btn btn-success col-md-4">Guardar</button>
            </div>
        </div>
    </div>
</div>