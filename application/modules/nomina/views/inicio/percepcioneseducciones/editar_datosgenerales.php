<div class="card mt-5">
	<div class="card-body">
		<h4 class="card-title text-dark">Datos generales de la Percepción o Deducción</h4>


		<form>

			<div class="row">
				<div class="col-sm-6">
					<div class="form-group row">
						<label for="staticEmail" class="col-sm-3 col-form-label">Clave</label>
						<div class="col-sm-9">
							<input  readonly class="form-control-plaintext" value="">
						</div>
					</div>
                </div>
            </div>
            <div class="row">
				<div class="col-sm-6">
					<div class="form-group row">
						<label for="staticEmail" class="col-sm-3 col-form-label">Estatus</label>
						<div class="col-sm-9">
							<select class="custom-select">
								<option value="1">Alta</option>
								<option value="2">Baja</option>
								<option value="3">Cálculo</option>
								<option value="3">Inactiva</option>
							</select>
						</div>
					</div>
                </div>
                
                <div class="col-sm-6">
					<div class="form-group row">
						<label for="staticEmail" class="col-sm-3 col-form-label">Clasificación</label>
						<div class="col-sm-9">
							<select class="custom-select">
								<option value="1">Alta</option>
								<option value="2">Baja</option>
								<option value="3">Cálculo</option>
								<option value="3">Inactiva</option>
							</select>
						</div>
					</div>
				</div>
			</div>


            <div class="row">
				<div class="col-sm-6">
					<div class="form-group row">
						<label for="staticEmail" class="col-sm-3 col-form-label">Descripción</label>
						<div class="col-sm-9">
                            <input type="text" class="form-control" >
						</div>
					</div>
                </div>
                
                <div class="col-sm-6">
					<div class="form-group row">
						<label for="staticEmail" class="col-sm-3 col-form-label">Fórmula</label>
						<div class="col-sm-9">
                            <input type="text" class="form-control" >
						</div>
					</div>
				</div>
			</div>


		</form>

	</div>
</div>
