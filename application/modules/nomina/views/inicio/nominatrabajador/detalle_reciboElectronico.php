<div class="row">
	<div class="col-sm-12">

		<div class="card">
			<div class="card-body">

				<table id="table_content" class="table">
					<thead>
						<tr>
							<th scope="col">Folio</th>
							<th scope="col">Fecha de timbrado</th>
							<th scope="col">UUID</th>
							<th scope="col">Estado CFDI</th>
						</tr>
					</thead>
					<tbody>
					</tbody>
					<tfoot>
						<tr>
							<th scope="col">Folio</th>
							<th scope="col">Fecha de timbrado</th>
							<th scope="col">UUID</th>
							<th scope="col">Estado CFDI</th>
						</tr>
					</tfoot>
				</table>
			</div>
		</div>

	</div>

</div>
