<script>
<?php 
function colores_graph($expresion){
    $color = 'blue';
    switch ($expresion) {
        case 1:
            $color = '#ffd562';
            break;
        case 2:
            $color = '#4169E1';
            break;
        case 3:
            $color = 'black';
            break;
        case 4:
            $color = 'green';
            break;
    }
    return $color;
}
?>
    var contenido = [ <?php $cont = 1; foreach ($contenido as $key => $value) { echo '["'.$key.'",'.$value.',"color:'.colores_graph($cont).'"],';  $cont++; } ?>];
</script>

<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
<script type="text/javascript">

  

    google.charts.load('current', {packages:['corechart']});
      google.charts.setOnLoadCallback(drawStuff);

        function drawStuff() {
          var data = new google.visualization.DataTable();
          data.addColumn('string', 'Pagos');
          data.addColumn('number', 'Montos');
          data.addColumn({type: 'string', role: 'style'});
          data.addRows(contenido);
          

         var options = {
            colors:['red','#009900'],
            title: 'Percepciones y deducciones',
            legend: 'none',
           
            vAxis: { format:'decimal'},
            height: 500,
            chartArea: {width: '95%', height: '80%'},
            vAxis: {textPosition: 'in', gridlines: {count: 10}, minorGridlines: {count: 10}, textStyle: {fontSize: 12}},
         };

         var chart = new google.visualization.ColumnChart(document.getElementById('piechart'));
         chart.draw(data, options);

      };
</script>

<div class="card">
  <div class="card-body">
        <div class="row">
            <div class="col-sm-12">
                <div id="chart_wrap">
                    <div id="piechart"></div>
                </div>
            </div>
        </div>
  </div>
</div>