<script>
	var id_trabajador = "{id_trabajador}";
	var id_movimiento = "{id_movimiento}";
	var id_periodo = "{id_periodo}";

</script>

<div class="row mt-5 mb-3">
	<div class="col-sm-12">
		<h2>Alta de horas extras</h2>
	</div>
</div>

<div class="card mt-4">
	<div class="card-body ">

		<div class="row">
			<div class="col-sm-2">
				<div class="form-group">
					<label for="">Clave:</label>
					<input type="" class="form-control-plaintext" readonly value="<?php echo $trabajador['Clave']; ?>">
				</div>
			</div>
			<div class="col-sm-5">
				<div class="form-group">
					<label for="" class="">Nombre:</label>
					<input type="" class="form-control-plaintext" readonly
						value="<?php echo $trabajador['Nombre'].' '.$trabajador['Apellido_1'].' '.$trabajador['Apellido_2']; ?>">
				</div>
			</div>
			<div class="col-sm-5">
				<div class="form-group">
					<label for="">Departamento:</label>
					<input type="" class="form-control-plaintext" readonly
						value="<?php echo $trabajador['Descripcion_Departamento']; ?>">
				</div>
			</div>
		</div>
	</div>
</div>
<div class="card mt-4">
	<div class="card-body ">
		<form id="general_form">
			<div class="col-sm-12">
				<div class="form-group row">
					<label for="Fecha" class="col-sm-2 col-form-label">Fecha</label>
					<div class="col-sm-5">
						<input type="date" id="Fecha" class="form-control" name="Fecha"
							min="<?php echo $periodo['FechaInicio']; ?>"
							max="<?php echo $periodo['FechaFin']; ?>">
						<small id="msg_Fecha" class="form-text text-danger"></small>
					</div>
				</div>
			</div>

			<div class="col-sm-12">
				<div class="form-group row">
					<label for="Horas" class="col-sm-2 col-form-label">Horas</label>
					<div class="col-sm-5">
						<input type="number" class="form-control" min="0" max="3" id="Horas" step="0.01" name="Horas">
						<small id="msg_Horas" class="form-text text-danger"></small>
					</div>
				</div>
			</div>

			<div class="col-sm-12">
				<div class="form-group row mt-3">
					<div class="mx-auto col-sm-5">
						<button
							onclick="window.location.href = '<?php echo site_url('nomina/inicio/horas_extras'); ?>';"
							type="button" class="btn btn-danger col-md-4">Regresar</button>
						<button onclick="Apps.guardar(this);" type="button"
							class="btn btn-success col-md-4">Guardar</button>
					</div>
				</div>
			</div>

		</form>

	</div>
</div>
