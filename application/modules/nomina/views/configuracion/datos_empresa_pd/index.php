<form id="general_form">

	<div class="row">
		<div class="col-sm-6">

			<div class="card mt-4">
				<div class="card-body ">

					<h4 class="mb-5">Percepciones</h4>

					<div class="row">


						{grupo_1}
						<div class="col-sm-12">

							<div class="form-group">
								<label for="{key}" class="">{label}</label>
								<div class="">
									{input}
									<small id="msg_{key}" class="form-text text-danger"></small>
								</div>
							</div>

						</div>
						{/grupo_1}

					</div>

				</div>
			</div>
		</div>

		<div class="col-sm-6">

			<div class="card mt-4">
				<div class="card-body ">

					<h4 class="mb-5">Deducciones</h4>

					<div class="row">


						{grupo_2}
						<div class="col-sm-12">

							<div class="form-group">
								<label for="{key}" class="">{label}</label>
								<div class="">
									{input}
									<small id="msg_{key}" class="form-text text-danger"></small>
								</div>
							</div>

						</div>
						{/grupo_2}

					</div>

				</div>
			</div>
		</div>

		<div class="col-sm-12">

			<div class="card mt-4">
				<div class="card-body ">

					<h4 class="mb-5">Recibos electrónicos</h4>

					<div class="row">


						{grupo_3}
						<div class="col-sm-12">

							<div class="form-group">
								<label for="{key}" class="">{label}</label>
								<div class="">
									{input}
									<small id="msg_{key}" class="form-text text-danger"></small>
								</div>
							</div>

						</div>
						{/grupo_3}

					</div>

				</div>
			</div>
		</div>

	</div>
</form>


<div class="row">
	<div class="col-sm-12">
		<div class="form-group mt-3">
			<div class="row">
				<div class="col-sm-3">&nbsp;</div>
				<div class="col-sm-9">
					<button onclick="Apps.guardar(this);" type="button"
						class="btn btn-success col-md-4">Guardar</button>
				</div>
			</div>
		</div>
	</div>
