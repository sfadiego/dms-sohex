<script>
    var  id = "{id}";
</script>

<div class="row">
    <div class="col-sm-12">
        <nav class="nav nav-pills nav-fill">
            <a class="nav-item nav-link {tab_1}" href="{url_1}">Razon social y logo</a>
            <a class="nav-item nav-link {tab_2}" href="{url_2}">Registros patronales</a>
            <a class="nav-item nav-link {tab_3}" href="{url_3}">Domicilio fiscal</a>
        </nav>
    </div>

    <div class="col-sm-12">
        <div class="mt-2" >
            {content_form}
        </div>
    </div>
</div>