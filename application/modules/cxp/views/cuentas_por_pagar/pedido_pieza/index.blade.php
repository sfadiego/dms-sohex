@layout('tema_luna/layout')
@section('contenido')
<div class="container-fluid panel-body">
    <h1 class="mt-4">{{ isset($titulo) ? $titulo : '' }}</h1>
    <ol class="breadcrumb mb-4">
        <li class="breadcrumb-item"><?php echo isset($titulo) ? $titulo : "" ?></li>
        <li class="breadcrumb-item"><?php echo isset($subtitulo) ? $subtitulo : "" ?></li>
    </ol>

    <div class="row">
        <div class="col-md-6">
            <div class="form-group">
                <label for="">Número de proveedor:</label>
                <select class="form-control" id="proveedor_id" name="proveedor_id" style="width: 100%;">
                    <option value="">Selecionar ...</option>
                    @if(!empty($cat_proveedor))
                    @foreach ($cat_proveedor as $proveedor)
                        <option value="{{ $proveedor->id}}"> {{$proveedor->proveedor_numero}} - {{ $proveedor->proveedor_nombre }}</option>
                        @endforeach
                    @endif
                </select>
                <div id="proveedor_id_error" class="invalid-feedback"></div>
            </div>
        </div>
        <div class="col-md-6 ">
            <button class="btn btn-primary mt-4" id="btn_filtrar" onclick="filtrarproveedor()"> Filtrar</button>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <div class="table-responsive">
                <table class="table table-striped table-bordered" id="tabla_piezas_proveedor" width="100%" cellspacing="0">
                </table>
            </div>
        </div>
    </div>
</div>

@endsection
@section('scripts')
<script>
    var tabla = $('#tabla_piezas_proveedor').DataTable({
		language: {
			url: PATH_LANGUAGE
		},
		"ajax": {
			url: PATH_API + "api/pedidos/proveedor",
			type: 'POST',
			data: {
				proveedor_id: function () {
					return $('#proveedor_id').val()
				}
			}
		},
		columns: [
            {
                title: "#",
				'data': 'id'
            },
            {
                title: "No. de pieza",
				'data': 'no_identificacion'
            },
            {
                title: "Producto",
				'data': 'descripcion'
            },
            {
                title: "Cantidad",
				'data': 'cantidad_solicitada'
            },
            {
                title: "Estatus",
				'data': function({estatus_id}){
                    if(estatus_id == 1){
                        return 'proceso'
                    }else if (estatus_id == 2){
                        return 'pedidas'
                    }else if (estatus_id == 3){
                        return 'no pedida'
                    }else if (estatus_id == 4){
                        return 'backorder'
                    }

                }
			},
			{
				'data': function (data) {
					return '-';//utils.isDefined(data.unidad) && data.unidad ? data.unidad : null
				}
			}
		]
    });
    
    filtrarproveedor=()=> tabla.ajax.reload();
</script>
{{-- <script src="{{ base_url('js/cxp/listado_cxp.js') }}"></script> --}}
@endsection