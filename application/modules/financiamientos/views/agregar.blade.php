@layout('tema_luna/layout')
@section('contenido')
    <div class="container-fluid panel-body">
        <h1 class="mt-4"><?php echo isset($titulo) ? $titulo : ''; ?></h1>
        <ol class="breadcrumb mb-4">
            <li class="breadcrumb-item active"><?php echo isset($titulo) ? $titulo : ''; ?>
            </li>
        </ol>
        <form id="frm">
            <input type="hidden" name="id" id="id" value="{{ $id }}">
            <div class="row">
                <div class="col-sm-4">
                    <label for="">Asesor</label>
                    {{ $drop_asesores }}

                </div>
                <div class="col-sm-4">
                    <label for="">Cliente</label>
                    {{ $drop_clientes }}

                </div>
                <div class="col-sm-4">
                    <label for="">Fecha Buró</label>
                    {{ $fecha_buro }}

                </div>
            </div>
            <div class="row">
                <div class="col-sm-4">
                    <label for="">Se ingresa</label> <br>
                    Si {{ $se_ingresa_si }}
                    No {{ $se_ingresa_no }}
                </div>
                <div class="col-sm-4">
                    <label for="">Propuesta</label>
                    {{ $drop_propuesta }}
                </div>
                <div class="col-sm-4">
                    <label for="">Fecha de ingreso</label>
                    {{ $fecha_ingreso }}
                </div>
            </div>
            <div class="row">
                <div class="col-sm-4">
                    <label for="">¿Hay unidad?</label> <br>
                    Si {{ $hay_unidad_si }}
                    No {{ $hay_unidad_no }}
                </div>
                <div class="col-sm-4">
                    <label for="">Financiera 1</label> <br>
                    {{ $drop_financiera1 }}
                </div>
                <div class="col-sm-4">
                    <label for="">Financiera 2</label> <br>
                    {{ $drop_financiera2 }}
                </div>
            </div>
            <div class="row">
                <div class="col-sm-4">
                    <label for="">Unidad</label>
                    {{ $drop_unidad }}
                </div>
                <div class="col-sm-4">
                    <label for="">Catálogo</label> <br>
                    {{ $drop_catalogo }}
                </div>
                <div class="col-sm-4">
                    <label for="">MF</label>
                    {{ $mf }}
                </div>
            </div>
            <div class="row">
                <div class="col-sm-4">
                    <label for="">Comisión financiera</label>
                    {{ $comision_financiera }}
                </div>
                <div class="col-sm-4">
                    <label for="">Perfil</label>
                    {{ $drop_perfil }}
                </div>
                <div class="col-sm-4">
                    <label for="">Score</label>
                    {{ $score }}
                </div>
            </div>
            <div class="row">
                <div class="col-sm-4">
                    <label for="">Cuenta más alta</label>
                    {{ $cuenta_mas_alta }}
                </div>
                <div class="col-sm-4">
                    <label for="">Tiempo de la cuenta</label>
                    {{ $tiempo_cuenta }}
                </div>
                <div class="col-sm-4">
                    <label for="">Mensualidad</label>
                    {{ $mensualidad }}
                </div>
            </div>
            <div class="row">
                <div class="col-sm-4">
                    <label for="">Egreso en bruto</label>
                    {{ $egreso_bruto }}
                </div>
                <div class="col-sm-4">
                    <label for="">Total</label>
                    {{ $total }}
                </div>
                <div class="col-sm-4">
                    <label for="">Ingreso Indep</label>
                    {{ $ingreso_indep }}
                </div>
            </div>
            <div class="row">
                <div class="col-sm-4">
                    <label for="">Total</label>
                    {{ $total2 }}
                </div>
                <div class="col-sm-4">
                    <label for="">Nivel de endeudamiento</label>
                    {{ $nivel_endeudamiento }}
                </div>
                <div class="col-sm-4">
                    <label for="">Ingreso nómina</label>
                    {{ $ingreso_nomina }}
                </div>
            </div>
            <div class="row">
                <div class="col-sm-4">
                    <label for="">Arraigo casa</label>
                    {{ $arraigo_casa }}
                </div>
                <div class="col-sm-4">
                    <label for="">Arraigo empleo</label>
                    {{ $arraigo_empleo }}
                </div>
                @if ($id != 0)
                    <div class="col-sm-4">
                        <label for="">Estatus</label>
                        {{ $drop_estatus }}
                    </div>
                @endif
            </div>
            @if ($id != 0)
                <div class="row">
                    <div class="col-sm-4">
                        <label for="">Comentario F&I</label>
                        {{ $comentario }}
                    </div>
                </div>
            @endif
            <hr>
            <h3>Medición de tiempo</h3>
            <div class="row">
                <div class="col-sm-4">
                    <label for="">Fecha de condicionamiento</label>
                    {{ $fecha_condicionamiento }}
                </div>
                <div class="col-sm-4">
                    <label for="">Hora de condicionamiento</label>
                    {{ $hora_condicionamiento }}
                </div>
                <div class="col-sm-4">
                    <label for="">Fecha de cumplimiento</label>
                    {{ $fecha_cumplimiento }}
                </div>
            </div>
            <div class="row">
                <div class="col-sm-4">
                    <label for="">Hora de cumplimiento</label>
                    {{ $hora_cumplimiento }}
                </div>
                <div class="col-sm-4">
                    <label for="">Fecha de aprobación</label>
                    {{ $fecha_aprobacion }}
                </div>
                <div class="col-sm-4">
                    <label for="">Hora de aprobación</label>
                    {{ $hora_aprobacion }}
                </div>
            </div>
            <div class="row">
                <div class="col-sm-4">
                    <label for="">Fecha de contrato</label>
                    {{ $fecha_contrato }}
                </div>
                <div class="col-sm-4">
                    <label for="">Hora de contrato</label>
                    {{ $hora_contrato }}
                </div>
                <div class="col-sm-4">
                    <label for="">Fecha de compra</label>
                    {{ $fecha_compra }}
                </div>
            </div>
            <div class="row">
                <div class="col-sm-4">
                    <label for="">Hora de compra</label>
                    {{ $hora_compra }}
                </div>
                @if ($id != 0)
                    <div class="col-sm-4">
                        <label for="">Estatus plan piso</label>
                        {{ $id_estatus_piso }}
                    </div>
                    <div class="col-sm-4">
                        <label for="">Comentarios contrato/compra</label>
                        {{ $input_comentarios_compra }}
                    </div>
                @endif
            </div>
            <hr>
            <h3>Administración de utilidad</h3>
            <div class="row">
                <div class="col-sm-4">
                    <label for="">Plan</label>
                    {{ $plan }}
                </div>
                <div class="col-sm-4">
                    <label for="">Bono</label>
                    {{ $bono }}
                </div>
                <div class="col-sm-4">
                    <label for="">Plazo</label>
                    {{ $plazo }}
                </div>
            </div>
            <div class="row">
                <div class="col-sm-4">
                    <label for="">Precio unidad</label> <br>
                    {{ $precio_unidad }}
                </div>
                <div class="col-sm-4">
                    <label for="">Enganche</label>
                    {{ $enganche }}
                </div>
                <div class="col-sm-4">
                    <label for="">Monto a financiar</label>
                    {{ $monto_financiar }}
                </div>
            </div>
            <div class="row">
                <div class="col-sm-4">
                    <label for="">% comisión</label> <br>
                    {{ $comisiones }}
                </div>
                <div class="col-sm-4">
                    <label for="">Comisión agencia</label>
                    {{ $comision_agencia }}
                </div>
                <div class="col-sm-4">
                    <label for="">Comisión asesor</label>
                    {{ $comision_asesor }}
                </div>
            </div>
            <div class="row">
                <div class="col-sm-4">
                    <label for="">Tipo de seguro</label> <br>
                    {{ $seguros }}
                </div>
                <div class="col-sm-4">
                    <label for="">Compañía</label>
                    {{ $companias }}
                </div>
                <div class="col-sm-4">
                    <label for="">No. de poliza</label>
                    {{ $no_poliza }}
                </div>
            </div>
            <div class="row">
                <div class="col-sm-4">
                    <label for="">Inicio de vigencia</label> <br>
                    {{ $inicio_vigencia }}
                </div>
                <div class="col-sm-4">
                    <label for="">Vencimiento anual</label>
                    {{ $vencimiento_anual }}
                </div>
                <div class="col-sm-4">
                    <label for="">Prima total</label>
                    {{ $prima_total }}
                </div>
            </div>
            <div class="row">
                <div class="col-sm-4">
                    <label for="">IVA</label> <br>
                    {{ $iva }}
                </div>
                <div class="col-sm-4">
                    <label for="">Emisión de Poliza</label>
                    {{ $emision_poliza }}
                </div>
                <div class="col-sm-4">
                    <label for="">Prima neta</label>
                    {{ $prima_neta }}
                </div>
            </div>
            <div class="row">
                <div class="col-sm-4">
                    <label for="">Udi agencia</label> <br>
                    {{ $udi_agencia }}
                </div>
                <div class="col-sm-4">
                    <label for="">comision asesor</label>
                    {{ $comision_asesor_prima }}
                </div>
                <div class="col-sm-4">
                    <label for="">% UDI</label>
                    {{ $udis }}
                </div>
            </div>
            <hr>
            <h3>Ford Crédit</h3>
            <div class="row">
                <div class="col-sm-4">
                    <label for="">Tipo de seguro</label> <br>
                    {{ $seguros_fc }}
                </div>
                <div class="col-sm-4">
                    <label for="">Compañía de seguro</label>
                    {{ $companias_fc }}
                </div>
                <div class="col-sm-4">
                    <label for="">No. de poliza</label>
                    {{ $no_poliza_fc }}
                </div>
            </div>
            <div class="row">
                <div class="col-sm-4">
                    <label for="">Inicio de vigencia</label> <br>
                    {{ $inicio_vigencia_fc }}
                </div>
                <div class="col-sm-4">
                    <label for="">Vencimiento anual</label>
                    {{ $vencimiento_anual_fc }}
                </div>
                <div class="col-sm-4">
                    <label for="">Prima total</label>
                    {{ $prima_total_fc }}
                </div>
            </div>
            <div class="row">
                <div class="col-sm-4">
                    <label for="">IVA</label> <br>
                    {{ $iva_fc }}
                </div>
                <div class="col-sm-4">
                    <label for="">Emisión de Poliza</label>
                    {{ $emision_poliza_fc }}
                </div>
                <div class="col-sm-4">
                    <label for="">Prima neta</label>
                    {{ $prima_neta_fc }}
                </div>
            </div>
            <div class="row">
                <div class="col-sm-4">
                    <label for="">Udi agencia</label> <br>
                    {{ $udi_agencia_fc }}
                </div>
                <div class="col-sm-4">
                    <label for="">comision asesor</label>
                    {{ $comision_asesor_prima_fc }}
                </div>
                <div class="col-sm-4">
                    <label for="">% UDI</label>
                    {{ $udis_fc }}
                </div>
            </div>
            @if ($bitacora == 'compras')
            <h3>Bitácora Compras</h3>
            <div class="row">
                <div class="col-sm-4">
                    <label for="">Fecha de envío</label> <br>
                    {{ $fecha_envio }}
                </div>
                <div class="col-sm-4">
                    <label for="">Año</label>
                    {{ $anio }}
                </div>
                <div class="col-sm-4">
                    <label for="">Serie</label>
                    {{ $serie }}
                </div>
            </div>
            <div class="row">
                <div class="col-sm-4">
                    <label for="">Uso seguro</label> <br>
                    {{$uso_seguro}}
                </div>
                <div class="col-sm-4">
                    <label for="">Extensión de garantía</label>
                    {{ $extension_garantia }}
                </div>
                <div class="col-sm-4">
                    <label for="">Accesorios</label>
                    {{ $accesorios }}
                </div>
            </div>
            <div class="row">
                <div class="col-sm-8">
                    <label for="">Observaciones</label> <br>
                    {{ $observaciones }}
                </div>
            </div>
            @endif
        </form>
        <br>
        @if ($bitacora == 'inicial')
            <button type="button" id="guardar-inicial" class="btn btn-primary pull-right">Guardar bitácora inicial</button>
        @else
            <button type="button" id="guardar-compras" class="btn btn-primary pull-right">Guardar bitácora compras</button>
        @endif
        <br><br>
        <div id="mensaje" class="alert alert-success" role="alert" style="display:none;"></div>
    </div>
    </div>


@endsection
@section('scripts')
    <script src="<?php echo base_url(); ?>js/custom/numeric.js"></script>
    <script type="text/javascript">
        var bitacora = "{{ $bitacora }}";
        $('.numeric').numeric();
        $('.positive').numeric();
        if (bitacora == 'inicial') {
            $("#guardar-inicial").on('click', function() {
                const data = {
                    id_asesor_ventas: $("#id_asesor_ventas").val(),
                    id_cliente: $("#id_cliente").val(),
                    fecha_buro: $("#fecha_buro").val(),
                    se_ingresa: $("input[name='se_ingresa']:checked").val(),
                    id_propuesta: $("#id_propuesta").val(),
                    fecha_ingreso: $("#fecha_ingreso").val(),
                    hay_unidad: $("input[name='hay_unidad']:checked").val(),
                    id_financiera1: $("#id_financiera1").val(),
                    id_financiera2: $("#id_financiera2").val(),
                    id_unidad: $("#id_unidad").val(),
                    id_catalogo: $("#id_catalogo").val(),
                    precio_unidad: $("#precio_unidad").val(),
                    enganche: $("#enganche").val(),
                    mf: $("#mf").val(),
                    plan: $("#plan").val(),
                    comision_financiera: $("#comision_financiera").val(),
                    id_perfil: $("#id_perfil").val(),
                    score: $("#score").val(),
                    cuenta_mas_alta: $("#cuenta_mas_alta").val(),
                    tiempo_cuenta: $("#tiempo_cuenta").val(),
                    mensualidad: $("#mensualidad").val(),
                    egreso_bruto: $("#egreso_bruto").val(),
                    total: $("#total").val(),
                    total2: $("#total2").val(),
                    ingreso_indep: $("#ingreso_indep").val(),
                    ingreso_nomina: $("#ingreso_nomina").val(),
                    nivel_endeudamiento: $("#nivel_endeudamiento").val(),
                    arraigo_casa: $("#arraigo_casa").val(),
                    arraigo_empleo: $("#arraigo_empleo").val(),
                    //comentario: $("#comentario").val(),
                    id_comision: $("#id_comision").val(),
                    comision_agencia: $("#comision_agencia").val(),
                    comision_asesor: $("#comision_asesor").val(),
                    id_tipo_seguro: $("#id_tipo_seguro").val(),
                    id_compania: $("#id_compania").val(),
                    inicio_vigencia: $("#inicio_vigencia").val(),
                    vencimiento_anual: $("#vencimiento_anual").val(),
                    no_poliza: $("#no_poliza").val(),
                    prima_total: $("#prima_total").val(),
                    iva: $("#iva").val(),
                    emision_poliza: $("#emision_poliza").val(),
                    prima_neta: $("#prima_neta").val(),
                    udi_agencia: $("#udi_agencia").val(),
                    comision_asesor_prima: $("#comision_asesor_prima").val(),
                    id_udi: $("#id_udi").val(),
                    monto_financiar: $("#monto_financiar").val(),
                    inicio_vigencia_fc: $("#inicio_vigencia_fc").val(),
                    vencimiento_anual_fc: $("#vencimiento_anual_fc").val(),
                    prima_total_fc: $("#prima_total_fc").val(),
                    iva_fc: $("#iva_fc").val(),
                    emision_poliza_fc: $("#emision_poliza_fc").val(),
                    prima_neta_fc: $("#prima_neta_fc").val(),
                    udi_agencia_fc: $("#udi_agencia_fc").val(),
                    comision_asesor_prima_fc: $("#comision_asesor_prima_fc").val(),
                    id_udi_fc: $("#id_udi_fc").val(),
                    id_tipo_seguro_fc: $("#id_tipo_seguro_fc").val(),
                    id_compania_fc: $("#id_compania_fc").val(),
                    no_poliza_fc: $("#no_poliza_fc").val(),
                    bono: $("#bono").val(),
                    plazo: $("#plazo").val(),
                    //MEDICIÓN
                    fecha_condicionamiento: $("#fecha_condicionamiento").val(),
                    hora_condicionamiento: $("#hora_condicionamiento").val(),
                    fecha_cumplimiento: $("#fecha_cumplimiento").val(),
                    hora_cumplimiento: $("#hora_cumplimiento").val(),
                    fecha_aprobacion: $("#fecha_aprobacion").val(),
                    hora_aprobacion: $("#hora_aprobacion").val(),
                    fecha_contrato: $("#fecha_contrato").val(),
                    hora_contrato: $("#hora_contrato").val(),
                    fecha_compra: $("#fecha_compra").val(),
                    hora_compra: $("#hora_compra").val(),
                    //id_estatus_piso: $("#id_estatus_piso").val(),
                    //comentarios_compra: $("#comentarios_compra").val(),

                };
                if ($("#id").val() != 0) {
                    ajax.put('api/financiamientos/' + $("#id").val(), data,
                        function(response, headers) {
                            if (headers.status == 400) {
                                return ajax.showValidations(headers);
                            }
                            var titulo = (headers.status != 200) ? headers.message :
                                "Información actualizada con éxito";
                            utils.displayWarningDialog("Información actualizada con éxito", "success", function(
                                data) {
                                return window.location.href = base_url + 'financiamientos/listado';
                            })
                        })
                } else {
                    ajax.post('api/financiamientos', data,
                        function(response, headers) {
                            if (headers.status == 400) {
                                return ajax.showValidations(headers);
                            }
                            var titulo = (headers.status != 200) ? headers.message :
                                "Información guardada con éxito";
                            utils.displayWarningDialog("Información guardada con éxito", "success", function(
                                data) {
                                return window.location.href = base_url + 'financiamientos/listado';
                            })
                        })
                }
            })
            $("#comision_agencia").on("change paste keyup", function() {
                var val = $(this).val();
                var comision = val * 0.30;
                $("#comision_asesor").val(comision);
            });
            $("#precio_unidad").on("change paste keyup", function() {
                var financiar;
                var precioUnidad = $(this).val();
                precioUnidad = precioUnidad.replace(",", "");
                var enganche = $.trim($("#enganche").val());
                if (enganche.length == 0) {
                    enganche = 0;
                    $("#monto_financiar").val(precioUnidad);
                } else {
                    enganche = enganche.replace(",", "");
                    financiar = precioUnidad - enganche;
                }
                $("#monto_financiar").val(financiar);
                var monto = $("#monto_financiar").val()
                var porcentaje = $("#id_comision option:selected").text()
                porcentaje = porcentaje.replace("%", "");
                porcentaje = porcentaje / 100;
                comAgencia = monto * porcentaje;
                $("#comision_agencia").val(comAgencia);
                var comAsesor = comAgencia * 0.3;
                $("#comision_asesor").val(comAsesor);
            });
            $("#enganche").on("change paste keyup", function() {
                var enganche = $(this).val();
                var precioUnidad = $("#precio_unidad").val();
                enganche = enganche.replace(",", "");
                precioUnidad = precioUnidad.replace(",", "");
                var financiar = precioUnidad - enganche
                $("#monto_financiar").val(financiar);
                var monto = $("#monto_financiar").val();
                var porcentaje = $("#id_comision option:selected").text()
                porcentaje = porcentaje.replace("%", "");
                porcentaje = porcentaje / 100;
                var comisionAgencia = 1 * (monto * porcentaje)
                $("#comision_agencia").val(comisionAgencia);
                var comisionAsesor = comisionAgencia * 0.3;
                $("#comision_asesor").val(comisionAsesor);
            });
            $("#id_comision").change(function() {
                var monto = $("#monto_financiar").val();
                var porcentaje = $("#id_comision option:selected").text();
                porcentaje = porcentaje.replace("%", "");
                porcentaje = porcentaje / 100;
                var comisionAgencia = 1 * (monto * porcentaje)
                $("#comision_agencia").val(comisionAgencia);
                var comisionAsesor = comisionAgencia * 0.3;
                $("#comision_asesor").val(comisionAsesor);
            });
            $("#prima_total").on("change paste keyup", function() {
                var primaTotal = $(this).val();
                primaTotal = primaTotal.replace(",", "");
                var iva = $("#iva").val();
                iva = primaTotal * 0.16;
                $("#iva").val(iva);
                var emisionPoliza = $("#emision_poliza").val();
                emisionPoliza = emisionPoliza.replace(",", "");
                var primaNeta = primaTotal - iva - emisionPoliza;
                $("#prima_neta").val(primaNeta);
                var comisionAgencia = primaNeta * 0.24;
                $("#udi_agencia").val(comisionAgencia);
                var comisionAsesor = comisionAgencia * 0.30;
                $("#comision_asesor_prima").val(comisionAsesor);
            });
            $("#emision_poliza").on("change paste keyup", function() {
                var emisionPoliza = $(this).val();
                var primaTotal = $("#prima_total").val();
                var iva = $("#iva").val();
                primaTotal = primaTotal.replace(",", "");
                emisionPoliza = emisionPoliza.replace(",", "");
                iva = iva.replace(",", "");
                var primaNeta = primaTotal - emisionPoliza - iva;
                $("#prima_neta").val(primaNeta);
                var comisionAgencia = primaNeta * 0.24;
                $("#udi_agencia").val(comisionAgencia);
                var comisionAsesor = comisionAgencia * 0.30;
                $("#comision_asesor_prima").val(comisionAsesor);
            });
            //FORD CREDIT
            $("#prima_total_fc").on("change paste keyup", function() {
                var primaTotal = $(this).val();
                primaTotal = primaTotal.replace(",", "");
                var iva = $("#iva_fc").val();
                iva = primaTotal * 0.16;
                $("#iva_fc").val(iva);
                var emisionPoliza = $("#emision_poliza_fc").val();
                emisionPoliza = emisionPoliza.replace(",", "");
                var primaNeta = primaTotal - iva - emisionPoliza;
                $("#prima_neta_fc").val(primaNeta);
                var comisionAgencia = primaNeta * 0.24;
                $("#udi_agencia_fc").val(comisionAgencia);
                var comisionAsesor = comisionAgencia * 0.30;
                $("#comision_asesor_prima_fc").val(comisionAsesor);
            });
            $("#emision_poliza_fc").on("change paste keyup", function() {
                var emisionPoliza = $(this).val();
                var primaTotal = $("#prima_total_fc").val();
                var iva = $("#iva_fc").val();
                primaTotal = primaTotal.replace(",", "");
                emisionPoliza = emisionPoliza.replace(",", "");
                iva = iva.replace(",", "");
                var primaNeta = primaTotal - emisionPoliza - iva;
                $("#prima_neta_fc").val(primaNeta);
                var comisionAgencia = primaNeta * 0.24;
                $("#udi_agencia_fc").val(comisionAgencia);
                var comisionAsesor = comisionAgencia * 0.30;
                $("#comision_asesor_prima_fc").val(comisionAsesor);
            });
            $(".js_unidad").on('change', function() {
                $("#id_unidad").val($(this).val());
                $("#id_catalogo").val($(this).val());
            })
        } else {
            $("#guardar-compras").on('click', function() {
                const data = {
                    fecha_envio: $("#fecha_envio").val(),
                    anio: $("#anio").val(),
                    serie: $("#serie").val(),
                    id_uso_seguro: $("#id_uso_seguro").val(),
                    accesorios: $("#accesorios").val(),
                    extension_garantia: $("#extension_garantia").val(),
                    observaciones: $("#observaciones").val()
                };
                ajax.put('api/financiamientos/update-bitacoras/' + $("#id").val(), data,
                    function(response, headers) {
                        if (headers.status == 400) {
                            return ajax.showValidations(headers);
                        }
                        var titulo = (headers.status != 200) ? headers.message :
                            "Información actualizada con éxito";
                        utils.displayWarningDialog("Información actualizada con éxito", "success", function(
                            data) {
                            return window.location.href = base_url + 'financiamientos/listado_bitacora_'+bitacora;
                        })
                    })

            })
        }

    </script>
@endsection
