<?php

defined('BASEPATH') or exit('No direct script access allowed');
class Metricas extends MX_Controller
{
  public function __construct()
  {
    parent::__construct();
    date_default_timezone_set('America/Mexico_City');
  }

  public function index()
  {
  	$data['titulo'] = "";//"Contabilidad/Consultas/polizas";
    $this->blade->render('metricas/metricas',$data);
  }

  public function actividades()
  {
  	$data['titulo'] = "";//"Contabilidad/Catalogos/saldos";
    $this->blade->render('metricas/actividad',$data);
  }

  public function uso()
  {
    $data['titulo'] = "";//"Contabilidad/Catalogos/saldos";
    $this->blade->render('metricas/uso',$data);
  }

 
}