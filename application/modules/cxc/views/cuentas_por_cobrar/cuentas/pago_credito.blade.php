@layout('tema_luna/layout')
@section('contenido')
<div class="container-fluid panel-body">
	<h1 class="mt-4"><?php echo isset($titulo) ? $titulo : "" ?></h1>
	<ol class="breadcrumb mb-4">
		<li class="breadcrumb-item"><?php echo isset($titulo) ? $titulo : "" ?></li>
		<li class="breadcrumb-item"><?php echo isset($subtitulo) ? $subtitulo : "" ?></li>
	</ol>
	<div class="row">
		<div class="col-md-12">
			<table width="100%" class="table table-bordered" cellpadding="5">
				<tr>
					<th colspan="2">Datos del cliente</th>
				</tr>
				<tr>
					<td width="120px">Nombre: </td>
					<td><?php echo isset($cliente->nombre) ? $cliente->nombre . ' '.$cliente->apellido_paterno . ' '.$cliente->apellido_materno : ''; ?>
					</td>
				</tr>
				<tr>
					<td width="120px">Núm. cliente: </td>
					<td><?php echo isset($cliente->numero_cliente) ? $cliente->numero_cliente : ''; ?></td>
				</tr>
				<tr>
					<td width="120px">Domicilio: </td>
					<td><?php echo isset($cliente->direccion) ? $cliente->direccion.' '.$cliente->colonia.' '.$cliente->municipio.' '.$cliente->estado : ''; ?>
					</td>
				</tr>
			</table>
		</div>
	</div>
	<h3>Detalle de la compra </h3>
	<div class="row mt-4">
		<div class="col-md-12">
			<table width="100%" class="table table-bordered" cellpadding="5">
				<tr>
					<th colspan="4">Detalle de la compra [ <?php echo isset($data_cuentas->estatus_cuenta) ? $data_cuentas->estatus_cuenta : ''; ?> ]</th>
				<tr>
					<th width="120px">Folio: </th>
					<td><?php echo isset($data_cuentas->folio) ? $data_cuentas->folio : ''; ?></td>
					<th width="120px">Concepto: </th>
					<td><?php echo isset($data_cuentas->concepto) ? $data_cuentas->concepto : ''; ?></td>
				</tr>
				<tr>
					<th width="120px">Saldo neto: </th>
					<td><?php echo isset($data_cuentas->importe) ? '$'.(number_format($data_cuentas->importe,2)) : ''; ?></td>
					<th width="120px">Tasa de interes %: </th>
					<td><?php echo isset($data_cuentas->tasa_interes) ? $data_cuentas->tasa_interes : ''; ?></td>
				</tr>
				<tr>
					<th width="120px">Total a pagar: </th>
					<td><?php echo isset($data_cuentas->total) ? '$'.(number_format($data_cuentas->total,2)) : ''; ?></td>
					<th width="120px">Plazo: </th>
					<td><?php echo isset($data_cuentas->plazo_credito) ? $data_cuentas->plazo_credito : ''; ?></td>

				</tr>
				<tr>
					<th width="120px">Monto actual: </th>
					<td><?php echo '$'.(number_format($saldo_actual,2)); ?></td>
					<th width="120px">Monto abonado: </th>
					<td><?php echo isset($total_abonado) ? '$'.(number_format($total_abonado,2)) : ''; ?></td>
				</tr>
				<tr>
					<th width="120px">Abono mensual: </th>
					<?php if($data_cuentas->tipo_forma_pago_id == 2) { ?>
					<td><?php echo  '$'. (number_format($abono_mensual,2)); ?>
					<?php } else { ?>
					<td>0</td>
					<?php } ?>
					</td>
					<th width="120px">Fecha compra: </th>
					<td><?php echo isset($data_cuentas->fecha) ? utils::aFecha($data_cuentas->fecha,true) : ''; ?></td>

				</tr>
			</table>
		</div>
	</div>
	<h3>Listado de abonos</h3>
	<div class="row">
		<div class="col-md-12 mb-1">
			<div class="table-responsive">
				<table class="table table-striped table-bordered" id="tbl_abonos" width="100%" cellspacing="0">
				</table>
			</div>
		</div>
	</div>
	<div class="col-md-12 mt-5 mb-5">
		<div class="text-right">
			<a href="<?php echo site_url('cxc/index');?>" type="button" class="btn btn-secondary col-md-2"><i class="fa fa-chevron-left"></i>&nbsp; Regresar</a>
		</div>
	</div>
</div>

@endsection

@section('scripts')
<script type="text/javascript">
	let count = 1;
	let cuenta_por_cobrar_id = "<?php echo isset($data_cuentas->id) ? $data_cuentas->id : ''; ?>";
	let cantidad_mes = "<?php echo isset($data_cuentas->cantidad_mes) ? $data_cuentas->cantidad_mes : ''; ?>";
	let importe = "<?php echo isset($data_cuentas->importe) ? $data_cuentas->importe : ''; ?>";
	let total_pagar = "<?php echo isset($data_cuentas->total) ? $data_cuentas->total : ''; ?>";
	let saldo_actual = "<?php echo isset($saldo_actual) ? $saldo_actual : ''; ?>";
	let abonoacomulado = 0;
</script>
<script src="{{ base_url('js/cxc/cuentas/detalle_credito.js') }}"></script>
@endsection


