<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Fordcredit extends MX_Controller
{
    public function __construct()
    {
        parent::__construct();
        date_default_timezone_set('America/Mexico_City');
        ini_set('max_execution_time', 300);
        $this->load->library('curl');
        $this->load->helper('general');
    }

    public function detalleSolicitud($id_solicitud)
    {
        $dataFromApi = $this->curl->curlGet('api/solicitud-credito/' . $id_solicitud);
        $dataSolicitud = procesarResponseApiJsonToArray($dataFromApi);
        $data['modulo'] = "Cuentas por cobrar";
        $data['titulo'] = "solicitudes";
        $data['submodulo'] = "Detalle de la solicitud";
        $data['solicitud']  = count($dataSolicitud) > 0 ? $dataSolicitud[0] : [];
        $this->blade->render('fordcredit/detalle', $data);
    }

    public function fordCredit()
    {
        $responseData = $this->curl->curlGet('api/clientes');
        $clientes = procesarResponseApiJsonToArray($responseData);
        $data['cat_cliente']  = count($clientes) > 0 ? $clientes : [];
        $data['modulo'] = "Ford credit";
        $data['submodulo'] = "Detalle";
        $data['titulo'] = "formulario";
        $this->blade->render('fordcredit/fordcredit', $data);
    }

    public function ajax_solicitudes_credito()
    {
        $responseData = $this->curl->curlGet('api/solicitud-credito');
        $response = procesarResponseApiJsonToArray($responseData);
        $data['data'] = count($response) > 0 ? $response : [];
        echo json_encode($data);
    }

    public function solicitud_credito()
    {
        $data['modulo'] = "Solicitudes de credito";
        $data['submodulo'] = "solicitudes";
        $data['titulo'] = "Solicitudes de credito";

        $this->blade->render('fordcredit/listado', $data);
    }
}
