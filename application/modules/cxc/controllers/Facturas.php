<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Facturas extends MX_Controller
{
    public function __construct()
    {
        parent::__construct();
        date_default_timezone_set('America/Mexico_City');
        //300 segundos  = 5 minutos
        ini_set('max_execution_time',300);

       
        
    }

    public function index()
    {
    	//$this->load->library('curl'); 
        //$this->load->helper('general');

        //$dataFromApi = $this->curl->curlGet('api//');
        $dataRegistro = [];
        //$dataRegistro = procesarResponseApiJsonToArray($dataFromApi); 
        $data['data'] = $dataRegistro;
        
        $data['modulo'] = "CXP";
        $data['submodulo'] = "Saldos";
        $data['titulo'] = "Facturas";
        $data['subtitulo'] = "Consultas";

        $this->blade->render('facturas/listado', $data);
    }
}