<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Index extends MX_Controller
{
    public function __construct()
    {
        parent::__construct();
        date_default_timezone_set('America/Mexico_City');
        ini_set('max_execution_time', 300);
        $this->load->library('curl');
        $this->load->helper('general');
    }

    public function index()
    {
        $data['titulo'] = "Cuenta por cobrar";
        $data['subtitulo'] = "Listado";
        $cat_clientes = $this->curl->curlGet('api/clientes');
        $estatus_cuentas = $this->curl->curlGet('api/estatus-cuentas');
        $data['cat_clientes'] = procesarResponseApiJsonToArray($cat_clientes);
        $data['estatus_cuentas'] = procesarResponseApiJsonToArray($estatus_cuentas);

        $this->blade->render('cuentas_por_cobrar/cuentas/listado', $data);
    }

    public function detalle_pago($id)
    {
        $data['titulo'] = "Cuenta por cobrar";
        $data['modulo'] = "Cuenta por cobrar";
        $dataFromApi = $this->curl->curlGet('api/cuentas-por-cobrar/' . $id);
        $data_cuentas = procesarResponseApiJsonToArray($dataFromApi);
        $apiCliente = $this->curl->curlGet('api/clientes/' . $data_cuentas->cliente_id);
        $enganche_api = $this->curl->curlGet('api/abonos-por-cobrar/abonos-by-orden-entrada?orden_entrada_id='.$id.'&tipo_abono_id=1&estatus_abono_id=3');
        $abonos_pendientes_api = $this->curl->curlGet('api/abonos-por-cobrar/abonos-by-orden-entrada?orden_entrada_id='.$id.'&tipo_abono_id=2&estatus_abono_id=1');
        $abonos_pagados_api = $this->curl->curlGet('api/abonos-por-cobrar/abonos-by-orden-entrada?orden_entrada_id='.$id.'&estatus_abono_id=3');
        
        $enganche = count(json_decode($enganche_api))>0 ? procesarResponseApiJsonToArray($enganche_api) : [];
        $abonos_pendientes = procesarResponseApiJsonToArray($abonos_pendientes_api);
        $abonos_pagados = procesarResponseApiJsonToArray($abonos_pagados_api);
        $tipo_pago = $this->curl->curlGet('api/tipo-pago');
        $tipo_abono = $this->curl->curlGet('api/tipo-abono');

        $data['cat_tipo_pago'] = procesarResponseApiJsonToArray($tipo_pago);
        $data['cat_tipo_abono'] = procesarResponseApiJsonToArray($tipo_abono);
        $data['cliente'] = current(procesarResponseApiJsonToArray($apiCliente));
        $data['total_abonado'] = $this->procesar_total_abonos($abonos_pagados);
        $data['data_cuentas']  = $data_cuentas;
        $data['enganche'] = $this->procesar_enganche($enganche);
        $data['saldo_actual']  = $data_cuentas->total - $data['total_abonado'];
        $data['abono_mensual'] = $abonos_pendientes ? current($abonos_pendientes)->total_abono : 0;

        $data['subtitulo'] = "Cuenta por cobrar / Detalle pago";
        $this->blade->render('cuentas_por_cobrar/cuentas/pago_credito',$data);
    }

    private function procesar_enganche($enganches) {
        $total_enganche = 0;
        if(isset($enganches) && is_array($enganches)) {
            foreach($enganches as $enganche) { 
                if($enganche->estatus_abono_id == 3) {
                    $total_enganche += $enganche->total_pago;
                }
            } 
        }
        return $total_enganche;
    }

    private function procesar_total_abonos($abonos) {
        $total_abono = 0;
        if(isset($abonos) && is_array($abonos)) {
            foreach($abonos as $abono) { 
                if($abono->estatus_abono_id == 3) {
                    $total_abono += $abono->total_pago;
                }
            } 
        }
        return $total_abono;
    }

    public function relacion_cobranza()
    {
        $data['titulo'] = "Cuenta por cobrar";
        $data['subtitulo'] = "Relación cobranza";
        $cat_estatus_abonos = $this->curl->curlGet('api/estatus-abono');
        $cat_clientes = $this->curl->curlGet('api/clientes');
        
        $data['fecha_inicio'] = $this->_data_first_month_day();
        $data['fecha_fin'] = $this->_data_last_month_day();

        $data['cat_clientes'] = procesarResponseApiJsonToArray($cat_clientes);
        $data['cat_estatus_abonos'] = procesarResponseApiJsonToArray($cat_estatus_abonos);

        $this->blade->render('cuentas_por_cobrar/relacion_cobranza/listado', $data);
    }

    public function gestion_cobranza()
    {
        $data['titulo'] = "Cuenta por cobrar";
        $data['subtitulo'] = "Gestión cobranza";
        $usuariosAPI = $this->curl->curlPost('api/usuarios/usuario-by-rol', [
            'rol_id' => 5,
        ]);
        $data['gestores'] = procesarResponseApiJsonToArray($usuariosAPI);
        $cat_estatus_abonos = $this->curl->curlGet('api/estatus-abono');
        $cat_clientes = $this->curl->curlGet('api/clientes');
        
        $data['fecha_inicio'] = $this->_data_first_month_day();
        $data['fecha_fin'] = $this->_data_last_month_day();

        $data['cat_clientes'] = procesarResponseApiJsonToArray($cat_clientes);
        $data['cat_estatus_abonos'] = procesarResponseApiJsonToArray($cat_estatus_abonos);

        $this->blade->render('cuentas_por_cobrar/gestion_cobranza/listado', $data);
    }

    function _data_last_month_day() { 
        $month = date('m');
        $year = date('Y');
        $day = date("d", mktime(0,0,0, $month+1, 0, $year));
   
        return date('Y-m-d', mktime(0,0,0, $month, $day, $year));
    }

    function _data_first_month_day() {
        $month = date('m');
        $year = date('Y');
        return date('Y-m-d', mktime(0,0,0, $month, 1, $year));
    }
}
