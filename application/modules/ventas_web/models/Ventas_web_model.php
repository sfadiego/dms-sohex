<?php if(!defined('BASEPATH')) exit('No direct script access allowed');

class Ventas_web_model extends CI_Model {
	//guarda venta
  	//obtener el siguiente usuario de telemarketing
  	public function getTelemarketingSiguiente(){
  		$usuariosT = $this->getUsuariosTelemarketing();
  		$q = $this->db->limit(1)->select('id_telemarketing')->order_by('id','desc')->get('ventas_web');
  		if($q->num_rows()==1){
  			$id_tm = $q->row()->id_telemarketing;
  			if($id_tm == null){
  				return $usuariosT[0]->id;
  			}
  			//Validar si es el último asesor, regresar al primero
  			if($id_tm>=count($usuariosT)){
  				return $usuariosT[0]->id;
  			}else{
  				$q = $this->db->limit(1)->select('id')->order_by('id','asc')->where('activo',1)->where('id >',$id_tm)->get('telemarketing');
	  			if($q->num_rows()==1){

	  				return $q->row()->id;
	  			}else{
	  				return 1;

	  			}
  			}
  		}else{
  			return 1;
  		}
  	}
  	//obtener el asesor que sigue
  	public function getAsesorSiguiente(){
  		$asesores = $this->getAsesores();
  		$q = $this->db->limit(1)->select('id_asesor')->order_by('id','desc')->where('id_asesor !=','')->get('ventas_web');  		
  		if($q->num_rows()==1){
  			$id_asesor = $q->row()->id_asesor;
  			//Validar si es el último asesor, regresar al primero
  			if($id_asesor == null){
  				return $asesores[0]->id;
  			}
  			if($id_asesor>=count($asesores)){
  				return $asesores[0]->id;
  			}else{
  				$q = $this->db->limit(1)->select('id')->order_by('id','asc')->where('activo',1)->where('id >',$id_asesor)->get('asesores_web');
	  			if($q->num_rows()==1){

	  				return $q->row()->id;
	  			}else{
	  				return 1;

	  			}
  			}
  		}else{
  			return 1;
  		}
  	}
  	public function getAllVentas($noContactar=false,$fecha='',$vista='telemarketing'){
  		//si es true noContactar significa que se va traer todos los registros en campo noContactar = 0
  		if($noContactar){
  			$this->db->where('noContactar',0);
  		}
  		if($fecha==''){
  			$fecha = date('Y-m-d');
  		}
  		//Si la vista es telemarketing y el statusPerfil es igual a 5, es decir que es un usuario de ventas web se limita sólo a su usuario
  		if($vista == 'telemarketing' && $this->session->userdata('statusPerfil')==5){
  			$this->db->where('v.id_usuario_telemarketing',$this->session->userdata('id'));
  			$this->db->where('fecha_programacion',$fecha);
  			$this->db->join('asesores_web a','v.id_asesor=a.id','left');
        $this->db->where('informacion_erronea',0);
  		}
  		if($vista == 'prospectos' && $this->session->userdata('statusPerfil')==5){
  			$this->db->where('v.id_usuario',$this->session->userdata('id'));
  			$this->db->join('asesores_web a','v.id_asesor=a.id','left');
  			$this->db->where('fecha_programacion',$fecha);
        if(isset($_POST['informacion_erronea']) && $_POST['informacion_erronea']!=''){
            $this->db->where('informacion_erronea',$_POST['informacion_erronea']);
        }
  		}
  		if($vista == 'asesores_web' && $this->session->userdata('statusPerfil')==5){
  			$this->db->where('v.id_usuario_asesor',$this->session->userdata('id'));
  			$this->db->join('asesores_web a','v.id_asesor=a.id');
  			$this->db->where('fecha_cita',$fecha);
  			$this->db->or_where('fecha_cita is null');
        $this->db->where('informacion_erronea',0);
  		}

  		if($this->session->userdata('statusPerfil')==1 || $this->session->userdata('statusPerfil')==4 ){
          $this->db->join('asesores_web a','v.id_asesor=a.id','left');
      }
      

  		//telemarketing true cuando se va hacer el join con los usuarios de telemarketing de lo contrario va con asesores
  		return $this->db->join('cat_unidades u','u.id=v.id_unidad')
  						->join('citas_ventas c','c.id_venta=v.id','left')
  						->join('telemarketing t','v.id_telemarketing = t.id','left')
  						->join('cat_origen o','v.id_origen=o.id')
  						->select('u.tipo as unidad,t.nombre as usuario_telemarketing,v.*,c.id as id_cita,a.asesor,c.status,o.origen,c.fecha_cita')
  						->get('ventas_web v')
  						->result();
  	}
  	public function saveComentario(){
    
       $id= $this->input->post('id_venta');
       $datos = array('comentario' => $this->input->post('comentario'),
                      'id_venta' => $id,
                      'fecha_creacion' =>date('Y-m-d H:i:s'),
                      'id_usuario'=>$this->session->userdata('id'),
                      'fecha_notificacion' =>date2sql($this->input->post('cronoFecha')).' '.$this->input->post('cronoHora'),
        );
        $this->db->insert('historial_comentarios_ventas',$datos);

        //Insertar notificación
        if($this->input->post('comentario')!=''){

          if($this->input->post('cronoFecha')!='' && $this->input->post('cronoFecha')!='cronoHora'){
            $date = date2sql($this->input->post('cronoFecha'));
            $notific['texto']= 'Cliente: '.$this->input->post('cliente').' '.$this->input->post('comentario');
            $notific['id_user'] = $this->session->userdata('id_usuario');
            $notific['estado'] = 1;
            $notific['tipo_notificacion'] = 2;
            $notific['fecha_hora'] = $date.' '.$this->input->post('cronoHora');
            $notific['estadoWeb'] = 1;
            $this->db->insert('noti_user',$notific);
            
            //Datos del usuario
            $usuario_sesion = $this->getInfoUserById();
            $celular = '';
            if($usuario_sesion!=null){
            	$celular = $usuario_sesion->adminTelefono;
            }
            $this->ProgramarNotificacion($celular,$this->input->post('comentario'),$date,$this->input->post('cronoHora'));
          }
        }
        $this->db->set('fecha_programacion',date2sql($this->input->post('cronoFecha')))->where('id',$id)->update('ventas_web');
        //Actualizar intentos

        if($this->input->post('comentario')!=''){
          $this->UpdateIntentos($id);
        }
        if($this->input->post('no_contactar')){
          $this->db->where('id',$id)->set('noContactar',1)->update('ventas_web');
        }

        echo 1;exit();
    }
    public function saveComentarioAsesor(){
    
       $id= $this->input->post('id_venta');
       $datos = array('comentario' => $this->input->post('comentario'),
                      'id_venta' => $id,
                      'fecha_creacion' =>date('Y-m-d H:i:s'),
                      'id_usuario'=>$this->session->userdata('id'),
        );
        $this->db->insert('historial_comentarios_asesores_ventas',$datos);
        echo 1;exit();
    }
    public function saveComentarioInfoErronea(){
    
       $id= $this->input->post('id_venta');
       $datos = array('comentario' => $this->input->post('comentario'),
                      'id_venta' => $id,
                      'fecha_creacion' =>date('Y-m-d H:i:s'),
                      'id_usuario'=>$this->session->userdata('id'),
        );
        $this->db->insert('historial_info_erronea',$datos);
        $this->db->where('id',$id)->set('informacion_erronea',1)->update('ventas_web');
        echo 1;exit();
    }
    public function UpdateIntentos($id=''){
	    $q = $this->db->where('id',$id)->select('intentos')->from('ventas_web')->get();
	    if($q->num_rows()==1){
	      $intentos =  $q->row()->intentos;
	      $this->db->where('id',$id)->set('intentos',$intentos+1)->update('ventas_web');
	      
	    }
    }
    //Guardar la cita
    public function guardarCita(){
    	$id= $this->input->post('id');
		$datos = array('id_venta' => $this->input->post('id_venta'),
			'id_asesor' => $this->input->post('id_asesor'),
			'status' => 1,
			'id_usuario' => $this->session->userdata('id'),
			'fecha_cita' => date2sql($this->input->post('fecha')),
			'horario' => $this->input->post('horario'),
		);		
		if($this->validarCita($datos['fecha_cita'],$datos['horario'],$id)){
			echo -1;exit();
		}
		if($id==0){
			$datos['created_at'] = date('Y-m-d H:i:s');
			$this->db->where('id',$_POST['id_venta'])->set('id_asesor',$_POST['id_asesor'])->update('ventas_web');
			//Actualizar el id del asesor correspondiente a la tabla de admin
			$id_usuario_asesor= $this->getIdUsuario($_POST['id_asesor'],'asesores_web');
			$this->db->where('id',$_POST['id_venta'])->set('id_usuario_asesor',$id_usuario_asesor)->update('ventas_web');
			
			$exito = $this->db->insert('citas_ventas',$datos);
			$id= $this->db->insert_id();
			//Enviar notificacion asesor
			$datos_citas = $this->getDatosCitaVenta($id);
			$telefono_asesor = $this->getInfoAsesor($this->input->post('id_asesor'))->telefono;
			
			$cliente = $datos_citas->nombre.' '.$datos_citas->ap.' '.$datos_citas->am.', tel: '.$datos_citas->telefono_celular;
			$mensaje = "Se agendó una cita: ".$cliente.' fecha: '.$datos_citas->fecha_cita.' '.$datos_citas->horario;
			

			$this->notificacion($telefono_asesor,$mensaje);
      if($_POST['id_asesor']==3){
        //Enviar copia al supervisor
        $this->notificacion('3310623813','CP. '.$mensaje);
      }
		}else{
			$datos['updated_at'] = date('Y-m-d H:i:s');
			$exito = $this->db->where('id',$id)->update('citas_ventas',$datos);
		}
		if($exito){
			echo 1;die();
		} else{
			echo 0; die();
		}
    }
    public function getDatosCitaVenta($idcita=''){
		return $this->db->select('v.id,v.nombre,v.ap,v.am,v.telefono_celular,c.fecha_cita,c.horario')
						->join('ventas_web v','c.id_venta = v.id')
						->where('c.id',$idcita)
						->get('citas_ventas c')
						->row();

    }
    public function notificacion($celular_sms='',$mensaje_sms=''){

    	$ch = curl_init();
    	curl_setopt($ch, CURLOPT_URL,"https://sohex.mx/cs/sohex_notificaciones/index.php/app_notificaciones/enviar_notificacion");
    	curl_setopt($ch, CURLOPT_POST, 1);
    	curl_setopt($ch, CURLOPT_POSTFIELDS,
    		"celular=".$celular_sms."&mensaje=".$mensaje_sms."&sucursal=M2319"."");
    	curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    	$server_output = curl_exec($ch);
    	curl_close ($ch);
    }
    //Validar si existe la cita en un horario
    public function validarCita($fecha_cita='',$horario='',$id){
    	$q = $this->db->where('fecha_cita',$fecha_cita)->where('horario',$horario)->where('id !=',$id)->get('citas_ventas')->result();
    	if(count($q)>0){
    		return true;
    	}else{
    		return false;
    	}
    }
    public function horariosOcupados($id_asesor='',$fecha=''){
    	$datos =  $this->db->where('id_asesor',$id_asesor)->where('fecha_cita',$fecha)->select('horario')->get('citas_ventas')->result_array();
    	$array_horarios = array();
    	foreach ($datos as $key => $value) {
    		$array_horarios[] = $value['horario'];
    	}
    	return $array_horarios;
    }
    //Validar si existe la cita en un horario
    public function ExisteCita($id_venta=''){
    	$q = $this->db->where('id_venta',$id_venta)->limit(1)->get('citas_ventas');
    	if($q->num_rows()==1){
    		return $q->row()->id;
    	}else{
    		return 0;
    	}
    }
    //Validar si existe la cita en un horario
    public function getInfoAsesor($id=''){
    	$q = $this->db->where('id',$id)->limit(1)->get('asesores_web');
    	if($q->num_rows()==1){
    		return $q->row();
    	}else{
    		return '';
    	}
    }
    //obtiene los asesores
  	public function getAsesores(){
	   return $this->db->where('activo',1)->get('asesores_web')->result();
  	}
  	 //obtiene los asesores
  	public function getUsuariosTelemarketing(){
	   return $this->db->where('activo',1)->get('telemarketing')->result();
  	}
  	public function getDatosVenta($fecha='',$id_asesor=''){
	 return $this->db->select('c.id,c.fecha_cita,c.horario,c.status,v.nombre,v.ap,v.am,v.telefono_casa,v.telefono_celular,v.email,u.tipo as unidad,a.asesor,a.activo')
	 				->join('ventas_web v','c.id_venta = v.id')
	 				->join('asesores_web a','v.id_asesor = a.id')
	 				->join('cat_unidades u','v.id_unidad = u.id')
	 				->where('c.fecha_cita',$fecha)
	 				->where('v.id_asesor',$id_asesor)
	 				->get('citas_ventas c')
	 				->result();

 	}
 	public function tabla_asesores($fecha='',$valor='',$tiempo_aumento=30,$tipo=0){
 		$tmpl = array (
 			'table_open' => '<table class="table table-bordered" cellspacing="0" width="100%">');
 		$this->table->set_template($tmpl);
 		$this->table->set_heading('Horario','# Cita','Cliente','Unidad','Valoraciones');
		$contador_asesores = 0;
		$asesores = procesarResponseApiJsonToArray($this->curl->curlPost('api/usuarios/usuario-by-rol', [
            'rol_id' => 13
        ]));
 		$array_asesores = array();
 		foreach ($asesores as $key => $value) {
			$array_asesores[$key] = $value;
			$dataCita = procesarResponseApiJsonToArray($this->curl->curlPost('api/telemarketing/citas-ventas/datos-citas', [
				'fecha' => date2sql($fecha),
				'id_asesor'=>$value->id
			]));
 			$array_asesores[$key]->datos_cita = $dataCita;
 		}

 		if(count($array_asesores)>0){
 			foreach($asesores as $c => $value) {
 				if($contador_asesores%2==$tipo){
 					$row=array();
 					$row[]=array('class'=>'text-center col-sm-1 ','data'=>$value->nombre.' '.$value->apellido_paterno.' '.$value->apellido_materno,'colspan'=>5, 'style'=>'background-color:white');
 					$this->table->add_row($row);
 					$time = '09:00';
 					$contador=0;
 					while($contador<$valor){
 						$row=array();
 						$bandera = false;
 						if(isset($value->datos_cita)){
 							foreach ($value->datos_cita as $key => $val) {
 								if($val->horario==$time.':00' && !$bandera){
 									$id_status_cita = $val->id_status;
 									$cliente = $val->nombre_cliente.' '.$val->ap_cliente.' '.$val->am_cliente;
 									$id_cita = $val->id;
 									$unidad = $val->unidad;
 									$bandera = true;
 								}
 							}
 						}

 						if($bandera){
 							$style = "";
 							$row[]=array('width'=>'10%','data'=>$time,'style'=>$style);
							$acciones = '';
							$row[]=array('data'=>$id_cita,'style'=>$style);
 							$row[]=array('data'=>$cliente,'style'=>$style);
 							$row[]=array('data'=>$unidad,'style'=>$style);
 							$clase = ($id_status_cita==2)?"verde":"gris";
							 $acciones.='<a href="" data-id="'.$id_cita.'" class="js_cambiar_status '.$clase.'  elemento_'.$id_cita.'"  aria-hidden="true" data-toggle="tooltip" data-placement="top" title="Llegó" data-status="2" >
							 	<i class="pe pe-7s-check"></i>
							 </a>';

 							$clase = ($id_status_cita==3)?"verde":"gris";
							 $acciones.= '<a href="" data-id="'.$id_cita.'" class=" '.$clase.' js_cambiar_status elemento_'.$id_cita.'"  aria-hidden="true" data-toggle="tooltip" data-placement="top" title="No llegó" data-status="3">
							 <i class="pe-7s-less"></i>
							 </a>';
 							$row[]=array('data'=>$acciones,'style'=>$style);
 						}else{
 							$row[]=array('width'=>'10%','data'=>$time);
 							$row[]=array('data'=>'','colspan'=>5);
 						}
 						$this->table->add_row($row);

 						$timestamp = strtotime($time) + $tiempo_aumento*60;
 						$time = date('H:i', $timestamp);
 						$contador++;
 					} 
 				} 
 				$contador_asesores ++;
 			}
 		}else{
 			$row=array();
 			$row[]=array('class'=>'text-center col-sm-1','data'=>'No hay registros para mostrar','colspan'=>4);
 			$this->table->add_row($row);
 		}
 		return $this->table->generate();
 	}
 	//Retornar el usuario al que corresponde el usuario de marketing o asesores
 	public function getIdUsuario($id='',$tabla=''){
 		$q = $this->db->limit(1)->where('id',$id)->select('id_usuario_corresponde')->get($tabla); 		
 		if($q->num_rows()==1){
 			return $q->row()->id_usuario_corresponde;
 		}else{
 			return null;
 		}
 	}
  //Retornar el usuario al que corresponde el usuario de marketing o asesores
  public function getIdAsesorVentasById(){
    $q = $this->db->limit(1)->where('id_usuario_corresponde',$this->session->userdata('id'))->select('id')->get('asesores_web');     
    if($q->num_rows()==1){
      return $q->row()->id;
    }else{
      return null;
    }
  }
 	//Retornar el usuario de la tabla admin
 	public function getInfoUserById($id=''){
 		$q = $this->db->limit(1)->where('adminId',$this->session->userdata('id'))->get('admin'); 		
 		if($q->num_rows()==1){
 			return $q->row();
 		}else{
 			return null;
 		}
 	}
 	public function getHistorialProactivo(){
		$fecha_inicio = date2sql($this->input->post('finicio'));
	    $fecha_fin = date2sql($this->input->post('ffin'));

	    if($fecha_inicio!='' && $fecha_fin!=''){
	    	$this->db->where('date(fecha_creacion) >=',$fecha_inicio);
	    	$this->db->where('date(fecha_creacion) <=',$fecha_fin);
	    }
	    if($this->session->userdata('statusPerfil')==5){
	    	$this->db->where('h.id_usuario',$this->session->userdata('id'));
	    }
	    return $this->db->select('h.*, a.adminNombre as usuario, v.am,v.ap,v.nombre')
	    				->join('admin a','a.adminId = h.id_usuario')
	    				->join('ventas_web v','h.id_venta = v.id')
	    				->get('historial_comentarios_ventas h')
	    				->result(); 
 	}
 	public function ProgramarNotificacion($celular='',$mensaje='',$fecha='',$hora=''){
	    $sucursal = "M2319"; // sucursal
	    if($hora==''){
	      $hora="12:00:00";
	    }
	    $fecha = $fecha.' '.$hora;
	    $ch = curl_init();
	    curl_setopt($ch, CURLOPT_URL,"https://sohex.mx/cs/sohex_notificaciones/index.php/app_notificaciones/proactivo");
	    curl_setopt($ch, CURLOPT_POST, 1);
	    curl_setopt($ch, CURLOPT_POSTFIELDS,
	                "celular=".$celular."&texto=".$mensaje."&sucursal=".$sucursal."&fecha=".$fecha."");
	    // Receive server response ...
	    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
	    $server_output = curl_exec($ch);
	    curl_close ($ch);
	    //var_dump($server_output);
	}
  public function getInfoReportes(){
    //print_r($_POST);die();
    if(isset($_POST['id_unidad']) && $_POST['id_unidad']!=''){
      $this->db->where('id_unidad',$_POST['id_unidad']);
    }
    if(isset($_POST['id_asesor']) && $_POST['id_asesor']!=''){
      $this->db->where('id_asesor',$_POST['id_asesor']);
    }
    if(isset($_POST['id_telemarketing']) && $_POST['id_telemarketing']!=''){
      $this->db->where('id_telemarketing',$_POST['id_telemarketing']);
    }
    if(isset($_POST['id_origen']) && $_POST['id_origen']!=''){
      $this->db->where('id_origen',$_POST['id_origen']);
    }


    if(isset($_POST['fecha_inicio']) && $_POST['fecha_inicio']!=''){
      $this->db->where('date(fecha_creacion) >=',date2sql($_POST['fecha_inicio']));
    }

    if(isset($_POST['fecha_fin']) && $_POST['fecha_fin']!=''){
      $this->db->where('date(fecha_creacion) <=',date2sql($_POST['fecha_fin']));
    }

    return $this->db->get('v_reportes_ventas_web')->result();
  }


}