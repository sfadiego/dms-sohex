<table id="tbl_proactivo" class="table table-striped table-bordered">
	<thead>
		<tr class="tr_principal">
		<th>Usuario</th>
		<th>Comentario</th>
		<th>Cliente</th>
		<th>Fecha de contacto</th>
	</tr>
	</thead>
	<tbody>
		@foreach($proactivo as $p => $registro)
			<tr>
				<td>{{$registro->usuario}}</td>
				<td>{{$registro->comentario}}</td>
				<td>{{$registro->nombre.' '.$registro->ap.' '.$registro->am}}</td>
				<td>{{$registro->fecha_creacion}}</td>
			</tr>
		@endforeach
	</tbody>
</table>