<form action="" id="frm_comentarios">
	<input type="hidden" name="id_venta" id="id_venta" value="{{$id_venta}}">
	<div class="row">
		<div class="col-sm-12">
			<div style="margin-top: 10px" class="alert alert-info alert-dismissible" role="alert">
				<strong>Ingresa un comentario para que el prospecto de venta pueda corregir las observaciones</strong>.
			</div>
		</div>
	</div>
	<div class="row">
		<div class="col-sm-12">
			<label>Comentario</label>
			{{$input_comentario}}
			<span class="error error_comentario"></span>
		</div>
	</div>
</form>