@layout('tema_luna/layout')
<style>
    .visible {
        visibility: visible;
    }

    .novisible {
        display: none !important;
    }

</style>
@section('contenido')
    <div class="container-fluid panel-body">
        <h1 class="mt-4"><?php echo isset($titulo) ? $titulo : ''; ?></h1>
        <form id="frm">
            <input type="hidden" name="id" id="id" value="{{ $input_id }}">
			<input type="hidden" name="id_asesor_telemarketing_siguiente" id="id_asesor_telemarketing_siguiente" value="{{ $id_asesor_telemarketing_siguiente }}">
			<input type="hidden" name="id_asesor_ventas_siguiente" id="id_asesor_ventas_siguiente" value="{{ $id_asesor_ventas_siguiente }}">
            <div class="row">
                <div class="col-sm-4 cliente-registrado">
                    <label for="">Cliente</label>
                    {{ $drop_clientes }}
                    <div class="error error_id_cliente"></div>
                </div>
                <div class="col-sm-4">
                    <label for="">¿Cliente nuevo?</label>
                    <input type="checkbox" name="cliente_nuevo" id="cliente_nuevo" value="0">
                </div>
            </div>
            <div class="row nuevo-cliente novisible">
                <div class="col-sm-4">
                    <label>Nombre</label>
                    {{ $input_nombre }}
                    <div class="error error_nombre"></div>
                </div>
                <div class="col-sm-4">
                    <label>Apellido paterno</label>
                    {{ $input_ap }}
                    <div class="error error_apellido_paterno"></div>
                </div>
                <div class="col-sm-4">
                    <label>Apellido materno</label>
                    {{ $input_am }}
                    <div class="error error_apellido_materno"></div>
                </div>
            </div>
            <br>
            <div class="row nuevo-cliente novisible">
                <div class="col-sm-4">
                    <label>Teléfono de casa/oficina</label>
                    {{ $input_telefono_casa }}
                    <div class="error error_telefono_casa"></div>
                </div>
                <div class="col-sm-4">
                    <label>Teléfono celular</label>
                    {{ $input_telefono_celular }}
                    <div class="error error_telefono_celular"></div>
                </div>
                <div class="col-sm-4">
                    <label>Email</label>
                    {{ $input_email }}
                    <div class="error error_email"></div>
                </div>
            </div>
            <br>
            <div class="row">
                <div class="col-sm-4">
                    <label>Unidad de interes</label>
                    {{ $drop_unidad }}
                    <div class="error error_id_unidad"></div>
                </div>
                <div class="col-sm-4">
                    <label>Origen</label>
                    {{ $drop_origen }}
                    <div class="error error_id_origen"></div>
                </div>
                <div class="col-sm-4">
                    <label>Asesor telemarketing </label>
                    {{ $drop_telemarketing }}
                    <div class="error error_id_usuario_telemarketing"></div>
                </div>
            </div>
            <br>
            <div class="row">
                <div class="col-sm-4">
                    <label>Asesor de venta</label>
                    {{ $drop_asesores }}
                    <div class="error error_id_asesor_web_asignado"></div>
                </div>
            </div>
        </form>
        <br>
        <button type="button" id="guardar" class="btn btn-primary">Guardar</button>
        <div id="mensaje" class="alert alert-success" role="alert" style="display:none;"></div>
    </div>
    </div>
@endsection
@section('scripts')
    <script type="text/javascript">
        $("#id_usuario_telemarketing option:not(:selected)").attr("disabled", true);
        const cliente_nuevo = () => {
            if ($("#cliente_nuevo").prop('checked')) {
                $(".cliente-registrado").removeClass('visible').addClass('novisible');
                $(".nuevo-cliente").removeClass('novisible');
            } else {
                $(".cliente-registrado").removeClass('novisible');
                $(".nuevo-cliente").removeClass('visible').addClass('novisible');
            }
        }
        $("#cliente_nuevo").on('click', cliente_nuevo)

        $("#guardar").on('click', function() {
            if ($("#id_usuario_telemarketing").val() != '' && $("#id_asesor_web_asignado").val() != '') {
                utils.displayWarningDialog(
                    "Solamente puedes seleccionar un asesor telemarketing o un asesor de venta, ambos NO",
                    'warning',
                    function(result) {});
            } else {
                const data = {
                    id_origen: $("#id_origen").val(),
                    id_unidad: $("#id_unidad").val(),
                    id_usuario_creo: "{{ $this->session->userdata('id') }}",
                    id_usuario_telemarketing: $("#id_usuario_telemarketing").val(),
                    id_asesor_web_asignado: $("#id_asesor_web_asignado").val(),
                }
                if ($("#cliente_nuevo").prop('checked')) {
                    data.nombre = $("#nombre").val();
                    data.apellido_paterno = $("#apellido_paterno").val();
                    data.apellido_materno = $("#apellido_materno").val();
                    data.telefono = $("#telefono").val();
                    data.telefono_secundario = $("#telefono_secundario").val();
                    data.correo_electronico = $("#correo_electronico").val();
                } else {
                    data.id_cliente = $("#id_cliente").val();
                }
                if ($("#id").val() != 0) {
                    ajax.put('api/telemarketing/ventas-web/' + $("#id").val(), data,
                        function(response, headers) {
                            if (headers.status == 400) {
                                return ajax.showValidations(headers);
                            }
                            var titulo = (headers.status != 200) ? headers.message :
                                "Venta actualizada con éxito";
                            utils.displayWarningDialog("Venta actualizada con éxito", "success", function(
                                data) {
                                return window.location.href = base_url + 'ventas_web/listado';
                            })
                        })
                } else {
                    ajax.post('api/telemarketing/ventas-web', data,
                        function(response, headers) {
                            if (headers.status == 400) {
                                return ajax.showValidations(headers);
                            }
                            var titulo = (headers.status != 200) ? headers.message : "Venta guardada con éxito";
                            utils.displayWarningDialog("Venta guardada con éxito", "success", function(data) {
                                return window.location.href = base_url + 'ventas_web/listado';
                            })
                        })
                }
            }
        })
        var existe_cita = "{{ $existe_cita }}";
        var vista = "{{ $vista }}";
        //Si ya existe la cita no dejar editar ni el asesor ni el de telemarketing
        if (existe_cita) {
            $("#id_asesor option:not(:selected)").attr("disabled", true);
            $("#id_telemarketing option:not(:selected)").attr("disabled", true);
        }

        if (vista == 'asesores_web') {
            $("#id_asesor option:not(:selected)").attr("disabled", true);
            $("#id_telemarketing").attr("disabled", true);
        }
        $(".addOrigen").on('click', function() {
            var url = site_url + "/ventas_web/agregar_origen/0";
            customModal(url, {}, "GET", "md", agregarOrigen, "", "Guardar", "Cancelar", "Agregar origen",
                "modalOrigen");
        });
        $("body").on("change", '#id_origen', function() {
            if ($(this).val() == 1) {
                $("#id_usuario_telemarketing").val('');
                $("#id_usuario_telemarketing").prop('disabled', true);
                $("#id_asesor_web_asignado").prop('disabled', false);
				$("#id_asesor_web_asignado").val($("#id_asesor_ventas_siguiente").val());
				
            } else {
                $("#id_asesor_web_asignado").val('');
                $("#id_usuario_telemarketing").prop('disabled', false);
                $("#id_asesor_web_asignado").prop('disabled', true);
                $("#id_usuario_telemarketing").val($("#id_asesor_telemarketing_siguiente").val());

            }
        });

        function agregarOrigen() {
            var url = site_url + "/ventas_web/agregar_origen";
            ajaxJson(url, {
                "origen": $("#origen").val()
            }, "POST", "async", function(result) {
                result = JSON.parse(result);
                if (!result.exito) {
                    //Se recorre el json y se coloca el error en la div correspondiente
                    $.each(result.errors, function(i, item) {
                        $(".error_" + i).empty();
                        $(".error_" + i).append(item);
                        $(".error_" + i).css("color", "red");
                    });
                } else {
                    ExitoCustom("Registro agregado correctamente", function() {
                        $("#id_origen").append("<option value='" + result.id + "'>" + result.origen +
                            "</option>");
                        $("#id_origen").val(result.id);
                        $(".close").trigger('click');
                    });
                }
            });
        }

    </script>
@endsection
