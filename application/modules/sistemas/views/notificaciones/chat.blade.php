@layout('tema_luna/layout')
<style>
    * {
        box-sizing: border-box;
    }

    .chat_window {
        background-color: #fff;
        border: 2px solid #ccc;
    }

    .messages {
        position: relative;
        list-style: none;
        padding: 20px 10px 0 10px;
        margin: 0;
        height: 600px;
        overflow: scroll;
    }

    .messages .message {
        clear: both;
        overflow: hidden;
        margin-bottom: 20px;
        transition: all 0.5s linear;
        opacity: 0;
    }

    .messages .message.left .avatar {
        background-color: #2c2929;
        box-shadow: 5px 5px 5px rgb(10 11 11 / 20%);
        float: left;
        text-align: center;
        padding-top: 20px;
        color: white;
        overflow: hidden;
        width: 60px;
    }

    .messages .message.left .text_wrapper {
        background-color: #f1f1f1;
        box-shadow: 5px 5px 5px rgb(10 11 11 / 20%);
        margin-left: 20px;
    }

    .messages .message.left .text_wrapper::after,
    .messages .message.left .text_wrapper::before {
        right: 100%;
        border-right-color: #f1f1f1;
    }

    .messages .message.left .text {
        color: #2c2929;
    }

    .messages .message.right .avatar {
        background-color: #356aac;
        box-shadow: 5px 5px 5px rgb(10 11 11 / 20%);
        float: right;
        text-align: center;
        padding-top: 20px;
        color: white;
        overflow: hidden;
        width: 60px;
    }

    .messages .message.right .text_wrapper {
        background-color: #becaff;
        box-shadow: 5px 5px 5px rgb(10 11 11 / 20%);
        margin-right: 20px;
        float: right;
    }

    .messages .message.right .text_wrapper::after,
    .messages .message.right .text_wrapper::before {
        left: 100%;
        border-left-color: #becaff;
    }

    .messages .message.right .text {
        color: #45829b;
    }

    .messages .message.appeared {
        opacity: 1;
    }

    .messages .message .avatar {
        width: 60px;
        height: 60px;
        border-radius: 50%;
        display: inline-block;
    }

    .messages .message .text_wrapper {
        display: inline-block;
        padding: 20px;
        border-radius: 6px;
        width: calc(100% - 85px);
        min-width: 100px;
        position: relative;
    }

    .messages .message .text_wrapper::after,
    .messages .message .text_wrapper:before {
        top: 18px;
        border: solid transparent;
        content: " ";
        height: 0;
        width: 0;
        position: absolute;
        pointer-events: none;
    }

    .messages .message .text_wrapper::after {
        border-width: 13px;
        margin-top: 0px;
    }

    .messages .message .text_wrapper::before {
        border-width: 15px;
        margin-top: -2px;
    }

    .messages .message .text_wrapper .text {
        font-size: 18px;
        font-weight: 300;
    }

    .bottom_wrapper {
        position: relative;
        width: 100%;
        background-color: #fff;
        padding: 20px 20px;
        bottom: 0;
    }

    .bottom_wrapper .message_input_wrapper {
        display: inline-block;
        height: 50px;
        border-radius: 25px;
        border: 1px solid #bcbdc0;
        width: calc(100% - 160px);
        position: relative;
        padding: 0 20px;
    }

    .bottom_wrapper .message_input_wrapper .message_input {
        border: none;
        height: 100%;
        box-sizing: border-box;
        width: calc(100% - 40px);
        position: absolute;
        outline-width: 0;
        background-color: white;
    }

    .bottom_wrapper .send_message {
        width: 140px;
        height: 50px;
        display: inline-block;
        border-radius: 50px;
        background-color: #a3d063;
        border: 2px solid #a3d063;
        color: #fff;
        cursor: pointer;
        transition: all 0.2s linear;
        text-align: center;
        float: right;
    }

    .bottom_wrapper .sending {
        display: none;
        width: 140px;
        height: 50px;
        border-radius: 50px;
        background-color: #a3d063;
        border: 2px solid #a3d063;
        color: #fff;
        cursor: pointer;
        transition: all 0.2s linear;
        text-align: center;
        float: right;
    }

    .bottom_wrapper .send_message:hover {
        color: #a3d063;
        background-color: #fff;
    }

    .bottom_wrapper .send_message .text {
        font-size: 18px;
        font-weight: 300;
        display: inline-block;
        line-height: 48px;
    }

    .bottom_wrapper .sending .text {
        font-size: 18px;
        font-weight: 300;
        display: inline-block;
        line-height: 48px;
    }
</style>
@section('contenido')
<div class="container-fluid panel-body">
    <h1 class="mt-4">CHAT</h1>
    <ol class="breadcrumb mb-4">
        <li class="breadcrumb-item ">{{ ucwords($this->uri->segment(1)) }}</li>
        <li class="breadcrumb-item">{{ ucwords($this->uri->segment(2)) }}</li>
        <li class="breadcrumb-item active">{{ ucwords($this->uri->segment(3)) }}</li>
    </ol>
    <div class="row">
        <div class="col-md-12">
            <div class="chat_window">
                <div id="cargando" class="spin text-center">
                    <i style="font-size:150px" class="fa fa-spinner fa-pulse fa-fw"></i>
                    <span class="sr-only">Loading...</span>
                </div>
                <ul class="messages" id="chat">

                </ul>
                <div class="bottom_wrapper clearfix">
                    <div class="message_input_wrapper"> <input class="message_input" id="mensaje_chat" placeholder="Escribe el mensaje"></div>
                    <div class="send_message" onclick="enviarNotificacion()">
                        <div class="icon"></div>
                        <div class="text">Enviar</div>
                    </div>
                    <div class="sending">
                        <div class="icon"></div>
                        <div class="text">Enviando ...</div>
                    </div>
                </div>
                <input type="hidden" id="telefono2" value="<?php echo $destinatario; ?>" />
            </div>
        </div>
    </div>
</div>
@endsection
@section('scripts')
<script type="text/javascript">
    $(document).ready(function() {
        getNotificacionesChat();
        setInterval(getNotificacionesChat, 10000);
    });

    function getNotificacionesChat() {
        var objDiv = document.getElementById("chat");
        objDiv.scrollTop = objDiv.scrollHeight;
        ajax.get(`api/chat`, {
            telefono1: telefono,
            telefono2: $("#telefono2").val(),
        }, function(response, headers) {
            $("#chat").html('');
            let user = '';
            let tipo = '';
            if (headers.status == 200) {
                if (response && response.length >= 1) {
                    $.each(response.reverse(), function(key, value) {
                        tipo = telefono == value.celular ? 'left' : 'right';
                        $("#cargando").hide();
                        $("#chat").append('<li class="message ' + tipo + ' appeared">' +
                            '<div class="avatar">' + value.destinatario + '</div>' +
                            '<div class="text_wrapper">' +
                            '<div class="text"> ' + value.mensaje + '</div>' +
                            '<div class="text-right">' + value.fecha_creacion + '</div>' +
                            '</div>' +
                            '</li>');
                    });
                } else {
                    $("#cargando").hide();
                }
            } else {
                $("#cargando").hide();
            }
        });
    }

    function enviarNotificacion(_this) {
        $(".send_message").hide();
        $(".sending").show();
        ajax.post(`api/notificaciones`, {
            mensaje: $('#mensaje_chat').val(),
            telefono: $("#telefono2").val(),
            celular2: telefono,
            mensaje_respuesta: $(_this).data('respuesta')
        }, function(response, headers) {
            if (headers.status == 200) {
                toastr.success("Notificación enviada correctamente");
                $('#mensaje_chat').val('');
                $(".send_message").show();
                $(".sending").hide();

            } else {
                toastr.error("Ocurrio un error al enviar el mensaje");
                $(".send_message").show();
                $(".sending").hide();
            }
        });
    }

    function obtenerFechaMostrar(fecha) {
        const dia = 2,
            mes = 1,
            anio = 0;
        split = fecha.split(' ');
        fecha = split[0].split('-');
        return fecha[dia] + '/' + fecha[mes] + '/' + fecha[anio] + ' ' + split[1];
    }
</script>
@endsection