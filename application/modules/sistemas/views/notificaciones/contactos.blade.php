@layout('tema_luna/layout')
@section('contenido')
<div class="container-fluid panel-body">
	<h1 class="mt-4">Panel de notificaciones</h1>
	<ol class="breadcrumb mb-4">
		<li class="breadcrumb-item ">{{ ucwords($this->uri->segment(1)) }}</li>
		<li class="breadcrumb-item">{{ ucwords($this->uri->segment(2)) }}</li>
		<li class="breadcrumb-item active">{{ ucwords($this->uri->segment(3)) }}</li>
	</ol>

	<div class="row">
		<div class="col-md-12">
			<div class="row">
				<?php
				if (isset($usuarios) && $usuarios) {
					foreach ($usuarios as $usuario) {
						if ($usuario->id != $this->session->userdata('id')) { ?>
							<div class="col-md-3">
								<div class="panel panel-filled " style="border:1px solid #ccc; border-radius:8px; background-color:#f9f9f9">
									<div style="padding:10px" class="">
										<div class="btn-group pull-right m-b-md">
											<button class="btn btn-primary" title="Enviar notificación" class="btn btn-primary" onclick="modalNotificacion(this)" data-nombre="{{ $usuario->nombre }}" data-telefono="{{ $usuario->telefono }}"><i style="color:#f7af3e" class="fa fa-envelope"></i></button>
											<button class="btn btn-primary" title="Comenzar chat" class="btn btn-primary" onclick="gotoChat(this)" data-telefono="{{ $usuario->telefono }}"><i style="color:#22ae22" class="fa fa-comment-alt"></i></button>
										</div>
										<i class="fa fa-user-circle fa-3x"></i>
										{{ $usuario->nombre}}<br />
										<p>
											<b>Usuario: </b> {{ $usuario->usuario }}<br />
											<b>Teléfono: </b> {{ $usuario->telefono }}<br />
											<b>Rol: </b> {{ $usuario->rol }}<br />
										</p>
									</div>
								</div>
							</div>
				<?php }
					}
				} ?>
			</div>
		</div>
	</div>
</div>
@endsection
@section('modal')
<div class="modal fade" id="modal-notificacion" data-toggle="modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
	<div class="modal-dialog modal-dialog-centered" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title" id="title_modal">Notificación</h5>
			</div>
			<div class="modal-body" style="padding:20px !important;">
				<div class="row">
					<div class="col-md-12">
						<label>Número telefónico: <span id="telefono_modal" style="font-weight:bold"></span></label>
						<div class="form-group">
							<textarea id="mensaje" class="form-control" maxlength="120" style="min-height:140px"></textarea>
							<input type="hidden" name="celular_modal" id="celular_modal">
						</div>
					</div>
				</div>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
				<button onclick="setNotificaciones()" id="btn-modal-agregar" type="button" class="btn btn-primary"><i class="fas fa-envelope"></i> Enviar</button>
			</div>
		</div>
	</div>
</div>
@endsection

@section('scripts')
<script>
	// $('#tbl_usuarios').DataTable({
	// 	language: {
	// 		url: PATH_LANGUAGE
	// 	},
	// 	"ajax": {
	// 		url: PATH_API + "api/usuarios",
	// 		type: 'GET',
	// 		dataSrc: ""
	// 	},
	// 	columns: [{
	// 			title: "#",
	// 			data: 'id',
	// 		},
	// 		{
	// 			title: "Nombre",
	// 			data: 'nombre',
	// 		},
	// 		{
	// 			title: "Usuario",
	// 			data: 'usuario',
	// 		},
	// 		{
	// 			title: "Teléfono",
	// 			data: 'telefono',
	// 		},
	// 		{
	// 			title: "Rol",
	// 			data: 'rol',
	// 		},
	// 		{
	// 			title: "-",
	// 			render: function(data, type, row) {
	// 				btn_notificacion =
	// 					'<button title="Enviar notificación" class="btn btn-primary" onclick="modalNotificacion(this)" data-nombre="' + row.nombre + '" data-telefono="' + row.telefono + '"><i class="fas fa-envelope"></i> </button>';
	// 				btn_chat =
	// 					'<button style="display:none" title="Comenzar chat"  class="btn btn-primary" onclick="gotoChat(this)" data-telefono="' + row.telefono + '"><i class="fas fa-comment-alt"></i> </button>';
	// 				return btn_notificacion + ' ' + btn_chat;
	// 			}
	// 		}


	// 	]
	// });

	function modalNotificacion(_this) {
		$("#telefono_modal").html($(_this).data('telefono'));
		var user = $(_this).data('nombre');
		$("#title_modal").text('Notificacion ' + user);
		$("#celular_modal").val($(_this).data('telefono'));

		$("#modal-notificacion").modal('show');
	}

	function gotoChat(_this) {
		$("#telefono_modal").html($(_this).data('telefono'));
		window.location.href = PATH + '/sistemas/notificaciones/chat?destinatario=' + $(_this).data('telefono')
	}

	function setNotificaciones() {
		ajax.post(`api/notificaciones`, {
			mensaje: $("#mensaje").val(),
			telefono: $("#celular_modal").val(),
			celular2: telefono
		}, function(response, headers) {
			if (headers.status == 200) {
				toastr.success("Notificación enviada correctamente");
			}
		});
		$("#modal-notificacion").modal('hide');

	}
</script>
@endsection