@layout('tema_luna/layout')
@section('contenido')
<div class="container-fluid panel-body">
    <h1 class="mt-4"><?php echo isset($modulo) ? $modulo : "" ?></h1>
    <ol class="breadcrumb mb-4">
        <li class="breadcrumb-item"><?php echo isset($submodulo) ? $submodulo : "" ?></li>
        <li class="breadcrumb-item"><?php echo isset($titulo) ? $titulo : "" ?></li>
        <li class="breadcrumb-item active"><?php echo isset($subtitulo) ? $subtitulo : "" ?></li>
    </ol>

    <div class="row mb-3">
        <div class="col-md-8"></div>
        <div class="col-md-4" align="right">
            <a class="btn btn-primary" href="<?php echo base_url('autos/Recepcion/alta') ?>">Recibir unidad</a>
        </div>
    </div>
    
    <div class="row">
        <div class="col-md-12">
            <div class="table-responsive">
                <table class="table table-bordered" id="tbl_recepcion" width="100%" cellspacing="0">
                    <thead>
                        <tr>
                            <th>#</th>
                            <th>Unidad</th>
                            <th>VIN</th>
                            <th>Ingresado por:</th>
                            <th>N° Remisión</th>
                            <th>Folio Pedido</th>
                            <th>-</th>
                            <th>-</th>
                            <th>-</th>
                            <th>-</th>                            
                        </tr>
                    </thead>
                    <tbody>
                         @if(!empty($data))
                            @foreach ($data as $key => $item)
                                <tr>
                                    <td>{{ $item->id_unidad }} </td>
                                    <td>{{$item->unidad_descripcion }} </td>
                                    <td>{{$item->vin}}</td>
                                    <td>{{$item->usuario_recibe}}</td>
                                    <td>{{$item->n_remision}}</td>
                                    <td>{{$item->folio_pedido}}</td>
                                    <td>
                                        <a href="{{ base_url('autos/Recepcion/editar/'.$item->id_unidad) }}"  class="btn btn-primary" type="button">
                                            <i class="fas fa-pen"></i>
                                        </a>
                                    </td>
                                    <td>
                                        <a href="{{ base_url('autos/Checklist/alta/'.$item->id_unidad) }}" class="btn btn-success" type="button" title="Checklist de Entrada">
                                            <i class="fas fa-clipboard-list"></i>
                                        </a>
                                    </td>
                                    <td>
                                        <a href="{{ base_url('autos/Salidas/alta/'.$item->id_unidad) }}" class="btn btn-warning" type="button" title="Salida">
                                            <i class="fas fa-car-alt"></i>
                                        </a>
                                    </td>
                                    
                                    <td>

                                        <a href="#" class="btn btn-danger" type="button" title="Eliminar" disabled>
                                            <i class="far fa-trash-alt"></i>
                                        </a>
                                    </td>
                                </tr>
                            @endforeach
                         @else
                            <tr>
                                <td colspan="11" align="center"> 
                                    No se encontraron registros
                                </td>
                            </tr>
                        @endif
                    </tbody>
                    <tfoot>
                        <tr>
                            <th>#</th>
                            <th>Unidad</th>
                            <th>VIN</th>
                            <th>Ingresado por:</th>
                            <th>N° Remisión</th>
                            <th>Folio Pedido</th>
                            <th>-</th>
                            <th>-</th>
                            <th>-</th>
                            <th>-</th>
                        </tr>
                    </tfoot>
                </table>
            </div>
        </div>
    </div>
</div>

@endsection

@section('scripts')
	<script>
	</script>
@endsection