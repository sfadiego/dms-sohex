@layout('tema_luna/layout')
@section('contenido')
<div class="container-fluid panel-body">
    <h1 class="mt-4"><?php echo isset($modulo) ? $modulo : "" ?></h1>
    <ol class="breadcrumb mb-4">
        <li class="breadcrumb-item"><?php echo isset($submodulo) ? $submodulo : "" ?></li>
        <li class="breadcrumb-item"><?php echo isset($titulo) ? $titulo : "" ?></li>
        <li class="breadcrumb-item active"><?php echo isset($subtitulo) ? $subtitulo : "" ?></li>
    </ol>

    <?php //utils::pre($data_preventa); ?>
    <h3 class="mt-3 mb-4">Datos de la preventa</h3>
    <div class="row">
        <div class="col-md-4">
            <input type="hidden" name="preventa_id" id="preventa_id" value="{{ isset($data_preventa) ? $data_preventa->id :'' }}">
            <?php renderSelectArray('marca_id', 'Marca', $catalogo_marcas, 'id', 'nombre', isset($data_preventa) ? $data_preventa->marca_id : null, true) ?>
        </div>
        <div class="col-md-4">
            <?php renderSelectArray('modelo_id', 'Modelo', $catalogo_modelos, 'id', 'nombre', isset($data_preventa) ? $data_preventa->modelo_id : null, true) ?>
        </div>
        <div class="col-md-4">
            <?php renderSelectArray('color_id', 'Color', $catalogo_color, 'id', 'nombre',  isset($data_preventa) ? $data_preventa->color_id : null, true) ?>
            <input type="hidden" name="id_estatus" id="id_estatus" value="1">
            <input type="hidden" name="moneda" id="moneda" value="MXN">
        </div>
        <div class="col-md-4">
            <?php renderInputText("text", "tasa_interes", "Tasa de interes",  isset($data_preventa) ? $data_preventa->tasa_interes : 0, true); ?>
        </div>
        <div class="col-md-4">
            <div class="form-group">
                <label for="id_tipo_auto">Tipo de auto</label>
                <select disabled class="form-control" name="id_tipo_auto" id="id_tipo_auto">
                    <option value=""> Seleccionar ..</option>
                    <option value="1" selected>Nuevo</option>
                </select>
                <div id="id_tipo_auto_error" class="invalid-feedback"></div>
            </div>
        </div>
        <div class="col-md-4">
            <?php renderSelectArray('anio_id', 'Año', $catalogo_anio, 'id', 'nombre', isset($data_preventa) ? $data_preventa->id_anio : null, true) ?>
        </div>
    </div>
    <div class="row">
        <div class="col-md-4">
            <?php renderInputText("text", "combustible", "Combustible",  isset($data_preventa) ? $data_preventa->combustible : null, true); ?>
        </div>
        <div class="col-md-4">
            <?php renderInputText("text", "motor", "Motor",  isset($data_preventa) ? $data_preventa->motor : '', true); ?>
        </div>
        <div class="col-md-4">
            <?php renderInputText("text", "transmision", "Transmision",  isset($data_preventa) ? $data_preventa->transmision : '', true); ?>
        </div>
        <div class="col-md-4">
            <?php renderInputText("text", "n_cilindros", "Cilindros",  isset($data_preventa) ? $data_preventa->numero_cilindros : '', true); ?>
            <input type="hidden" name="precio_venta" value="{{ isset($data_preventa) ? $data_preventa->precio_venta : '' }}" id="precio_venta">
        </div>
        <div class="col-md-8">
            <?php renderInputTextArea("unidad_descripcion", "Descripcion de unidad",  isset($data_preventa) ? $data_preventa->unidad_descripcion : ''); ?>
        </div>
    </div>

    <h3 class="mt-3 mb-4">Datos del cliente</h3>
    <div class="row">
        <div class="col-md-3">
            <?php renderInputText("text", "nombre_cliente", "Nombre de cliente",  isset($data_preventa) ? $data_preventa->nombre_cliente : null, true); ?>
        </div>
        <div class="col-md-3">
            <?php renderInputText("text", "cliente_apellido_paterno", "Apellido materno",  isset($data_preventa) ? $data_preventa->cliente_apellido_paterno : null, true); ?>
        </div>
        <div class="col-md-3">
            <?php renderInputText("text", "cliente_apellido_materno", "Apellido paterno",  isset($data_preventa) ? $data_preventa->cliente_apellido_materno : null, true); ?>
        </div>
    </div>

    <h3 class="mt-3 mb-4">Datos del vendedor</h3>
    <div class="row">
        <div class="col-md-6">
            <?php renderInputText("text", "vendedor", "Vendedor",  isset($data_preventa) ? $data_preventa->nombre_vendedor . ' ' . $data_preventa->apellido_paterno_vendedor : $nombre_usuario, isset($data_preventa) ? true : false); ?>
            <input type="hidden" id="vendedor_id" name="vendedor_id" value="{{ isset($data_preventa) ? $data_preventa->id_vendedor : $id_vendedor }}">

        </div>
        <div class="col-md-6">
            <div class="form-group">
                <label for="fecha_alta">Fecha de alta:</label>
                <input type="date" class="form-control" min="{{ date('Y-m-d') }}" value="{{ isset($data->created_at) ? $data->created_at :  date('Y-m-d') }}" id="orden_fecha" name="orden_fecha" placeholder="">
                <div id="fecha_alta_error" class="invalid-feedback"></div>
            </div>
        </div>
    </div>
    
    <h3 class="mt-4 mb-4">Equipo opcional</h3>
    <div class="row">
        <div class="col-md-12">
            <table class="table table-bordered" id="tbl_carrito" width="100%" cellspacing="0">
                <thead>
                    <tr>
                        <th>#</th>
                        <th>No identificación</th>
                        <th>Descripcion</th>
                        <th>Total</th>
                        <th>Precio</th>
                        <th>Cantidad</th>
                        <th>Unidad</th>
                        <th>-</th>
                    </tr>
                </thead>
                <tfoot>
                    <tr>
                        <th>#</th>
                        <th>No identificación</th>
                        <th>Descripcion</th>
                        <th>Total</th>
                        <th>Valor unitario</th>
                        <th>Cantidad</th>
                        <th>Unidad</th>
                        <th>-</th>
                    </tr>
                </tfoot>
            </table>
        </div>
    </div>
	<h3 class="mt-4 mb-4">Productos disponibles</h3>
	
    <div class="row">
        <div class="col-md-12">
            <div class="table-responsive">
                <table class="table table-bordered" id="tbl_productos" width="100%" cellspacing="0">
                    <thead>
                        <tr>
                            <th>#</th>
                            <th>No identificación</th>
                            <th>Descripcion</th>
                            <th>Valor unitario</th>
                            <th>Existencia <br />total producto</th>
                            <th>Existencia <br />almacen principal</th>
                            <th>Existencia <br />almacen secundario</th>
                            <th>-</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($listado_productos as $item)
                        <tr>
                            <td>{{ $item->id }}</td>
                            <td>{{ $item->no_identificacion }}</td>
                            <td>{{ $item->descripcion }}</td>
                            <td>
                                $ {{$item->valor_unitario}}
                            </td>
                            <td>{{ $item->cantidad_actual }}</td>
                            <td>{{ $item->cantidad_almacen_primario }}</td>
                            <td>{{ $item->cantidad_almacen_secundario }}</td>
                            <td>
                                @if ($item->cantidad_almacen_primario == 0)
                                - -
                                @else
                                <button onclick="modalDetalle(this)" data-toggle="modal" 
                                    data-product_name='{{ $item->descripcion}}' 
                                    data-product_id='{{ $item->id}}' 
                                    data-no_identificacion='{{ $item->no_identificacion}}' 
                                    data-cantidad_almacen_primario='{{ $item->cantidad_almacen_primario}}' 
                                    data-valor_unitario='{{ $item->valor_unitario}}' class="btn btn-success">
                                    <i class="fas fa-shopping-cart"></i>
                                </button>
                                @endif
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                    <tfoot>
                        <tr>
                        <tr>
                            <th>#</th>
                            <th>No identificación</th>
                            <th>Descripcion</th>
                            <th>Valor unitario</th>
                            <th>Existencia <br />total producto</th>
                            <th>Existencia <br />almacen principal</th>
                            <th>Existencia <br />almacen secundario</th>
                            <th>-</th>
                        </tr>
                        </tr>
                    </tfoot>
                </table>
            </div>
        </div>
    </div>
</div>
@endsection
@section('scripts')
<script>
    var tabla_carrito = $('#tbl_carrito').DataTable({
		language: {
			url: PATH_LANGUAGE
		},
		"ajax": {
			url: base_url + "autos/equipoAdicional/ajax_detalle_equipo_carrito",
			type: 'POST',
			data: {
				preventa_id: function () {
					return $('#preventa_id').val()
				}
			}
		},
		columns: [{
				'data': 'item_id'
			},
			{
				'data': function (data) {
					return utils.isDefined(data.no_identificacion) && data.no_identificacion ? data
						.no_identificacion : null
				}
			},
			{
				'data': function (data) {
					return utils.isDefined(data.descripcion) && data.descripcion ? data
						.descripcion : null
				}
			},
			{
				'data': function (data) {
					return utils.isDefined(data.total) ? "$ " + data.total : null
                    // return data.item_id
				}
			},
			{
				'data': function (data) {
					return utils.isDefined(data.valor_unitario) ? "$ " + data.valor_unitario : null
				}
			},
			{
				'data': function (data) {
					return utils.isDefined(data.cantidad) && data.cantidad ? data.cantidad : null
				}
			},
			{
				'data': function (data) {
					return utils.isDefined(data.unidad) && data.unidad ? data.unidad : null
				}
			},
			{
				'data': function (data) {
					// if (utils.isDefined(estatus_venta_id) && estatus_venta_id != 1) {
						return '--';
					// } else {
					// 	return "<button type='button' class='btn-borrar btn btn-primary' data-id=" + data
					// 		.id + "><i class='fas fa-trash'></i></button>";
					// }
                    // return data.item_id
				}
			}
		]
	});

    function modalDetalle(_this) {
        $("#producto_id").val($(_this).data('product_id'));
        $("#valor_unitario").val($(_this).data('valor_unitario'));
        $("#no_identificacion").val($(_this).data('no_identificacion'));
        // $("#cantidad_almacen_primario").val($(_this).data('cantidad_almacen_primario'));
        var producto_name = $(_this).data('product_name');
        $("#title_modal").text(producto_name);
        $("#modal-producto-detalle").modal('show');
    }

    function agregarproducto(id) {
		toastr.info("Añadiendo producto al carro de compras");
		$.isLoading({
			text: "Añadiendo producto al carro de compras...."
		});
        
		let folio_id = $('#folio_id').val();
		let producto_id = $("#producto_id").val();
		let cantidad = $("#cantidad").val();
		let venta_auto_id = $("#preventa_id").val();
        let precio = $("#valor_unitario").val();
		ajax.post(`api/autos/equipo-opcional`, {
            producto_id,cantidad,venta_auto_id,precio
        }, function (response, headers) {
			if (headers.status == 201 || headers.status == 200) {
				$("#modal-producto-detalle").modal('hide');
				tabla_carrito.ajax.reload();
		// 		$.ajax({
		// 			type: "GET",
		// 			url: base_url + "refacciones/salidas/ajax_calcular_venta_total/" + $('#folio_id')
		// 			.val(),
		// 			dataType: "json",
		// 			success: function (response) {
		// 				let venta = parseInt(response.venta_total);
		// 				let titulo = "Producto añadido al carro de compras correctamente"
		// 				$('#venta_total').val(venta);
		// 				utils.displayWarningDialog(titulo, 'success', function (result) {
		// 					window.location.reload();
		// 				});
		// 			}, complete: function() {
						$.isLoading("hide");
		// 			} 
		// 		});
			} else {
				$.isLoading("hide");
			}
		})
	}
</script>
@endsection

@section('modal')
<div class="modal fade" id="modal-producto-detalle" data-toggle="modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="title_modal"></h5>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-12">
                        <?php echo renderInputText("number", "cantidad", "Cantidad", 1); ?>
                        <input type="hidden" name="producto_id" id="producto_id">
                        <input type="hidden" name="valor_unitario" id="valor_unitario">
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
                <button onclick="agregarproducto()" id="btn-modal-agregar" type="button" class="btn btn-primary"><i class="fas fa-shopping-cart"></i> Agregar</button>
            </div>
        </div>
    </div>
</div>
@endsection