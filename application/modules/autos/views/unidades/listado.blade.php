@layout('tema_luna/layout')
@section('contenido')
<div class="container-fluid panel-body">
    <h1 class="mt-4"><?php echo isset($modulo) ? $modulo : "" ?></h1>
    <ol class="breadcrumb mb-4">
        <li class="breadcrumb-item"><?php echo isset($submodulo) ? $submodulo : "" ?></li>
        <li class="breadcrumb-item"><?php echo isset($titulo) ? $titulo : "" ?></li>
        <li class="breadcrumb-item active"><?php echo isset($subtitulo) ? $subtitulo : "" ?></li>
    </ol>

    <div class="row mb-3">
        <div class="col-md-10"></div>
        <div class="col-md-2 text-rigth">
            <a class="btn btn-primary" href="<?php echo base_url('autos/unidades/alta') ?>">
                Agregar nueva unidad
            </a>
        </div>
    </div>

    <div class="row">
        <div class="col-md-12">
            <div class="table-responsive">
                <table class="table table-bordered" id="tbl_cat_unidades" width="100%" cellspacing="0">
                    <thead>
                        <tr>
                            <th>#</th>
                            <th>Clave</th>
                            <th>Unidad</th>
                            <th>-</th>
                        </tr>
                    </thead>
                    <tbody>
                    </tbody>
                    <tfoot>
                        <tr>
                            <th>#</th>
                            <th>Clave</th>
                            <th>Unidad</th>
                            <th>-</th>
                        </tr>
                    </tfoot>
                </table>
            </div>
        </div>
    </div>
</div>

@endsection

@section('scripts')
<script type="text/javascript">
    var tabla_unidades = $('#tbl_cat_unidades').DataTable({
        ajax: base_url + "autos/unidades/ajax_catalogo_autos",
        columns: [{
                'data': function(data) {
                    return data.id
                }
            },
            {
                'data': function(data) {
                    return data.clave
                }
            },
            {
                'data': function(data) {
                    return data.nombre
                }
            },
            {
                'data': function(data) {
                    return "<a href='" + site_url + '/autos/unidades/editar/' + data.id + "' class='btn btn-primary'><i class='fas fa-pen'></i></a>";
                }
            }
        ]
    });
</script>
@endsection