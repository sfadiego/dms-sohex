@layout('tema_luna/layout')
@section('contenido')
<div class="container-fluid panel-body">
    <h1 class="mt-4"><?php echo isset($modulo) ? $modulo : "" ?></h1>
    <ol class="breadcrumb mb-4">
        <li class="breadcrumb-item"><?php echo isset($submodulo) ? $submodulo : "" ?></li>
        <li class="breadcrumb-item"><?php echo isset($titulo) ? $titulo : "" ?></li>
        <li class="breadcrumb-item active"><?php echo isset($subtitulo) ? $subtitulo : "" ?></li>
    </ol>

    <div class="row">
        <div class="col-md-12">
            <form id="form-unidades" data-id="{{ isset($detalle->id) ? $detalle->id : '' }}" method="post">
                <div class="row">
                    <div class="col-md-3">
                        <?php renderInputText("text", "clave", "clave",  isset($detalle->clave) ? $detalle->clave : '', false); ?>
                    </div>
                    <div class="col-md-9">
                        <?php renderInputText("text", "nombre", "Descripción",  isset($detalle->nombre) ? $detalle->nombre : '', false); ?>
                        <input type="hidden" name="categoria_id" value="1" id="categoria_id">
                        <input type="hidden" name="tiempo_lavado" value="0" id="tiempo_lavado">
                        
                    </div>
                </div>
                
                <div class="row">
                    <div class="col-md-12">
                        <br>
                        @if (isset($detalle->id))
                        <button id="btn-actualizar" class="btn btn-success" type="button">Actualizar</button>
                        @else
                        <button id="btn-guardar" class="btn btn-success" type="button">Guardar</button>
                        @endif
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
@endsection

@section('scripts')
<script type="">
    //Para guardar registro por primera vez
        $("#btn-guardar").on('click', function() {
            $(".invalid-feedback").html("");
            ajax.post('api/catalogo-autos', form(), function(response, headers) {

                if (headers.status == 400) {
                    return ajax.showValidations(headers);
                }
                
                if (headers.status == 200 || headers.status == 201) {
					utils.displayWarningDialog('Unidad registrada..', "success", function(data) {
                        return window.location.href = base_url + 'autos/Unidades/index';
                    })
				}
            })
        });

        //Para actualizar un registro
        $("#btn-actualizar").on('click', function() {
            $(".invalid-feedback").html("");
            var id = $("#form-unidades").data('id');

            ajax.put('api/catalogo-autos/'+id, form(), function(response, headers) {
                if (headers.status == 400) {
                    return ajax.showValidations(headers);
                }
                
                utils.displayWarningDialog("Unidad actualizada con éxito", "success", function(data) {
                    return window.location.href = base_url + 'autos/Unidades/index';
                })
            })

        });

        let form = function() {
             return  {
                nombre: document.getElementById("nombre").value,
                clave: document.getElementById("clave").value,
                categoria_id: document.getElementById("categoria_id").value,
                tiempo_lavado: document.getElementById("tiempo_lavado").value
            };
        }
    </script>
@endsection