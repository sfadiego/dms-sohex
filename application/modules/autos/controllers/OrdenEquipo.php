<?php

defined('BASEPATH') or exit('No direct script access allowed');

class OrdenEquipo extends MX_Controller
{
    public function __construct()
    {
        parent::__construct();
        date_default_timezone_set('America/Mexico_City');
        //300 segundos  = 5 minutos
        ini_set('max_execution_time',300);

       
        
    }
    
    public function index()
    {
        $this->load->library('curl');
        $this->load->helper('general');

        $dataFromApi = $this->curl->curlGet('api/ordenequipo/');
        $dataRegistro = [];
        $dataRegistro = procesarResponseApiJsonToArray($dataFromApi);
        $data['data'] = $dataRegistro;

        $data['modulo'] = "Autos";
        $data['submodulo'] = "Inventario";
        $data['titulo'] = "Orden de equipo";
        $data['subtitulo'] = "Listado";        
        
        $this->blade->render('ordenEquipo/listado', $data);
    }

    public function agregarOrden($id='')
    {
        $data['data'] = [];
        
        $data['modulo'] = "Autos";
        $data['submodulo'] = "Inventario";
        $data['titulo'] = "Orden de equipo";
        $data['subtitulo'] = "Registro";

        $this->blade->render('ordenEquipo/alta', $data);
    }

    public function editar($id='')
    {
        $data['data'] = [];
        
        $data['modulo'] = "Autos";
        $data['submodulo'] = "Inventario";
        $data['titulo'] = "Orden de equipo";
        $data['subtitulo'] = "Edición";

        $this->blade->render('ordenEquipo/alta', $data);
    }
}
