<?php

defined('BASEPATH') or exit('No direct script access allowed');

class Reportes extends MX_Controller
{
    public function __construct()
    {
        parent::__construct();
        date_default_timezone_set('America/Mexico_City');
        //300 segundos  = 5 minutos
        ini_set('max_execution_time',300);

       
        
    }
    
    public function index()
    {
        $this->load->library('curl');
        $this->load->helper('general');
    }

    public function reporteventas()
    {
        $this->load->library('curl');
        $this->load->helper('general');
        
        $data['modulo'] = "Autos";
        $data['submodulo'] = "Reportes";
        $data['titulo'] = "Ventas diarias";
        $data['subtitulo'] = "Consulta";

        $data['menu'] = "menu_reportes_ventas";
        $this->blade->render('reportes/generarReporte', $data);
    }

    public function reportepolizasventas()
    {
        $this->load->library('curl');
        $this->load->helper('general');

        $data['modulo'] = "Autos";
        $data['submodulo'] = "Reportes";
        $data['titulo'] = "Pólizas de ventas";
        $data['subtitulo'] = "Consulta";

        $data['menu'] = "menu_reportes_poliza_ventas";
        $this->blade->render('reportes/generarReporte', $data);
    }

    public function reportepolizacompras()
    {
        $this->load->library('curl');
        $this->load->helper('general');

        $data['modulo'] = "Autos";
        $data['submodulo'] = "Reportes";
        $data['titulo'] = "Póliza de compras";
        $data['subtitulo'] = "Consulta";

        $data['menu'] = "menu_reportes_poliza_compras";
        $this->blade->render('reportes/generarReporte', $data);
    }

    public function reportecomisiones()
    {
        $this->load->library('curl');
        $this->load->helper('general');

        $data['modulo'] = "Autos";
        $data['submodulo'] = "Reportes";
        $data['titulo'] = "Comisiones";
        $data['subtitulo'] = "Consulta";

        $data['menu'] = "menu_reportes_comisiones";
        $this->blade->render('reportes/generarReporte', $data);
    }

    public function reporteexistencias()
    {
        $this->load->library('curl');
        $this->load->helper('general');

        $data['modulo'] = "Autos";
        $data['submodulo'] = "Reportes";
        $data['titulo'] = "Existencias";
        $data['subtitulo'] = "Consulta";

        $data['menu'] = "menu_reportes_existencias";
        $this->blade->render('reportes/generarReporte', $data);
    }

    public function reporteutilidad()
    {
        $this->load->library('curl');
        $this->load->helper('general');

        $data['modulo'] = "Autos";
        $data['submodulo'] = "Reportes";
        $data['titulo'] = "Utilidad";
        $data['subtitulo'] = "Consulta";

        $data['menu'] = "menu_reportes_utilidad";
        $this->blade->render('reportes/generarReporte', $data);
    }

    public function generarReporte($parametro = '',$fecha_1 = '',$fecha_2 = '',$campo = '')
    {
        $data['data'] = [];
        $data['titulo'] = "Unidades";
        $this->blade->render('reportes/alta', $data);
    }
}
