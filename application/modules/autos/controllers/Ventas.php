<?php

defined('BASEPATH') or exit('No direct script access allowed');

class Ventas extends MX_Controller
{
    public function __construct()
    {
        parent::__construct();
        date_default_timezone_set('America/Mexico_City');
        //300 segundos  = 5 minutos
        ini_set('max_execution_time', 300);
        $this->load->library('curl');
        $this->load->helper('general');
    }

    public function index()
    {
        $data['modulo'] = "Autos";
        $data['submodulo'] = "Venta";
        $data['titulo'] = "Pre pedidos";
        $data['subtitulo'] = "listado";

        $this->blade->render('ventas/listado', $data);
    }

    public function unidades()
    {

        $api_response = $this->curl->curlGet('api/unidades/resumen-contabilidad');
        $data_response = procesarResponseApiJsonToArray($api_response);
        $data['total_unidades'] = isset($data_response) ? $data_response[0] : [];

        $dataFromApi = $this->curl->curlGet('api/unidades');
        $dataregistros = procesarResponseApiJsonToArray($dataFromApi);
        $data['data'] = isset($dataregistros) ? $dataregistros : [];

        $data['modulo'] = "Autos";
        $data['submodulo'] = "Ventas";
        $data['titulo'] = "Unidades";
        $data['subtitulo'] = "Unidades disponibles";

        $this->blade->render('ventas/catalogo_unidades', $data);
    }

    public function ajax_preventas()
    {
        $query = [];
        if ($this->input->post('fecha_inicio')) {
            $query['fecha_inicio'] = $this->input->post('fecha_inicio');
        }
        if ($this->input->post('fecha_fin')) {
            $query['fecha_fin'] = $this->input->post('fecha_fin');
        }
        
        $responseData = $this->curl->curlGet('api/autos/pre-pedidos?' . http_build_query($query));
        $response = procesarResponseApiJsonToArray($responseData);
        $data['data'] = isset($response) ? $response : [];
        echo json_encode($data);
    }

    public function ajax_preventaEquipoOpcional()
    {
        $query = [];
        if ($this->input->post('fecha_inicio')) {
            $query['fecha_inicio'] = $this->input->post('fecha_inicio');
        }
        if ($this->input->post('fecha_fin')) {
            $query['fecha_fin'] = $this->input->post('fecha_fin');
        }
        
        $responseData = $this->curl->curlGet('api/autos/pre-pedidos/preventa-equipo-opcional?' . http_build_query($query));
        $response = procesarResponseApiJsonToArray($responseData);
        $data['data'] = isset($response) ? $response : [];
        echo json_encode($data);
    }

    public function detalle_venta($id_venta = '')
    {
        $catalogos = $this->returnCatalogosForVentas();
        // utils::pre($catalogos);
        $data['cat_cfdi'] = $catalogos['cat_cfdi'];
        $data['nombre_usuario'] =  $catalogos['nombre_usuario'];
        $data['id_vendedor'] = $catalogos['id_vendedor'];
        $data['catalogo_unidades'] = $catalogos['catalogo_unidades'];
        $data['catalogo_modelos'] = $catalogos['catalogo_modelos'];
        $data['catalogo_marcas'] = $catalogos['catalogo_marcas'];
        $data['catalogo_anio'] = $catalogos['catalogo_anio'];
        $data['catalogo_color'] = $catalogos['catalogo_color'];
        $data['catalogo_clientes'] = $catalogos['catalogo_clientes'];
        $data['plazo_credito'] = $catalogos['plazo_credito'];
        $data['tipo_forma_pago'] = $catalogos['tipo_forma_pago'];
        $data['tipo_pago'] = $catalogos['tipo_pago'];

        if ($id_venta != '') {

            $data['subtitulo'] = "detalle";
            $detalle_venta = $this->curl->curlGet('api/venta-unidades/' . $id_venta);
            $validar = procesarResponseApiJsonToArray($detalle_venta);
            $data['data_venta'] = count($validar) > 0 ? $validar[0] : [];
        }

        $data['modulo'] = "Autos";
        $data['submodulo'] = "Venta";
        $data['titulo'] = "Pre pedidos";
        $this->blade->render('ventas/detalle_venta', $data);
    }

    public function preventa_formulario($id_unidad = '')
    {

        $catalogos = $this->returnCatalogosForVentas();
        // utils::pre($catalogos);
        $data['cat_cfdi'] = $catalogos['cat_cfdi'];
        $data['nombre_usuario'] =  $catalogos['nombre_usuario'];
        $data['id_vendedor'] = $catalogos['id_vendedor'];
        $data['catalogo_unidades'] = $catalogos['catalogo_unidades'];
        $data['catalogo_modelos'] = $catalogos['catalogo_modelos'];
        $data['catalogo_marcas'] = $catalogos['catalogo_marcas'];
        $data['catalogo_anio'] = $catalogos['catalogo_anio'];
        $data['catalogo_color'] = $catalogos['catalogo_color'];
        $data['catalogo_clientes'] = $catalogos['catalogo_clientes'];
        $data['plazo_credito'] = $catalogos['plazo_credito'];
        $data['tipo_forma_pago'] = $catalogos['tipo_forma_pago'];
        $data['tipo_pago'] = $catalogos['tipo_pago'];

        $data['id_tipo_auto'] = 1; //seminuevo

        if ($id_unidad != '') {
            // $detalle_venta = $this->curl->curlGet('api/venta-unidades/' . $id);
            // $validar = procesarResponseApiJsonToArray($detalle_venta);
            // $data['data_venta'] = count($validar) > 0 ? $validar[0] : [];
            $detalle_unidad = $this->curl->curlGet('api/unidades/' . $id_unidad);
            $unidad = procesarResponseApiJsonToArray($detalle_unidad);
            $data['detalle_unidad'] = count($unidad) > 0 ? $unidad[0] : [];
        }

        $data['modulo'] = "Autos";
        $data['submodulo'] = "Ventas";
        $data['titulo'] = "Pre-venta";
        $data['subtitulo'] = "Registro";
        $this->blade->render('ventas/preventa_alta', $data);
    }

    public function returnCatalogosForVentas()
    {
        $cfdi = $this->curl->curlGet('api/cfdi');
        $unidades = $this->curl->curlGet('api/unidades');
        $modelos = $this->curl->curlGet('api/catalogo-autos');
        $marcas = $this->curl->curlGet('api/catalogo-marcas');
        $anio = $this->curl->curlGet('api/catalogo-anio');
        $color = $this->curl->curlGet('api/catalogo-colores');
        $clientes = $this->curl->curlGet('api/clientes');
        $plazo_credito = $this->curl->curlGet('api/catalogo-plazo-credito');
        $tipo_forma_pago = $this->curl->curlGet('api/catalogo-tipo-forma-pago');
        $tipo_pago = $this->curl->curlGet('api/tipo-pago');

        $data['nombre_usuario'] = $this->session->userdata('nombre') . " " . $this->session->userdata('apellido_paterno') . " " . $this->session->userdata('apellido_materno');
        $data['id_vendedor'] = $this->session->userdata('id');

        $data['cat_cfdi'] = procesarResponseApiJsonToArray($cfdi);
        $data['catalogo_unidades'] = procesarResponseApiJsonToArray($unidades);
        $data['catalogo_modelos'] = procesarResponseApiJsonToArray($modelos);
        $data['catalogo_marcas'] = procesarResponseApiJsonToArray($marcas);
        $data['catalogo_anio'] = procesarResponseApiJsonToArray($anio);
        $data['catalogo_color'] = procesarResponseApiJsonToArray($color);
        $data['catalogo_clientes'] = procesarResponseApiJsonToArray($clientes);
        $data['plazo_credito'] = procesarResponseApiJsonToArray($plazo_credito);;
        $data['tipo_forma_pago'] = procesarResponseApiJsonToArray($tipo_forma_pago);
        $data['tipo_pago'] = procesarResponseApiJsonToArray($tipo_pago);

        return $data;
    }

    public function ajax_tabla_amortizacion()
    {
        if ($this->input->post('precio_venta') == '') {
            $data['data'] = [];
            echo json_encode($data);
            die();
        }

        // utils::pre($this->input->post());
        $precio_venta = $this->input->post('precio_venta');
        $plazo = $this->input->post('plazo');
        $enganche = $this->input->post('enganche');
        $detalle = [];
        if ($plazo > 0) {
            $total = $precio_venta - $enganche; // number_format($precio_venta, 2, '.', '');
            $mensualidades = number_format($total / $plazo, 2, '.', '');
            // $residuo = $precio_venta - ($mensualidades * $i);
            for ($i = 1; $i <=  $plazo; $i++) {
                $monto_total_actual = $mensualidades * $i - $total;

                $item = new \stdClass;
                $item->id = $i;
                $item->plazo_credito = "Mensualidad " . $i;
                $item->saldo_insoluto = "$ " . number_format(abs($monto_total_actual), 2, '.', '');
                $item->pago_periodo = "$ " . $mensualidades;
                array_push($detalle, $item);
            }
            $data['data'] = $detalle;
            echo json_encode($data);
            die();
        }

        $detalle['plazo_credito'] = "Contado ";
        $detalle['saldo_insoluto'] = "$ " . 0;
        $detalle['id'] =  1;
        $detalle['pago_periodo'] = "$ " . $precio_venta;
        $data['data'] = [$detalle];
        echo json_encode($data);
        die();
    }

    public function entrega_unidad($id = '')
    {
        $data['data'] = [];
        $data['modulo'] = "Autos";
        $data['submodulo'] = "Ventas";
        $data['titulo'] = "Entrega de Unidades";
        $data['subtitulo'] = "Entrega";
        
        $catalogos = $this->returnCatalogosForVentas();
        // utils::pre($catalogos);
        $data['cat_cfdi'] = $catalogos['cat_cfdi'];
        $data['nombre_usuario'] =  $catalogos['nombre_usuario'];
        $data['id_vendedor'] = $catalogos['id_vendedor'];
        $data['catalogo_unidades'] = $catalogos['catalogo_unidades'];
        $data['catalogo_modelos'] = $catalogos['catalogo_modelos'];
        $data['catalogo_marcas'] = $catalogos['catalogo_marcas'];
        $data['catalogo_anio'] = $catalogos['catalogo_anio'];
        $data['catalogo_color'] = $catalogos['catalogo_color'];
        $data['catalogo_clientes'] = $catalogos['catalogo_clientes'];
        $data['plazo_credito'] = $catalogos['plazo_credito'];
        $data['tipo_forma_pago'] = $catalogos['tipo_forma_pago'];
        $data['tipo_pago'] = $catalogos['tipo_pago'];
        
        $detalle_venta = $this->curl->curlGet('api/venta-unidades/' . $id);
        $data['venta'] = procesarResponseApiJsonToArray($detalle_venta);

        $this->blade->render('ventas/preventa_alta', $data);
    }
}
