<?php

defined('BASEPATH') or exit('No direct script access allowed');

class Consultas extends MX_Controller
{
    public function __construct()
    {
        parent::__construct();
        date_default_timezone_set('America/Mexico_City');
        //300 segundos  = 5 minutos
        ini_set('max_execution_time',300);

       
        
    }

    public function index()
    {
        
    }

    public function pedidos()
    {
        $data['data'] = [];

        $data['modulo'] = "Autos";
        $data['submodulo'] = "Ventas";
        $data['titulo'] = "Pedidos";
        $data['subtitulo'] = "Listado";

        $this->blade->render('consultas/listado', $data);
    }

    public function compras()
    {
        $data['data'] = [];

        $data['modulo'] = "Autos";
        $data['submodulo'] = "Ventas";
        $data['titulo'] = "Pedidos";
        $data['subtitulo'] = "Listado";

        $this->blade->render('consultas/listado', $data);
    }
}
