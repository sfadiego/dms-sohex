<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
class Faltantes extends MX_Controller
{
    public function __construct()
    {
        parent::__construct();

        parent::__construct();
        $this->load->library(array('session'));
        $this->load->helper(array('form', 'html', 'url', 'date', 'general'));
        $this->load->library('curl');
        date_default_timezone_set('America/Mexico_City');
    }
    public function index()
    {
        echo 1;
    }
    public function listado()
    {
        $data['info'] = procesarResponseApiJsonToArray($this->curl->curlPost('api/logistica/faltantes/get-all', []));
        $data['titulo'] = "Listado Faltantes";
        $data['bitacora'] = "inicial";
        $this->blade->render('faltantes/listado', $data);
    }
    public function agregar($id = 0)
    {
        if ($id == 0) {
            $info = new Stdclass();
        } else {
            $info = procesarResponseApiJsonToArray($this->curl->curlGet('api/logistica/faltantes/' . $id));
        }

        $data['folio'] = form_input('folio', set_value('folio', exist_obj($info, 'folio')), 'class="form-control" id="folio"');
        $unidades = procesarResponseApiJsonToArray($this->curl->curlGet('api/catalogo-autos'));
        $data['id_unidad'] = form_dropdown('id_unidad', array_combos($unidades, 'id', 'nombre', TRUE), set_value('id_unidad', exist_obj($info, 'id_unidad')), 'class="form-control busqueda" id="id_unidad"');
        $colores = procesarResponseApiJsonToArray($this->curl->curlGet('api/catalogo-colores'));
        $data['id_color'] = form_dropdown('id_color', array_combos($colores, 'id', 'nombre', TRUE), set_value('id_color', exist_obj($info, 'id_color')), 'class="form-control busqueda" id="id_color"');
        $data['serie'] = form_input('serie', set_value('serie', exist_obj($info, 'serie')), 'class="form-control" id="serie" maxlength="17"');
        $data['observaciones'] = form_textarea('observaciones', set_value('observaciones', exist_obj($info, 'observaciones')), 'class="form-control" id="observaciones"');
        $data['solicitud'] = form_input('solicitud', set_value('solicitud', exist_obj($info, 'solicitud')), 'class="form-control" id="solicitud"', 'date');
        $data['recibos'] = form_input('recibos', set_value('recibos', exist_obj($info, 'recibos')), 'class="form-control" id="recibos"');
        $data['fecha_entrega'] = form_input('fecha_entrega', set_value('fecha_entrega', exist_obj($info, 'fecha_entrega')), 'class="form-control" id="fecha_entrega"', 'date');
        $data['id'] = $id;
        $data['titulo'] = 'Faltantes';
        $this->blade->render('faltantes/agregar', $data);
    }
}
