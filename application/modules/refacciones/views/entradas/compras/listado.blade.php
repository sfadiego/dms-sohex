@layout('tema_luna/layout')
@section('contenido')
<div class="container-fluid panel-body">
    <h1 class="mt-4">{{ isset($titulo) ? $titulo : '' }}</h1>
    <div class="row mt-4">
        <div class="col-md-4">
            <div class="form-group">
                <label for="">Folio:</label>
                <input type="text" name="folio" id="folio" class="form-control"/>
                <div id="folio_error" class="invalid-feedback"></div>
            </div>
        </div>
        <div class="col-md-4">
            <div class="form-group">
                <label for="">Proveedor:</label>
                <select class="form-control" id="proveedor_id" name="proveedor_id" style="width: 100%;">
                    <option value="">Selecionar ...</option>
                    @if(!empty($cat_proveedor))
                    @foreach ($cat_proveedor as $proveedor)
                        <option value="{{ $proveedor->id}}"> {{$proveedor->proveedor_numero}} - {{ $proveedor->proveedor_nombre }}</option>
                        @endforeach
                    @endif
                </select>
                <div id="proveedor_id_error" class="invalid-feedback"></div>
            </div>
        </div>
        <div class="col-md-4 mt-4">
            <button type="button" id="btn-limpiar" onclick="limpiarfiltro()" class="btn btn-primary">
                <i class="fa fa-eraser" aria-hidden="true"></i> Limpiar
            </button>
            <button type="button" id="btn-buscar" onclick="filtrar()" class="btn btn-primary">
                <i class="fa fa-search" aria-hidden="true"></i> Filtrar
            </button>
        </div>
    </div>
    <hr/>
    <div class="row">
        <div class="col-md-12">
            <div class="table-responsive">
                <table class="table table-bordered" id="tabla_compras" width="100%" cellspacing="0">
                </table>
            </div>
        </div>
    </div>
</div>
@endsection

@section('scripts')
<script src="{{ base_url('js/refacciones/entradas/listadocompras.js') }}"></script>
<script type="text/javascript">
        $(document).ready(function(){
            $("#menu_refacciones").addClass("show");
            $("#refacciones_entradas").addClass("show");
            $("#refacciones_entradas").addClass("active");
            $("#tables").addClass("show");
            $("#tables").addClass("active");
            $("#menu_entradas_listado_compras").addClass("active");
            $("#M02").addClass("active");
        });
</script>
@endsection