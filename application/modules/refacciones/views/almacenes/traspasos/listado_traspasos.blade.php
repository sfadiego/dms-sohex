@layout('tema_luna/layout')
@section('contenido')
<div class="container-fluid panel-body">
    <h1 class="mt-4">{{ isset($titulo) ? $titulo : '' }}</h1>
    <ol class="breadcrumb mb-4">
        <li class="breadcrumb-item active">Lista</li>
    </ol>
    <div class="row">
        <div class="col-md-12">
            <div class="table-responsive">
                <table class="table table-bordered" id="tbl_productos" width="100%" cellspacing="0">
                    <thead>
                        <tr>
                            <th>#</th>
                            <th>Producto</th>
                            <th>Almacen Origen</th>
                            <th>Almacen Destino</th>
                            <th>-</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($listado as $item)  
                            <tr>
                                <td>{{ $item->id }}</td>
                                <td>{{ $item->producto->descripcion }}</td>
                                <td>{{ $item->almacen_origen->nombre }}</td>
                                <td>{{ $item->almacen_origen->nombre }}</td>
                                <td>
                                    <a class="btn btn-success" href="{{ site_url('refacciones/almacenes/productoTraspaso/'.$item->id) }}"> 
                                        <i class="fas fa-map-marker-alt"></i> </a>
                                </td>
                            </tr>
                        @endforeach
                             
                    </tbody>
                    <tfoot>
                        <tr>
                            <th>#</th>
                            <th>Producto</th>
                            <th>Almacen Origen</th>
                            <th>Almacen Destino</th>
                            <th>-</th>
                        </tr>
                    </tfoot>
                </table>
            </div>
        </div>
    </div>
</div>
@endsection

@section('scripts')
<script>
    $(document).ready(function() {
        $('#tbl_productos').DataTable({})
    });

</script>
@endsection