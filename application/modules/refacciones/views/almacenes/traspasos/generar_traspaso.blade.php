@layout('tema_luna/layout')
@section('contenido')
<div class="container-fluid panel-body">
    <h1 class="mt-4">{{ isset($titulo) ? $titulo : '' }}</h1>
    <ol class="breadcrumb mb-4">
        <li class="breadcrumb-item ">{{ ucwords($this->uri->segment(1)) }}</li>
        <li class="breadcrumb-item">{{ ucwords($this->uri->segment(2)) }}</li>
        <li class="breadcrumb-item active">{{ $bread_active}}</li>
    </ol>



    <div class="row">
        <div class="col-md-12">
            <form id="frm-traspaso">
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="producto_id_sat">Almacen Actual:</label>
                            <select name="almacen_origen_id" class="form-control" id="almacen_origen_id">
                                <option value=""> Seleccionar almacen</option>
                                @foreach ($almacenes as $almacen)
                                @if (isset($producto->almacen_id) && $producto->almacen_id == $almacen->id)
                                <option selected value="{{ $almacen->id}}"> {{ $almacen->nombre}}</option>
                                @else
                                <option value="{{ $almacen->id}}"> {{ $almacen->nombre}}</option>
                                @endif

                                @endforeach
                            </select>
                            <div id="almacen_origen_id_error" class="invalid-feedback"></div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="producto_id_sat">Almacen Destino:</label>
                            <select name="almacen_destino_id" class="form-control" id="almacen_destino_id">
                                <option value=""> Seleccionar almacen</option>
                                @foreach ($almacenes as $almacen)
                                <option value="{{ $almacen->id}}"> {{ $almacen->nombre}}</option>
                                @endforeach
                            </select>
                            <div id="almacen_destino_id_error" class="invalid-feedback"></div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6 mt-4">
                        <button id="btn-traspaso" class="btn btn-primary col-md-6" type="button"> 
                            <i class="fa fa-list" aria-hidden="true"></i> Generar </button>
                            
                    </div>
                </div>
            </form>
        </div>
    </div>
    <br />
    <h1 class="mt-4">Traspasos realizados</h1>
    <table class="table table-bordered" id="tbl_traspaso" width="100%" cellspacing="0">
        <thead>
            <tr>
                <th>#</th>
                <th>Almacen origen</th>
                <th>Almacen enviado</th>
                <th>Fecha traspaso</th>
                <th>Estatus traspaso</th>
                <th class="text-center">Detalle</th>
                <th class="text-center">Devolucion</th>
            </tr>
        </thead>
        <tbody>
            @if(isset($traspasos) && count($traspasos) >= 1)
                @foreach ($traspasos as $item)   
                    <tr>
                        <td>{{ $item->id }}</td>
                        <td>{{ $item->almacen_origen->nombre }}</td>
                        <td>{{ $item->almacen_destino->nombre }}</td>
                        
                        <td>{{ date('d/m/yy', strtotime($item->created_at)) }}</td>
                        <td>{{ $item->estatus == '0' ? 'Pendiente' : 'Traspasado'  }}</td>
                        <td class="text-center">
                            <a class="btn btn-success" href="{{ site_url('refacciones/almacenes/productoTraspaso/'.$item->id) }}"> 
                                <i class="fas fa-list"></i> </a>
                        </td>
                        <td class="text-center">
                            <a class="btn btn-success" href="{{ site_url('refacciones/almacenes/productoTraspaso/'.$item->id) }}"> 
                                <i class="fas fa-money-check-alt"></i>
                            </a>
                        </td>
                    </tr>
                @endforeach
            @endif
        </tbody>
        <tfoot>
            <tr>
                <th>#</th>
                <th>Almacen origen</th>
                <th>Almacen enviado</th>
                <th>Fecha traspaso</th>
                <th>Estatus traspaso</th>
                <th class="text-center">Detalle</th>
                <th class="text-center">Devolucion</th>
            </tr>
        </tfoot>
    </table>
</div>



@endsection

@section('scripts')
<script>
   
    $("#tbl_traspaso").DataTable({});

        $("#btn-traspaso").on('click', function() {
            if($("#almacen_origen_id").val() == $("#almacen_destino_id").val()){
                let titulo = "Los almacenes no pueden ser los mismos!"
                utils.displayWarningDialog(titulo, 'warning', function(result) {});
                return false;
            }

            ajax.post('api/traspasos', {
                almacen_destino_id: document.getElementById("almacen_destino_id").value,
                almacen_origen_id: document.getElementById("almacen_origen_id").value
            }, function(response, header) {
                if (header.status == 201 || header.status == 200) {
                    let titulo = "Generando traspaso!"
                    utils.displayWarningDialog(titulo, 'success', function(result) {
                        return window.location.href = base_url + "refacciones/almacenes/productoTraspaso/" + response.id;
                    });
                }

                if (header.status == 500) {
                    let titulo = "Error traspasando producto!"
                    utils.displayWarningDialog(titulo, 'warning', function(result) {});
                }
            })
        })
</script>
<script type="text/javascript">
    $(document).ready(function() {
        $("#menu_refacciones").addClass("show");
        $("#refacciones_almacen").addClass("show");
        $("#refacciones_almacen").addClass("active");
        $("#refacciones_almacen_sub").addClass("show");
        $("#refacciones_almacen_sub").addClass("active");
        $("#almacen_traspaso").addClass("active");
    });
</script>
@endsection