@layout('tema_luna/layout')
@section('contenido')
<div class="container-fluid panel-body">
    <h1 class="mt-4">{{ isset($titulo) ? $titulo : '' }}</h1>
    <ol class="breadcrumb mb-4">
        <li class="breadcrumb-item active">Lista</li>
    </ol>
    <div class="row">
        <div class="col-md-12">
            <div class="table-responsive">
                <table class="table table-bordered" id="tbl_productos" width="100%" cellspacing="0">
                    <thead>
                        <tr>
                            <th>#</th>
                            <th>No. identificación</th>
                            <th>Descripcion</th>
                            <th>Precio publico</th>
                            <th>Existencia</th>
                            <th>Unidad</th>
                            <th>-</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($listado as $item)    
                            <tr>
                                <td>{{ $item->id }}</td>
                                <td>{{ $item->clave_prod_serv }}</td>
                                <td>{{ $item->descripcion }}</td>
                                <td>
                                    $ {{ porcentajeProducto($item->rel_precio->precio_publico,$item->precio_factura)}}
                                </td>
                                <td>{{ $item->cantidad }}</td>
                                <td>{{ $item->unidad }}</td>
                                <td>
                                    <a class="btn btn-success" href="{{ site_url('refacciones/almacenes/productoTraspaso/'.$item->id) }}"> 
                                        <i class="fas fa-map-marker-alt"></i> </a>
                                </td>
                            </tr>
                        @endforeach
                             
                    </tbody>
                    <tfoot>
                        <tr>
                            <th>#</th>
                            <th>No. identificación</th>
                            <th>Descripcion</th>
                            <th>Precio</th>
                            <th>Unidad</th>
                            <th>Unidad</th>
                            <th>-</th>
                        </tr>
                    </tfoot>
                </table>
            </div>
        </div>
    </div>
</div>
@endsection

@section('scripts')
<script>
    $(document).ready(function() {
        $('#tbl_productos').DataTable({})
    });

</script>
<script type="text/javascript">
        $(document).ready(function(){
            $("#menu_refacciones").addClass("show");
            $("#refacciones_almacen").addClass("show");
            $("#refacciones_almacen").addClass("active");
            $("#refacciones_almacen_sub").addClass("show");
            $("#refacciones_almacen_sub").addClass("active");
            $("#menu_refacciones_traspaso").addClass("active");
        });
</script>
@endsection