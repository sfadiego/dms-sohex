
<div class="modal fade" id="modalproveedor" tabindex="-1" role="dialog" aria-labelledby="modalproveedor" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="title_proveedor">Proveedor</h5>
            </div>
            <div class="modal-body">
                <form id="frmmodalproveedor" class="row">
                    <div class="col-md-12">
                        <?php renderSelectArray('select_proveedor_id', 'Proveedor', $proveedores, 'id', 'proveedor_nombre', null) ?>
                        <input type="hidden" name="selected_pedidoproducto_id" id="selected_pedidoproducto_id">
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
                <button onclick="updateproovedor()" type="button" class="btn btn-primary"> <i class="fas fa-list"></i> Aceptar </button>
            </div>
        </div>
    </div>
</div>