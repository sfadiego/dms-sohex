@layout('tema_luna/layout')
@section('contenido')
<div class="container-fluid panel-body">
    <h1 class="mt-4"><?php echo isset($titulo) ? $titulo : "" ?></h1>
    <ol class="breadcrumb mb-4">
        <li class="breadcrumb-item active"><?php echo isset($titulo) ? $titulo : "" ?></li>
    </ol>

    <div class="row">
        <div class="col-md-4">
            <div class="form-group">
                <label for="folio">Folio</label>
                <input id="folio" placeholder="Número de folio" name="folio" type="text" class="form-control" value="">
                <div id="tipo_cuenta_error" class="invalid-feedback"></div>
            </div>
        </div>
        <div class="col-md-4">
            <div class="form-group">
                <label for="">Tipo de cuenta:</label>
                <select class="form-control" id="tipo_cuenta" name="tipo_cuenta" style="width: 100%;">
                    <option value="">Selecionar ...</option>
                    @if(!empty($cat_cuentas))
                    @foreach ($cat_cuentas as $cuenta)
                    <option value="{{$cuenta->id}}">{{$cuenta->no_cuenta}} {{$cuenta->nombre_cuenta}}</option>
                    @endforeach
                    @endif
                </select>
                <div id="tipo_cuenta_error" class="invalid-feedback"></div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-8 mt-4">
            <button type="button" id="btn-buscar" class="btn btn-primary col-md-6">
                <i class="fa fa-search" aria-hidden="true"></i> Buscar
            </button>
        </div>
    </div>
    <hr>
    <div class="row">
        <div class="col-md-12">
            <div class="table-responsive">
                <table class="table table-bordered" id="tbl-busqueda" width="100%" cellspacing="0">
                    <thead>
                        <tr>
                            <th>#</th>
                            <th>Folio</th>
                            <th>Tipo de cuenta</th>
                            <th>Fecha</th>
                            <th>-</th>
                        </tr>
                    </thead>
                    <tfoot>
                        <tr>
                            <th>#</th>
                            <th>Folio</th>
                            <th>Tipo de cuenta</th>
                            <th>Fecha</th>
                            <th>-</th>
                        </tr>
                    </tfoot>
                </table>
            </div>
        </div>
    </div>
    <hr>
    <a href=""></a>
</div>
@endsection

@section('scripts')
<script>
    var tabla_folios = $('#tbl-busqueda').DataTable({
        "ajax": {
            url: base_url + "refacciones/productos/ajax_search_by_folios",
            type: 'POST',
            data: {
                folio: function() {
                    return $('#folio').val()
                }
            }
        },
        columns: [{
                'data': 'id'
            },
            {
                'data': 'folio'
            },
            {
                'data': 'nombre_cuenta'
            },
            {
                'data': function(data) {
                    return data.created_at;
                }
            },
            {
                'data': function(data) {
                    switch (data.tipo_cuenta_id) {
                        case 2:
                            return "<a target='_blank' href='" + base_url +'refacciones/almacenes/productoTraspaso/'+data.traspaso[0].id+  "' class='btn btn-primary'><i class='fas fa-list'></i></a>";
                            break;
                        case 3:
                            return "<a target='_blank' href='" + base_url +'refacciones/entradas/detalleOrden/'+data.compras[0].id+  "' class='btn btn-primary'><i class='fas fa-list'></i></a>";
                            break;
                            case 1:
                                return "<a target='_blank' href='" + base_url +'refacciones/salidas/detalleventa/'+data.id+  "' class='btn btn-primary'><i class='fas fa-list'></i></a>";
                            break;
                        default:
                            return '--';
                            break;
                    }

                }
            }
        ]
    });

    $("#folio").on('keyup', function() {
        let folio = $("#folio").val();
        $("#folio").val(folio.toUpperCase());
    });

    $("#btn-buscar").on('click', function() {
        let folio = $("#folio").val();
        if (folio == '') {
            utils.displayWarningDialog("Folio requerido", "warning", function(data) {});
        }
        tabla_folios.ajax.reload();
        // $(".invalid-feedback").html("");
        // ajax.post(`api/folio/get-by-folio/${folio.toUpperCase()}`, {

        // }, function(response, headers) {
        //     console.log(response);

        //     // tabla_carrito_traspaso.ajax.reload();
        //     // if (headers.status == 400) {
        //     //     return ajax.showValidations(headers);
        //     // }
        //     // utils.displayWarningDialog(headers.message, "success", function(data) {
        //     //     return window.location.href = base_url + `refacciones/productos/listado`;
        //     // })

        // })

    })
</script>
<script type="text/javascript">
    $(document).ready(function() {
        $("#menu_refacciones").addClass("show");
        $("#refacciones_almacen").addClass("show");
        $("#refacciones_almacen").addClass("active");
        $("#refacciones_almacen_sub").addClass("show");
        $("#refacciones_almacen_sub").addClass("active");
        $("#almacen_productos").addClass("active");
        $("#M02").addClass("active");
    });
</script>
@endsection