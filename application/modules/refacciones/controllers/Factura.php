<?php

defined('BASEPATH') or exit('No direct script access allowed');

class Factura extends MX_Controller
{

    public function index()
    {
    }

    public function vistafactura($id)
    {

        if ($id) {
            $this->load->library('curl');
            $this->load->helper('general');
            $dataFromApi = $this->curl->curlGet('api/facturas/' . $id);

            $datafactura = procesarResponseApiJsonToArray($dataFromApi);

            if (count($datafactura) == 0) {
                return $this->listado();
            }
            $data['data'] = count($datafactura) > 0 ? $datafactura[0] : [];
            $data['readOnly'] = true;
            $data['titulo'] = "Detalles de Factura";
            $this->blade->render('facturas/readonly_factura', $data);
        }
    }

    public function subirFactura()
    {
        $data['titulo'] = "Subir factura";
        $this->blade->render('facturas/upload', $data);
    }

    public function listado()
    {
        $this->load->library('curl');
        $this->load->helper('general');
        $dataFromApi = $this->curl->curlGet('api/orden-compra/facturas-listado');
        $datafactura = procesarResponseApiJsonToArray($dataFromApi);
        $data['data'] = isset($datafactura) ? $datafactura : [];
        $data['titulo'] = "Facturas";
        $data['subtitulo'] = "Listado de Facturas";
        
        $this->blade->render('facturas/listado', $data);
    }

    public function polizaFactura($id_factura)
    {
        $this->load->library('curl');
        $this->load->helper('general');
        $data['titulo'] = "Poliza de factura";
        $detalle_producto = $this->curl->curlGet('api/facturas/' . $id_factura);
        $detalle = procesarResponseApiJsonToArray($detalle_producto);
        $data['folio'] = isset($detalle) ? $detalle[0] : [];
        $view = $this->load->view('polizas/poliza_facturas/template', $data, true);
        $this->curl->curldownloadPdf('api/pdf', base64_encode($view));
    }
}

/* End of file Facturas.php */
