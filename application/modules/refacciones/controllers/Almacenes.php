<?php

defined('BASEPATH') or exit('No direct script access allowed');
class Almacenes extends MX_Controller
{
  public function __construct()
  {
    parent::__construct();
    $this->load->library('curl');
    $this->load->helper('general');
    date_default_timezone_set('America/Mexico_City');
  }

  public function construccion()
  {
    $this->blade->render('productos/construccion');
  }

  public function listadoremplazos()
  {
    $dataFromApi = $this->curl->curlGet('api/productos');
    $data['listado'] = !empty(procesarResponseApiJsonToArray($dataFromApi)) ? procesarResponseApiJsonToArray($dataFromApi) : [];
    $data['titulo'] = 'Remplazos';
    $data['bread_active'] = 'Listado remplazo';
    $this->blade->render('almacenes/remplazo/listado_remplazo_producto', $data);
  }

  public function productoRemplazo($id_producto = '')
  {
    $producto = $this->curl->curlGet('api/productos/' . $id_producto);
    $ultimosRemplazos = $this->curl->curlGet('api/producto-remplazo-almacen/getby-idproducto/', $id_producto);
    $data['remplazos_producto'] = procesarResponseApiJsonToArray($ultimosRemplazos);
    $cat_talleres = $this->curl->curlGet('api/talleres');
    $cat_precios = $this->curl->curlGet('api/precios');
    $data['producto'] = procesarResponseApiJsonToArray($producto);
    $data['cat_talleres'] = procesarResponseApiJsonToArray($cat_talleres);
    $data['cat_precios'] = procesarResponseApiJsonToArray($cat_precios);
    $almacenes = $this->curl->curlGet('api/almacen');
    $data['almacenes'] = isset($almacenes) ? procesarResponseApiJsonToArray($almacenes) : [];

    $data['titulo'] = 'Remplazos';
    $this->blade->render('almacenes/remplazo/producto_remplazo', $data);
  }

  public function generarTraspaso()
  {
    $dataFromApi = $this->curl->curlGet('api/productos');
    $data['listado'] = procesarResponseApiJsonToArray($dataFromApi);
    $almacenes = $this->curl->curlGet('api/almacen');
    $data['almacenes'] = isset($almacenes) ? procesarResponseApiJsonToArray($almacenes) : [];
    $data['bread_active'] = 'Generar traspasos';

    $dataApiTraspasos = $this->curl->curlGet('api/traspasos');
    $data['traspasos'] = procesarResponseApiJsonToArray($dataApiTraspasos);

    $this->blade->render('almacenes/traspasos/generar_traspaso', $data);
  }

  public function generarPolizaTraspaso($id_traspaso = '')
  {
    $dataApiTraspasos = $this->curl->curlGet('api/traspasos/' . $id_traspaso);
    $total_traspaso = $this->curl->curlGet('api/traspasos/poliza/venta-total/' . $id_traspaso);
    $parse_total_traspaso = procesarResponseApiJsonToArray($total_traspaso);
    $parseData = procesarResponseApiJsonToArray($dataApiTraspasos);
    $data['traspaso'] = count($parseData) > 0 ? $parseData[0] : [];
    $data['total_traspaso'] = isset($parse_total_traspaso) > 0 ? $parse_total_traspaso : [];
    // TODO: produccion no jala
    // $this->load->view('polizas/poliza_traspaso/poliza_traspaso', $data);
    $view = $this->load->view('polizas/poliza_traspaso/poliza_traspaso', $data, true);
    $this->curl->curldownloadPdf('api/pdf', base64_encode($view));
  }


  public function productoTraspaso($traspaso_id = '')
  {
    if ($traspaso_id == '') {
      $this->generarTraspaso();
    }


    $traspaso = $this->curl->curlGet('api/traspasos/' . $traspaso_id);
    $traspaso_array = procesarResponseApiJsonToArray($traspaso);
    $data['traspaso_detalle'] = isset($traspaso_array) ? $traspaso_array[0] : [];
    if($data['traspaso_detalle']->estatus == 1){
      $dataFromApi = $this->curl->curlGet('api/productos/stockActual');
    } else {
      $dataFromApi = $this->curl->curlGet('api/productos/stockActual?traspaso_id='.$traspaso_id);
    }
    $data['productos'] = !empty(procesarResponseApiJsonToArray($dataFromApi)) ? procesarResponseApiJsonToArray($dataFromApi) : [];
    // dd($data['productos']);
    $almacenes = $this->curl->curlGet('api/almacen');
    $data['almacenes'] = isset($almacenes) ? procesarResponseApiJsonToArray($almacenes) : [];
    $this->blade->render('almacenes/traspasos/producto_traspaso', $data);
  }

  public function ajax_get_lista_traspaso()
  {
    if ($this->input->post('traspaso_id') == '') {
      $data['data'] = [];
      echo json_encode($data);
      die();
    }

    $detalleVenta = $this->curl->curlGet('api/producto-traspaso-almacen/get-detalle-traspaso-by-id/' . $this->input->post('traspaso_id'));
    $detalle = procesarResponseApiJsonToArray($detalleVenta);
    $data['data'] = isset($detalle) ? $detalle : [];
    echo json_encode($data);
  }
}
