<div id="layoutSidenav_nav">
    <nav class="sb-sidenav accordion sb-sidenav-dark" id="sidenavAccordion">
        <div class="sb-sidenav-menu">
            <div class="nav">
                <div class="sb-sidenav-menu-heading">Refacciones</div>
                <a class="nav-link" href="<?php echo base_url('refacciones/productos/construccion'); ?>">
                    <div class="sb-nav-link-icon"><i class="fas fa-tachometer-alt"></i></div>
                    DMS Ford
                </a>

                <div class="sb-sidenav-menu-heading">Menu</div>
                <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#colapse_almacen" aria-expanded="false" aria-controls="collapseLayouts">
                    <div class="sb-nav-link-icon"><i class="fas fa-columns"></i></div>Almacén
                    <div class="sb-sidenav-collapse-arrow"><i class="fas fa-angle-down"></i></div>
                </a>
                <div class="collapse" id="colapse_almacen" aria-labelledby="headingOne" data-parent="#sidenavAccordion">
                    <nav class="sb-sidenav-menu-nested nav">
                        <a class="nav-link" href="<?php echo base_url('refacciones/productos/listado'); ?>">Productos</a>
                        <a class="nav-link" href="<?php echo base_url('refacciones/almacenes/listadotraspasoAlmacen'); ?>">Traspaso almacén</a>
                        <a class="nav-link" href="<?php echo base_url('refacciones/almacenes/listadoremplazos'); ?>">Reemplazos</a>
                    </nav>
                </div>
                <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#colapse_salidas" aria-expanded="false" aria-controls="collapseLayouts">
                    <div class="sb-nav-link-icon"><i class="fas fa-columns"></i></div>Salidas
                    <div class="sb-sidenav-collapse-arrow"><i class="fas fa-angle-down"></i></div>
                </a>
                <div class="collapse" id="colapse_salidas" aria-labelledby="headingOne" data-parent="#sidenavAccordion">
                    <nav class="sb-sidenav-menu-nested nav">
                        <a class="nav-link" href="<?php echo base_url('refacciones/salidas/ventasMostrador'); ?>">Ventas mostrador</a>
                        <a class="nav-link" href="<?php echo base_url('refacciones/salidas/ventanillaTaller'); ?>">Ventanilla taller</a>
                        <a class="nav-link" href="<?php echo base_url('refacciones/salidas/otrasSalidas'); ?>">Otras salidas Serv.</a>
                        <a class="nav-link" href="<?php echo base_url('refacciones/salidas/devolucion'); ?>">Devolucion a proveedor</a>
                        <a class="nav-link" href="<?php echo base_url('refacciones/salidas/polizaVentas'); ?>">Poliza de ventas</a>
                        <a class="nav-link" href="<?php echo base_url('refacciones/salidas/polizadevProveedor'); ?>">Poliza dev. proveedor</a>
                        <a class="nav-link" href="<?php echo base_url('refacciones/salidas/polizaOtrasSalidas'); ?>">Poliza otras salidas</a>
                    </nav>
                </div>
                <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#colapse_entradas" aria-expanded="false" aria-controls="collapseLayouts">
                    <div class="sb-nav-link-icon"><i class="fas fa-columns"></i></div>Entradas
                    <div class="sb-sidenav-collapse-arrow"><i class="fas fa-angle-down"></i></div>
                </a>
                <div class="collapse" id="colapse_entradas" aria-labelledby="headingOne" data-parent="#sidenavAccordion">
                    <nav class="sb-sidenav-menu-nested nav">
                        <a class="nav-link" href="<?php echo base_url('refacciones/entradas/compras'); ?>">Compras</a>
                        <a class="nav-link" href="<?php echo base_url('refacciones/entradas/devVentas'); ?>">Dev. Ventas</a>
                        <a class="nav-link" href="<?php echo base_url('refacciones/entradas/polizaCompras?tipo_poliza=4'); ?>">Poliza compras</a>
                        <a class="nav-link" href="<?php echo base_url('refacciones/entradas/polizaDevMostrador?tipo_poliza=5'); ?>">Poliza dev. mostrador</a>
                        <a class="nav-link" href="<?php echo base_url('refacciones/entradas/polizaTraspasos?tipo_poliza=6'); ?>">Poliza traspasos</a>
                    </nav>
                </div>
                <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#alternos" aria-expanded="false" aria-controls="collapseLayouts">
                    <div class="sb-nav-link-icon"><i class="fas fa-columns"></i></div>Alternos
                    <div class="sb-sidenav-collapse-arrow"><i class="fas fa-angle-down"></i></div>
                </a>
                <div class="collapse" id="alternos" aria-labelledby="headingOne" data-parent="#sidenavAccordion">
                    <nav class="sb-sidenav-menu-nested nav">
                    <a class="nav-link" href="{{ base_url('refacciones/alternos/') }}">Reimpresion de doctos</a>
                    <a class="nav-link" href="{{ base_url('catalogos/preciosController')}}">Actualizacion precios</a>
                    </nav>
                </div>
                <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#consultas" aria-expanded="false" aria-controls="collapseLayouts">
                    <div class="sb-nav-link-icon"><i class="fas fa-columns"></i></div>Consultas
                    <div class="sb-sidenav-collapse-arrow"><i class="fas fa-angle-down"></i></div>
                </a>
                <div class="collapse" id="consultas" aria-labelledby="headingOne" data-parent="#sidenavAccordion">
                    <nav class="sb-sidenav-menu-nested nav">
                    <a class="nav-link" href="{{ base_url('refacciones/consultas/inventario') }}">Inventario</a>
                    <a class="nav-link" href="{{ base_url('refacciones/consultas/kardex')}}">Kardex</a>
                    </nav>
                </div>
                <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#colapse_compras" aria-expanded="false" aria-controls="collapseLayouts">
                    <div class="sb-nav-link-icon"><i class="fas fa-columns"></i></div>Facturas
                    <div class="sb-sidenav-collapse-arrow"><i class="fas fa-angle-down"></i></div>
                </a>
                <div class="collapse" id="colapse_compras" aria-labelledby="headingOne" data-parent="#sidenavAccordion">
                    <nav class="sb-sidenav-menu-nested nav">
                        <a class="nav-link" href="<?php echo base_url('refacciones/factura/listado'); ?>">Facturas</a>
                    </nav>
                </div>
                <a class="nav-link" href="<?php echo base_url('autos/Principal/index/'); ?>">
                    <div class="sb-nav-link-icon">
                        <i class="fas fa-book-open"></i>
                    </div>
                    Autos
                </a>

                <a class="nav-link" href="<?php echo base_url('seminuevos/Principal/index/'); ?>">
                    <div class="sb-nav-link-icon">
                        <i class="fas fa-book-open"></i>
                    </div>
                    Seminuevos
                </a>
                
                <div class="sb-sidenav-menu-heading">Admin</div>
                <a class="nav-link" href="<?php echo base_url('catalogos/catalogos/'); ?>">
                    <div class="sb-nav-link-icon">
                    <i class="fas fa-book-open"></i>
                </div>Admin
                </a>
                
                <div class="collapse" id="collapsePages" aria-labelledby="headingTwo" data-parent="#sidenavAccordion">
                    <nav class="sb-sidenav-menu-nested nav accordion" id="sidenavAccordionPages">
                        <a class="nav-link collapsed" href="<?php echo base_url('refacciones/productos/listado'); ?>" data-toggle="collapse" data-target="#pagesCollapseAuth" aria-expanded="false" aria-controls="pagesCollapseAuth">
                            Authentication
                            <div class="sb-sidenav-collapse-arrow">
                                <i class="fas fa-angle-down"></i>
                            </div>
                        </a>
                        <div class="collapse" id="pagesCollapseAuth" aria-labelledby="headingOne" data-parent="#sidenavAccordionPages">
                            <nav class="sb-sidenav-menu-nested nav">
                                <a class="nav-link" href="login.html">Login</a>
                                <a class="nav-link" href="register.html">Register</a>
                                <a class="nav-link" href="password.html">Forgot Password</a>
                            </nav>
                        </div>
                        <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#pagesCollapseError" aria-expanded="false" aria-controls="pagesCollapseError">Error
                            <div class="sb-sidenav-collapse-arrow"><i class="fas fa-angle-down"></i></div></a>
                        <div class="collapse" id="pagesCollapseError" aria-labelledby="headingOne" data-parent="#sidenavAccordionPages">
                            <nav class="sb-sidenav-menu-nested nav"><a class="nav-link" href="401.html">401 Page</a><a class="nav-link" href="404.html">404 Page</a><a class="nav-link" href="500.html">500 Page</a></nav>
                        </div>
                    </nav>
                </div>
                
                
            </div>
        </div>
        <div class="sb-sidenav-footer">
            <div class="small">Usuario:</div>
            En desarrollo
        </div>
    </nav>
</div>