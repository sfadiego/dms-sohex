<div id="layoutSidenav_nav">
    <nav class="sb-sidenav accordion sb-sidenav-dark" id="sidenavAccordion">
        <div class="sb-sidenav-menu">
            <div class="nav">
                <div class="sb-sidenav-menu-heading">Autos</div>
                <a class="nav-link" href="<?php echo base_url('menu/'); ?>">
                    <div class="sb-nav-link-icon"><i class="fas fa-tachometer-alt"></i></div>
                    DMS 
                </a>

                <div class="sb-sidenav-menu-heading">Menu</div>
               
                <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#colapse_catalogos" aria-expanded="false" aria-controls="collapseLayouts">
                    <div class="sb-nav-link-icon"><i class="fas fa-columns"></i></div>Catálogos
                    <div class="sb-sidenav-collapse-arrow"><i class="fas fa-angle-down"></i></div>
                </a>
                <div class="collapse" id="colapse_catalogos" aria-labelledby="headingOne" data-parent="#sidenavAccordion">
                    <nav class="sb-sidenav-menu-nested nav">
                        <a class="nav-link" href="<?php echo base_url('autos/Unidades/index'); ?>">Unidades</a>
                        <a class="nav-link" href="<?php echo base_url('autos/Principal/construccion'); ?>">Tabla Isan</a>
                        <a class="nav-link" href="<?php echo base_url('autos/Vendedores/index'); ?>">Vendedores</a>
                        
                    </nav>
                </div>
                <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#colapse_inventario" aria-expanded="false" aria-controls="collapseLayouts">
                    <div class="sb-nav-link-icon"><i class="fas fa-columns"></i></div>Inventario
                    <div class="sb-sidenav-collapse-arrow"><i class="fas fa-angle-down"></i></div>
                </a>
                <div class="collapse" id="colapse_inventario" aria-labelledby="headingOne" data-parent="#sidenavAccordion">
                    <nav class="sb-sidenav-menu-nested nav">
                        <a class="nav-link" href="<?php echo base_url('autos/Recepcion/index'); ?>">Recepción</a>
                        <a class="nav-link" href="<?php echo base_url('autos/EquipoAdicional/index'); ?>">Equipo Opcional</a>
                        <a class="nav-link" href="<?php echo base_url('autos/Salidas/index'); ?>">Salidas</a>
                        <a class="nav-link" href="<?php echo base_url('autos/OrdenEquipo/index'); ?>">Ordenes de equipos</a>
                    </nav>
                </div>

                <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#colapse_entradas" aria-expanded="false" aria-controls="collapseLayouts">
                    <div class="sb-nav-link-icon"><i class="fas fa-columns"></i></div>Ventas
                    <div class="sb-sidenav-collapse-arrow"><i class="fas fa-angle-down"></i></div>
                </a>
                <div class="collapse" id="colapse_entradas" aria-labelledby="headingOne" data-parent="#sidenavAccordion">
                    <nav class="sb-sidenav-menu-nested nav">
                        <a class="nav-link" href="<?php echo base_url('autos/Principal/construccion'); ?>">Pre-pedidos</a>
                        <a class="nav-link" href="<?php echo base_url('autos/Principal/construccion'); ?>">Entrega de unidades</a>
                    </nav>
                </div>

                <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#colapse_reportes" aria-expanded="false" aria-controls="collapseLayouts">
                    <div class="sb-nav-link-icon"><i class="fas fa-columns"></i></div>Reportes
                    <div class="sb-sidenav-collapse-arrow"><i class="fas fa-angle-down"></i></div>
                </a>
                <div class="collapse" id="colapse_reportes" aria-labelledby="headingOne" data-parent="#sidenavAccordion">
                    <nav class="sb-sidenav-menu-nested nav">
                        <a class="nav-link" href="<?php echo base_url('autos/Reportes/reporteventas'); ?>">Ventas diarias</a>
                        <a class="nav-link" href="<?php echo base_url('autos/Reportes/reportepolizasventas'); ?>">Póliza de ventas</a>
                        <a class="nav-link" href="<?php echo base_url('autos/Reportes/reportepolizacompras'); ?>">Póliza de compras</a>
                        <a class="nav-link" href="<?php echo base_url('autos/Reportes/reportecomisiones'); ?>">Comisiones</a>
                        <a class="nav-link" href="<?php echo base_url('autos/Reportes/reporteexistencias'); ?>">Existencias</a>
                        <a class="nav-link" href="<?php echo base_url('autos/Reportes/reporteutilidad'); ?>">Utilidad</a>
                    </nav>
                </div>
                
                <div class="collapse" id="collapsePages" aria-labelledby="headingTwo" data-parent="#sidenavAccordion">
                    <nav class="sb-sidenav-menu-nested nav accordion" id="sidenavAccordionPages">
                        <a class="nav-link collapsed" href="<?php echo base_url('refacciones/productos/listado'); ?>" data-toggle="collapse" data-target="#pagesCollapseAuth" aria-expanded="false" aria-controls="pagesCollapseAuth">
                            Authentication
                            <div class="sb-sidenav-collapse-arrow">
                                <i class="fas fa-angle-down"></i>
                            </div>
                        </a>
                        <div class="collapse" id="pagesCollapseAuth" aria-labelledby="headingOne" data-parent="#sidenavAccordionPages">
                            <nav class="sb-sidenav-menu-nested nav">
                                <a class="nav-link" href="login.html">Login</a>
                                <a class="nav-link" href="register.html">Register</a>
                                <a class="nav-link" href="password.html">Forgot Password</a>
                            </nav>
                        </div>
                        <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#pagesCollapseError" aria-expanded="false" aria-controls="pagesCollapseError">Error
                            <div class="sb-sidenav-collapse-arrow"><i class="fas fa-angle-down"></i></div></a>
                        <div class="collapse" id="pagesCollapseError" aria-labelledby="headingOne" data-parent="#sidenavAccordionPages">
                            <nav class="sb-sidenav-menu-nested nav"><a class="nav-link" href="401.html">401 Page</a><a class="nav-link" href="404.html">404 Page</a><a class="nav-link" href="500.html">500 Page</a></nav>
                        </div>
                    </nav>
                </div>
                
            </div>
        </div>
        <div class="sb-sidenav-footer">
            <div class="small">Usuario:</div>
            En desarrollo
        </div>
    </nav>
</div>