<div id="layoutSidenav_nav">
    <nav class="sb-sidenav accordion sb-sidenav-dark" id="sidenavAccordion">
        <div class="sb-sidenav-menu">
            <div class="nav">
                <div class="sb-sidenav-menu-heading">Contabilidad</div>
                
                <div class="sb-sidenav-menu-heading">Menu</div>
               
                <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#colapse_almacen" aria-expanded="false" aria-controls="collapseLayouts">
                    <div class="sb-nav-link-icon"><i class="fas fa-columns"></i></div>Catalogos
                    <div class="sb-sidenav-collapse-arrow"><i class="fas fa-angle-down"></i></div>
                </a>
                <div class="collapse" id="colapse_almacen" aria-labelledby="headingOne" data-parent="#sidenavAccordion">
                    <nav class="sb-sidenav-menu-nested nav">
                        <a class="nav-link" href="<?php echo base_url('contabilidad/catalogos'); ?>">Cuentas</a>
                        <a class="nav-link" href="<?php echo base_url('contabilidad/catalogos/estado_financiero'); ?>">Estado financiero</a>
                        
                    </nav>
                </div>
                <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#colapse_salidas" aria-expanded="false" aria-controls="collapseLayouts">
                    <div class="sb-nav-link-icon"><i class="fas fa-columns"></i></div>Movimientos
                    <div class="sb-sidenav-collapse-arrow"><i class="fas fa-angle-down"></i></div>
                </a>
                <div class="collapse" id="colapse_salidas" aria-labelledby="headingOne" data-parent="#sidenavAccordion">
                    <nav class="sb-sidenav-menu-nested nav">
                        <a class="nav-link" href="<?php echo base_url('contabilidad/movimientos/facturas'); ?>">Facturación</a>
                        <a class="nav-link" href="<?php echo base_url('contabilidad/movimientos/index'); ?>">Captura poliza</a>
                 
                    </nav>
                </div>

                <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#colapse_entradas" aria-expanded="false" aria-controls="collapseLayouts">
                    <div class="sb-nav-link-icon"><i class="fas fa-columns"></i></div>Consultas
                    <div class="sb-sidenav-collapse-arrow"><i class="fas fa-angle-down"></i></div>
                </a>
                <div class="collapse" id="colapse_entradas" aria-labelledby="headingOne" data-parent="#sidenavAccordion">
                    <nav class="sb-sidenav-menu-nested nav">
                        <a class="nav-link" href="<?php echo base_url('contabilidad/consultas/polizas'); ?>">Polizas</a>
                        <a class="nav-link" href="<?php echo base_url('contabilidad/consultas/saldos'); ?>">Saldos</a>
         
                    </nav>
                </div>

                <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#colapse_reportes" aria-expanded="false" aria-controls="collapseLayouts">
                    <div class="sb-nav-link-icon"><i class="fas fa-columns"></i></div>Reportes
                    <div class="sb-sidenav-collapse-arrow"><i class="fas fa-angle-down"></i></div>
                </a>
                <div class="collapse" id="colapse_reportes" aria-labelledby="headingOne" data-parent="#sidenavAccordion">
                    <nav class="sb-sidenav-menu-nested nav">
                        <a class="nav-link" href="#">Diario de movimientos</a>
                        <a class="nav-link" href="#">Mayor mensual</a>
                        <a class="nav-link" href="#">Balanza de comprobación</a>
                        <a class="nav-link" href="#">Historico mayor</a>
                        <a class="nav-link" href="<?php echo base_url('refacciones/productos/construccion'); ?>">Aux. VS Conta</a>
                        <a class="nav-link" href="<?php echo base_url('refacciones/productos/construccion'); ?>">Estado financiero</a>
                    </nav>
                </div>

                <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#colapse_utileria" aria-expanded="false" aria-controls="collapseLayouts">
                    <div class="sb-nav-link-icon"><i class="fas fa-columns"></i></div>Utileria
                    <div class="sb-sidenav-collapse-arrow"><i class="fas fa-angle-down"></i></div>
                </a>
                <div class="collapse" id="colapse_utileria" aria-labelledby="headingOne" data-parent="#sidenavAccordion">
                    <nav class="sb-sidenav-menu-nested nav">
                        <a class="nav-link" href="<?php echo base_url('refacciones/productos/construccion'); ?>">Diario de movimientos</a>
                        <a class="nav-link" href="<?php echo base_url('refacciones/productos/construccion'); ?>">Mayor mensual</a>
                        <a class="nav-link" href="<?php echo base_url('refacciones/productos/construccion'); ?>">Balanza de comprobación</a>
                        <a class="nav-link" href="<?php echo base_url('refacciones/productos/construccion'); ?>">Historico mayor</a>
                        <a class="nav-link" href="<?php echo base_url('refacciones/productos/construccion'); ?>">Aux. VS Conta</a>
                        <a class="nav-link" href="<?php echo base_url('refacciones/productos/construccion'); ?>">Estado financiero</a>
                    </nav>
                </div>

               
                
                <div class="collapse" id="collapsePages" aria-labelledby="headingTwo" data-parent="#sidenavAccordion">
                    <nav class="sb-sidenav-menu-nested nav accordion" id="sidenavAccordionPages">
                        <a class="nav-link collapsed" href="<?php echo base_url('refacciones/productos/listado'); ?>" data-toggle="collapse" data-target="#pagesCollapseAuth" aria-expanded="false" aria-controls="pagesCollapseAuth">
                            Authentication
                            <div class="sb-sidenav-collapse-arrow">
                                <i class="fas fa-angle-down"></i>
                            </div>
                        </a>
                        <div class="collapse" id="pagesCollapseAuth" aria-labelledby="headingOne" data-parent="#sidenavAccordionPages">
                            <nav class="sb-sidenav-menu-nested nav">
                                <a class="nav-link" href="login.html">Login</a>
                                <a class="nav-link" href="register.html">Register</a>
                                <a class="nav-link" href="password.html">Forgot Password</a>
                            </nav>
                        </div>
                        <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#pagesCollapseError" aria-expanded="false" aria-controls="pagesCollapseError">Error
                            <div class="sb-sidenav-collapse-arrow"><i class="fas fa-angle-down"></i></div></a>
                        <div class="collapse" id="pagesCollapseError" aria-labelledby="headingOne" data-parent="#sidenavAccordionPages">
                            <nav class="sb-sidenav-menu-nested nav"><a class="nav-link" href="401.html">401 Page</a><a class="nav-link" href="404.html">404 Page</a><a class="nav-link" href="500.html">500 Page</a></nav>
                        </div>
                    </nav>
                </div>
                
                
            </div>
        </div>
        <div class="sb-sidenav-footer">
            <div class="small">Usuario:</div>
            En desarrollo
        </div>
    </nav>
</div>