
<nav class="navbar navbar-expand-lg navbar-dark bg-dark fixed-top" id="mainNav">
  <a class="navbar-brand" href="">Menú Refacciones</a>

  <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>
  <div class="collapse navbar-collapse" id="navbarResponsive">
    <ul class="navbar-nav navbar-sidenav" id="exampleAccordion">


      <il class="nav-item" data-toggle="tooltip" data-placement="right" title="Components">
        <a class="nav-link nav-link-collapse collapsed" data-toggle="collapse" href="#collapseComponents" data-parent="#exampleAccordion">
          <i class="fa fa-fw fa-wrench"></i>
          <span class="nav-link-text"><b>Refacciones</b></span>  
        </a>
        <ul class="sidenav-second-level collapse" id="collapseComponents">
          <li>
            <a href="{{base_url('citas/index')}}">Vendedores</a>
            <a class="nav-link nav-link-collapse collapsed" data-toggle="collapse" href="#comp_almacen" >Almacén</a>
                <ul class="sidenav-second-level collapse" id="comp_almacen">
                  <li>
                    <a style="color:white;" href="{{base_url('refacciones/productos/listado')}}">Productos</a>
                    <a style="color:white;" href="{{base_url('citas/index')}}">Traspaso almacén</a>
                    <a style="color:white;" href="{{base_url('citas/index')}}">Reemplazos</a>
                  </li> 
                </ul>
            <a class="nav-link nav-link-collapse collapsed" data-toggle="collapse" href="#comp_salidas" >Salidas</a>
                <ul class="sidenav-second-level collapse" id="comp_salidas">
                  <li>
                    <a style="color:white;" href="{{base_url('citas/index')}}">Ventas mostrador</a>
                    <a style="color:white;" href="{{base_url('citas/index')}}">Ventas taller</a>
                    <a style="color:white;" href="{{base_url('citas/index')}}">Otras salidas</a>
                    <a style="color:white;" href="{{base_url('citas/index')}}">Dev a proveedor</a>
                    <a style="color:white;" href="{{base_url('citas/index')}}">Poliza ventas</a>
                    <a style="color:white;" href="{{base_url('citas/index')}}">Poliza dev. proveedor</a>
                    <a style="color:white;" href="{{base_url('citas/index')}}">Poliza otras salidas</a>
                  </li> 
                </ul>
            <a class="nav-link nav-link-collapse collapsed" data-toggle="collapse" href="#comp_entradas" >Entradas</a>
                <ul class="sidenav-second-level collapse" id="comp_entradas">
                  <li>
                    <a style="color:white;" href="{{base_url('citas/index')}}">Compras</a>
                    <a style="color:white;" href="{{base_url('citas/index')}}">Dev. Ventas</a>
                    <a style="color:white;" href="{{base_url('citas/index')}}">Poliza de compras</a>
                    <a style="color:white;" href="{{base_url('citas/index')}}">Poliza dev. mostrador</a>
                    <a style="color:white;" href="{{base_url('citas/index')}}">Poliza traspasos</a>
                  </li> 
                </ul>
            <a class="nav-link nav-link-collapse collapsed" data-toggle="collapse" href="#comp_alternos" >Alternos</a>
                <ul class="sidenav-second-level collapse" id="comp_alternos">
                  <li>
                    <a style="color:white;" href="{{base_url('citas/index')}}">Reeimpresion de documentos</a>
                    <a style="color:white;" href="{{base_url('citas/index')}}">Actualizcion de precios</a>
                  </li> 
                </ul>
            <a href="{{base_url('citas/index')}}">VipArchivos</a>
            <a class="nav-link nav-link-collapse collapsed" data-toggle="collapse" href="#comp_consultas" >Consultas</a>
                <ul class="sidenav-second-level collapse" id="comp_consultas">
                  <li>
                    <a style="color:white;" href="{{base_url('citas/index')}}">Inventario</a>
                    <a style="color:white;" href="{{base_url('citas/index')}}">Kardex</a>
                    <a style="color:white;" href="{{base_url('citas/index')}}">Req. Mostrador</a>
                    <a style="color:white;" href="{{base_url('citas/index')}}">Req. Taller</a>
                    <a style="color:white;" href="{{base_url('citas/index')}}">Compras</a>
                  </li> 
                </ul>
          </li>
        </ul>
      </li>
      <il class="nav-item" data-toggle="tooltip" data-placement="right" title="Components">
        <a class="nav-link nav-link-collapse collapsed" data-toggle="collapse" href="#comp_reportes" data-parent="#exampleAccordion">
          <i class="fa fa-fw fa-wrench"></i>
          <span class="nav-link-text1"><b>Reportes</b></span>
        </a>
        <ul class="sidenav-second-level collapse" id="comp_reportes">
          <li>
            <a class="nav-link nav-link-collapse collapsed" data-toggle="collapse" href="#comp_ventas" >Ventas</a>
                <ul class="sidenav-second-level collapse" id="comp_ventas">
                  <li>
                    <a style="color:white;" href="{{base_url('citas/index')}}">Vendedores</a>
                    <a style="color:white;" href="{{base_url('citas/index')}}">Fac.Diarias</a>
                    <a style="color:white;" href="{{base_url('citas/index')}}">Ana. Vtas vs Costos</a>
                    <a style="color:white;" href="{{base_url('citas/index')}}">Dev. Ventas</a>
                    <a style="color:white;" href="{{base_url('citas/index')}}">Otras Salidas</a>
                  </li> 
                </ul>
        <a class="nav-link nav-link-collapse collapsed" data-toggle="collapse" href="#comp_compras" >Compras</a>
                <ul class="sidenav-second-level collapse" id="comp_compras">
                  <li>
                    <a style="color:white;" href="{{base_url('citas/index')}}">Compras Diarias</a>
                    <a style="color:white;" href="{{base_url('citas/index')}}">Sugerido Compras</a>
                    <a style="color:white;" href="{{base_url('citas/index')}}">Dev.Prov.</a>
                  </li> 
                </ul>
        <a class="nav-link nav-link-collapse collapsed" data-toggle="collapse" href="#comp_inventarios" >Inventarios</a>
                <ul class="sidenav-second-level collapse" id="comp_inventarios">
                  <li>
                    <a style="color:white;" href="{{base_url('citas/index')}}">Control de Inv.</a>
                    <a style="color:white;" href="{{base_url('citas/index')}}">Mejores productos</a>
                  </li> 
                </ul>
           </li>
            </ul>
        <il class="nav-item" data-toggle="tooltip" data-placement="right" title="Components">
        <a class="nav-link nav-link-collapse collapsed" data-toggle="collapse" href="#comp_utilerias" data-parent="#exampleAccordion">
          <i class="fa fa-fw fa-wrench"></i>
          <span class="nav-link-text1"><b>Utileria</b></span>
        </a>
        <ul class="sidenav-second-level collapse" id="comp_utilerias">
          <li>
            <a class="nav-link nav-link-collapse collapsed" data-toggle="collapse" href="#comp_cierre" >cierre de mes</a>
                <ul class="sidenav-second-level collapse" id="comp_cierre">
                  <li>
                    <a style="color:white;" href="{{base_url('citas/index')}}">Reporte ciclico</a>
                  </li> 
                </ul>
        <a class="nav-link nav-link-collapse collapsed" data-toggle="collapse" href="#comp_comisiones" >comisiones</a>
                <ul class="sidenav-second-level collapse" id="comp_comisiones">
                  <li>
                    <a style="color:white;" href="{{base_url('citas/index')}}">Ajustes al inventario</a>
                  </li> 
                </ul>
            </li>
          </ul>
            </li>
    <ul class="navbar-nav sidenav-toggler">
      <li class="nav-item">
        <a class="nav-link text-center" id="sidenavToggler">
          <i class="fa fa-fw fa-angle-left"></i>
        </a>
      </li>
    </ul>

  </div>
  <ul class="navbar-nav ml-auto">
    <li class="nav-item">
      <a href="{{site_url('login/logout')}}" class="nav-link">
      Salir</a>
    </li>
  </ul>
</nav>
