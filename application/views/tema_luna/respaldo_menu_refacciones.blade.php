<li class="nav-category">
    Movimientos
</li>
<li id="refacciones_almacen">
    <a href="#refacciones_almacen_sub" data-toggle="collapse" aria-expanded="false">
        Almacen<span class="sub-nav-icon"> <i class="stroke-arrow"></i> </span>
    </a>
    <ul id="refacciones_almacen_sub" class="nav nav-second collapse nav-cat-3">
        <li id="almacen_productos">
            <a href="<?php echo base_url('refacciones/productos/listado'); ?>">Productos</a>
        </li>
        <li id="almacen_traspaso"><a href="<?php echo base_url('refacciones/almacenes/generarTraspaso'); ?>">Traspaso almacen</a></li>
        <li id="almacen_remplazos"><a href="<?php echo base_url('refacciones/almacenes/listadoremplazos'); ?>">Reemplazos</a></li>

    </ul>
</li>
<li id="refacciones_salidas">
    <a href="#refacciones_salidas_sub" data-toggle="collapse" aria-expanded="false">
        Salidas<span class="sub-nav-icon"> <i class="stroke-arrow"></i> </span>
    </a>
    <ul id="refacciones_salidas_sub" class="nav nav-second collapse nav-cat-3">
        <li id="lista_ventas"><a href="<?php echo base_url('refacciones/salidas/listadoVentas'); ?>">Listado de ventas</a></li>
        <li id="ventas_mostrador"><a href="<?php echo base_url('refacciones/salidas/ventasMostrador'); ?>">Ventas mostrador</a></li>
        <li id="ventanilla_taller"><a href="<?php echo base_url('refacciones/salidas/ventanillaTaller'); ?>">Ventanilla taller</a></li>
        <li id="otras_salidas"><a href="<?php echo base_url('refacciones/salidas/otrasSalidas'); ?>">Otras Salidas Serv. Exce.</a></li>
        <li id="dev_a_prov"><a href="<?php echo base_url('refacciones/salidas/devolucion'); ?>">Dev. A Prov.</a></li>
        <li id="poliza_ventas"><a href="<?php echo base_url('refacciones/polizas?tipo_poliza=1'); ?>">Poliza Ventas</a></li>
        <li id="poliza_dev_prov"><a href="<?php echo base_url('refacciones/polizas'); ?>">Poliza Dev.Prov</a></li>
        <li id="poliza_otras_salidas"><a href="<?php echo base_url('refacciones/polizas'); ?>">Poliza Otras Salidas</a></li>

    </ul>
</li>
<li id="refacciones_entradas">
    <a href="#tables" data-toggle="collapse" aria-expanded="false">
        Entradas<span class="sub-nav-icon"> <i class="stroke-arrow"></i> </span>
    </a>
    <ul id="tables" class="nav nav-second collapse nav-cat-3">
        <li id="menu_entradas_compras"><a href="<?php echo base_url('refacciones/entradas/ordencompra'); ?>">Compras</a></li>
        <li id="menu_entradas_dev_ventas"><a href="<?php echo base_url('refacciones/entradas/listadoVentas'); ?>">Dev. Ventas</a></li>
        <li id="menu_entradas_listado_compras"><a href="<?php echo base_url('refacciones/entradas/listadoEntradas'); ?>">Listado de compras</a></li>
        <li id="menu_entradas_poliza_compras"><a href="<?php echo base_url('refacciones/polizas?tipo_poliza=4'); ?>">Poliza de Compras</a></li>
        <li id="menu_entradas_poliza_mostrador"><a href="<?php echo base_url('refacciones/polizas?tipo_poliza=5'); ?>">Poliza Dev. Mostrador</a></li>
        <li id="menu_entradas_poliza_traspasos"><a href="<?php echo base_url('refacciones/polizas?tipo_poliza=6'); ?>">Poliza Traspasos</a></li>
    </ul>
</li>
<li id="refacciones_alternos">
    <a href="#forms" data-toggle="collapse" aria-expanded="false">
        Alternos <span class="sub-nav-icon"> <i class="stroke-arrow"></i> </span>
    </a>
    <ul id="forms" class="nav nav-second collapse nav-cat-3">
        <li id="menu_alternos_poliza_reimpresion"><a href="{{ base_url('refacciones/alternos/') }}"> Reimpresión de Doctos</a></li>
        <li id="menu_alternos_poliza_actualizacion"><a href="{{ base_url('catalogos/preciosController')}}"> Actualización de Precios</a></li>
    </ul>
</li>
<li class="nav-category">
    Facturas
</li>
<li class="nav-cat-3">
    <li id="facturas_listado"><a  href="<?php echo base_url('refacciones/factura/listado'); ?>">Listado</a></li>
</li>
<li class="nav-category">
    Consultas
</li>
<li id="req_mostrador">
    <a href="#extras" data-toggle="collapse" aria-expanded="false">
        Req. Mostrador <span class="sub-nav-icon"> <i class="stroke-arrow"></i> </span>
    </a>
    <ul id="extras" class="nav nav-second collapse nav-cat-3">
        <li id="mostrador_req_mostrador"><a href="{{ base_url('refacciones/consultas/inventario') }}">Req. Mostrador</a></li>
        <li id="mostrador_kardex"><a href="{{ base_url('refacciones/kardex')}}">Kardex</a></li>
    </ul>
</li>
<li id="req_taller">
    <a href="#common" data-toggle="collapse" aria-expanded="false">
        Req. Taller <span class="sub-nav-icon"> <i class="stroke-arrow"></i> </span>
    </a>
    <ul id="common" class="nav nav-second collapse nav-cat-3">
        <li><a href="login.html">Req. Taller</a></li>
    </ul>
</li>
<li id="req_compras">
    <a href="#compras" data-toggle="collapse" aria-expanded="false">
        Compras <span class="sub-nav-icon"> <i class="stroke-arrow"></i> </span>
    </a>
    <ul id="compras" class="nav nav-second collapse nav-cat-3">
        <li><a href="login.html">Req. Taller</a></li>
    </ul>
</li>
<li class="nav-category">
    Reportes
</li>
<li id="ventas">
    <a href="#refa_reportes" data-toggle="collapse" aria-expanded="false">
        Ventas <span class="sub-nav-icon"> <i class="stroke-arrow"></i> </span>
    </a>
    <ul id="refa_reportes" class="nav nav-second collapse nav-cat-3">
        <li><a href="#">Vendedores</a></li>
        <li><a href="#">Fac.Diarias</a></li>
        <li><a href="#">Ana. Vtas vs Costos</a></li>
        <li><a href="#">Dev. Ventas</a></li>
        <li><a href="#">Otras Salidas</a></li>
    </ul>
</li>
<li id="compras">
    <a href="#refa_com" data-toggle="collapse" aria-expanded="false">
        Compras <span class="sub-nav-icon"> <i class="stroke-arrow"></i> </span>
    </a>
    <ul id="refa_com" class="nav nav-second collapse nav-cat-3">
        <li><a href="#">Compras Diarias</a></li>
        <li><a href="#">Sugerido Compras</a></li>
        <li><a href="#">Dev.Prov.</a></li>
    </ul>
</li>
<li id="inventario">
    <a href="#refa_inven" data-toggle="collapse" aria-expanded="false">
        Inventario <span class="sub-nav-icon"> <i class="stroke-arrow"></i> </span>
    </a>
    <ul id="refa_inven" class="nav nav-second collapse nav-cat-3">
        <li><a href="#">Control de Inv.</a></li>
        <li><a href="#">Mejores productos</a></li>
    </ul>
</li>
<li class="nav-category">
    Utileria
</li>
<li id="cierre_mes">
    <a href="#refa_utile" data-toggle="collapse" aria-expanded="false">
        Cierre de mes <span class="sub-nav-icon"> <i class="stroke-arrow"></i> </span>
    </a>
    <ul id="refa_utile" class="nav nav-second collapse nav-cat-3">
        <li><a href="#">Reporte Ciclico</a></li>
        <li><a href="#">Mejores productos</a></li>
        <li><a href="#">Inv.Fisico</a></li>
    </ul>
</li>
<li id="comisiones">
    <a href="#refa_comisiones" data-toggle="collapse" aria-expanded="false">
        Comisiones <span class="sub-nav-icon"> <i class="stroke-arrow"></i> </span>
    </a>
    <ul id="refa_comisiones" class="nav nav-second collapse nav-cat-3">
        <li><a href="#">Comisiones</a></li>
    </ul>
</li>