<?php


defined('BASEPATH') or exit('No direct script access allowed');

class MConsultas extends CI_Model
{

    public function __construct()
    {
        $this->load->database();
    }
    public function getDataByIdCita($id = '')
    {
        if ($id == '') {
            return [];
        }


        $query = $this->db->select('*')->from('prepiking_extra_cita')->where('id_cita', $id)->get();
        if (isset($query)) {
            return $query->result_array();
        }

        return [];
        // $query = $this->db->get();
    }

    public function getCita_info($id = '')
    {
        if ($id == '') {
            return [];
        }

        $this->db->select('id_cita,vehiculo_placas, vehiculo_numero_serie,asesor,datos_nombres,datos_apellido_paterno,datos_apellido_materno')
            ->from('citas')
            ->where('id_cita', $id);

        $query = $this->db->get();
        return $query->result_array();
    }

    public function getCita_info_completa($id_cita = '')
    {
        if ($id_cita == '') {
            return [];
        }

        $this->db->where("c.id_cita", $id_cita)
            ->join('ordenservicio AS o', 'o.id_cita = c.id_cita')
            ->join('tecnicos AS t', 'c.id_tecnico = t.id', 'left')
            ->join('cat_tipo_orden AS cto', 'cto.id = o.id_tipo_orden', 'left')
            ->join('categorias AS cat', 'cat.id = o.idcategoria', 'left')
            ->join('subcategorias AS subcat', 'subcat.id = o.idsubcategoria', 'left')
            ->select('o.id_cita ,t.nombre AS tecnico, cat.categoria,subcat.subcategoria, o.vehiculo_kilometraje AS km, o.consecutivo_dms,c.asesor,c.datos_nombres,c.datos_apellido_paterno,c.datos_apellido_materno,c.vehiculo_modelo, o.numero_cliente, o.nombre_compania, o.rfc,c.vehiculo_placas, c.vehiculo_numero_serie, o.vehiculo_identificacion');

        $query = $this->db->get('citas AS c');
        return $query->result_array();
    }

    public function listaPresupuestoCampo($campo = '')
    {

        $sql = "SELECT t.nombre AS tecnico,c.vehiculo_modelo,c.asesor,c.vehiculo_placas,prer.* " .
            "FROM presupuesto_registro AS prer " .
            "LEFT JOIN ordenservicio AS o ON o.id_cita = prer.id_cita " .
            "LEFT JOIN citas AS c ON c.id_cita = o.id_cita " .
            "LEFT JOIN tecnicos AS t ON c.id_tecnico = t.id " .
            "WHERE t.nombre LIKE ? OR c.vehiculo_modelo LIKE ? OR c.asesor LIKE ? OR c.vehiculo_placas LIKE ? OR prer.id_cita LIKE ? " .
            "OR c.vehiculo_numero_serie  LIKE ?  OR o.vehiculo_identificacion LIKE ? AND prer.identificador = ? " .
            "ORDER BY prer.id DESC LIMIT 160";

        $query = $this->db->query($sql, array('%' . $campo . '%', '%' . $campo . '%', '%' . $campo . '%', '%' . $campo . '%', '%' . $campo . '%', '%' . $campo . '%', '%' . $campo . '%', 'M'));
        $data = $query->result();
        // lquery();
        // die();
        return $data;
    }

    public function listaCotizacionEstatus($status = '')
    {

        $sql = "SELECT t.nombre AS tecnico,c.vehiculo_modelo,c.asesor,c.vehiculo_placas,prer.* " .
            "FROM presupuesto_registro AS prer " .
            "LEFT JOIN ordenservicio AS o ON o.id_cita = prer.id_cita " .
            "LEFT JOIN citas AS c ON c.id_cita = o.id_cita " .
            "LEFT JOIN tecnicos AS t ON c.id_tecnico = t.id " .
            "WHERE prer.estado_refacciones = ? OR prer.estado_refacciones_garantias = ? AND prer.identificador = ? " .
            "ORDER BY prer.id DESC LIMIT 160";

        $query = $this->db->query($sql, array($status, $status, 'M'));
        $data = $query->result();
        return $data;
    }

    public function listaPresupuestoEstatusCampo($campo = '', $status = '')
    {

        $sql = "SELECT t.nombre AS tecnico,c.vehiculo_modelo,c.asesor,c.vehiculo_placas,prer.* " .
            "FROM presupuesto_registro AS prer " .
            "LEFT JOIN ordenservicio AS o ON o.id_cita = prer.id_cita " .
            "LEFT JOIN citas AS c ON c.id_cita = o.id_cita " .
            "LEFT JOIN tecnicos AS t ON c.id_tecnico = t.id " .
            "WHERE t.nombre LIKE ? OR c.vehiculo_modelo LIKE ? OR c.asesor LIKE ? OR c.vehiculo_placas LIKE ? OR prer.id_cita LIKE ? " .
            "OR c.vehiculo_numero_serie LIKE ? OR o.vehiculo_identificacion LIKE ? AND prer.estado_refacciones = ? OR prer.estado_refacciones_garantias = ? AND prer.identificador = ? " .
            "ORDER BY prer.id DESC LIMIT 160";

        $query = $this->db->query($sql, array('%' . $campo . '%', '%' . $campo . '%', '%' . $campo . '%', '%' . $campo . '%', '%' . $campo . '%', '%' . $campo . '%', '%' . $campo . '%', $status, $status, 'M'));
        $data = $query->result();
        return $data;
    }

    public function precargaConsulta($idOrden = 0)
    {
        $sql = "SELECT  o.*,c.*,t.nombre AS tecnico, cat.categoria, subcat.subcategoria " .
            "FROM citas AS c " .
            "LEFT JOIN tecnicos AS t ON c.id_tecnico = t.id " .
            "LEFT JOIN ordenservicio AS o ON o.id_cita = c.id_cita " .
            "LEFT JOIN categorias AS cat ON cat.id = o.idcategoria " .
            "LEFT JOIN subcategorias AS subcat ON subcat.id = o.idsubcategoria " .
            "WHERE c.id_cita = ?";
        $query = $this->db->query($sql, array($idOrden));
        $data = $query->result();
        return $data;
    }

    public function get_result_field($campo_1 = "", $value_1 = "", $campo_2 = "", $value_2 = "", $tabla = "")
    {
        $result = $this->db->where($campo_1, $value_1)
            ->where($campo_2, $value_2)
            ->get($tabla)
            ->result();
        return $result;
    }
}

/* End of file MConsultas.php */
