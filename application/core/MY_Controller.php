<?php

class MY_Controller extends CI_Controller {

    public function __construct()
    {
        parent::__construct();
    }

    public function output($html){
		$this->blade->render('build', array(
            'scripts' => $this->scripts,
            'titulo_modulo' => $this->title,
            'modulo' => 'Nómina',
            'breadcrumb' => $this->breadcrumb,
            'content'=>$html
        ));
    }

    public function response($data = array(),$status = 200){
        $this->output
            ->set_status_header($status)
            ->set_content_type('application/json', 'utf-8')
            ->set_output(json_encode($data))
            ->_display();
        exit;
    }
}